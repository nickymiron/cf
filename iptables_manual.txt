Forwarding from port 80 to 8080 is setup from iptables with this command:

iptables -t nat -A PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080

To delete it, replace the -A with -D in the command:

iptables -t nat -D PREROUTING -p tcp --dport 80 -j REDIRECT --to-port 8080
