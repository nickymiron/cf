package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.dao.*;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.model.transaction.Transaction;
import com.google.common.collect.Iterables;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDateTime;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.*;

/**
 * Created by indrek.ruubel on 29/01/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@Rollback
@ActiveProfiles("test")
public class LoanServiceSpec extends IntSpec {

    @Autowired
    InvestmentDao investmentDao;

    @Autowired
    BorrowerUserProfileDao borrowerUserProfileDao;

    @Autowired
    TransactionDao transactionDao;

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    InvestorUserProfileDao investorUserProfileDao;

    @Autowired
    InterestService interestService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    LoanDao loanDao;

    @Autowired
    BalanceDao balanceDao;

    @Autowired
    BalanceService balanceService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    MathContext mathContext;

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    InvestmentService investmentService;

//    @Before
//    public void setUp() throws SQLException {
//        DbTestUtil.resetAutoIncrementColumns(applicationContext, "loan", "investment",
//                "balance", "transaction", "investor_user_profile",
//                "borrower_user_profile");
//    }

    @Test
    public void shouldSaveLoanRequest() throws Exception {
        User user = createUser();
        createCreditApplication(user, CreditGradeConstructor.A);

        Loan loanRequest = loanService.createLoanRequest(new BigDecimal(1000), user);
        assertNotNull(loanRequest.getId());
    }

    @Test
    public void shouldSaveInvestAndSubtractFromBalance() throws Exception {
        User user = createUser();
        InvestorUserProfile investor = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;
        createCreditApplication(user, CreditGradeConstructor.TEST0);
        Loan loanRequest = loanService.createLoanRequest(new BigDecimal(300), user);

        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(200), currency, investor2.getUser().getUserProfile());

        Balance latestBalanceBeforeInvestingOnInvestor = balanceService.getLatestBalance(investor.getUser().getUserProfile());
        Balance latestBalanceBeforeInvestingOnInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(100), loanRequest, investor);
        investmentService.invest(new BigDecimal(200), loanRequest, investor2);

        Balance latestBalanceOnInvestor = balanceService.getLatestBalance(investor.getUser().getUserProfile());
        Balance latestBalanceOnInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        assertTrue(latestBalanceBeforeInvestingOnInvestor.getClosingBalance().compareTo(new BigDecimal(100)) == 0);
        assertTrue(latestBalanceBeforeInvestingOnInvestor2.getClosingBalance().compareTo(new BigDecimal(200)) == 0);

        assertTrue(latestBalanceOnInvestor.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);
        assertTrue(latestBalanceOnInvestor2.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);
    }

    @Test
    public void shouldStartPeriodForLoan() throws Exception {
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower.getUser());

        InvestorUserProfile investor = createInvestor();
        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(100), loan, investor);

        List<Investment> investments = investmentDao.findAllByLoan(loan);
        Balance balance = balanceService.getLatestBalance(investor.getUser().getUserProfile());
        List<Transaction> transactions = transactionService.findAllByUserOrderByCreatedDesc(investor.getUser());

        loan = loanService.findById(loan.getId());

        assertFalse(transactions.isEmpty());
        assertFalse(balance == null);
        assertEquals(investments.size(), 1);
        assertNotNull(loan.getFundedDateTime());

        List<Investment> allByLoan = investmentDao.findAllByLoan(loan);
        assertEquals(allByLoan.size(), 1);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionIfTooMuchInvestments() throws Exception {
        Currency currency = Currency.EUR;
        User user = createUser();
        InvestorUserProfile investor = createInvestor();

        createCreditApplication(user, CreditGradeConstructor.TEST0);

        Loan loanRequest = loanService.createLoanRequest(new BigDecimal(100), user);

        investmentService.invest(new BigDecimal(200), loanRequest, investor);
    }

    @Test
    public void shouldStartLoanPeriodWith2Investors() throws Exception {
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        Loan loan = loanService.createLoanRequest(new BigDecimal(200, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(100), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(100), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(100), loan, investor1);
        investmentService.invest(new BigDecimal(100), loan, investor2);

        List<Investment> investments = investmentDao.findAllByLoan(loan);
        List<Balance> balances1 = balanceService.findAllByUserProfile(investor1.getUser().getUserProfile());
        List<Balance> balances2 = balanceService.findAllByUserProfile(investor2.getUser().getUserProfile());
        List<Transaction> transactions1 = transactionService.findAllByUserOrderByCreatedDesc(investor1.getUser());
        List<Transaction> transactions2 = transactionService.findAllByUserOrderByCreatedDesc(investor2.getUser());

        assertNotNull(loan.getFundedDateTime());
        assertEquals(investments.size(), 2);
        assertFalse(balances1.isEmpty());
        assertFalse(balances2.isEmpty());
        assertFalse(transactions1.isEmpty());
        assertFalse(transactions2.isEmpty());
    }

    @Test
    public void shouldNotStartLoanPeriodWith2Investors() throws Exception {
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        Loan loan = loanService.createLoanRequest(new BigDecimal(200, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(100), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(100), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(99), loan, investor1);
        investmentService.invest(new BigDecimal(100), loan, investor2);

        List<Investment> investments = investmentDao.findAllByLoan(loan);
        List<Balance> balances1 = balanceService.findAllByUserProfile(investor1.getUser().getUserProfile());
        List<Balance> balances2 = balanceService.findAllByUserProfile(investor2.getUser().getUserProfile());
        List<Transaction> transactions1 = transactionService.findAllByUserOrderByCreatedDesc(investor1.getUser());
        List<Transaction> transactions2 = transactionService.findAllByUserOrderByCreatedDesc(investor2.getUser());

        assertNull(loan.getFundedDateTime());
        assertEquals(investments.size(), 2);
        assertFalse(balances1.isEmpty());
        assertFalse(balances2.isEmpty());
        assertFalse(transactions1.isEmpty());
        assertFalse(transactions2.isEmpty());
    }

    @Test
    public void shouldPayoffLoanClear() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(50), currency,investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(50), currency,investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(50), loan, investor1);
        investmentService.invest(new BigDecimal(50), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1).minusSeconds(1));

        interestService.calculateAllInterests();

        BigDecimal totalCompoundedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(loan.getApr()), new BigDecimal(100), 2);


        BigDecimal investorsInterestWithFee = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(loan.getApr()), new BigDecimal(50), 2);

        balanceService.addDeposit(new BigDecimal(102), currency, borrower.getUser().getUserProfile());
        loanBalanceService.payoff(new BigDecimal(100).add(totalCompoundedInterest), borrower);

        LoanBalance loanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);

        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        // Fee calculation
        BigDecimal investorsReturnWithoutFee = calculateAmountWithoutFee(loan.getApr(), investorsInterestWithFee);

        assertTrue(BigDecimal.ZERO.compareTo(loanBalance.getEndBalance()) == 0);
        assertTrue(new BigDecimal(50).add(investorsReturnWithoutFee).setScale(8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor1.getClosingBalance().setScale(8, RoundingMode.HALF_UP)) == 0);
        assertTrue(new BigDecimal(50).add(investorsReturnWithoutFee).setScale(8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor2.getClosingBalance().setScale(8, RoundingMode.HALF_UP))== 0);
    }

    @Test
    public void shouldPayOffInterestProportionally() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(50), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(50), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(50), loan, investor1);
        investmentService.invest(new BigDecimal(50), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1).minusSeconds(1));

        interestService.calculateAllInterests();

        BigDecimal totalInterestWithFee = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(creditGrade.getApr()), new BigDecimal(100), 2);

        BigDecimal totalInterestWithoutFee = calculateAmountWithoutFee(loan.getApr(), totalInterestWithFee);
        BigDecimal totalFee = totalInterestWithFee.subtract(totalInterestWithoutFee);
        BigDecimal feePerInvestor = totalFee.multiply(new BigDecimal(0.5));

        BigDecimal payoffAmount = new BigDecimal(0.03);
        BigDecimal payoffPerInvestor = payoffAmount.multiply(new BigDecimal(0.5));

        balanceService.addDeposit(payoffAmount, currency, borrower.getUser().getUserProfile());
        loanBalanceService.payoff(payoffAmount, borrower);

        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        // This is payed to investor balance
        BigDecimal paymentPerInvestor = payoffPerInvestor.subtract(feePerInvestor);

        Iterable<Investment> activeInvestments = investmentService.findAllActiveInvestmentsOnBorrowerOrderByAscending(borrower);

        // Principal shouldn't have been touched
        assertTrue(activeInvestments.iterator().next().getLatestSnapshop().getClosingPrincipal().compareTo(new BigDecimal(50)) == 0);

        assertTrue(paymentPerInvestor.setScale(8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor1.getClosingBalance().setScale(8, RoundingMode.HALF_UP)) == 0);
        assertTrue(paymentPerInvestor.setScale(8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor2.getClosingBalance().setScale(8, RoundingMode.HALF_UP)) == 0);
    }

    @Test
    public void shouldPayOffInterestAndSomePrincipalProportionally() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(10000, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(5000), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(5000), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(5000), loan, investor1);
        investmentService.invest(new BigDecimal(5000), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1).minusSeconds(1));

        interestService.calculateAllInterests();

        BigDecimal totalInterestWithFee = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(creditGrade.getApr()), new BigDecimal(10000), 2);
        BigDecimal totalInterestWithoutFee = calculateAmountWithoutFee(loan.getApr(), totalInterestWithFee);
        BigDecimal totalFee = totalInterestWithFee.subtract(totalInterestWithoutFee);
        BigDecimal feePerInvestor = totalFee.multiply(new BigDecimal(0.5));

        // Borrower loanBalance is 10006.576423343973550669919309440000
        LoanBalance loanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);

        assertTrue(new BigDecimal(10000).add(totalInterestWithFee).compareTo(loanBalance.getClosingPrincipal().add(loanBalance.getClosingInterest())) == 0);

        balanceService.addDeposit(new BigDecimal(3000), currency, borrower.getUser().getUserProfile());
        loanBalanceService.payoff(new BigDecimal(3000), borrower);

        // This gets payed per investor
        BigDecimal payedToInvestor = new BigDecimal(1500).subtract(feePerInvestor);

        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        assertTrue(payedToInvestor.setScale(8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor1.getClosingBalance().setScale(8, RoundingMode.HALF_UP)) == 0);
        assertTrue(payedToInvestor.setScale(8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor2.getClosingBalance().setScale(8, RoundingMode.HALF_UP)) == 0);
    }


    @Test
    public void shouldPayOffInterestAndSomePrincipalClearAndHaveLeftOver() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        User user = borrower.getUser();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.TEST0);
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(10000, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(5000), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(5000), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(5000), loan, investor1);
        investmentService.invest(new BigDecimal(5000), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1).minusSeconds(1));

        interestService.calculateAllInterests();

        BigDecimal totalCompoundedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(creditGrade.getApr()), new BigDecimal(10000), 2);

        BigDecimal interestPerInvestorWithFee = totalCompoundedInterest.multiply(new BigDecimal(0.5));
        BigDecimal interestPerInvestorWithoutFee = calculateAmountWithoutFee(loan.getApr(), interestPerInvestorWithFee);

        BigDecimal payOffAmount = new BigDecimal(11000, mathContext);

        balanceService.addDeposit(payOffAmount, currency, borrower.getUser().getUserProfile());
        loanBalanceService.payoff(payOffAmount, borrower);

        LoanBalance loanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);

        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        // Borrower cleared his balance, it's zero
        assertTrue(BigDecimal.ZERO.compareTo(loanBalance.getEndBalance()) == 0);

        // Investors got money back + interest
        assertTrue(latestBalanceInvestor1.getClosingBalance().setScale(8, RoundingMode.HALF_UP)
                .compareTo((new BigDecimal(5000).add(interestPerInvestorWithoutFee)).setScale(8, RoundingMode.HALF_UP)) == 0);
        assertTrue(latestBalanceInvestor2.getClosingBalance().setScale(8, RoundingMode.HALF_UP)
                .compareTo((new BigDecimal(5000).add(interestPerInvestorWithoutFee)).setScale(8, RoundingMode.HALF_UP)) == 0);
    }


    @Test
    public void simulateTheProcessStepByStepShouldBeAsExpected() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        InvestorUserProfile investor3 = createInvestor();
        InvestorUserProfile investor4 = createInvestor();

        balanceService.addDeposit(new BigDecimal(50), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(50), currency, investor2.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(23), currency, investor3.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(10), currency, investor4.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(50), loan, investor1);
        investmentService.invest(new BigDecimal(50), loan, investor2);

        // Calculate for one day, becomes 100.0328767
        interestService.calculateAllInterests(); // 0

        LoanBalance balance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        System.out.println("becomes 100.0328767: " + balance.getEndBalance());

        setLastInterestCalculationToLoanAndInvestments(loan, LocalDateTime.now().minusDays(1));

        // Calculate another day, becomes 100.0657642
        interestService.calculateAllInterests(); // 0

        balance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        System.out.println("becomes 100.0657642: " + balance.getEndBalance());

        // Take another loan for 33
        Loan loan2 = loanService.createLoanRequest(new BigDecimal(33, mathContext), borrower.getUser());
        investmentService.invest(new BigDecimal(23), loan2, investor3);
        investmentService.invest(new BigDecimal(10), loan2, investor4);

        // Calculate interest 1 day, becomes 133.0766135
        interestService.calculateAllInterests(); // 0

        balance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        System.out.println("becomes 133.0766135: " + balance.getEndBalance());

        setLastInterestCalculationToLoanAndInvestments(loan, LocalDateTime.now().minusDays(1));
        setLastInterestCalculationToLoanAndInvestments(loan2, LocalDateTime.now().minusDays(1));

        // Should become 133.1203648
        interestService.calculateAllInterests(); // 0

        balance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        System.out.println("becomes 133.1203648: " + balance.getEndBalance());

        List<AbstractMap.SimpleEntry> loans = new ArrayList<>();
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(100), 1));
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(33), 2));

        BigDecimal expected = interestService.compoundOnMultipleAmounts(loans,
                interestService.calculateDailyInterest(creditGrade.getApr()));

        assertTrue(expected.setScale(8, RoundingMode.HALF_UP)
                .compareTo(balance.getEndBalance().setScale(8, RoundingMode.HALF_UP)) == 0);
    }

    @Test(expected = Exception.class)
    public void simulateProcessAndmakeSurePayedOffInvestmentsDontGetPayed() throws Exception {
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);
        CreditGrade creditGrade = creditApplication.getCreditGrade();

        Loan loan = loanService.createLoanRequest(new BigDecimal(10000, mathContext), borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(5000), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(5000), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(5000), loan, investor1);
        investmentService.invest(new BigDecimal(5000), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1).minusSeconds(1));

        interestService.calculateAllInterests();

        BigDecimal totalCompoundedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(creditGrade.getApr()), new BigDecimal(10000), 2);

        BigDecimal investorInterest = totalCompoundedInterest.multiply(new BigDecimal(0.5));

        BigDecimal payOffAmount = new BigDecimal(11000, mathContext);
        LoanBalance loanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);

        balanceService.addDeposit(payOffAmount, currency, borrower.getUser().getUserProfile());

        loanBalanceService.payoff(payOffAmount, borrower);

        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        Balance userBalance = balanceService.getLatestBalance(borrower.getUser().getUserProfile());

        // This is one payoff, which clears it, there shouldn't be anything added after this
        List<Transaction> investor1Payments = transactionService.getAllUserTransactionsByType(investor1.getUser(), "INVESTOR_INTEREST_PAYBACK");

        // Let's payoff again, should throw exception
        balanceService.addDeposit(payOffAmount, currency, borrower.getUser().getUserProfile());
        loanBalanceService.payoff(payOffAmount, borrower);
    }

    @Test
    public void payOffAndLoansHaveNoInterestClearStartegy() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        BigDecimal loanAmount = new BigDecimal(10000, mathContext);
        BigDecimal fundAmount = new BigDecimal(5000, mathContext);

        Loan loan = loanService.createLoanRequest(loanAmount, borrower.getUser());

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(10000), currency, borrower.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(5000), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(5000), currency, investor2.getUser().getUserProfile());

        investmentService.invest(fundAmount, loan, investor1);
        investmentService.invest(fundAmount, loan, investor2);

        loanBalanceService.payoff(loanAmount, borrower);

        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor2.getUser().getUserProfile());
        LoanBalance loanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);

        List<Transaction> investor1InterestPayments = transactionService.getAllUserTransactionsByType(investor1.getUser(), "INVESTOR_INTEREST_PAYBACK");
        List<Transaction> investor2InterestPayments = transactionService.getAllUserTransactionsByType(investor2.getUser(), "INVESTOR_INTEREST_PAYBACK");

        assertTrue(latestBalanceInvestor1.getClosingBalance().compareTo(fundAmount) == 0);
        assertTrue(latestBalanceInvestor2.getClosingBalance().compareTo(fundAmount) == 0);

        // No interest charged
        assertTrue(loanBalance.getEndBalance().compareTo(BigDecimal.ZERO) == 0);

        // Therefore no interest transactions either
        assertEquals(investor1InterestPayments.size(), 0);
        assertEquals(investor2InterestPayments.size(), 0);
    }

    @Test
    public void shouldFindLoanInvestmentsWithPrincipal() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.A);

        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(100), borrower);
        investmentService.invest(new BigDecimal(100), loan, investor);

        Iterable<Investment> investments = loanService.findActiveInvestmentsWithPrincipal(investor, borrower);

        assertTrue(Iterables.size(investments) == 1);
        assertTrue(investments.iterator().next().getLatestSnapshop().getClosingPrincipal().compareTo(new BigDecimal(100)) == 0);
    }

    @Test
    public void shouldNotFindClosedLoanInvestmentsWithPrincipal() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.A);

        balanceService.addDeposit(new BigDecimal(100), currency, borrower.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(100), borrower);
        investmentService.invest(new BigDecimal(100), loan, investor);

        loanBalanceService.payoff(new BigDecimal(100), borrower);

        Iterable<Investment> investments = investmentService.findAllActiveInvestmentsOnBorrowerOrderByAscending(borrower);

        assertTrue(Iterables.size(investments) == 0);
    }

    @Test
    public void shouldSplitInvestments() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;

        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(45), currency, investor.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(70), currency, investor2.getUser().getUserProfile());

        investmentService.reserv(creditApplication, new BigDecimal(45), investor.getUser());
        investmentService.reserv(creditApplication, new BigDecimal(70), investor2.getUser());

        Loan loan = loanService.requestAndFundLoan(new BigDecimal(105), borrower);

        Balance investor1Balance = balanceService.getLatestBalance(investor.getUser().getUserProfile());
        Balance investor2Balance = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        assertTrue(investor1Balance.getClosingBalance().compareTo(new BigDecimal(5)) == 0);
        assertTrue(investor2Balance.getClosingBalance().compareTo(new BigDecimal(5)) == 0);
        assertTrue(loan.getFundedDateTime() != null);
    }

    @Test
    public void shouldSplitInvestments2() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;

        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(35), currency, investor.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(70), currency, investor2.getUser().getUserProfile());

        investmentService.reserv(creditApplication, new BigDecimal(35), investor.getUser());
        investmentService.reserv(creditApplication, new BigDecimal(70), investor2.getUser());

        Loan loan = loanService.requestAndFundLoan(new BigDecimal(100), borrower);

        Balance investor1Balance = balanceService.getLatestBalance(investor.getUser().getUserProfile());
        Balance investor2Balance = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        assertTrue(investor1Balance.getClosingBalance().compareTo(new BigDecimal(5)) == 0);
        assertTrue(investor2Balance.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);
        assertTrue(loan.getFundedDateTime() != null);
    }

    @Test
    public void shouldSplitInvestments3() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;

        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(500), currency, investor2.getUser().getUserProfile());

        investmentService.reserv(creditApplication, new BigDecimal(33), investor.getUser());
        investmentService.reserv(creditApplication, new BigDecimal(70), investor2.getUser());

        Loan loan = loanService.requestAndFundLoan(new BigDecimal(50), borrower);

        Balance investor1Balance = balanceService.getLatestBalance(investor.getUser().getUserProfile());
        Balance investor2Balance = balanceService.getLatestBalance(investor2.getUser().getUserProfile());

        assertTrue(investor1Balance.getClosingBalance().compareTo(new BigDecimal(70)) == 0);
        assertTrue(investor2Balance.getClosingBalance().compareTo(new BigDecimal(480)) == 0);
        assertTrue(loan.getFundedDateTime() != null);
    }

}
