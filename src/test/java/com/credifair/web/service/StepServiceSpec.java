package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.model.BorrowerFlowStep;
import com.credifair.web.model.BorrowerUserProfile;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static junit.framework.Assert.assertEquals;

/**
 * Created by indrek.ruubel on 24/03/2016.
 */
@Ignore
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
public class StepServiceSpec extends IntSpec {

	@Autowired
	BorrowerStepService borrowerStepService;

	@Autowired
	VerificationService verificationService;

	@Autowired
	BorrowerUserProfileService borrowerUserProfileService;

	@Autowired
	CreditApplicationService creditApplicationService;

	@Autowired
	RecipientService recipientService;

	@Autowired
	UserService userService;

	@Test
	public void shouldReturnDashboard() {
		BorrowerUserProfile borrower = createBorrower();

		BorrowerFlowStep step = borrowerStepService.getStep(borrower.getUser());

		assertEquals(BorrowerFlowStep.GET_STARTED, step);

	}

//	@Test
//	public void shouldReturnApplicationStep1() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		CreditApplication creditApplication = stepService.startApplicationProcess(borrower.getUser());
//		user.setCreditApplication(creditApplication);
//		BorrowerFlowStep step = stepService.getStep(user);
//
//		assertEquals(BorrowerFlowStep.APPLICATION_FORM, step);
//	}

//	@Test
//	public void shouldReturnApplicationStep2() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.APPLICATION_STEP2, step);
//	}
//
//	@Test
//	public void shouldReturnApplicationStep3() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.APPLICATION_STEP3, step);
//	}
//
//	@Test
//	public void shouldReturnLiabilitiesStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.LIABILITIES, step);
//	}
//
//	@Test
//	public void shouldReturnWaitingForInitialOffer() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.WAITING_FOR_OFFER, step);
//	}
//
//	@Test
//	public void shouldReturnInitialOfferStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.OFFER, step);
//	}
//
//
//	@Test
//	public void shouldReturnIncomeDocumentsUploadStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.INCOME_DOCUMENTS_UPLOAD, step);
//	}
//
//	@Test
//	public void shouldReturnIdDocumentsUploadStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.ID_DOCUMENTS_UPLOAD, step);
//	}
//
//	@Test
//	public void shouldReturnAddressDocumentsUploadStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.ADDRESS_PROOF_DOCUMENTS_UPLOAD, step);
//	}
//
//	@Test
//	public void shouldReturnWaitingForFinalOfferStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.WAITING_FINAL_OFFER, step);
//	}
//
//	@Test
//	public void shouldReturnWaitingFinalOfferAfterReviewingDocsStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.WAITING_FINAL_OFFER, step);
//	}
//
//	@Test
//	public void shouldReturnSignDocsStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.SIGN_DOCS, step);
//	}
//
//	@Test
//	public void shouldReturnBankDetailsStep() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.BANK_DETAILS, step);
//	}
//
//	@Test
//	public void shouldReturnDashboardAgain() {
//		BorrowerUserProfile borrower = createBorrower();
//		User user = borrower.getUser();
//
//		stepService.startApplicationProcess(user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP1, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP2, user);
//		fillStepFields(BorrowerFlowStep.APPLICATION_STEP3, user);
//		CreditApplication creditApplication = creditApplicationService.assignGradeToCAFromRequest(user, CreditGradeConstructor.AA);
//		creditApplicationService.acceptInitialOffer(creditApplication);
//		recipientService.saveIbanRecipient("EE3242342424", borrower);
//
//		BorrowerFlowStep step = stepService.getStep(borrower.getUser());
//
//		assertEquals(BorrowerFlowStep.GET_STARTED, step);
//		fail("TODO: Bank details have to be verified before letting to dashboard");
//	}

//	private void fillStepFields(BorrowerFlowStep step, User user) {
//		List<UserValueFieldType> values = verificationService.formStepParams.get(step);
//		for (UserValueFieldType value : values) {
//			userValueFieldService.saveUserValueField(value, "wat", user);
//		}
//	}

}
