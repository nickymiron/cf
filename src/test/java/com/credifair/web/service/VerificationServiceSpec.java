package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.model.User;
import com.credifair.web.model.verification.AddressVerification;
import com.credifair.web.model.verification.IdVerification;
import com.credifair.web.model.verification.IncomeVerification;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by indrek.ruubel on 14/03/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
public class VerificationServiceSpec extends IntSpec {

	@Autowired
	VerificationService verificationService;

	BorrowerStepService borrowerStepService;

	UserService userService;

	@Before
	public void setup() {
		User loggedInUser = createUser();
		borrowerStepService = Mockito.spy(BorrowerStepService.class);
		userService = Mockito.spy(UserService.class);
		Mockito.doReturn(loggedInUser).when(userService).getLoggedInUser();
		verificationService.borrowerStepService = borrowerStepService;
		verificationService.userService = userService;
	}

//	@Test
//	public void shouldNotPutApplicationOnMarket() {
//		User user = createUser();
//
//		verificationService.checkIfVerificationSuccessAndChangeState(user);
//
//		verify(borrowerStepService, times(0)).putCreditApplicationOnMarket(user);
//	}

//	@Test
//	public void shouldNotPutApplicationOnMarketWhenVerificationsPending() {
//		User user = createUser();
//		IdVerification idVerification = createIdVerification(user);
//		AddressVerification addressVerification = createAddressVerification(user);
//		IncomeVerification incomeVerification = createIncomeVerification(user);
//
//		verificationService.checkIfVerificationSuccessAndChangeState(user);
//
//		verify(borrowerStepService, times(0)).putCreditApplicationOnMarket(user);
//	}

//	@Test
//	public void shouldPutApplicationOnMarket() {
//		User user = createUser();
//		IdVerification idVerification = createIdVerification(user);
//		AddressVerification addressVerification = createAddressVerification(user);
//		IncomeVerification incomeVerification = createIncomeVerification(user);
//
//		Mockito.doNothing().when(borrowerStepService).putCreditApplicationOnMarket(user);
//
//		verificationService.verifyVerificationById(idVerification.getId());
//		verificationService.verifyVerificationById(addressVerification.getId());
//		verificationService.verifyVerificationById(incomeVerification.getId());
//
//		verify(borrowerStepService, times(1)).putCreditApplicationOnMarket(user);
//	}

	@Test
	public void shouldNotPutApplicationOnMarketIfOneRejection() {
		User user = createUser();
		IdVerification idVerification = createIdVerification(user);
		AddressVerification addressVerification = createAddressVerification(user);
		IncomeVerification incomeVerification = createIncomeVerification(user);

		Mockito.doNothing().when(borrowerStepService).putCreditApplicationOnMarket(user);

		verificationService.unverifyVerificationById(idVerification.getId());
		verificationService.verifyVerificationById(addressVerification.getId());
		verificationService.verifyVerificationById(incomeVerification.getId());

		verify(borrowerStepService, times(0)).putCreditApplicationOnMarket(user);
	}

}
