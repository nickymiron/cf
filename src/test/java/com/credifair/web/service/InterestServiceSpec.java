package com.credifair.web.service;

import com.credifair.web.model.Investment;
import com.credifair.web.model.InvestmentSnapshop;
import com.credifair.web.model.Loan;
import org.junit.Before;
import org.junit.Test;
import org.springframework.test.context.ActiveProfiles;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertTrue;
import static org.junit.Assert.assertFalse;

/**
 * Created by indrek.ruubel on 29/01/2016.
 */
@ActiveProfiles("test")
public class InterestServiceSpec {

    InterestService interestService;
    BigDecimal dailyInterest;

    @Before
    public void setup() {
        interestService = new InterestService();
        try {
            dailyInterest = interestService.calculateDailyInterest(new BigDecimal(0.12));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Test
    public void shouldCalculateDailyInterest() throws Exception {
        BigDecimal interest = interestService.calculateDailyInterest(new BigDecimal(0.12));
        BigDecimal expected = new BigDecimal("0.000328767123288");
        assertEquals(expected, interest);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionIfPercentLargerThanOne() throws Exception {
        BigDecimal interest = interestService.calculateDailyInterest(new BigDecimal(1.001));
    }

    @Test
    public void shouldCompoundInterestCorrectlyForOneDay() {
        BigDecimal newAmount = interestService.compoundInterestOnAmountForNDays(new BigDecimal(0.01), new BigDecimal(1000), 1);
        // 1% of 1000 is 10, the return value should be 10
        assertEquals(new BigDecimal(10.00).setScale(2), newAmount.setScale(2, RoundingMode.HALF_UP));
    }
    
    @Test
    public void shouldCompoundCorrectlyFor7Days() {
        BigDecimal newAmount = interestService.compoundInterestOnAmountForNDays(dailyInterest, new BigDecimal(1000), 7);
        newAmount = newAmount.round(new MathContext(10, RoundingMode.HALF_UP));
        //  interest should be 2.303640951
        BigDecimal expected = new BigDecimal(2.303640951).round(new MathContext(10, RoundingMode.HALF_UP));
        assertEquals(expected, newAmount);
    }

    @Test
    public void shouldCompoundCorrectlyFor27Days() {
        BigDecimal newAmount = interestService.compoundInterestOnAmountForNDays(dailyInterest, new BigDecimal(567), 27);
        newAmount = newAmount.round(new MathContext(8, RoundingMode.HALF_UP));
        // Interest should be 5.0546663
        BigDecimal expected = new BigDecimal(5.0546663).round(new MathContext(8, RoundingMode.HALF_UP));
        assertEquals(expected, newAmount);
    }

    @Test
    public void shouldDivideProportionally2Guys() {
        List<Investment> data = new ArrayList<>();
        data.add(createInvestmentWithLatestSnapshot(new BigDecimal(100)));
        data.add(createInvestmentWithLatestSnapshot(new BigDecimal(200)));
        data = interestService.calculateRatios(data);
        assertEquals(data.get(0).getRatio(), new BigDecimal("0.33"));
        assertEquals(data.get(1).getRatio(), new BigDecimal("0.67"));
    }

    @Test
    public void shouldDivideProportionally3Guys() {
        List<Investment> data = new ArrayList<>();
        data.add(createInvestmentWithLatestSnapshot(new BigDecimal(549)));
        data.add(createInvestmentWithLatestSnapshot(new BigDecimal(549)));
        data.add(createInvestmentWithLatestSnapshot(new BigDecimal(549)));
        data = interestService.calculateRatios(data);
        assertEquals(data.get(0).getRatio(), new BigDecimal("0.33"));
        assertEquals(data.get(1).getRatio(), new BigDecimal("0.33"));
        assertEquals(data.get(1).getRatio(), new BigDecimal("0.33"));
    }

    @Test
    public void shouldCalculateRightAmountOfDaysToCalculateInterestFrom() {
        Investment loan = createInvestmentWithLastCalculated(LocalDateTime.now().minusDays(15));
        int days = interestService.getDaysToCalculateInterestFor(loan);
        assertEquals(15, days);
    }

    @Test
    public void shouldAssertTrue() {
        Loan loan = new Loan();
        loan.setLastCalculatedInterest(LocalDateTime.now());
        boolean hasBeenCalculatedToday = interestService.hasBeenCalculatedToday(loan);
        assertTrue(hasBeenCalculatedToday);
    }

    @Test
    public void shouldAssertFalse() {
        Loan loan = new Loan();
        loan.setLastCalculatedInterest(LocalDate.now().atStartOfDay().minusMinutes(3));
        boolean hasBeenCalculatedToday = interestService.hasBeenCalculatedToday(loan);
        assertFalse(hasBeenCalculatedToday);
    }

    @Test
    public void shouldAssertTrueIfEqual() {
        Loan loan = new Loan();
        loan.setLastCalculatedInterest(LocalDate.now().atStartOfDay());
        boolean hasBeenCalculatedToday = interestService.hasBeenCalculatedToday(loan);
        assertTrue(hasBeenCalculatedToday);
    }

    @Test
    public void shouldNotBeOverGracePeriod() {
        Loan loan = new Loan();
        loan.setFundedDateTime(LocalDateTime.now().plusSeconds(1));
        boolean over = interestService.hasGoneOverGracePeriod(loan, 0);
        assertFalse(over);
    }

    @Test
    public void shouldBeOverGracePeriodWith0InterestDays() {
        Loan loan = new Loan();
        loan.setFundedDateTime(LocalDateTime.now().minusSeconds(1));
        boolean over = interestService.hasGoneOverGracePeriod(loan, 0);
        assertTrue(over);
    }

    @Test
    public void shouldBeOverGracePeriod3() {
        Loan loan = new Loan();
        loan.setFundedDateTime(LocalDateTime.now().minusDays(5).minusSeconds(1));
        boolean over = interestService.hasGoneOverGracePeriod(loan, 5);
        assertTrue(over);
    }

    @Test
    public void shouldBeOverGracePeriod2() {
        Loan loan = new Loan();
        loan.setFundedDateTime(LocalDateTime.now().minusDays(1).minusSeconds(1));
        boolean over = interestService.hasGoneOverGracePeriod(loan, 1);
        assertTrue(over);
    }

    @Test
    public void shouldBeOverGracePeriod() {
        Loan loan = new Loan();
        loan.setFundedDateTime(LocalDateTime.now().minusDays(6));
        boolean over = interestService.hasGoneOverGracePeriod(loan, 5);
        assertTrue(over);
    }

    @Test
    public void shouldCalculateRightAmountOfDaysToCalculateInterestFrom2() {
        Investment loan = createInvestmentWithInvested(LocalDateTime.now().minusDays(15));
        int days = interestService.getDaysToCalculateInterestFor(loan);
        assertEquals(16, days);
    }

    @Test
    public void shouldCalculateRightAmountOfDaysToCalculateInterestFrom3() {
        Investment loan = createInvestmentWithLastCalculated(LocalDateTime.now().minusDays(17));
        int days = interestService.getDaysToCalculateInterestFor(loan);
        assertEquals(17, days);
    }

    @Test
    public void shouldCaldulateRightAmountOfDaysToCalculateInterestFrom4() {
        Investment loan = createInvestmentWithLastCalculated(LocalDateTime.now());
        int days = interestService.getDaysToCalculateInterestFor(loan);
        assertEquals(0, days);
    }

    @Test
    public void shouldCalculateFromInvested() {
        Investment loan = createInvestmentWithInvested(LocalDateTime.now());
        int days = interestService.getDaysToCalculateInterestFor(loan);
        assertEquals(1, days);
    }

    @Test
    public void calculateInterestFor0Days() throws Exception {
        BigDecimal redsult = interestService.compoundInterestOnAmountForNDays(interestService.calculateDailyInterest(new BigDecimal(0.12)),
                new BigDecimal(1000), 0);
        assertEquals(BigDecimal.ZERO, redsult);
    }

    private Investment createInvestmentWithLastCalculated(LocalDateTime dateTime) {
        Investment loan = new Investment(null, null, null, null);
        InvestmentSnapshop snapshot = new InvestmentSnapshop();
        snapshot.setCreated(dateTime);
        snapshot.setType(InvestmentSnapshop.DifferenceType.INCREMENTED_INTEREST);
        loan.setLatestSnapshop(snapshot);
        return loan;
    }

    private Investment createInvestmentWithInvested(LocalDateTime dateTime) {
        Investment loan = new Investment(null, null, null, null);
        loan.setInvestedDateTime(dateTime);
        InvestmentSnapshop snapshot = new InvestmentSnapshop();
        snapshot.setCreated(dateTime);
        snapshot.setType(InvestmentSnapshop.DifferenceType.INCREMENTED_PRINCIPAL);
        loan.setLatestSnapshop(snapshot);
        return loan;
    }

    private Investment createInvestmentWithLatestSnapshot(BigDecimal amount) {
        Investment investment = new Investment();
        InvestmentSnapshop investmentSnapshop = new InvestmentSnapshop(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, amount, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, investment, null);
        investment.setLatestSnapshop(investmentSnapshop);
        return investment;
    }

}
