package com.credifair.web.service;

import com.credifair.web.dao.BorrowerUserProfileDao;
import com.credifair.web.dao.InvestorUserProfileDao;
import com.credifair.web.dao.UserDao;
import com.credifair.web.dao.UserProfileDao;
import com.credifair.web.model.Country;
import com.credifair.web.model.Role;
import com.credifair.web.model.User;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;

import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by indrek.ruubel on 19/01/2016.
 */
@ActiveProfiles("test")
public class UserServiceSpec {

    UserDao userDao;
    RoleService roleService;
    UserService userService;
    BorrowerUserProfileDao borrowerUserProfileDao;
    InvestorUserProfileDao investorUserProfileDao;
    UserProfileDao userProfileDao;
    CreditApplicationService creditApplicationService;

    private String ROLE_USER = "ROLE_USER";
    private String email = "test@gmail.com";
    private String password = "password";

    @Before
    public void setup() {
        userDao = Mockito.spy(UserDao.class);
        roleService = Mockito.spy(new RoleService());
        borrowerUserProfileDao = Mockito.mock(BorrowerUserProfileDao.class);
        investorUserProfileDao = Mockito.mock(InvestorUserProfileDao.class);
        userProfileDao = Mockito.mock(UserProfileDao.class);
        creditApplicationService = Mockito.mock(CreditApplicationService.class);
        userService = new UserService(userDao, roleService, borrowerUserProfileDao, investorUserProfileDao, userProfileDao);
        userService.creditApplicationService = creditApplicationService;
    }

    @Test
    public void shouldSaveUser() {
        Role roleBorrower = new Role(ROLE_USER);

        Mockito.doReturn(roleBorrower).when(roleService).findByRole(ROLE_USER);

        User user = userService.saveCustomer(email, password, Country.EE);

        verify(roleService, times(1)).findByRole(ROLE_USER);
        verify(roleService, times(0)).saveRole(ROLE_USER);
        verify(userDao, times(1)).save(user);
    }

    @Test
    public void shouldCreate3UserProfiles() {
        Role roleBorrower = new Role(ROLE_USER);

        Mockito.doReturn(roleBorrower).when(roleService).findByRole(ROLE_USER);

        User user = userService.saveCustomer(email, password, Country.EE);

        verify(investorUserProfileDao, times(1)).save(user.getInvestorUserProfile());
        verify(borrowerUserProfileDao, times(1)).save(user.getBorrowerUserProfile());
        verify(userProfileDao, times(1)).save(user.getUserProfile());
    }

    @Test
    public void userGetsAddedClientRole() {
        Role roleBorrower = new Role(ROLE_USER);

        Mockito.doReturn(roleBorrower).when(roleService).findByRole(ROLE_USER);

        User user = userService.saveCustomer(email, password, Country.EE);
        List<Role> roles = user.getRoles();

        assertEquals(roles.size(), 1);
        assertEquals(roles.get(0).getRole(), ROLE_USER);
    }

    @Test
    public void shouldNotSaveUserIfEmailExists() {
        Mockito.doReturn(new User()).when(userDao).findByEmail(email);

        User user = userService.saveCustomer(email, password);

        assertNull(user);
    }

}
