package com.credifair.web.service;

import com.credifair.web.dao.EmailSubscriberDao;
import com.credifair.web.model.EmailSubscriber;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;

import static junit.framework.Assert.assertNull;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by indrek.ruubel on 19/01/2016.
 */
@ActiveProfiles("test")
public class EmailSubscriberServiceSpec {

    EmailSubscriberService emailSubscriberService;

    EmailSubscriberDao emailSubscriberDao;

    private String email = "test@gmail.com";

    @Before
    public void setup() {
        emailSubscriberDao = Mockito.spy(EmailSubscriberDao.class);
        emailSubscriberService = new EmailSubscriberService(emailSubscriberDao);
    }

    @Test
    public void shouldSaveSubscriber() {
        EmailSubscriber emailSubscriber = emailSubscriberService.saveSubscriber(email, null);

        verify(emailSubscriberDao, times(1)).save(emailSubscriber);
    }

    @Test
    public void shouldNotSaveSubscriberIfEmailExists() {
        Mockito.doReturn(new EmailSubscriber()).when(emailSubscriberDao).findByEmail(email);

        EmailSubscriber emailSubscriber = emailSubscriberService.saveSubscriber(email, null);

        assertNull(emailSubscriber);
        verify(emailSubscriberDao, times(0)).save(emailSubscriber);
    }

}
