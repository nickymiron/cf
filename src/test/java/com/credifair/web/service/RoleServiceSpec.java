package com.credifair.web.service;

import com.credifair.web.dao.RoleDao;
import com.credifair.web.dao.UserDao;
import com.credifair.web.model.Role;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.springframework.test.context.ActiveProfiles;

import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

/**
 * Created by indrek.ruubel on 19/01/2016.
 */
@ActiveProfiles("test")
public class RoleServiceSpec {

    RoleService roleService;
    RoleDao roleDao;

    private String ROLE_CLIENT = "ROLE_CLIENT";

    @Before
    public void setup() {
        roleDao = Mockito.spy(RoleDao.class);
        roleService = new RoleService(roleDao);
    }

    @Test
    public void shouldSaveRole() {
        Role role = roleService.saveRole(ROLE_CLIENT);

        verify(roleDao, times(1)).save(role);
    }

    @Test
    public void shouldCallDao() {
        roleService.findByRole(ROLE_CLIENT);

        verify(roleDao, times(1)).findByRole(ROLE_CLIENT);
    }

}
