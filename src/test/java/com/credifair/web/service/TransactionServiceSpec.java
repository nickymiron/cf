package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.DbTestUtil;
import com.credifair.web.dao.BalanceDao;
import com.credifair.web.dao.InvestmentDao;
import com.credifair.web.dao.LoanDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditGradeConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.sql.SQLException;

import static junit.framework.Assert.assertEquals;

/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
public class TransactionServiceSpec extends IntSpec {

    @Autowired
    private ApplicationContext applicationContext;

    @Autowired
    TransactionService transactionService;

    @Autowired
    InvestmentDao investmentDao;

    @Autowired
    BalanceService balanceService;

    @Autowired
    BalanceDao balanceDao;

    @Autowired
    LoanService loanService;

    @Autowired
    InvestmentService investmentService;

    @Autowired
    LoanDao loanDao;

    @Before
    public void setUp() throws SQLException {
        DbTestUtil.resetAutoIncrementColumns(applicationContext, "loan", "investment",
                "transaction", "balance", "investor_user_profile",
                "borrower_user_profile");
    }

    @Test
    public void shouldCalculateCorrectBalance() throws Exception {

        InvestorUserProfile investor = createInvestor();
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(1000), borrower.getUser());
        investmentService.invest(new BigDecimal(50), loan, investor);

        Balance balance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(investor.getUser().getUserProfile());

        assertEquals(new BigDecimal(50), balance.getClosingBalance());
    }

    @Test
    public void shouldCalculateCorrectBalanceAfterRepeatedInvestmentTransactions() throws Exception {

        InvestorUserProfile investor = createInvestor();
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(600), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(1000), borrower.getUser());

        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);

        Balance balance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(investor.getUser().getUserProfile());

        assertEquals(new BigDecimal(400), balance.getClosingBalance());
    }

    @Test
    public void shouldCalculateCorrectBalanceAfterRepeatedInvestmentAndPayinTransactions() throws Exception {

        InvestorUserProfile investor = createInvestor();
        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(600), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(1000), borrower.getUser());
        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);

        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);

        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        Balance balance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(investor.getUser().getUserProfile());

        assertEquals(new BigDecimal(600), balance.getClosingBalance());
    }

}
