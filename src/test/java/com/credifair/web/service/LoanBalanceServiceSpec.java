package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.DbTestUtil;
import com.credifair.web.dao.TransactionDao;
import com.credifair.web.model.*;
import com.credifair.web.model.Currency;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.model.transaction.Transaction;
import com.credifair.web.model.transaction.link.TransactionLink;
import com.google.common.collect.Iterables;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.*;

import static junit.framework.Assert.*;

/**
 * Created by indrek.ruubel on 09/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@Rollback
@ActiveProfiles("test")
public class LoanBalanceServiceSpec extends IntSpec {

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    InterestService interestService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    TransactionDao borrowerTransactionDao;

    @Autowired
    BalanceService balanceService;

    @Autowired
    InvestmentService investmentService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    InvestorBalanceService investorBalanceService;

    @Autowired
    private ApplicationContext applicationContext;

    @Value(value = "${cf.percentage.fee}")
    BigDecimal cfFeePercentage;

    @Autowired
    MathContext mathContext;

    @Before
    public void setUp() throws SQLException {
        DbTestUtil.resetAutoIncrementColumns(applicationContext, "loan", "investment",
                "transaction", "balance", "investor_user_profile",
                "borrower_user_profile");
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenLoanNotFullyFunded() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        Loan loan = loanService.createLoanRequest(new BigDecimal(100), borrower);

        LoanBalance loanBalance = loanBalanceService.addPrincipal(loan, "da");
    }

    @Test
    public void shouldAddInterestToLoanBalance() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        loanBalanceService.addInterest(borrower, new BigDecimal(100), currency);

        LoanBalance latest = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        assertTrue(latest.getClosingInterest().compareTo(new BigDecimal(100)) == 0);
        assertTrue(latest.getType().equals(LoanBalance.DifferenceType.INCREMENTED_INTEREST));
    }


    @Test
    public void shouldCalculateRightBalanceForBorrower() throws Exception {

        InvestorUserProfile investor = createInvestor();
		User user = createUser();
        Currency currency = Currency.EUR;
        createCreditApplication(user, CreditGradeConstructor.TEST0);
        balanceService.addDeposit(new BigDecimal(600), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(100), user);


        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);

        // Fake target reached date to invoke interest calculation
        setFundedDateTime(loan, LocalDateTime.now().minusDays(1));

        interestService.calculateAllInterests();

        LoanBalance balance = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());

        BigDecimal expectedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(loan.getApr()),
                new BigDecimal(100), 2);

        if (balance == null) {
            fail();
        } else {
            assertEquals(new BigDecimal(100).add(expectedInterest), balance.getEndBalance());
        }
    }


    @Test
    public void shouldCalculateRightBalanceFor67Days() throws Exception {

        InvestorUserProfile investor = createInvestor();
		User user = createUser();
        Currency currency = Currency.EUR;
        createCreditApplication(user, CreditGradeConstructor.TEST0);
        balanceService.addDeposit(new BigDecimal(1000), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(666), user);
        investmentService.invest(new BigDecimal(444), loan, investor);
        investmentService.invest(new BigDecimal(222), loan, investor);

        // Fake target reached date to invoke interest calculation
        setFundedDateTime(loan, LocalDateTime.now().minusDays(67));

        interestService.calculateAllInterests();

        LoanBalance balance = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());

        BigDecimal expectedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(loan.getApr()),
                new BigDecimal(666), 68);

        if (balance == null) {
            fail();
        } else {
            assertEquals(new BigDecimal(666).add(expectedInterest), balance.getEndBalance());
        }
    }

    @Test
    public void shouldCalculateConsecutiveInterests() throws Exception {

        BigDecimal dailyInterestRate = interestService.calculateDailyInterest(new BigDecimal(0.12));

        InvestorUserProfile investor = createInvestor();
		User user = createUser();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.TEST5);
        balanceService.addDeposit(new BigDecimal(1000), currency, investor.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(100), user);
        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);

        // Fake target reached date to invoke interest calculation
        setFundedDateTime(loan, LocalDateTime.now().minusDays(10));

        Loan loan2 = loanService.createLoanRequest(new BigDecimal(200), user);
        investmentService.invest(new BigDecimal(100), loan2, investor);
        investmentService.invest(new BigDecimal(100), loan2, investor);

        // Fake target reached date to invoke interest calculation
        setFundedDateTime(loan2, LocalDateTime.now().minusDays(6));

        interestService.calculateAllInterests();

        LoanBalance balance = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());

        List<AbstractMap.SimpleEntry> loans = new ArrayList<>();
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(100), 4));
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(200), 2));

        BigDecimal expected = interestService.compoundOnMultipleAmounts(loans, dailyInterestRate);

        if (balance == null) {
            fail();
        } else {
            System.out.println("Expected: " + expected);
            System.out.println("Actual  : " + balance.getEndBalance());
            assertEquals(expected, balance.getEndBalance());
        }
    }


    @Test
    public void shouldNotCalculateInterestIfUnderGracePeriod() throws Exception {

        InvestorUserProfile investor = createInvestor();
		User user = createUser();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.TEST21);
        balanceService.addDeposit(new BigDecimal(1000), currency, investor.getUser().getUserProfile());

        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(100), user);
        investmentService.invest(new BigDecimal(50), loan, investor);
        investmentService.invest(new BigDecimal(50), loan, investor);

        // Fake target reached date to invoke interest calculation
        setFundedDateTime(loan, LocalDateTime.now().minusDays(10));

        Loan loan2 = loanService.createLoanRequest(new BigDecimal(200), user);
        investmentService.invest(new BigDecimal(100), loan2, investor);
        investmentService.invest(new BigDecimal(100), loan2, investor);

        // Fake target reached date to invoke interest calculation
        setFundedDateTime(loan2, LocalDateTime.now().minusDays(6));

        interestService.calculateAllInterests();

        LoanBalance balance = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());

        if (balance == null) {
            fail();
        } else {
            assertEquals(new BigDecimal(300), balance.getEndBalance());
        }
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionCauseNoBalanceToPayoff() throws Exception {
        BorrowerUserProfile borrower = createBorrower();
        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        loanService.createLoanRequest(new BigDecimal(100), borrower);
        loanBalanceService.payoff(new BigDecimal(100), borrower);
    }

    @Test
    public void shouldPayoffLoan() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;
        InvestorUserProfile investor = createInvestor();
        BigDecimal apr = new BigDecimal(0.12);

        // Create creditApplication for borrower
        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        // Put money on investor account
        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        // Do request for loan and invest in it
        Loan loan = loanService.createLoanRequest(new BigDecimal(100), borrower.getUser());
        investmentService.invest(new BigDecimal(100), loan, investor);

        // Put money on borrower account
        balanceService.addDeposit(new BigDecimal(100), currency, borrower.getUser().getUserProfile());

        // Calculate interest
        interestService.calculateAllInterests();

        LoanBalance loanbalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        BigDecimal realLoanBalance = loanbalance.getClosingPrincipal().add(loanbalance.getClosingInterest());

        // Try paying off some loan
        loanBalanceService.payoff(new BigDecimal(100), borrower);

        BigDecimal expectedInterestLeftover = interestService.compoundInterestOnAmountForNDays(interestService.calculateDailyInterest(apr), new BigDecimal(100), 1);

        // Get latest loanBalance
        LoanBalance loanbalanceLater = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        BigDecimal realLoanBalanceLeftOver = loanbalanceLater.getClosingPrincipal().add(loanbalanceLater.getClosingInterest());

        assertTrue(realLoanBalance.compareTo(new BigDecimal(100).add(expectedInterestLeftover)) == 0);
        assertTrue(expectedInterestLeftover.compareTo(realLoanBalanceLeftOver) == 0);
    }

    @Test
    public void shouldPayoffLoanAndDepositLeftover() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;
        InvestorUserProfile investor = createInvestor();
        BigDecimal apr = new BigDecimal(0.12);

        // Create creditApplication for borrower
        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        // Put money on investor account
        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        // Do request for loan and invest in it
        Loan loan = loanService.createLoanRequest(new BigDecimal(100), borrower.getUser());
        investmentService.invest(new BigDecimal(100), loan, investor);

        // Put money on borrower account
        balanceService.addDeposit(new BigDecimal(100), currency, borrower.getUser().getUserProfile());

        // Calculate interest, fuck it for now
//        interestService.calculateAllInterests();

        // Try paying off some loan
        loanBalanceService.payoff(new BigDecimal(50), borrower);

        // Get latest loanBalance
        LoanBalance loanbalanceLater = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        BigDecimal loanBalance = loanbalanceLater.getClosingPrincipal().add(loanbalanceLater.getClosingInterest());

        Balance latestBalance = balanceService.getLatestBalance(borrower.getUser().getUserProfile());
        BigDecimal closingBalance = latestBalance.getClosingBalance();

        assertTrue(loanBalance.compareTo(new BigDecimal(50)) == 0);
        assertTrue(closingBalance.compareTo(new BigDecimal(50)) == 0);
    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenNoCreditApplication() throws Exception {
        BorrowerUserProfile borrower = createBorrower();
        loanService.createLoanRequest(new BigDecimal(200), borrower);
    }

    @Test
    public void shouldPayOffBasedOnRatio() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(50), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(50), currency, investor2.getUser().getUserProfile());

        Loan loanRequest = loanService.createLoanRequest(new BigDecimal(100), borrower);

        investmentService.invest(new BigDecimal(50), loanRequest, investor1);
        Loan loan = loanService.findById(loanRequest.getId());
        assertTrue(loan.getFundedDateTime() == null);
        investmentService.invest(new BigDecimal(50), loanRequest, investor2);
        assertFalse(loan.getFundedDateTime() == null);

        // These should be 0
        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        assertTrue(latestBalanceInvestor1.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);
        assertTrue(latestBalanceInvestor2.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);

        // Should calculate interest for 1 day
        interestService.calculateAllInterests();

        BigDecimal expectedInterestFor1Day = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(loan.getApr()),
                new BigDecimal(100), 1);

        // This should have the loanBalance with 1 day interst
        LoanBalance latestLoanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        assertTrue(latestLoanBalance.getClosingInterest().compareTo(expectedInterestFor1Day) == 0);
        assertTrue(latestLoanBalance.getClosingPrincipal().compareTo(new BigDecimal(100)) == 0);
        BigDecimal loanBalance = latestLoanBalance.getClosingPrincipal().add(latestLoanBalance.getClosingInterest());

        // Borrower adds deposit that he owes
        balanceService.addDeposit(loanBalance,
                currency, borrower.getUser().getUserProfile());

        // Balance should be the same as he added
        Balance latestBalance = balanceService.getLatestBalance(borrower.getUser().getUserProfile());
        assertTrue(latestBalance.getClosingBalance().compareTo(loanBalance) == 0);

        // LoanBalance should now equal with the balance
        latestLoanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        loanBalance = latestLoanBalance.getClosingPrincipal().add(latestLoanBalance.getClosingInterest());
        assertTrue(latestBalance.getClosingBalance().compareTo(loanBalance) == 0);

        // Try paying off loanBalance from balance exactly
        loanBalanceService.payoff(latestBalance.getClosingBalance(), borrower);

        // Balance should be zeroed now
        latestBalance = balanceService.getLatestBalance(borrower.getUser().getUserProfile());
        assertTrue(latestBalance.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);

        // And loanBalance should also be zeroed
        latestLoanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        loanBalance = latestLoanBalance.getClosingPrincipal().add(latestLoanBalance.getClosingInterest());
        assertTrue(loanBalance.compareTo(BigDecimal.ZERO) == 0);

        // All investments should be closed
        List<Investment> investments = investmentService.findAllActiveInvestmentsForInvestors(investor1, investor2);
        assertTrue(investments.size() == 0);

        // Loan should be closed
        loan = loanService.findById(loan.getId());
        assertTrue(loan.getClosedDateTime() != null);

        // 12%  12
        // 10%   x

        // Calculating investor balances
        BigDecimal apr = loan.getApr(); // ex. 12%
        BigDecimal investorApr = apr.subtract(cfFeePercentage);// CF cut is 2%

        BigDecimal principal = new BigDecimal(50);
        // investorApr * interestAndFee / apr
        BigDecimal totalInterestEarned = (investorApr.multiply(expectedInterestFor1Day)).divide(apr, mathContext);
        BigDecimal interestEarnedPerInvestor = totalInterestEarned.multiply(new BigDecimal(0.5));

        // This will be transferred to our swiss account, email codename indrekruubel@gmail.com
        BigDecimal totalFee = expectedInterestFor1Day.subtract(totalInterestEarned);

        Iterable<Transaction> transactions = transactionService.getAllTransactionsByType("FEE_PAYBACK");

        assertEquals(2, Iterables.size(transactions));

        BigDecimal totalFeeInDb = BigDecimal.ZERO;
        for (Transaction transaction : transactions) {
            totalFeeInDb = totalFeeInDb.add(transaction.getAmount());
        }

        // Balance on the swiss account should be equal to totalFee
        assertTrue(totalFee.setScale(8, RoundingMode.HALF_UP)
                .compareTo(totalFeeInDb.setScale(8, RoundingMode.HALF_UP)) == 0);

        BigDecimal expectedInvestorBalance = principal.add(interestEarnedPerInvestor);

        latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        latestBalanceInvestor2 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());

        // Investors have gotten payed the principal with interest on their balances
        assertTrue(expectedInvestorBalance.setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor1.getClosingBalance().setScale(
                        8, RoundingMode.HALF_UP)) == 0);
        assertTrue(expectedInvestorBalance.setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor2.getClosingBalance().setScale(
                        8, RoundingMode.HALF_UP)) == 0);

        // Investors have investorBalance snapshots with the principal and interest repayments snapshots
        List<InvestorBalance> latestInvestor1Balances = investorBalanceService.getAllInvestorBalancesDesc(investor1);
        List<InvestorBalance> latestInvestor2Balances = investorBalanceService.getAllInvestorBalancesDesc(investor2);

        assertTrue(latestInvestor1Balances.get(0).getPrincipalRepaid().compareTo(new BigDecimal(50)) == 0);
        assertTrue(latestInvestor1Balances.get(1).getInterestRepaid().setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(interestEarnedPerInvestor.setScale(
                        8, RoundingMode.HALF_UP)) == 0);

        assertTrue(latestInvestor2Balances.get(0).getPrincipalRepaid().compareTo(new BigDecimal(50)) == 0);
        assertTrue(latestInvestor2Balances.get(1).getInterestRepaid().setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(interestEarnedPerInvestor.setScale(
                        8, RoundingMode.HALF_UP)) == 0);

    }

    @Test(expected = Exception.class)
    public void shouldThrowExceptionWhenTryingToPayoffTwice() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        Currency currency = Currency.EUR;

        createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(50), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(50), currency, investor2.getUser().getUserProfile());

        Loan loanRequest = loanService.createLoanRequest(new BigDecimal(100), borrower);

        investmentService.invest(new BigDecimal(50), loanRequest, investor1);
        Loan loan = loanService.findById(loanRequest.getId());
        assertTrue(loan.getFundedDateTime() == null);
        investmentService.invest(new BigDecimal(50), loanRequest, investor2);
        assertFalse(loan.getFundedDateTime() == null);

        // These should be 0
        Balance latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        Balance latestBalanceInvestor2 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        assertTrue(latestBalanceInvestor1.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);
        assertTrue(latestBalanceInvestor2.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);

        // Should calculate interest for 1 day
        interestService.calculateAllInterests();

        BigDecimal expectedInterestFor1Day = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(loan.getApr()),
                new BigDecimal(100), 1);

        // This should have the loanBalance with 1 day interst
        LoanBalance latestLoanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        assertTrue(latestLoanBalance.getClosingInterest().compareTo(expectedInterestFor1Day) == 0);
        assertTrue(latestLoanBalance.getClosingPrincipal().compareTo(new BigDecimal(100)) == 0);
        BigDecimal loanBalance = latestLoanBalance.getClosingPrincipal().add(latestLoanBalance.getClosingInterest());

        // Borrower adds deposit that he owes
        balanceService.addDeposit(loanBalance,
                currency, borrower.getUser().getUserProfile());

        // Balance should be the same as he added
        Balance latestBalance = balanceService.getLatestBalance(borrower.getUser().getUserProfile());
        assertTrue(latestBalance.getClosingBalance().compareTo(loanBalance) == 0);

        // LoanBalance should now equal with the balance
        latestLoanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        loanBalance = latestLoanBalance.getClosingPrincipal().add(latestLoanBalance.getClosingInterest());
        assertTrue(latestBalance.getClosingBalance().compareTo(loanBalance) == 0);

        // Try paying off loanBalance from balance exactly
        loanBalanceService.payoff(latestBalance.getClosingBalance(), borrower);
        loanBalanceService.payoff(latestBalance.getClosingBalance(), borrower);

        // Balance should be zeroed now
        latestBalance = balanceService.getLatestBalance(borrower.getUser().getUserProfile());
        assertTrue(latestBalance.getClosingBalance().compareTo(BigDecimal.ZERO) == 0);

        // And loanBalance should also be zeroed
        latestLoanBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower);
        loanBalance = latestLoanBalance.getClosingPrincipal().add(latestLoanBalance.getClosingInterest());
        assertTrue(loanBalance.compareTo(BigDecimal.ZERO) == 0);

        // All investments should be closed
        List<Investment> investments = investmentService.findAllActiveInvestmentsForInvestors(investor1, investor2);
        assertTrue(investments.size() == 0);

        // Loan should be closed
        loan = loanService.findById(loan.getId());
        assertTrue(loan.getClosedDateTime() != null);

        // 12%  12
        // 10%   x

        // Calculating investor balances
        BigDecimal apr = loan.getApr(); // ex. 12%
        BigDecimal investorApr = apr.subtract(cfFeePercentage);// CF cut is 2%

        BigDecimal principal = new BigDecimal(50);
        // investorApr * interestAndFee / apr
        BigDecimal totalInterestEarned = (investorApr.multiply(expectedInterestFor1Day)).divide(apr, mathContext);
        BigDecimal interestEarnedPerInvestor = totalInterestEarned.multiply(new BigDecimal(0.5));

        // This will be transferred to our swiss account, email codename indrekruubel@gmail.com
        BigDecimal totalFee = expectedInterestFor1Day.subtract(totalInterestEarned);
        Balance balance = balanceService.getLatestBalanceForUserWithEmail("indrekruubel@gmail.com");

        // Balance on the swiss account should be equal to totalFee
        assertTrue(totalFee.setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(balance.getClosingBalance().setScale(8, RoundingMode.HALF_UP)) == 0);

        BigDecimal expectedInvestorBalance = principal.add(interestEarnedPerInvestor);

        latestBalanceInvestor1 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());
        latestBalanceInvestor2 = balanceService.getLatestBalance(investor1.getUser().getUserProfile());

        // Investors have gotten payed the principal with interest on their balances
        assertTrue(expectedInvestorBalance.setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor1.getClosingBalance().setScale(
                        8, RoundingMode.HALF_UP)) == 0);
        assertTrue(expectedInvestorBalance.setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(latestBalanceInvestor2.getClosingBalance().setScale(
                        8, RoundingMode.HALF_UP)) == 0);

        // Investors have investorBalance snapshots with the principal and interest repayments snapshots
        List<InvestorBalance> latestInvestor1Balances = investorBalanceService.getAllInvestorBalancesDesc(investor1);
        List<InvestorBalance> latestInvestor2Balances = investorBalanceService.getAllInvestorBalancesDesc(investor2);

        assertTrue(latestInvestor1Balances.get(0).getPrincipalRepaid().compareTo(new BigDecimal(50)) == 0);
        assertTrue(latestInvestor1Balances.get(1).getInterestRepaid().setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(interestEarnedPerInvestor.setScale(
                        8, RoundingMode.HALF_UP)) == 0);

        assertTrue(latestInvestor2Balances.get(0).getPrincipalRepaid().compareTo(new BigDecimal(50)) == 0);
        assertTrue(latestInvestor2Balances.get(1).getInterestRepaid().setScale(
                8, RoundingMode.HALF_UP)
                .compareTo(interestEarnedPerInvestor.setScale(
                        8, RoundingMode.HALF_UP)) == 0);

    }

    @Test
    public void shouldSaveAllTransactionsWhenPayoff() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();
        Currency currency = Currency.EUR;

        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        // Add money to investor
        balanceService.addDeposit(new BigDecimal(100), currency, investor.getUser().getUserProfile());

        // Reserv money
        investmentService.reserv(creditApplication, new BigDecimal(100), investor.getUser());

        // Request and get funded
        Loan loan = loanService.requestAndFundLoan(new BigDecimal(100), borrower);

        // Add money to borrower's balance
        balanceService.addDeposit(new BigDecimal(100), currency, borrower.getUser().getUserProfile());

        // Generate some fees and interest
        interestService.calculateAllInterests();

        // Pay off
        loanBalanceService.payoff(new BigDecimal(100), borrower);

        // Check if all the transactions are created
        Iterable<Transaction> transactions = transactionService.findAllWhereUserIn(borrower.getUser(), investor.getUser());

        Map<String, List<Transaction>> transactionCounts = new HashMap<>();

        for (Transaction transaction : transactions) {
            String type = transaction.getType();
            List<Transaction> countsSoFar = transactionCounts.get(type);
            if (countsSoFar == null) {
                transactionCounts.put(type, new ArrayList<Transaction>(){{add(transaction);}});
            } else {
                countsSoFar.add(transaction);
                transactionCounts.put(type, countsSoFar);
            }
        }

        assertEquals(1, transactionCounts.get("BORROWER_BORROW").size());
        assertEquals(1, transactionCounts.get("INVESTOR_INVEST").size()); // InvestmentSnapshot, Balance

        assertEquals(2, transactionCounts.get("DEPOSIT").size()); // Balance * 2
        assertEquals(1, transactionCounts.get("BORROWER_PRINCIPAL_PAYBACK").size()); // LoanBalance
        assertEquals(1, transactionCounts.get("BORROWER_INTEREST_PAYBACK").size()); // LoanBalance
        assertEquals(1, transactionCounts.get("FEE_PAYBACK").size()); // InvestmentSnapshot
        assertEquals(1, transactionCounts.get("INVESTOR_INTEREST_PAYBACK").size()); // Balance, InvestorBalance
        assertEquals(1, transactionCounts.get("INVESTOR_PRINCIPAL_PAYBACK").size()); // Balance, InvestorBalance
        assertEquals(1, transactionCounts.get("BORROWER_INTEREST_INCREMENT").size()); // LoanBalance
        assertEquals(1, transactionCounts.get("INVESTOR_INTEREST_INCREMENT").size()); // InvestmentSnapshot
        assertEquals(1, transactionCounts.get("FEE_INCREMENT").size()); // InvestmentSnapshot

        // Fetch all the transaction links
        Iterable<TransactionLink> links = transactionService.findAllLinksWhereTransactionIn(transactions);

        Map<String, List<TransactionLink>> transactionLinkCounts = new HashMap<>();
        for (TransactionLink transactionLink : links) {
            String type = transactionLink.getType();
            List<TransactionLink> countsSoFar = transactionLinkCounts.get(type);
            if (countsSoFar == null) {
                transactionLinkCounts.put(type, new ArrayList<TransactionLink>(){{add(transactionLink);}});
            } else {
                countsSoFar.add(transactionLink);
                transactionLinkCounts.put(type, countsSoFar);
            }
        }

        assertEquals(2, transactionLinkCounts.get("INVESTOR_BALANCE").size());
        assertEquals(4, transactionLinkCounts.get("INVESTMENT_SNAPSHOT").size());
        assertEquals(3, transactionLinkCounts.get("LOAN_BALANCE").size());
        assertEquals(6, transactionLinkCounts.get("BALANCE").size());
    }

    @Test
    public void shouldGiveBackNextPaymentSchedule() throws Exception {

        BorrowerUserProfile borrower = createBorrower();
        InvestorUserProfile investor = createInvestor();

        CreditApplication creditApplication = createCreditApplication(borrower.getUser(), CreditGradeConstructor.TEST0);

        // Add money to investor
        balanceService.addDeposit(new BigDecimal(10), Currency.EUR, investor.getUser().getUserProfile());

        // Reserv money
        investmentService.reserv(creditApplication, new BigDecimal(10), investor.getUser());

        // Request and get funded
        Loan loan = loanService.requestAndFundLoan(new BigDecimal(10), borrower);

        // Generate some fees and interest
        interestService.calculateAllInterests();

        List<HashMap<String, Object>> payoffSchedule = loanBalanceService.getPayoffSchedule(borrower.getUser());

        System.out.println(payoffSchedule);

        fail("TODO");

    }


}
