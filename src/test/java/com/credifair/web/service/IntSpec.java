package com.credifair.web.service;

import com.credifair.web.dao.*;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.model.verification.AddressVerification;
import com.credifair.web.model.verification.IdVerification;
import com.credifair.web.model.verification.IncomeVerification;
import org.springframework.beans.factory.annotation.Autowired;

import javax.persistence.EntityManager;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

/**
 * Created by indrek.ruubel on 09/02/2016.
 */
public class IntSpec {

    @Autowired
    InvestorUserProfileDao investorUserProfileDao;

    @Autowired
    BorrowerUserProfileDao borrowerUserProfileDao;

    @Autowired
    TransactionDao borrowerTransactionDao;

    @Autowired
    LoanService loanService;

    @Autowired
    UserDao userDao;

    @Autowired
    LoanDao loanDao;

    @Autowired
    CreditGradeDao creditGradeDao;

    @Autowired
    CreditApplicationDao creditApplicationDao;

    @Autowired
    VerificationDao verificationDao;

    @Autowired
    UserService userService;

    @Autowired
    InvestmentDao investmentDao;

    @Autowired
    InvestmentSnapshotDao investmentSnapshotDao;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    EntityManager entityManager;

    @Autowired
    CreditLimitService creditLimitService;

    @Autowired
    MathContext mathContext;

    protected InvestorUserProfile createInvestor() {
        User user = userService.saveCustomer("investor" + UUID.randomUUID().toString() + "@doe.com", "pass");
        InvestorUserProfile investorUserProfile = user.getInvestorUserProfile();

        flushClear();

        investorUserProfile.setUser(user);
        return investorUserProfile;
    }

    protected BorrowerUserProfile createBorrower() {
        User user = userService.saveCustomer("borrower" + UUID.randomUUID().toString() + "@doe.com", "pass");
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();

        flushClear();

        borrowerUserProfile.setUser(user);
        return borrowerUserProfile;
    }

    protected UserProfile createUserProfile() {
        User user = userService.saveCustomer("borrower" + UUID.randomUUID().toString() + "@doe.com", "pass");
        UserProfile userProfile = user.getUserProfile();

        flushClear();

        userProfile.setUser(user);
        return userProfile;
    }

    protected User createUser() {
        User user = userService.saveCustomer("user" + UUID.randomUUID().toString() + "@doe.com", "pass");
        flushClear();

        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        borrowerUserProfile.setUser(user);
        InvestorUserProfile investorUserProfile = user.getInvestorUserProfile();
        investorUserProfile.setUser(user);
        UserProfile userProfile = user.getUserProfile();
        user.setUserProfile(userProfile);
        return user;
    }

    protected CreditApplication createCreditApplication(User user, CreditGradeConstructor grade) {
        CreditApplication creditApplication = creditApplicationService.getStarted(user);
        creditApplicationService.assignGradeToCAWithMinimalAPR(creditApplication, grade);
        creditLimitService.assignNewCreditLimit(creditApplication, new BigDecimal(2000), null);
        user.getBorrowerUserProfile().setCreditApplication(creditApplication);
        return creditApplication;
    }

    protected BigDecimal calculateAmountWithoutFee(BigDecimal apr, BigDecimal amount) {
        // Fee calculation
        // 12%  investor1Interest
        // 10%  x
        BigDecimal investorApr = apr.subtract(new BigDecimal(0.02));
        BigDecimal amountWithoutFee = (investorApr.multiply(amount)).divide(apr, mathContext);
        return amountWithoutFee;
    }

    protected IdVerification createIdVerification(User user) {
        IdVerification verification = new IdVerification(user);
        verificationDao.save(verification);
        return verification;
    }

    protected AddressVerification createAddressVerification(User user) {
        AddressVerification verification = new AddressVerification(user);
        verificationDao.save(verification);
        return verification;
    }

    protected IncomeVerification createIncomeVerification(User user) {
        IncomeVerification verification = new IncomeVerification(user);
        verificationDao.save(verification);
        return verification;
    }

    protected void setFundedDateTime(Loan loan, LocalDateTime fundedDateTime) {
        List<Investment> investments = investmentDao.findAllByLoan(loan);
        for (Investment loanInvestment : investments) {
            loanInvestment.setInvestedDateTime(fundedDateTime);
            investmentDao.save(loanInvestment);
        }
        loan.setFundedDateTime(fundedDateTime);
        loanDao.save(loan);
    }

    protected void setLastInterestCalculationToLoanAndInvestments(Loan loan, LocalDateTime lastCalculatedInterest) {
        List<Investment> investments = investmentDao.findAllByLoan(loan);
        for (Investment loanInvestment : investments) {
            InvestmentSnapshop latestSnapshop = loanInvestment.getLatestSnapshop();
            latestSnapshop.setCreated(lastCalculatedInterest);
            investmentSnapshotDao.save(latestSnapshop);
        }
        loan.setLastCalculatedInterest(lastCalculatedInterest);
        loanDao.save(loan);
    }

    protected void flushClear() {
        entityManager.flush();
        entityManager.clear();
    }

}
