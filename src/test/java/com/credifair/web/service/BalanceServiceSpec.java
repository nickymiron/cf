package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.model.Balance;
import com.credifair.web.model.Currency;
import com.credifair.web.model.User;
import com.credifair.web.model.UserProfile;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static junit.framework.Assert.assertFalse;
import static junit.framework.Assert.assertTrue;

/**
 * Created by indrek.ruubel on 06/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@Rollback
@ActiveProfiles("test")
public class BalanceServiceSpec extends IntSpec {

	@Autowired
	BalanceService balanceService;

	@Autowired
	LoanService loanService;

	Currency currency = Currency.EUR;

	@Test
	public void shouldAddMoneyToBalance() {
		UserProfile userProfile = createUserProfile();

		BigDecimal amount = new BigDecimal(100);

		balanceService.addDeposit(amount, currency, userProfile);

		Balance latestBalance = balanceService.getLatestBalance(userProfile);

		assertTrue(latestBalance.getClosingBalance().compareTo(amount) == 0);
	}

	@Test
	public void shouldHaveExactlyTheMoney() {
		UserProfile userProfile = createUserProfile();

		BigDecimal amount = new BigDecimal(100);
		balanceService.addDeposit(amount, currency, userProfile);
		boolean hasMoney = balanceService.hasEnoughMoneyOnBalance(userProfile, new BigDecimal(100));

		assertTrue(hasMoney);
	}

	@Test
	public void shouldHaveTheMoney() {
		UserProfile userProfile = createUserProfile();

		BigDecimal amount = new BigDecimal(200);
		balanceService.addDeposit(amount, currency, userProfile);
		boolean hasMoney = balanceService.hasEnoughMoneyOnBalance(userProfile, new BigDecimal(100));

		assertTrue(hasMoney);
	}

	@Test
	public void shouldNotHaveTheMoney() {
		UserProfile userProfile = createUserProfile();

		BigDecimal amount = new BigDecimal(99);
		balanceService.addDeposit(amount, currency, userProfile);
		boolean hasMoney = balanceService.hasEnoughMoneyOnBalance(userProfile, new BigDecimal(100));

		assertFalse(hasMoney);
	}

	@Test(expected = Exception.class)
	public void shouldThrowExceptionIfBalanceIsNegated() throws Exception {
		User user = createUser();

		balanceService.addDeposit(new BigDecimal(100), currency, user.getUserProfile());

		balanceService.subtractInvestment(new BigDecimal(101), currency, user.getUserProfile());
	}

}
