package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.dao.StateDao;
import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.model.User;
import com.credifair.web.model.state.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;

import static junit.framework.Assert.assertTrue;

/**
 * Created by indrek.ruubel on 03/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
public class StateServiceSpec extends IntSpec {

	@Autowired
	CAStateService CAStateService;

	@Autowired
	StateDao stateDao;

	@Autowired
	CreditApplicationService creditApplicationService;

	@Test
	public void shouldCreateNewState() throws Exception {
		User user = createUser();
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = creditApplicationService.getStarted(user);
		creditApplicationService.assignGradeToCAWithMinimalAPR(creditApplication, CreditGradeConstructor.A);

		CAState newState = CAStateService.createNewState(borrowerUserProfile, creditApplication);
		CAState persisted = stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);

		assertTrue(newState instanceof NewCAState);
		assertTrue(persisted instanceof NewCAState);
	}

	@Test
	public void shouldCreatePreparingOfferState() throws Exception {
		User user = createUser();
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = creditApplicationService.getStarted(user);
		creditApplicationService.assignGradeToCAWithMinimalAPR(creditApplication, CreditGradeConstructor.A);

		CAState newState = CAStateService.createPreparingOfferState(borrowerUserProfile, creditApplication);
		CAState persisted = stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);

		assertTrue(newState instanceof PreparingOfferCAState);
		assertTrue(persisted instanceof PreparingOfferCAState);
	}

	@Test
	public void shouldCreateOfferCreatedState() throws Exception {
		User user = createUser();
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		CAState newState = CAStateService.createOfferCreatedState(borrowerUserProfile, creditApplication);
		CAState persisted = stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);

		assertTrue(newState instanceof OfferCreatedCAState);
		assertTrue(persisted instanceof OfferCreatedCAState);
	}

	@Test
	public void shouldCreateOfferAcceptedState() throws Exception {
		User user = createUser();
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		CAState newState = CAStateService.createOfferAcceptedState(borrowerUserProfile, creditApplication);
		CAState persisted = stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);

		assertTrue(newState instanceof OfferAcceptedCAState);
		assertTrue(persisted instanceof OfferAcceptedCAState);
	}

	@Test
	public void shouldCreateProcessingState() throws Exception {
		User user = createUser();
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		CAState newState = CAStateService.createProcessingState(borrowerUserProfile, creditApplication);
		CAState persisted = stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);

		assertTrue(newState instanceof ProcessingCAState);
		assertTrue(persisted instanceof ProcessingCAState);
	}

	@Test
	public void shouldCreateWaitingInvestorsState() throws Exception {
		User user = createUser();
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		CAState newState = CAStateService.createWaitingInvestorsState(borrowerUserProfile, creditApplication);
		CAState persisted = stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);

		assertTrue(newState instanceof WaitingInvestorsCAState);
		assertTrue(persisted instanceof WaitingInvestorsCAState);
	}

}
