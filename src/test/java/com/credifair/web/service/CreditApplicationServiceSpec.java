package com.credifair.web.service;

import com.credifair.web.Application;
import com.credifair.web.model.Currency;
import com.credifair.web.model.User;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditApplicationInvestor;
import com.credifair.web.model.credit.CreditGradeConstructor;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.math.BigDecimal;

import static junit.framework.Assert.*;

/**
 * Created by indrek.ruubel on 06/04/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@Rollback
@ActiveProfiles("test")
public class CreditApplicationServiceSpec extends IntSpec {

	@Autowired
	CreditApplicationService creditApplicationService;

	UserService userService;

	@Autowired
	LoanService loanService;

	@Autowired
	LoanBalanceService loanBalanceService;

	@Autowired
	BalanceService balanceService;

	@Autowired
	InvestmentService investmentService;

	@Before
	public void setup() {
		userService = Mockito.mock(UserService.class);
		creditApplicationService.userService = userService;
	}

	@Test
	public void shouldCalculateFundingLeftCorrectly() {
		User user = createUser();

		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);
		BigDecimal creditLimit = new BigDecimal(2000);
		BigDecimal amountLeft = creditApplicationService.calculateFundingLeft(creditApplication);

		assertTrue(amountLeft.compareTo(creditLimit) == 0);
	}

	@Test(expected = Exception.class)
	public void shouldNotInvestBecauseNoBalance() throws Exception {
		User user = createUser();
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		BigDecimal creditLimit = new BigDecimal(2000);
		CreditApplicationInvestor creditApplicationInvestor = investmentService.reserv(creditApplication.getId(), new BigDecimal(100), user);

		creditApplication = creditApplicationDao.findOne(creditApplication.getId());
		BigDecimal amountLeft = creditApplicationService.calculateFundingLeft(creditApplication);

		assertNull(creditApplicationInvestor);
		assertTrue(amountLeft.compareTo(creditLimit) == 0);
	}

	@Test
	public void shouldInvestBecauseEnoughBalanceOnAccount() throws Exception {
		User user = createUser();
		Currency currency = Currency.EUR;
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		BigDecimal creditLimit = new BigDecimal(2000);
		BigDecimal deposit = new BigDecimal(100);
		balanceService.addDeposit(deposit, currency, user.getUserProfile());

		CreditApplicationInvestor creditApplicationInvestor = investmentService.reserv(creditApplication.getId(),
				deposit, user);

		creditApplication = creditApplicationDao.findOne(creditApplication.getId());
		BigDecimal amountLeft = creditApplicationService.calculateFundingLeft(creditApplication);

		BigDecimal expected = creditLimit.subtract(deposit);
		assertNotNull(creditApplicationInvestor);
		assertTrue(amountLeft.compareTo(expected) == 0);
	}

	@Test(expected = Exception.class)
	public void shouldNotInvestBecauseNotEnoughBalanceOnAccount() throws Exception {
		User user = createUser();
		Currency currency = Currency.EUR;
		CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.A);

		BigDecimal creditLimit = new BigDecimal(2000);
		BigDecimal payIn = new BigDecimal(100);

		balanceService.addDeposit(payIn, currency, user.getUserProfile());

		CreditApplicationInvestor creditApplicationInvestor = investmentService.reserv(creditApplication.getId(),
				new BigDecimal(200), user);

		creditApplication = creditApplicationDao.findOne(creditApplication.getId());
		BigDecimal amountLeft = creditApplicationService.calculateFundingLeft(creditApplication);

		assertNull(creditApplicationInvestor);
		assertTrue(amountLeft.compareTo(creditLimit) == 0);
	}

}
