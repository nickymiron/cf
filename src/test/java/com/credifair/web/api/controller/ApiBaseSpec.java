package com.credifair.web.api.controller;

import com.credifair.web.api.request.CreateUserRequest;
import com.credifair.web.model.Country;
import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.web.FilterChainProxy;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.util.Base64Utils;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by indrek.ruubel on 19/01/2016.
 */
public class ApiBaseSpec {

    @Autowired
    protected WebApplicationContext context;

    @Autowired
    protected FilterChainProxy springSecurityFilterChain;

    protected MockMvc mvc;

    protected String authorization = "Basic "
            + new String(Base64Utils.encode("api-client:12345".getBytes()));

    protected String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";

    protected String getClientToken() throws Exception {

        String content = mvc
                .perform(
                        post("/api/v1/oauth/token")
                                .header("Authorization", authorization)
                                .contentType(
                                        MediaType.APPLICATION_FORM_URLENCODED)
                                .param("grant_type", "client_credentials"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        return content.substring(17, 53);
    }

    protected String registerUser(String email, String password, String clientToken) throws Exception {

        Gson gson = new Gson();
        CreateUserRequest req = new CreateUserRequest(email, password, Country.EE);
        String json = gson.toJson(req);

        String content = mvc
                .perform(
                        post("/api/v1/customer/")
                                .header("Authorization", "Bearer " + clientToken)
                                .contentType(
                                        MediaType.APPLICATION_JSON)
                                .content(json))
                .andExpect(status().isCreated())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();
        return content;
    }

    protected String getUserAccessToken(String email, String password) throws Exception {
        String content = mvc
                .perform(
                        post("/api/v1/oauth/token")
                                .header("Authorization", authorization)
                                .accept(MediaType.APPLICATION_JSON)
                                .contentType(
                                        MediaType.APPLICATION_FORM_URLENCODED)
                                .param("username", email)
                                .param("password", password)
                                .param("grant_type", "password")
                                .param("scope", "read write"))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();
        return content.substring(17, 53);
    }

}
