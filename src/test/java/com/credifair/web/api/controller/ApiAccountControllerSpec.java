package com.credifair.web.api.controller;

import com.credifair.web.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import spock.util.mop.ConfineMetaClassChanges;

import javax.transaction.Transactional;

import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by indrek.ruubel on 19/01/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@ConfineMetaClassChanges(value = ApiCustomerController.class)
@Transactional
@ActiveProfiles("test")
public class ApiAccountControllerSpec extends ApiBaseSpec {

    @InjectMocks
    ApiCustomerController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    public void accountGetUnauthorized() throws Exception {
        mvc.perform(get("/api/v1/customer/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void accountGetUnauthorizedWithRandomToken() throws Exception {
        mvc.perform(get("/api/v1/customer/")
                .header("Authorization", "Bearer fakeTokenYoMan")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void accountPostUnauthorized() throws Exception {
        mvc.perform(post("/api/v1/customer/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldGetClientToken() throws Exception {
        String clientToken = getClientToken();
        assertNotNull(clientToken);
        assertTrue(clientToken.length() > 10);
    }

    @Test
    public void shouldRegisterUser() throws Exception {
        String email = "freddy@mercury.com";
        String password = "oooohLovaBoyyy";

        String clientToken = getClientToken();

        String registered = registerUser(email, password, clientToken);
        assertTrue(registered.contains("freddy@mercury.com"));
    }

    @Test
    public void shouldFetchesUserAccessToken() throws Exception {
        String email = "freddy@mercury.com";
        String password = "oooohLovaBoyyy";

        String clientToken = getClientToken();
        registerUser(email, password, clientToken);
        String userAccessToken = getUserAccessToken(email, password);
        assertNotNull(userAccessToken);
        assertTrue(userAccessToken.length() > 10);
    }

    @Test
    public void shouldGetResponseFromApiWithToken() throws Exception {
        String email = "freddy@mercury.com";
        String password = "oooohLovaBoyyy";

        String clientToken = getClientToken();
        registerUser(email, password, clientToken);
        String userAccessToken = getUserAccessToken(email, password);

        String contentType = MediaType.APPLICATION_JSON + ";charset=UTF-8";

        String content = mvc
                .perform(
                        get("/api/v1/customer/info")
                                .header("Authorization", "Bearer " + userAccessToken))
                .andExpect(status().isOk())
                .andExpect(content().contentType(contentType))
                .andReturn().getResponse().getContentAsString();

        assertTrue(content.contains("firstName"));
    }

    @Test
    public void shouldntGetInfoWithClientToken() throws Exception {
        String clientToken = getClientToken();

        String response = mvc.perform(get("/api/v1/customer/info")
                .header("Authorization", "Bearer " + clientToken)
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isForbidden())
                .andReturn().getResponse().getContentAsString();

        assertTrue(response.contains("access_denied"));
    }

}
