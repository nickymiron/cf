package com.credifair.web.api.controller;

import com.credifair.web.Application;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import javax.transaction.Transactional;

import static org.junit.Assert.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by indrek.ruubel on 19/01/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
public class ApiMarketingControllerSpec extends ApiBaseSpec {

    @InjectMocks
    ApiMarketingController controller;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        mvc = MockMvcBuilders.webAppContextSetup(context)
                .addFilter(springSecurityFilterChain).build();
    }

    @Test
    public void marketingPostUnauthorized() throws Exception {
        mvc.perform(get("/api/v1/marketing/")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldNotSubscribeEmailNoToken() throws Exception {
        String email = "testemail@gmail.com";

        mvc.perform(post("/api/v1/marketing/subscribe/" + email)
            .contentType(
                    MediaType.APPLICATION_JSON)
            )
        .andExpect(status().isUnauthorized());
    }

    @Test
    public void shouldSubscribeEmail() throws Exception {
        String clientToken = getClientToken();
        String email = "testemail@gmail.com";

        String content = mvc
            .perform(
                    post("/api/v1/marketing/subscribe/" + email)
                            .header("Authorization", "Bearer " + clientToken)
                            .contentType(
                                    MediaType.APPLICATION_JSON)
                            )
            .andExpect(status().isCreated())
            .andReturn().getResponse().getContentAsString();

        assertTrue(content.contains(email));
    }
}
