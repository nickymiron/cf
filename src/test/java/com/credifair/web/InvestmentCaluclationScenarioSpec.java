package com.credifair.web;

import com.credifair.web.dao.*;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.service.*;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.context.ApplicationContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.MathContext;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.util.AbstractMap;
import java.util.ArrayList;
import java.util.List;

import static junit.framework.Assert.assertEquals;
import static junit.framework.Assert.assertNotNull;


/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@WebAppConfiguration
@SpringApplicationConfiguration(classes = Application.class)
@Transactional
@ActiveProfiles("test")
public class InvestmentCaluclationScenarioSpec extends IntSpec {


    @Autowired
    LoanService loanService;

    @Autowired
    InterestService interestService;

    @Autowired
    InvestmentDao investmentDao;

    @Autowired
    BorrowerUserProfileDao borrowerUserProfileDao;

    @Autowired
    TransactionDao borrowerTransactionDao;

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    InvestorUserProfileDao investorUserProfileDao;

    @Autowired
    TransactionService transactionService;

    @Autowired
    LoanDao loanDao;

    @Autowired
    MathContext mathContext;

    @Autowired
    BalanceService balanceService;

    @Autowired
    InvestmentService investmentService;

    @Autowired
    private ApplicationContext applicationContext;

    BorrowerUserProfile profile;

    @Before
    public void setUp() throws SQLException {
        DbTestUtil.resetAutoIncrementColumns(applicationContext, "loan", "investment",
                "transaction", "balance", "investor_user_profile",
                "borrower_user_profile");
        profile = Mockito.mock(BorrowerUserProfile.class);
    }

    @Test
    public void shouldCaluclateInterestIndividuallyAndAddUpWithTotalCompounding1Day() throws Exception {
        User user = createUser();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(user, CreditGradeConstructor.TEST21);
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(200, mathContext), user);

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(100), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(100), currency, investor2.getUser().getUserProfile());


        investmentService.invest(new BigDecimal(100), loan, investor1);
        investmentService.invest(new BigDecimal(100), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(22));

        interestService.calculateAllInterests();

        BigDecimal totalCompoundedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(creditGrade.getApr()), new BigDecimal(200), 2);

        assertNotNull(loan.getFundedDateTime());
    }

    @Test
    public void shouldCaluclateInterestIndividuallyAndAddUpWithTotalCompounding4Days() throws Exception {
        User borrower = createUser();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(borrower, CreditGradeConstructor.TEST21);
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(200, mathContext), borrower);

        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();

        balanceService.addDeposit(new BigDecimal(100), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(100), currency, investor2.getUser().getUserProfile());

        investmentService.invest(new BigDecimal(100), loan, investor1);
        investmentService.invest(new BigDecimal(100), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(25));

        interestService.calculateAllInterests();

        BigDecimal totalCompoundedInterest = interestService.compoundInterestOnAmountForNDays(
                interestService.calculateDailyInterest(creditGrade.getApr()), new BigDecimal(200), 5);

        assertNotNull(loan.getFundedDateTime());
    }


    @Test
    public void shouldCalculateInterestCorrectlyOnceADay() throws Exception {

        BigDecimal dailyInterestRate = interestService.calculateDailyInterest(new BigDecimal(0.12));

        User user = createUser();
        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;
        createCreditApplication(user, CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(200), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(200), currency, investor2.getUser().getUserProfile());

        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), user);
        investmentService.invest(new BigDecimal(50), loan, investor1);
        investmentService.invest(new BigDecimal(50), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1));

        // Calculate for 2 days
        interestService.calculateAllInterests();

        LoanBalance bal = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());
        System.out.println("Principal: " + bal.getClosingPrincipal());
        System.out.println("Interest: " + bal.getClosingInterest());

        Loan loan2 = loanService.createLoanRequest(new BigDecimal(100, mathContext), user);
        investmentService.invest(new BigDecimal(50), loan2, investor1);
        investmentService.invest(new BigDecimal(50), loan2, investor2);

        setFundedDateTime(loan, LocalDateTime.now());

        interestService.calculateAllInterests();

        bal = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());
        System.out.println("Principal: " + bal.getClosingPrincipal());
        System.out.println("Interest: " + bal.getClosingInterest());

        List<AbstractMap.SimpleEntry> loans = new ArrayList<>();
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(100), 1));
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(100), 1));

        BigDecimal expected = interestService.compoundOnMultipleAmounts(loans, dailyInterestRate);

        LoanBalance latestBalance = loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());

        assertEquals(expected, latestBalance.getEndBalance());
    }

    @Test
    public void shouldNotCalculateForever() throws Exception {

        BigDecimal dailyInterestRate = interestService.calculateDailyInterest(new BigDecimal(0.12));

        User borrower = createUser();
        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(borrower, CreditGradeConstructor.TEST0);

        balanceService.addDeposit(new BigDecimal(200), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(200), currency, investor2.getUser().getUserProfile());

        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower);

        investmentService.invest(new BigDecimal(50), loan, investor1);
        investmentService.invest(new BigDecimal(50), loan, investor2);

        setFundedDateTime(loan, LocalDateTime.now().minusDays(1));

        interestService.calculateAllInterests(); // 0

        Loan loan2 = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower);
        investmentService.invest(new BigDecimal(50), loan2, investor1);
        investmentService.invest(new BigDecimal(50), loan2, investor2);
        setFundedDateTime(loan2, LocalDateTime.now());

        interestService.calculateAllInterests(); // 0

        List<AbstractMap.SimpleEntry> loans = new ArrayList<>();
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(100), 1));
        loans.add(new AbstractMap.SimpleEntry<>(new BigDecimal(100), 1));

        BigDecimal expected = interestService.compoundOnMultipleAmounts(loans, dailyInterestRate);

        LoanBalance latestBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower.getBorrowerUserProfile());

        interestService.calculateAllInterests(); // 0

        LoanBalance newLatestBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower.getBorrowerUserProfile());

        assertEquals(expected, latestBalance.getEndBalance());
        assertEquals(latestBalance.getEndBalance(), newLatestBalance.getEndBalance());
    }

    @Test
    public void shouldNotCalculateInterestIfNotOverGracePeriod() throws Exception {

        User borrower = createUser();
        InvestorUserProfile investor1 = createInvestor();
        InvestorUserProfile investor2 = createInvestor();
        Currency currency = Currency.EUR;
        CreditApplication creditApplication = createCreditApplication(borrower, CreditGradeConstructor.TEST30);

        balanceService.addDeposit(new BigDecimal(200), currency, investor1.getUser().getUserProfile());
        balanceService.addDeposit(new BigDecimal(200), currency, investor2.getUser().getUserProfile());

        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower);
        investmentService.invest(new BigDecimal(50), loan, investor1);
        investmentService.invest(new BigDecimal(50), loan, investor2);
        setFundedDateTime(loan, LocalDateTime.now().minusDays(10));

        interestService.calculateAllInterests();

        Loan loan2 = loanService.createLoanRequest(new BigDecimal(100, mathContext), borrower);
        investmentService.invest(new BigDecimal(50), loan2, investor1);
        investmentService.invest(new BigDecimal(50), loan2, investor2);
        setFundedDateTime(loan2, LocalDateTime.now().minusDays(7));

        interestService.calculateAllInterests();

        BigDecimal expected = new BigDecimal(200);

        LoanBalance latestBalance = loanBalanceService.getLatestLoanBalanceForBorrower(borrower.getBorrowerUserProfile());

        assertEquals(expected, latestBalance.getEndBalance());

    }

}
