package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
public enum Country {

	EE("Estonia", "EE", "est", 372, "233", Currency.EUR),
	US("United States", "US", "usa", 1, "840", Currency.USD),
	GB("United Kingdom", "GB", "gbr", 44, "826", Currency.GBP);

	private String country;
	private String countryISO2code; // ISO3166_2 a.k.a ISO 2-alpha code
	private String countryISO3code; // ISO3166_3 a.k.a ISO 3-alpha code
	private Integer callingCode;
	private String numeric3Code; // ISO3166_1 a.k.a numeric-3
	private Currency currency;

	Country(String country, String countryISO2code, String countryISO3code, Integer callingCode, String numeric3Code, Currency currency) {
		this.country = country;
		this.countryISO2code = countryISO2code;
		this.countryISO3code = countryISO3code;
		this.callingCode = callingCode;
		this.numeric3Code = numeric3Code;
		this.currency = currency;
	}

	public String getCountry() {
		return country;
	}

	public String getCountryISO2code() {
		return countryISO2code;
	}

	public String getCountryISO3code() {
		return countryISO3code;
	}

	public Integer getCallingCode() {
		return callingCode;
	}

	public String getNumeric3Code() {
		return numeric3Code;
	}

	public Currency getCurrency() {
		return currency;
	}

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (Country country : values()) {
			out.add(country.name());
		}
		return out;
	}
}
