package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
public enum HomeOwnership {
	OWNER, MORTGAGE, OWNER_WITH_ENCUMBRANCE, LIVING_WITH_PARENTS;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (HomeOwnership homeOwnership : values()) {
			out.add(homeOwnership.name());
		}
		return out;
	}
}
