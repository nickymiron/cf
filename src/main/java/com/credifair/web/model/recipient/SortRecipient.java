package com.credifair.web.model.recipient;

import com.credifair.web.model.User;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 14/05/2016.
 */
@Entity
@DiscriminatorValue(value = "SORT")
public class SortRecipient extends Recipient {

	@Column(name = "sort_code")
	private String sortCode;

	@Column(name = "account_number")
	private String accountNumber;

	public SortRecipient() {
	}

	public SortRecipient(User user, String sortCode, String accountNumber) {
		super(user);
		this.sortCode = sortCode;
		this.accountNumber = accountNumber;
	}

	@Override
	public RecipientType getType() {
		return RecipientType.SORT;
	}

	@Override
	public String getSortCode() {
		return sortCode;
	}

	@Override
	public String getAccountNumber() {
		return accountNumber;
	}
}
