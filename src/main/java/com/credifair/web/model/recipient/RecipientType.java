package com.credifair.web.model.recipient;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
public enum RecipientType {
	SORT, IBAN;
}
