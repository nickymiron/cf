package com.credifair.web.model.recipient;

import com.credifair.web.model.User;

import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 14/05/2016.
 */
@Entity
@DiscriminatorValue(value = "IBAN")
public class IbanRecipient extends Recipient {

	@Column(name = "iban")
	private String iban;

	public IbanRecipient() {
	}

	public IbanRecipient(User user, String iban) {
		super(user);
		this.iban = iban;
	}

	@Override
	public RecipientType getType() {
		return RecipientType.IBAN;
	}

	@Override
	public String getIban() {
		return iban;
	}
}
