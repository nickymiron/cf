package com.credifair.web.model.recipient;

import com.credifair.web.model.User;

import javax.persistence.*;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
@Entity
@Table(name = "recipient", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
public abstract class Recipient {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user")
	private User user;

	@Column(name = "type", insertable = false, updatable = false)
	@Enumerated(EnumType.STRING)
	private RecipientType type;

	public Recipient() {
	}

	public Recipient(User user) {
		this.user = user;
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public RecipientType getType() {
		return type;
	}

	public String getIban() {
		return null;
	}

	public String getSortCode() {
		return null;
	}

	public String getAccountNumber() {
		return null;
	}
}
