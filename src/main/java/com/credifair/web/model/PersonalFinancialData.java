package com.credifair.web.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
@Entity
@Table(name = "personal_financial_data", schema = "credifair")
public class PersonalFinancialData {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_borrower_user_profile")
	private BorrowerUserProfile borrowerUserProfile;

	@Column(name = "monthly_net_income")
	private BigDecimal monthlyNetIncome;

	@Column(name = "other_regular_income")
	private BigDecimal otherRegularIncome;

	@Column(name = "home_ownership")
	@Enumerated(EnumType.STRING)
	private HomeOwnership homeOwnership;

	@Column(name = "living_expenses")
	private BigDecimal livingExpenses;

	@Column(name = "will_provide_docs")
	private Boolean willProvideDocumentsOfIncomeAndExpenses;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "personalFinancialData")
	private List<Liability> liabilities;

	public PersonalFinancialData() {
	}

	public PersonalFinancialData(BorrowerUserProfile borrowerUserProfile, BigDecimal monthlyNetIncome, BigDecimal otherRegularIncome, HomeOwnership homeOwnership, Boolean willProvideDocumentsOfIncomeAndExpenses, List<Liability> liabilities) {
		this.borrowerUserProfile = borrowerUserProfile;
		this.monthlyNetIncome = monthlyNetIncome;
		this.otherRegularIncome = otherRegularIncome;
		this.homeOwnership = homeOwnership;
		this.willProvideDocumentsOfIncomeAndExpenses = willProvideDocumentsOfIncomeAndExpenses;
		this.liabilities = liabilities;
	}

	public Long getId() {
		return id;
	}

	public BorrowerUserProfile getBorrowerUserProfile() {
		return borrowerUserProfile;
	}

	public BigDecimal getMonthlyNetIncome() {
		return monthlyNetIncome;
	}

	public BigDecimal getOtherRegularIncome() {
		return otherRegularIncome;
	}

	public HomeOwnership getHomeOwnership() {
		return homeOwnership;
	}

	public Boolean getWillProvideDocumentsOfIncomeAndExpenses() {
		return willProvideDocumentsOfIncomeAndExpenses;
	}

	public List<Liability> getLiabilities() {
		return liabilities;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile) {
		this.borrowerUserProfile = borrowerUserProfile;
	}

	public void setMonthlyNetIncome(BigDecimal monthlyNetIncome) {
		this.monthlyNetIncome = monthlyNetIncome;
	}

	public void setOtherRegularIncome(BigDecimal otherRegularIncome) {
		this.otherRegularIncome = otherRegularIncome;
	}

	public void setHomeOwnership(HomeOwnership homeOwnership) {
		this.homeOwnership = homeOwnership;
	}

	public void setWillProvideDocumentsOfIncomeAndExpenses(Boolean willProvideDocumentsOfIncomeAndExpenses) {
		this.willProvideDocumentsOfIncomeAndExpenses = willProvideDocumentsOfIncomeAndExpenses;
	}

	public void setLiabilities(List<Liability> liabilities) {
		this.liabilities = liabilities;
	}

	public BigDecimal getLivingExpenses() {
		return livingExpenses;
	}

	public void setLivingExpenses(BigDecimal livingExpenses) {
		this.livingExpenses = livingExpenses;
	}
}
