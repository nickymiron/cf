package com.credifair.web.model;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by indrek.ruubel on 16/01/2016.
 */
@Entity
@Table(name = "email_subscriber", schema = "credifair")
public class EmailSubscriber {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user")
    private User user;

    @Column(name = "created")
    private Date created;

    @Column(name = "last_updated")
    private Date lastUpdated;

    public EmailSubscriber() {
    }

    public EmailSubscriber(String email, User user, Date created, Date lastUpdated) {
        this.email = email;
        this.user = user;
        this.created = created;
        this.lastUpdated = lastUpdated;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public User getUser() {
        return user;
    }

    public Date getCreated() {
        return created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }
}
