package com.credifair.web.model.investor.state;

import com.credifair.web.model.InvestorUserProfile;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 13/04/2016.
 */
@Entity
@DiscriminatorValue(value = "NEW")
public class NewInvestorState extends InvestorState {

	public NewInvestorState() {
	}

	public NewInvestorState(InvestorUserProfile investorUserProfile) {
		super(investorUserProfile);
	}

	@Override
	public String getState() {
		return "NEW";
	}
}
