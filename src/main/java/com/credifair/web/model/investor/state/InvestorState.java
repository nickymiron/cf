package com.credifair.web.model.investor.state;

import com.credifair.web.model.InvestorUserProfile;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 13/04/2016.
 */

@Entity
@Table(name = "investor_state", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STATE")
public class InvestorState {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;

	@Column(name = "date_time", columnDefinition="DATETIME(3)", nullable = false)
	public LocalDateTime dateTime;

	@Column(name = "state", insertable = false, updatable = false)
	private String state;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_investor_user_profile")
	public InvestorUserProfile investorUserProfile;

	public InvestorState() {
	}

	public InvestorState(InvestorUserProfile investorUserProfile) {
		this.dateTime = LocalDateTime.now();
		this.investorUserProfile = investorUserProfile;
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}

	public String getState() {
		return state;
	}

	public InvestorUserProfile getInvestorUserProfile() {
		return investorUserProfile;
	}
}
