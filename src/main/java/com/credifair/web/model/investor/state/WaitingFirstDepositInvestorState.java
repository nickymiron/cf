package com.credifair.web.model.investor.state;

import com.credifair.web.model.InvestorUserProfile;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 13/04/2016.
 */
@Entity
@DiscriminatorValue(value = "WAITING_FIRST_DEPOSIT")
public class WaitingFirstDepositInvestorState extends InvestorState {

	public WaitingFirstDepositInvestorState() {
	}

	public WaitingFirstDepositInvestorState(InvestorUserProfile investorUserProfile) {
		super(investorUserProfile);
	}

	@Override
	public String getState() {
		return "WAITING_FIRST_DEPOSIT";
	}
}
