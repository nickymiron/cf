package com.credifair.web.model;

import javax.persistence.*;
import java.util.List;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@Entity
@Table(name = "role", schema = "credifair")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "role")
    private String role;

    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "user_role", schema="credifair", joinColumns = {
            @JoinColumn(name = "fk_role", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "fk_user",
                    nullable = false, updatable = false) })
    private List<User> users;

    public Role() {
    }

    public Role(String role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public String getRole() {
        return role;
    }

    public List<User> getUsers() {
        return users;
    }
}
