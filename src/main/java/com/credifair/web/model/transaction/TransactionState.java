package com.credifair.web.model.transaction;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
public enum TransactionState {
	PENDING, COMPLETED, CANCELLED;
}
