package com.credifair.web.model.transaction.bank;

import com.credifair.web.model.User;
import com.credifair.web.model.transaction.DepositTransaction;
import com.credifair.web.model.transaction.Transaction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
@Entity
@Table(name = "bank_transaction_link", schema = "credifair")
public class BankTransactionLink {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user")
	private User user;

	@Column(name = "amount", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
	private BigDecimal amount;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_transaction")
	private DepositTransaction transaction;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_bank_transaction")
	BankTransaction bankTransaction;

	@Column(name = "created", columnDefinition="DATETIME(3)")
	LocalDateTime created;

	public BankTransactionLink() {
	}

	public BankTransactionLink(User user, BigDecimal amount, BankTransaction bankTransaction, DepositTransaction transaction) {
		this.user = user;
		this.amount = amount;
		this.bankTransaction = bankTransaction;
		this.transaction = transaction;
		this.created = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public DepositTransaction getTransaction() {
		return transaction;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public BankTransaction getBankTransaction() {
		return bankTransaction;
	}
}
