package com.credifair.web.model.transaction.link;

import com.credifair.web.model.transaction.Transaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@DiscriminatorValue(value = "BALANCE")
public class BalanceTransactionLink extends TransactionLink {

	public BalanceTransactionLink() {
	}

	public BalanceTransactionLink(Transaction transaction, Long referenceId) {
		super(transaction, referenceId);
	}

	@Override
	public String getType() {
		return "BALANCE";
	}
}
