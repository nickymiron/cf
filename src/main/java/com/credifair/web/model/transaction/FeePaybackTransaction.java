package com.credifair.web.model.transaction;

import com.credifair.web.model.Currency;
import com.credifair.web.model.User;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 18/04/2016.
 */
@Entity
@DiscriminatorValue(value = "FEE_PAYBACK")
public class FeePaybackTransaction extends Transaction {

	public FeePaybackTransaction() {
	}

	public FeePaybackTransaction(BigDecimal amount, Currency currency, User user, String traceHash, TransactionState state) {
		super(amount, currency, user, traceHash, state);
	}

	@Override
	public String getType() {
		return "FEE_PAYBACK";
	}
}
