package com.credifair.web.model.transaction.link;

import com.credifair.web.model.transaction.Transaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@DiscriminatorValue(value = "INVESTMENT_SNAPSHOT")
public class InvestmentSnapshotTransactionLink extends TransactionLink {

	public InvestmentSnapshotTransactionLink() {
	}

	public InvestmentSnapshotTransactionLink(Transaction transaction, Long referenceId) {
		super(transaction, referenceId);
	}

	@Override
	public String getType() {
		return "INVESTMENT_SNAPSHOT";
	}
}
