package com.credifair.web.model.transaction.bank;

import com.credifair.web.model.Currency;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
@Entity
@Table(name = "bank_transaction", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "BANK_NAME")
abstract public class BankTransaction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "bank_name", insertable = false, updatable = false)
	private String bankName;

	@Enumerated(EnumType.STRING)
	@Column(name = "currency", nullable = false)
	private Currency currency;

	@Column(name = "amount", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
	private BigDecimal amount;

	// Id from the bank side
	@Column(name = "bank_transaction_id")
	private String bankTransactionId;

	@Column(name = "reference")
	private String reference;

	@Column(name = "state")
	@Enumerated(EnumType.STRING)
	private BankTransactionState state;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private BankTransactionType type;

	@Column(name = "bankCreated", columnDefinition="DATETIME(3)")
	LocalDateTime bankCreated;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "bankTransaction")
	private List<BankTransactionLink> links;

	@Column(name = "created", columnDefinition="DATETIME(3)")
	LocalDateTime created;

	@Column(name = "last_updated", columnDefinition="DATETIME(3)")
	LocalDateTime lastUpdated;

	public BankTransaction() {
	}

	public BankTransaction(Currency currency, BigDecimal amount, String bankTransactionId, String reference, BankTransactionState state, BankTransactionType type, LocalDateTime bankCreated) {
		this.currency = currency;
		this.amount = amount;
		this.bankTransactionId = bankTransactionId;
		this.reference = reference;
		this.state = state;
		this.type = type;
		this.bankCreated = bankCreated;
		this.created = LocalDateTime.now();
		this.lastUpdated = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public String getBankName() {
		return bankName;
	}

	public Currency getCurrency() {
		return currency;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public String getBankTransactionId() {
		return bankTransactionId;
	}

	public String getReference() {
		return reference;
	}

	public BankTransactionState getState() {
		return state;
	}

	public LocalDateTime getBankCreated() {
		return bankCreated;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setState(BankTransactionState state) {
		this.state = state;
	}

	public List<BankTransactionLink> getLinks() {
		return links;
	}

	public LocalDateTime getLastUpdated() {
		return lastUpdated;
	}

	public void setLastUpdated(LocalDateTime lastUpdated) {
		this.lastUpdated = lastUpdated;
	}

	public BankTransactionType getType() {
		return type;
	}

	@Override
	public String toString() {
		return "BankTransaction{" +
				"id=" + id +
				", bankName='" + bankName + '\'' +
				", currency=" + currency +
				", amount=" + amount +
				", state=" + state +
				'}';
	}
}
