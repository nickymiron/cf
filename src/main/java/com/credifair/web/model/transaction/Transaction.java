package com.credifair.web.model.transaction;


import com.credifair.web.model.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/01/2016.
 */
@Entity
@Table(name = "transaction", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
public abstract class Transaction {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "type", insertable = false, updatable = false)
    private String type;

    @Column(name = "amount", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal amount;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Column(name = "state")
    @Enumerated(EnumType.STRING)
    private TransactionState state;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user")
    private User user;

    @Column(name = "created", columnDefinition="DATETIME(3)")
    private LocalDateTime created;

    @Column(name = "trace_hash")
    private String traceHash;

    public Transaction() {
    }

    public Transaction(BigDecimal amount, Currency currency, User user, String traceHash, TransactionState state) {
        this.amount = amount;
        this.user = user;
        this.created = LocalDateTime.now();
        this.currency = currency;
        this.traceHash = traceHash;
        this.state = state;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        return type;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public Currency getCurrency() {
        return currency;
    }

    public User getUser() {
        return user;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public String getTraceHash() {
        return traceHash;
    }

    public TransactionState getState() {
        return state;
    }

    @Override
    public String toString() {
        return "Transaction{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", amount=" + amount +
                ", currency=" + currency +
                ", userId=" + user.getId() +
                ", dateTime=" + created +
                ", traceHash='" + traceHash + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
