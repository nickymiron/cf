package com.credifair.web.model.transaction;

import com.credifair.web.model.Currency;
import com.credifair.web.model.User;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@DiscriminatorValue(value = "INVESTOR_INVEST")
public class InvestorInvestTransaction extends Transaction {

	public InvestorInvestTransaction() {
	}

	public InvestorInvestTransaction(BigDecimal amount, Currency currency, User user, String traceHash, TransactionState state) {
		super(amount, currency, user, traceHash, state);
	}

	@Override
	public String getType() {
		return "INVESTOR_INVEST";
	}
}
