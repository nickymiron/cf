package com.credifair.web.model.transaction.bank;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
public enum BankTransactionState {
	ACTIVE,
	LINKED,
	ON_HOLD,
	IGNORED
}
