package com.credifair.web.model.transaction.link;

import com.credifair.web.model.transaction.Transaction;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@DiscriminatorValue(value = "LOAN_BALANCE")
public class LoanBalanceTransactionLink extends TransactionLink {

	public LoanBalanceTransactionLink(Transaction transaction, Long referenceId) {
		super(transaction, referenceId);
	}

	public LoanBalanceTransactionLink() {
	}

	@Override
	public String getType() {
		return "LOAN_BALANCE";
	}
}
