package com.credifair.web.model.transaction;

import com.credifair.web.model.Currency;
import com.credifair.web.model.User;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@DiscriminatorValue(value = "DEPOSIT")
public class DepositTransaction extends Transaction {

	public DepositTransaction() {
	}

	public DepositTransaction(BigDecimal amount, Currency currency, User user, String traceHash, TransactionState state) {
		super(amount, currency, user, traceHash, state);
	}

	@Override
	public String getType() {
		return "DEPOSIT";
	}
}
