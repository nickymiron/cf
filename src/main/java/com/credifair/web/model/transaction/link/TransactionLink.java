package com.credifair.web.model.transaction.link;

import com.credifair.web.model.transaction.Transaction;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@Table(name = "transaction_link", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
public abstract class TransactionLink {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_transaction")
	private Transaction transaction;

	@Column(name = "type", insertable = false, updatable = false)
	private String type;

	@Column(name = "reference_id")
	private Long referenceId;

	@Column(name = "created", columnDefinition="DATETIME(3)")
	private LocalDateTime created;

	public TransactionLink() {
	}

	public TransactionLink(Transaction transaction, Long referenceId) {
		this.transaction = transaction;
		this.referenceId = referenceId;
		this.created = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public Transaction getTransaction() {
		return transaction;
	}

	public String getType() {
		return type;
	}

	public Long getReferenceId() {
		return referenceId;
	}

	public LocalDateTime getCreated() {
		return created;
	}
}
