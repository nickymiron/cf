package com.credifair.web.model.transaction.bank;

import com.credifair.web.model.Currency;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
@Entity
@DiscriminatorValue(value = "BARCLAYS")
public class BarclaysBankTransaction extends BankTransaction {

	public BarclaysBankTransaction() {
	}

	public BarclaysBankTransaction(Currency currency, BigDecimal amount, String bankTransactionId, String reference, BankTransactionState state, BankTransactionType type, LocalDateTime bankCreated) {
		super(currency, amount, bankTransactionId, reference, state, type, bankCreated);
	}

	@Override
	public String getBankName() {
		return BankEnum.BARCLAYS.toString();
	}
}
