package com.credifair.web.model.transaction.bank;

/**
 * Created by indrek.ruubel on 30/04/2016.
 */
public enum BankTransactionType {
	RECEIVE, SEND;
}
