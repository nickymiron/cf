package com.credifair.web.model.transaction.bank;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
public enum BankEnum {
	SEB, BARCLAYS;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (BankEnum obj : values()) {
			out.add(obj.name());
		}
		return out;
	}
}
