package com.credifair.web.model;


import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/01/2016.
 */
@Entity
@Table(name = "loan_balance", schema = "credifair")
public class LoanBalance {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "opening_interest", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal openingInterest;

    @Column(name = "closing_interest", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal closingInterest;

    @Column(name = "opening_principal", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal openingPrincipal;

    @Column(name = "closing_principal", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal closingPrincipal;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_borrower_user_profile", nullable = false)
    private BorrowerUserProfile borrowerUserProfile;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Column(name = "created", columnDefinition="DATETIME(3)", nullable = false)
    private LocalDateTime created;

    public BigDecimal getEndBalance() {
        return closingInterest.add(closingPrincipal);
    }

    public enum DifferenceType {
        INCREMENTED_INTEREST, INCREMENTED_PRINCIPAL,
        DECREMENTED_INTEREST, DECREMENTED_PRINCIPAL
    }

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private DifferenceType type;

    public LoanBalance() {
    }

    public LoanBalance(Currency currency, BigDecimal openingInterest, BigDecimal closingInterest, BigDecimal openingPrincipal, BigDecimal closingPrincipal, BorrowerUserProfile borrowerUserProfile, DifferenceType type) {
        this.currency = currency;
        this.openingInterest = openingInterest;
        this.closingInterest = closingInterest;
        this.openingPrincipal = openingPrincipal;
        this.closingPrincipal = closingPrincipal;
        this.borrowerUserProfile = borrowerUserProfile;
        this.created = LocalDateTime.now();
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getOpeningInterest() {
        return openingInterest;
    }

    public BigDecimal getClosingInterest() {
        return closingInterest;
    }

    public BigDecimal getOpeningPrincipal() {
        return openingPrincipal;
    }

    public BigDecimal getClosingPrincipal() {
        return closingPrincipal;
    }

    public BorrowerUserProfile getBorrowerUserProfile() {
        return borrowerUserProfile;
    }

    public Currency getCurrency() {
        return currency;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public DifferenceType getType() {
        return type;
    }
}
