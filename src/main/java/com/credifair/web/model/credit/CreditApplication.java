package com.credifair.web.model.credit;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.Currency;
import com.credifair.web.model.state.CAState;

import javax.persistence.*;
import java.util.List;

/**
 * Created by indrek.ruubel on 16/03/2016.
 */
@Entity
@Table(name = "credit_application", schema = "credifair")
public class CreditApplication {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_borrower_user_profile", nullable = false)
	private BorrowerUserProfile borrowerUserProfile;

	@Column(name = "currency")
	@Enumerated(EnumType.STRING)
	private Currency currency;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "creditApplication")
	private List<CreditGrade> creditGrades;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_credit_grade")
	private CreditGrade creditGrade;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "creditApplication")
	private List<CreditApplicationInvestor> creditApplicationInvestors;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "creditApplication")
	private List<CreditLimit> limits;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_credit_limit")
	private CreditLimit creditLimit;

	@Column(name = "grace_period_days")
	private Integer gracePeriodDays;

	// List of subscribers

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "creditApplication")
	private List<CAState> states;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_state")
	private CAState state;

	public CreditApplication() {
	}

	public CreditApplication(BorrowerUserProfile borrowerUserProfile) {
		this.borrowerUserProfile = borrowerUserProfile;
	}

	public CreditApplication(BorrowerUserProfile borrowerUserProfile, Currency currency, CreditLimit creditLimit, int gracePeriodDays) {
		this.borrowerUserProfile = borrowerUserProfile;
		this.currency = currency;
		this.creditLimit = creditLimit;
		this.gracePeriodDays = gracePeriodDays;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public CreditLimit getCreditLimit() {
		return creditLimit;
	}

	public Currency getCurrency() {
		return currency;
	}

	public CAState getState() {
		return state;
	}

	public void setState(CAState state) {
		this.state = state;
	}

	public List<CreditGrade> getCreditGrades() {
		return creditGrades;
	}

	public CreditGrade getCreditGrade() {
		return creditGrade;
	}

	public Integer getGracePeriodDays() {
		return gracePeriodDays;
	}

	public List<CAState> getStates() {
		return states;
	}

	public void setCreditGrade(CreditGrade creditGrade) {
		this.creditGrade = creditGrade;
	}

	public void setCurrency(Currency currency) {
		this.currency = currency;
	}

	public void setCreditGrades(List<CreditGrade> creditGrades) {
		this.creditGrades = creditGrades;
	}

	public void setGracePeriodDays(Integer gracePeriodDays) {
		this.gracePeriodDays = gracePeriodDays;
	}

	public void setStates(List<CAState> states) {
		this.states = states;
	}

	public void setCreditApplicationInvestors(List<CreditApplicationInvestor> creditApplicationInvestors) {
		this.creditApplicationInvestors = creditApplicationInvestors;
	}

	public List<CreditApplicationInvestor> getCreditApplicationInvestors() {
		return creditApplicationInvestors;
	}

	public BorrowerUserProfile getBorrowerUserProfile() {
		return borrowerUserProfile;
	}

	public void setBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile) {
		this.borrowerUserProfile = borrowerUserProfile;
	}

	public void setCreditLimit(CreditLimit creditLimit) {
		this.creditLimit = creditLimit;
	}
}
