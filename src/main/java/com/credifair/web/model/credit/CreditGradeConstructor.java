package com.credifair.web.model.credit;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by indrek.ruubel on 18/03/2016.
 * TODO: BigDecimal fractional addition sucks ass, figure out once you have time.
 */
public enum CreditGradeConstructor {
	AA(new BigDecimal(0.08), new BigDecimal(0.10), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.08));
		add(new BigDecimal(0.09));
		add(new BigDecimal(0.10));
	}}, 21),
	A(new BigDecimal(0.10), new BigDecimal(0.13), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.10));
		add(new BigDecimal(0.11));
		add(new BigDecimal(0.12));
		add(new BigDecimal(0.13));
	}}, 21),
	B(new BigDecimal(0.13), new BigDecimal(0.16), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.13));
		add(new BigDecimal(0.14));
		add(new BigDecimal(0.15));
		add(new BigDecimal(0.16));
	}}, 21),
	C(new BigDecimal(0.16), new BigDecimal(0.18), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.16));
		add(new BigDecimal(0.17));
		add(new BigDecimal(0.18));
	}}, 21),
	D(new BigDecimal(0.18), new BigDecimal(0.20), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.18));
		add(new BigDecimal(0.19));
		add(new BigDecimal(0.20));
	}}, 21),

	// Testing values
	TEST0(new BigDecimal(0.12), new BigDecimal(0.13), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.12));
		add(new BigDecimal(0.13));
	}}, 0),
	TEST5(new BigDecimal(0.12), new BigDecimal(0.13), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.12));
		add(new BigDecimal(0.13));
	}}, 5),
	TEST21(new BigDecimal(0.12), new BigDecimal(0.12), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.12));
	}}, 21),
	TEST30(new BigDecimal(0.12), new BigDecimal(0.12), new ArrayList<BigDecimal>(){{
		add(new BigDecimal(0.12));
	}}, 30);

	private BigDecimal aprMin;
	private BigDecimal aprMax;
	private List<BigDecimal> aprs;
	private int gracePeriod;

	CreditGradeConstructor(BigDecimal aprMin, BigDecimal aprMax, List<BigDecimal> aprs, int gracePeriod) {
		this.aprMin = aprMin;
		this.aprMax = aprMax;
		this.aprs = aprs;
		this.gracePeriod = gracePeriod;
	}

	public BigDecimal getAprMin() {
		return aprMin;
	}

	public BigDecimal getAprMax() {
		return aprMax;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public List<BigDecimal> getAprs() {
		return aprs;
	}
}
