package com.credifair.web.model.credit;

import com.credifair.web.model.User;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
@Entity
@Table(name = "credit_limit", schema = "credifair")
public class CreditLimit {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "credit_limit")
	private BigDecimal creditLimit;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_credit_application")
	private CreditApplication creditApplication;

	@Column(name = "created", columnDefinition = "DATETIME(3)")
	private LocalDateTime created;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_created_by")
	private User createdBy;

	public CreditLimit() {
	}

	public CreditLimit(BigDecimal creditLimit, CreditApplication creditApplication, User createdBy) {
		this.creditLimit = creditLimit;
		this.creditApplication = creditApplication;
		this.created = LocalDateTime.now();
		this.createdBy = createdBy;
	}

	public Long getId() {
		return id;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public CreditApplication getCreditApplication() {
		return creditApplication;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public User getCreatedBy() {
		return createdBy;
	}


}
