package com.credifair.web.model.credit;

import com.credifair.web.model.InvestorUserProfile;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 05/04/2016.
 */
@Entity
@Table(name = "credit_application_investor", schema = "credifair")
public class CreditApplicationInvestor {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_investor_profile")
	private InvestorUserProfile investorUserProfile;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_credit_application")
	private CreditApplication creditApplication;

	@Column(name = "max_amount")
	private BigDecimal maxAmount;

	@Column(name = "created", columnDefinition = "DATETIME(3)")
	private LocalDateTime created;

	public CreditApplicationInvestor() {
	}

	public CreditApplicationInvestor(InvestorUserProfile investorUserProfile, CreditApplication creditApplication, BigDecimal maxAmount, LocalDateTime created) {
		this.investorUserProfile = investorUserProfile;
		this.creditApplication = creditApplication;
		this.maxAmount = maxAmount;
		this.created = created;
	}

	public Long getId() {
		return id;
	}

	public InvestorUserProfile getInvestorUserProfile() {
		return investorUserProfile;
	}

	public CreditApplication getCreditApplication() {
		return creditApplication;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setInvestorUserProfile(InvestorUserProfile investorUserProfile) {
		this.investorUserProfile = investorUserProfile;
	}

	public void setCreditApplication(CreditApplication creditApplication) {
		this.creditApplication = creditApplication;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public BigDecimal getMaxAmount() {
		return maxAmount;
	}

	public void setMaxAmount(BigDecimal maxAmount) {
		this.maxAmount = maxAmount;
	}

	@Override
	public String toString() {
		return "CreditApplicationInvestor{" +
				"id=" + id +
				", maxAmount=" + maxAmount +
				", created=" + created +
				'}';
	}
}
