package com.credifair.web.model.credit;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 02/04/2016.
 */
@Entity
@Table(name = "credit_grade", schema = "credifair")
public class CreditGrade {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_credit_application")
	private CreditApplication creditApplication;

	@Column(name = "grade_assigned")
	@Enumerated(EnumType.STRING)
	private CreditGradeConstructor creditGradeConstructor;

	@Column(name = "apr")
	private BigDecimal apr;

	public CreditGrade() {
	}

	public CreditGrade(CreditApplication creditApplication, CreditGradeConstructor creditGradeConstructor, BigDecimal apr) {
		this.creditApplication = creditApplication;
		this.creditGradeConstructor = creditGradeConstructor;
		this.apr = apr;
	}

	public Long getId() {
		return id;
	}

	public CreditApplication getCreditApplication() {
		return creditApplication;
	}

	public CreditGradeConstructor getCreditGradeConstructor() {
		return creditGradeConstructor;
	}

	public BigDecimal getApr() {
		return apr;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setCreditApplication(CreditApplication creditApplication) {
		this.creditApplication = creditApplication;
	}

	public void setApr(BigDecimal apr) {
		this.apr = apr;
	}
}
