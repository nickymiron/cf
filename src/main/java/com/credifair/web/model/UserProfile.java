package com.credifair.web.model;

import javax.persistence.*;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalUnit;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
@Entity
@Table(name = "user_profile", schema = "credifair")
public class UserProfile {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "userProfile")
	private User user;

	@Column(name = "first_name")
	private String firstName;

	@Column(name = "last_name")
	private String lastName;

	@Column(name = "date_of_birth")
	private LocalDate dateOfBirth;

	@Column(name = "phone_number")
	private String phoneNumber;

	@Column(name = "education")
	@Enumerated(EnumType.STRING)
	private Education education;

	@Column(name = "martial_status")
	@Enumerated(EnumType.STRING)
	private MaritalStatus maritalStatus;

	@OneToOne(fetch = FetchType.LAZY, mappedBy = "userProfile")
	private Address address;

	public UserProfile() {
	}

	public UserProfile(User user, String firstName, String lastName, LocalDate dateOfBirth, String phoneNumber, Education education, MaritalStatus maritalStatus, Address address) {
		this.user = user;
		this.firstName = firstName;
		this.lastName = lastName;
		this.dateOfBirth = dateOfBirth;
		this.phoneNumber = phoneNumber;
		this.education = education;
		this.maritalStatus = maritalStatus;
		this.address = address;
	}

	public Long getId() {
		return id;
	}

	public User getUser() {
		return user;
	}

	public String getFirstName() {
		return firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public LocalDate getDateOfBirth() {
		return dateOfBirth;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public Education getEducation() {
		return education;
	}

	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public Address getAddress() {
		return address;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public void setDateOfBirth(LocalDate dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public void setEducation(Education education) {
		this.education = education;
	}

	public void setMaritalStatus(MaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public long calculateAge() {
		long between = ChronoUnit.YEARS.between(dateOfBirth, LocalDate.now());
		return between;
	}
}
