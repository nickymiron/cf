package com.credifair.web.model;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.fieldtype.*;

import java.util.ArrayList;

/**
 * Created by indrek.ruubel on 14/03/2016.
 */
public enum UserValueFieldType {
	// Step 1
	FIRST_NAME(new TextFieldFieldType()),
	LAST_NAME(new TextFieldFieldType()),
	DATE_OF_BIRTH(new DateFieldType()),
	PHONE_NUMBER(new NumberFieldType()),

	EDUCATION(new SelectFieldType(new ArrayList<String>() {{
		addAll(Education.getAll());
	}})),

	MARITAL_STATUS(new SelectFieldType(new ArrayList<String>() {{
		addAll(MaritalStatus.getAll());
	}})),

	COUNTRY(new SelectFieldType(new ArrayList<String>() {{
		addAll(Country.getAll());
	}})),
	ADDRESS(new TextFieldFieldType()),
	CITY(new TextFieldFieldType()),
	POSTAL_CODE(new NumberFieldType()),


	// Step 2
	EMPLOYMENT_STATUS(new SelectFieldType(new ArrayList<String>() {{
		addAll(EmploymentStatus.getAll());
	}})),
	WORK_INDUSTRY(new SelectFieldType(new ArrayList<String>() {{
		addAll(WorkIndustry.getAll());
	}})),
	CURRENT_JOB_TITLE(new SelectFieldType(new ArrayList<String>() {{
		add("SOFTWARE_DEVELOPER");
		add("ACCOUNTANT");
		add("JANITOR");
		add("TAXI_DRIVER");
		add("MANAGER");
	}})),
	CURRENT_EMPLOYMENT_TIME(new SelectFieldType(new ArrayList<String>() {{
		addAll(EmploymentTime.getAll());
	}})),
	TOTAL_EMPLOYMENT_TIME(new SelectFieldType(new ArrayList<String>() {{
		addAll(EmploymentTime.getAll());
	}})),
	CURRENT_EMPLOYER(new TextFieldFieldType()),


	// Step 3
	HOME_OWNERSHIP(new SelectFieldType(new ArrayList<String>() {{
		addAll(HomeOwnership.getAll());
	}})),
	MONTHLY_NET_INCOME(new NumberFieldType()),
	OTHER_REGULAR_INCOME(new NumberFieldType()),
	LIVING_EXPENSES(new NumberFieldType()),

	WILL_YOU_PROVIDE_VERIFICATION_OF_YOUR_INCOME_AND_EXPENSES(new SelectFieldType(new ArrayList<String>() {{
		add("YES");
		add("NO");
	}})),

	// Step 4
	LIABILITIES(new LiabilitiesFieldType(new ArrayList<String>(){{
		addAll(LiabilityType.getAll());
	}}, new ArrayList<String>(){{
		addAll(LiabilitySecurity.getAll());
	}}));

	private FieldTypeInterface type;

	UserValueFieldType(FieldTypeInterface type) {
		this.type = type;
	}

	public FieldTypeInterface getType() {
		return type;
	}

	public FormFieldTypeWrapper wrapIt() {
		return type.wrapField(this);
	}
}
