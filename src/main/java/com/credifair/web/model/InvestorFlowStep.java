package com.credifair.web.model;

/**
 * Created by indrek.ruubel on 11/04/2016.
 */
public enum InvestorFlowStep {
	INVESTOR_DASHBOARD, INVESTOR_NEW, DEPOSIT;
}
