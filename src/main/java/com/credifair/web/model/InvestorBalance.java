package com.credifair.web.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Entity
@Table(name = "investor_balance", schema = "credifair")
public class InvestorBalance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "currency")
	@Enumerated(EnumType.STRING)
	private Currency currency;

	@Column(name = "principal_repaid", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
	private BigDecimal principalRepaid;

	@Column(name = "interest_repaid", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
	private BigDecimal interestRepaid;

	@Column(name = "capital_deployed", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
	private BigDecimal capitalDeployed;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_investor_user_profile")
	private InvestorUserProfile investorUserProfile;

	@Column(name = "created", columnDefinition="DATETIME(3)", nullable = false)
	private LocalDateTime created;

	public InvestorBalance() {
	}

	public InvestorBalance(BigDecimal principalRepaid, BigDecimal interestRepaid, BigDecimal capitalDeployed,
						   InvestorUserProfile investorUserProfile, Currency currency, LocalDateTime created) {
		this.principalRepaid = principalRepaid;
		this.interestRepaid = interestRepaid;
		this.capitalDeployed = capitalDeployed;
		this.investorUserProfile = investorUserProfile;
		this.currency = currency;
		this.created = created;
	}

	public InvestorBalance(BigDecimal principalRepaid, BigDecimal interestRepaid, BigDecimal capitalDeployed,
						   InvestorUserProfile investorUserProfile, Currency currency) {
		this.principalRepaid = principalRepaid;
		this.interestRepaid = interestRepaid;
		this.capitalDeployed = capitalDeployed;
		this.investorUserProfile = investorUserProfile;
		this.currency = currency;
		this.created = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public BigDecimal getPrincipalRepaid() {
		return principalRepaid;
	}

	public BigDecimal getInterestRepaid() {
		return interestRepaid;
	}

	public BigDecimal getCapitalDeployed() {
		return capitalDeployed;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public InvestorUserProfile getInvestorUserProfile() {
		return investorUserProfile;
	}

	public Currency getCurrency() {
		return currency;
	}
}
