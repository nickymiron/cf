package com.credifair.web.model.fieldtype;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.UserValueFieldType;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public class DateFieldType implements FieldTypeInterface {

	@Override
	public boolean validate(String value) {
		DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
		try {
			LocalDateTime.parse(value, formatter);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public FormFieldTypeWrapper wrapField(UserValueFieldType type) {
		return new FormFieldTypeWrapper(type.name(), "DATE", null);
	}

}
