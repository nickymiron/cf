package com.credifair.web.model.fieldtype;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.UserValueFieldType;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 21/03/2016.
 */
public class NumberFieldType implements FieldTypeInterface {

	@Override
	public boolean validate(String value) {
		try {
			new BigDecimal(value);
		} catch (Exception e) {
			return false;
		}
		return true;
	}

	@Override
	public FormFieldTypeWrapper wrapField(UserValueFieldType type) {
		return new FormFieldTypeWrapper(type.name(), "NUMBER", null);
	}

}
