package com.credifair.web.model.fieldtype;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.UserValueFieldType;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public interface FieldTypeInterface {
	boolean validate(String value);
	FormFieldTypeWrapper wrapField(UserValueFieldType type);
}
