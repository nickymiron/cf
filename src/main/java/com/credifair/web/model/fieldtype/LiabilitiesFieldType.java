package com.credifair.web.model.fieldtype;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.UserValueFieldType;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 30/03/2016.
 */
public class LiabilitiesFieldType implements FieldTypeInterface {

	private List<String> types;
	private List<String> securities;

	public LiabilitiesFieldType(List<String> types, List<String> securities) {
		this.types = types;
		this.securities = securities;
	}

	@Override
	public boolean validate(String value) {
		return false;
	}

	@Override
	public FormFieldTypeWrapper wrapField(UserValueFieldType type) {
		Map<String, Object> options = new HashMap<>();
		options.put("types", types);
		options.put("securities", securities);
		return new FormFieldTypeWrapper(type.name(), "LIABILITIES", options);
	}
}
