package com.credifair.web.model.fieldtype;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.UserValueFieldType;

import java.util.List;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public class SelectFieldType implements FieldTypeInterface {

	private List<String> options;

	public SelectFieldType(List<String> options) {
		this.options = options;
	}

	@Override
	public boolean validate(String value) {
		return true;
	}

	@Override
	public FormFieldTypeWrapper wrapField(UserValueFieldType type) {
		return new FormFieldTypeWrapper(type.name(), "SELECT", options);
	}

	public List<String> getOptions() {
		return options;
	}
}
