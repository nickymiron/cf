package com.credifair.web.model.fieldtype;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.UserValueFieldType;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public class TextFieldFieldType implements FieldTypeInterface {


	@Override
	public boolean validate(String value) {
		return true;
	}

	@Override
	public FormFieldTypeWrapper wrapField(UserValueFieldType type) {
		return new FormFieldTypeWrapper(type.name(), "TEXT", null);
	}
}
