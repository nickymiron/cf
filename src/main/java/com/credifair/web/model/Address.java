package com.credifair.web.model;

import javax.persistence.*;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
@Entity
@Table(name = "address", schema = "credifair")
public class Address {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_profile")
	private UserProfile userProfile;

	@Column(name = "address")
	private String address;

	@Column(name = "city")
	private String city;

	@Column(name = "postal_code")
	private String postalCode;

	public Address() {
	}

	public Address(UserProfile userProfile, String address, String city, String postalCode) {
		this.userProfile = userProfile;
		this.address = address;
		this.city = city;
		this.postalCode = postalCode;
	}

	public Long getId() {
		return id;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public String getAddress() {
		return address;
	}

	public String getCity() {
		return city;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setUserProfile(UserProfile userProfile) {
		this.userProfile = userProfile;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}
}
