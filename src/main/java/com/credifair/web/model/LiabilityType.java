package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public enum LiabilityType {
	LOAN, LEASE, PAYDAY_LOAN, CREDIT_CARD, REVOLVING_CREDIT, OTHER_CREDIT, DEBT_COLLECTION, MORTGAGE;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (LiabilityType liabilityType : values()) {
			out.add(liabilityType.name());
		}
		return out;
	}
}
