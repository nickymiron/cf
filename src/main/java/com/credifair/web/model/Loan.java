package com.credifair.web.model;


import com.credifair.web.model.credit.CreditGradeConstructor;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/01/2016.
 */
@Entity
@Table(name = "loan", schema = "credifair")
public class Loan {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "request_datetime", columnDefinition="DATETIME(3)")
    private LocalDateTime requestDateTime;

    @Column(name = "amount", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal amount;

    @Column(name = "funded_datetime", columnDefinition="DATETIME(3)")
    private LocalDateTime fundedDateTime;

    @Column(name = "closed_datetime", columnDefinition="DATETIME(3)")
    private LocalDateTime closedDateTime;

    @Column(name = "last_calculated_interest_datetime", columnDefinition="DATETIME(3)")
    private LocalDateTime lastCalculatedInterest;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "loan")
    private List<Investment> investments;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_borrower_user_profile")
    private BorrowerUserProfile borrowerUserProfile;

    @Column(name = "grade")
    @Enumerated(EnumType.STRING)
    private CreditGradeConstructor grade;

    @Column(name = "apr", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
    private BigDecimal apr;

    public Loan() {
    }

    public Loan(BigDecimal amount, CreditGradeConstructor grade, Currency currency, BorrowerUserProfile borrowerUserProfile, BigDecimal apr) {
        this.requestDateTime = LocalDateTime.now();
        this.amount = amount;
        this.grade = grade;
        this.currency = currency;
        this.borrowerUserProfile = borrowerUserProfile;
        this.apr = apr;
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getRequestDateTime() {
        return requestDateTime;
    }

    public BigDecimal getAmount() {
        return amount;
    }

    public LocalDateTime getFundedDateTime() {
        return fundedDateTime;
    }

    public LocalDateTime getClosedDateTime() {
        return closedDateTime;
    }

    public List<Investment> getInvestments() {
        return investments;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BorrowerUserProfile getBorrowerUserProfile() {
        return borrowerUserProfile;
    }

    public BigDecimal getApr() {
        return apr;
    }

    public void setFundedDateTime(LocalDateTime fundedDateTime) {
        this.fundedDateTime = fundedDateTime;
    }

    public void setClosedDateTime(LocalDateTime closedDateTime) {
        this.closedDateTime = closedDateTime;
    }

    public LocalDateTime getLastCalculatedInterest() {
        return lastCalculatedInterest;
    }

    public void setLastCalculatedInterest(LocalDateTime lastCalculatedInterest) {
        this.lastCalculatedInterest = lastCalculatedInterest;
    }

    public CreditGradeConstructor getGrade() {
        return grade;
    }
}
