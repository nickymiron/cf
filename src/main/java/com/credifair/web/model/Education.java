package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
public enum Education {

	PRIMARY, BASIC, VOCATIONAL, SECONDARY, HIGHER;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (Education education : values()) {
			out.add(education.name());
		}
		return out;
	}

}
