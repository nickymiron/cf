package com.credifair.web.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 05/02/2016.
 */
@Entity
@Table(name = "investment_snapshot", schema = "credifair")
public class InvestmentSnapshop {

    // Let it be - Beatles
    // Have total_interest_payed
    // total_principal_payed
    // total_fee_payed
    // Type - DECREMENTED_INTEREST, DECREMENTED_PRINCIPAL, DECREMENTED_FEE

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "opening_interest", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal openingInterest;

    @Column(name = "closing_interest", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal closingInterest;

    @Column(name = "opening_principal", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal openingPrincipal;

    @Column(name = "closing_principal", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal closingPrincipal;

    @Column(name = "opening_fee", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal openingFee;

    @Column(name = "closing_fee", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal closingFee;

    @Column(name = "total_fee_payed", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal totalFeePayed;

    @Column(name = "total_interest_payed", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal totalInterestPayed;

    @Column(name = "total_principal_payed", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)", nullable = false)
    private BigDecimal totalPrincipalPayed;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_investment")
    private Investment investment;

    @Column(name = "created", columnDefinition="DATETIME(3)")
    private LocalDateTime created;

    public enum DifferenceType {
        INCREMENTED_INTEREST, INCREMENTED_PRINCIPAL, INCREMENTED_FEE,
        DECREMENTED_INTEREST, DECREMENTED_PRINCIPAL, DECREMENTED_FEE
    }

    @Column(name = "type")
    @Enumerated(EnumType.STRING)
    private DifferenceType type;

    public InvestmentSnapshop() {
    }

    public InvestmentSnapshop(BigDecimal openingInterest, BigDecimal closingInterest, BigDecimal openingPrincipal, BigDecimal closingPrincipal, BigDecimal openingFee, BigDecimal closingFee, BigDecimal totalFeePayed, BigDecimal totalInterestPayed, BigDecimal totalPrincipalPayed, Investment investment, DifferenceType type) {
        this.openingInterest = openingInterest;
        this.closingInterest = closingInterest;
        this.openingPrincipal = openingPrincipal;
        this.closingPrincipal = closingPrincipal;
        this.openingFee = openingFee;
        this.closingFee = closingFee;
        this.totalFeePayed = totalFeePayed;
        this.totalInterestPayed = totalInterestPayed;
        this.totalPrincipalPayed = totalPrincipalPayed;
        this.investment = investment;
        this.created = LocalDateTime.now();
        this.type = type;
    }

    public Long getId() {
        return id;
    }

    public BigDecimal getOpeningInterest() {
        return openingInterest;
    }

    public BigDecimal getClosingInterest() {
        return closingInterest;
    }

    public BigDecimal getOpeningPrincipal() {
        return openingPrincipal;
    }

    public BigDecimal getClosingPrincipal() {
        return closingPrincipal;
    }

    public BigDecimal getOpeningFee() {
        return openingFee;
    }

    public BigDecimal getClosingFee() {
        return closingFee;
    }

    public Investment getInvestment() {
        return investment;
    }

    public LocalDateTime getCreated() {
        return created;
    }

    public DifferenceType getType() {
        return type;
    }

    public void setType(DifferenceType type) {
        this.type = type;
    }

    public void setCreated(LocalDateTime created) {
        this.created = created;
    }

    public BigDecimal getTotalFeePayed() {
        return totalFeePayed;
    }

    public BigDecimal getTotalInterestPayed() {
        return totalInterestPayed;
    }

    public BigDecimal getTotalPrincipalPayed() {
        return totalPrincipalPayed;
    }
}
