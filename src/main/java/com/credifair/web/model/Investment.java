package com.credifair.web.model;

import com.credifair.web.model.credit.CreditGradeConstructor;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.*;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/01/2016.
 */
@Entity
@Table(name = "investment", schema = "credifair")
public class Investment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "invested_datetime", columnDefinition = "DATETIME(3)")
    private LocalDateTime investedDateTime;

    @Column(name = "closed_datetime", columnDefinition = "DATETIME(3)")
    private LocalDateTime closedDateTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_loan", nullable = false)
    private Loan loan;

    @Enumerated(EnumType.STRING)
    @Column(name = "currency", nullable = false)
    private Currency currency;

    @Column(name = "grade")
    @Enumerated(EnumType.STRING)
    private CreditGradeConstructor grade;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "investment")
    private List<InvestmentSnapshop> snapshots;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_investment_snapshot")
    private InvestmentSnapshop latestSnapshop;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_investor_user_profile")
    private InvestorUserProfile investorUserProfile;

    @Transient
    private BigDecimal ratio;

    public Investment() {
    }

    public Investment(Loan loan) {
        this.loan = loan;
    }

    public Investment(Loan loan, CreditGradeConstructor grade, Currency currency, InvestorUserProfile investorUserProfile) {
        this.loan = loan;
        this.grade = grade;
        this.currency = currency;
        this.investorUserProfile = investorUserProfile;
        this.investedDateTime = LocalDateTime.now();
    }

    public Long getId() {
        return id;
    }

    public LocalDateTime getInvestedDateTime() {
        return investedDateTime;
    }

    public LocalDateTime getClosedDateTime() {
        return closedDateTime;
    }

    public void setClosedDateTime(LocalDateTime closedDateTime) {
        this.closedDateTime = closedDateTime;
    }

    public Loan getLoan() {
        return loan;
    }

    public Currency getCurrency() {
        return currency;
    }

    public List<InvestmentSnapshop> getSnapshots() {
        return snapshots;
    }

    public InvestorUserProfile getInvestorUserProfile() {
        return investorUserProfile;
    }

    public InvestmentSnapshop getLatestSnapshop() {
        return latestSnapshop;
    }

    public void setLatestSnapshop(InvestmentSnapshop latestSnapshop) {
        this.latestSnapshop = latestSnapshop;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public void setInvestedDateTime(LocalDateTime investedDateTime) {
        this.investedDateTime = investedDateTime;
    }

    public CreditGradeConstructor getGrade() {
        return grade;
    }
}
