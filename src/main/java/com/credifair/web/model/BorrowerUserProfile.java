package com.credifair.web.model;

import com.credifair.web.model.credit.CreditApplication;

import javax.persistence.*;
import java.util.List;

/**
 * Created by indrek.ruubel on 13/01/2016.
 */
@Entity
@Table(name = "borrower_user_profile", schema = "credifair")
public class BorrowerUserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "borrowerUserProfile")
    private User user;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "borrowerUserProfile")
    private PersonalFinancialData personalFinancialData;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "borrowerUserProfile")
    private Employment employment;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "borrowerUserProfile")
    private CreditApplication creditApplication;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "borrowerUserProfile")
    private List<Loan> loans;

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public BorrowerUserProfile() {
    }

    public BorrowerUserProfile(User user) {
        this.user = user;
    }

    public PersonalFinancialData getPersonalFinancialData() {
        return personalFinancialData;
    }

    public void setPersonalFinancialData(PersonalFinancialData personalFinancialData) {
        this.personalFinancialData = personalFinancialData;
    }

    public Employment getEmployment() {
        return employment;
    }

    public void setEmployment(Employment employment) {
        this.employment = employment;
    }

    public List<Loan> getLoans() {
        return loans;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public CreditApplication getCreditApplication() {
        return creditApplication;
    }

    public void setCreditApplication(CreditApplication creditApplication) {
        this.creditApplication = creditApplication;
    }
}
