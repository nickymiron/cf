package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public enum LiabilitySecurity {
	REAL_ESTATE, UNSECURED, OTHER_COLLATERAL, PERSONAL_GUARANTEE, COMPANY_GUARANTEE, OTHER_GUARANTEE, CAR, DEPOSIT;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (LiabilitySecurity liabilitySecurity : values()) {
			out.add(liabilitySecurity.name());
		}
		return out;
	}
}
