package com.credifair.web.model;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
@Entity
@Table(name = "liability", schema = "credifair")
public class Liability {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_personal_financial_data")
	private PersonalFinancialData personalFinancialData;

	@Column(name = "type")
	@Enumerated(EnumType.STRING)
	private LiabilityType typeOfLiability;

	@Column(name = "creditor")
	private String creditor;

	@Column(name = "mo_payment")
	private BigDecimal moPayment;

	@Column(name = "outstanding")
	private BigDecimal outstanding;

	@Column(name = "security")
	@Enumerated(EnumType.STRING)
	private LiabilitySecurity security;

	public Liability() {
	}

	public Liability(PersonalFinancialData personalFinancialData, LiabilityType typeOfLiability, String creditor, BigDecimal moPayment, BigDecimal outstanding, LiabilitySecurity security) {
		this.personalFinancialData = personalFinancialData;
		this.typeOfLiability = typeOfLiability;
		this.creditor = creditor;
		this.moPayment = moPayment;
		this.outstanding = outstanding;
		this.security = security;
	}

	public Long getId() {
		return id;
	}

	public LiabilityType getTypeOfLiability() {
		return typeOfLiability;
	}

	public String getCreditor() {
		return creditor;
	}

	public BigDecimal getMoPayment() {
		return moPayment;
	}

	public BigDecimal getOutstanding() {
		return outstanding;
	}

	public LiabilitySecurity getSecurity() {
		return security;
	}

	public PersonalFinancialData getPersonalFinancialData() {
		return personalFinancialData;
	}
}
