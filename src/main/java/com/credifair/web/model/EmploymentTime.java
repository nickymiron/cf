package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 10/04/2016.
 */
public enum EmploymentTime {

	LESS_THAN_SIX_MONTHS, SIX_MONTHS_TO_YEAR, YEAR_TO_FIVE_YEARS, MORE_THAN_FIVE_YEARS;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (EmploymentTime time : values()) {
			out.add(time.name());
		}
		return out;
	}
}
