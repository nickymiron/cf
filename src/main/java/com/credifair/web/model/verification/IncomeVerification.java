package com.credifair.web.model.verification;

import com.credifair.web.model.User;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
@Entity
@DiscriminatorValue(value = "INCOME")
public class IncomeVerification extends Verification {

	public IncomeVerification() {
	}

	public IncomeVerification(User user) {
		super(user);
	}
}
