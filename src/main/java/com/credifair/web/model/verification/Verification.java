package com.credifair.web.model.verification;

import com.credifair.web.model.FileUpload;
import com.credifair.web.model.User;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by indrek.ruubel on 03/04/2016.
 */
@Entity
@Table(name = "verification", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "TYPE")
public abstract class Verification {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;

	@Column(name = "type", insertable = false, updatable = false)
	private String type;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user")
	public User user;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "verification")
	public List<FileUpload> files;

	@Column(name = "created", columnDefinition="DATETIME(3)", nullable = false)
	public LocalDateTime created;

	@Column(name = "status")
	@Enumerated(EnumType.STRING)
	public VerificationStatus verificationStatus;

	@Column(name = "verified", columnDefinition="DATETIME(3)")
	public LocalDateTime verified;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_verified_by")
	public User verifiedBy;

	public Verification() {
	}

	public Verification(User user) {
		this.user = user;
		this.created = LocalDateTime.now();
		this.verificationStatus = VerificationStatus.NOT_VERIFIED;
	}

	public Long getId() {
		return id;
	}

	public String getType() {
		return type;
	}

	public User getUser() {
		return user;
	}

	public List<FileUpload> getFiles() {
		return files;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public LocalDateTime getVerified() {
		return verified;
	}

	public void setVerified(LocalDateTime verified) {
		this.verified = verified;
	}

	public void setVerifiedBy(User verifiedBy) {
		this.verifiedBy = verifiedBy;
	}

	public VerificationStatus getVerificationStatus() {
		return verificationStatus;
	}

	public User getVerifiedBy() {
		return verifiedBy;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setType(String type) {
		this.type = type;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public void setFiles(List<FileUpload> files) {
		this.files = files;
	}

	public void setCreated(LocalDateTime created) {
		this.created = created;
	}

	public void setVerificationStatus(VerificationStatus verificationStatus) {
		this.verificationStatus = verificationStatus;
	}

	public boolean isAccepted() {
		return verificationStatus.equals(VerificationStatus.VERIFIED) ? true : false;
	}
}
