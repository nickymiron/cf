package com.credifair.web.model.verification;

import com.credifair.web.model.User;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
@Entity
@DiscriminatorValue(value = "ID")
public class IdVerification extends Verification{

	public IdVerification() {
	}

	public IdVerification(User user) {
		super(user);
	}

}
