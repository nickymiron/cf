package com.credifair.web.model.verification;

/**
 * Created by indrek.ruubel on 03/04/2016.
 */
public enum VerificationStatus {
	NOT_VERIFIED, VERIFIED;
}
