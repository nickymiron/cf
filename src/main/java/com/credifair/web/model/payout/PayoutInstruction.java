package com.credifair.web.model.payout;

import com.credifair.web.model.Country;
import com.credifair.web.model.Currency;
import com.credifair.web.model.Loan;
import com.credifair.web.model.User;
import com.credifair.web.model.recipient.Recipient;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 29/05/2016.
 */
@Entity
@Table(name = "payout_instruction", schema = "credifair")
public class PayoutInstruction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Enumerated(EnumType.STRING)
	@Column(name = "type")
	private PayoutType payoutType;

	@Enumerated(EnumType.STRING)
	@Column(name = "state")
	private PayoutState payoutState;

	@Column(name = "amount")
	private BigDecimal amount;

	@Enumerated(EnumType.STRING)
	@Column(name = "currency")
	private Currency currency;

	@Enumerated(EnumType.STRING)
	@Column(name = "country")
	private Country country;

	@Column(name = "created", columnDefinition="DATETIME(3)")
	private LocalDateTime created;

	@Column(name = "transferred", columnDefinition="DATETIME(3)")
	private LocalDateTime transferred;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_transferred_by")
	private User transferredBy;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_recipient")
	private Recipient recipient;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_loan")
	private Loan loan;

	public PayoutInstruction(PayoutType payoutType, PayoutState payoutState, BigDecimal amount, Currency currency, Country country, Recipient recipient, Loan loan) {
		this.payoutType = payoutType;
		this.payoutState = payoutState;
		this.amount = amount;
		this.currency = currency;
		this.country = country;
		this.created = LocalDateTime.now();
		this.transferred = null;
		this.recipient = recipient;
		this.loan = loan;
	}

	public PayoutInstruction() {
	}

	public Long getId() {
		return id;
	}

	public PayoutType getPayoutType() {
		return payoutType;
	}

	public PayoutState getPayoutState() {
		return payoutState;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public Currency getCurrency() {
		return currency;
	}

	public Country getCountry() {
		return country;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public LocalDateTime getTransferred() {
		return transferred;
	}

	public Recipient getRecipient() {
		return recipient;
	}

	public Loan getLoan() {
		return loan;
	}

	public void setPayoutState(PayoutState payoutState) {
		this.payoutState = payoutState;
	}

	public void setTransferred(LocalDateTime transferred) {
		this.transferred = transferred;
	}

	public void setTransferredBy(User transferredBy) {
		this.transferredBy = transferredBy;
	}
}
