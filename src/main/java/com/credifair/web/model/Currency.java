package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 16/02/2016.
 */

public enum Currency {

    EUR("Euro", "€"),
    USD("United States dollar", "$"),
    GBP("Pound sterling", "£");

    String name;
    String symbol;

    Currency(String name, String symbol) {
        this.name = name;
        this.symbol = symbol;
    }

    public String getName() {
        return name;
    }

    public String getSymbol() {
        return symbol;
    }

    public static Collection<? extends String> getAll() {
        List<String> out = new ArrayList<>();
        for (Currency obj : values()) {
            out.add(obj.name());
        }
        return out;
    }
}
