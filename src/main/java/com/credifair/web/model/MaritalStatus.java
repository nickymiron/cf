package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
public enum MaritalStatus {
	MARRIED, COHABITANT, SINGLE, DIVORCED, WIDOW;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (MaritalStatus status : values()) {
			out.add(status.name());
		}
		return out;
	}
}
