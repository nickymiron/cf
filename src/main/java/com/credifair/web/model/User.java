package com.credifair.web.model;

import com.credifair.web.model.transaction.Transaction;
import com.credifair.web.model.verification.Verification;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@Entity
@Table(name = "user", schema = "credifair")
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "email")
    private String email;

    @Column(name = "password")
    private String password;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_role", schema="credifair", joinColumns = {
            @JoinColumn(name = "fk_user", nullable = false, updatable = false) },
            inverseJoinColumns = { @JoinColumn(name = "fk_role",
                    nullable = false, updatable = false) })
    private List<Role> roles;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_investor_user_profile")
    private InvestorUserProfile investorUserProfile;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_borrower_user_profile")
    private BorrowerUserProfile borrowerUserProfile;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_user_profile")
    private UserProfile userProfile;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Verification> verifications;

    @Column(name = "country", nullable = false)
    @Enumerated(EnumType.STRING)
    private Country country;

    @Column(name = "created")
    private Date created;

    @Column(name = "last_updated")
    private Date lastUpdated;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
    private List<Transaction> transactions;

    public User() {
    }

    public User(String email, String password, List<Role> roles, Date created, Date lastUpdated) {
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.created = created;
        this.lastUpdated = lastUpdated;
    }

    public User(String email, String password, List<Role> roles, Date created, Date lastUpdated, BorrowerUserProfile borrowerUserProfile) {
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.created = created;
        this.lastUpdated = lastUpdated;
        this.borrowerUserProfile = borrowerUserProfile;
    }

    public User(String email, String password, List<Role> roles, Date created, Date lastUpdated, BorrowerUserProfile borrowerUserProfile, InvestorUserProfile investorUserProfile, UserProfile userProfile) {
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.created = created;
        this.lastUpdated = lastUpdated;
        this.borrowerUserProfile = borrowerUserProfile;
        this.investorUserProfile = investorUserProfile;
        this.userProfile = userProfile;
    }

    public User(String email, String password, List<Role> roles, Date created, Date lastUpdated, BorrowerUserProfile borrowerUserProfile, InvestorUserProfile investorUserProfile, UserProfile userProfile, Country country) {
        this.email = email;
        this.password = password;
        this.roles = roles;
        this.created = created;
        this.lastUpdated = lastUpdated;
        this.borrowerUserProfile = borrowerUserProfile;
        this.investorUserProfile = investorUserProfile;
        this.userProfile = userProfile;
        this.country = country;
    }

    public Long getId() {
        return id;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public InvestorUserProfile getInvestorUserProfile() {
        return investorUserProfile;
    }

    public BorrowerUserProfile getBorrowerUserProfile() {
        return borrowerUserProfile;
    }

    public Date getCreated() {
        return created;
    }

    public Date getLastUpdated() {
        return lastUpdated;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    public void setInvestorUserProfile(InvestorUserProfile investorUserProfile) {
        this.investorUserProfile = investorUserProfile;
    }

    public void setBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile) {
        this.borrowerUserProfile = borrowerUserProfile;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public void setLastUpdated(Date lastUpdated) {
        this.lastUpdated = lastUpdated;
    }

    public UserProfile getUserProfile() {
        return userProfile;
    }

    public void setUserProfile(UserProfile userProfile) {
        this.userProfile = userProfile;
    }

    public List<Verification> getVerifications() {
        return verifications;
    }

    public Country getCountry() {
        return country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", email='" + email + '\'' +
                ", password='" + password + '\'' +
                ", roles=" + roles +
                ", investorUserProfile=" + investorUserProfile +
                ", borrowerUserProfile=" + borrowerUserProfile +
                ", created=" + created +
                ", lastUpdated=" + lastUpdated +
                '}';
    }

    public boolean hasRole(String roleStr) {
        for (Role role : roles) {
            if (role.getRole().equals(roleStr)) {
                return true;
            }
        }
        return false;
    }
}
