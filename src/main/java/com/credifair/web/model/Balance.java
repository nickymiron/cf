package com.credifair.web.model;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 13/03/2016.
 */
@Entity
@Table(name = "balance", schema = "credifair")
public class Balance {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "created", columnDefinition="DATETIME(3)", nullable = false)
	private LocalDateTime created;

	@Column(name = "opening_balance", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
	private BigDecimal openingBalance;

	@Column(name = "closing_balance", precision = 20, scale = 8, columnDefinition="DECIMAL(20,8)")
	private BigDecimal closingBalance;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_user_profile")
	private UserProfile userProfile;

	@Enumerated(EnumType.STRING)
	@Column(name = "currency", nullable = false)
	private Currency currency;

	public Balance() {
	}

	public Balance(BigDecimal openingBalance, BigDecimal closingBalance, UserProfile userProfile, Currency currency) {
		this.created = LocalDateTime.now();
		this.openingBalance = openingBalance;
		this.closingBalance = closingBalance;
		this.userProfile = userProfile;
		this.currency = currency;
	}

	public Long getId() {
		return id;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public BigDecimal getOpeningBalance() {
		return openingBalance;
	}

	public BigDecimal getClosingBalance() {
		return closingBalance;
	}

	public UserProfile getUserProfile() {
		return userProfile;
	}

	public Currency getCurrency() {
		return currency;
	}
}
