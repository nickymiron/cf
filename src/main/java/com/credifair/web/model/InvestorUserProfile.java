package com.credifair.web.model;

import com.credifair.web.model.investor.state.InvestorState;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by indrek.ruubel on 13/01/2016.
 */
@Entity
@Table(name = "investor_user_profile", schema = "credifair")
public class InvestorUserProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "investorUserProfile")
    private User user;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "investorUserProfile")
    private List<InvestorState> states;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "fk_investor_state")
    private InvestorState state;

    @Transient
    private BigDecimal investmentHolder = BigDecimal.ZERO;

    @Transient
    private BigDecimal ratio;

    public Long getId() {
        return id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<InvestorState> getStates() {
        return states;
    }

    public InvestorState getState() {
        return state;
    }

    public void setState(InvestorState state) {
        this.state = state;
    }

    public BigDecimal getInvestmentHolder() {
        return investmentHolder;
    }

    public void setInvestmentHolder(BigDecimal investmentHolder) {
        this.investmentHolder = investmentHolder;
    }

    public BigDecimal getRatio() {
        return ratio;
    }

    public void setRatio(BigDecimal ratio) {
        this.ratio = ratio;
    }

    public InvestorUserProfile() {
    }

    public InvestorUserProfile(User user) {
        this.user = user;
    }
}
