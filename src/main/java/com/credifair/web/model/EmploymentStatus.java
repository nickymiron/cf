package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
public enum EmploymentStatus {
	FULLY_EMPLOYED, PARTIALLY_EMPLOYED, ENTREPRENEUR, RETIREE, SELF_EMPLOYED, UNEMPLOYED;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (EmploymentStatus status : values()) {
			out.add(status.name());
		}
		return out;
	}
}
