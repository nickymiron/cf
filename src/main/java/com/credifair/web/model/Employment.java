package com.credifair.web.model;

import javax.persistence.*;

/**
 * Created by indrek.ruubel on 26/03/2016.
 */
@Entity
@Table(name = "employment", schema = "credifair")
public class Employment {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@OneToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_borrower_user_profile")
	private BorrowerUserProfile borrowerUserProfile;

	@Column(name = "employment_status")
	@Enumerated(EnumType.STRING)
	private EmploymentStatus employmentStatus;

	@Column(name = "occupation_area")
	@Enumerated(EnumType.STRING)
	private WorkIndustry workIndustry;

	@Column(name = "job_title")
	private String jobTitle;

	@Column(name = "current_employment_time")
	@Enumerated(EnumType.STRING)
	private EmploymentTime currentEmploymentTime;

	@Column(name = "total_employment_time")
	@Enumerated(EnumType.STRING)
	private EmploymentTime totalEmploymentTime;

	@Column(name = "current_employer")
	private String currentEmployer;

	public Employment() {
	}

	public Employment(BorrowerUserProfile borrowerUserProfile, EmploymentStatus employmentStatus, WorkIndustry workIndustry, String jobTitle, EmploymentTime currentEmploymentTime, EmploymentTime totalEmploymentTime, String currentEmployer) {
		this.borrowerUserProfile = borrowerUserProfile;
		this.employmentStatus = employmentStatus;
		this.workIndustry = workIndustry;
		this.jobTitle = jobTitle;
		this.currentEmploymentTime = currentEmploymentTime;
		this.totalEmploymentTime = totalEmploymentTime;
		this.currentEmployer = currentEmployer;
	}

	public Long getId() {
		return id;
	}

	public BorrowerUserProfile getBorrowerUserProfile() {
		return borrowerUserProfile;
	}

	public EmploymentStatus getEmploymentStatus() {
		return employmentStatus;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public String getCurrentEmployer() {
		return currentEmployer;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public void setBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile) {
		this.borrowerUserProfile = borrowerUserProfile;
	}

	public void setEmploymentStatus(EmploymentStatus employmentStatus) {
		this.employmentStatus = employmentStatus;
	}

	public void setJobTitle(String jobTitle) {
		this.jobTitle = jobTitle;
	}

	public void setCurrentEmployer(String currentEmployer) {
		this.currentEmployer = currentEmployer;
	}

	public EmploymentTime getCurrentEmploymentTime() {
		return currentEmploymentTime;
	}

	public void setCurrentEmploymentTime(EmploymentTime currentEmploymentTime) {
		this.currentEmploymentTime = currentEmploymentTime;
	}

	public EmploymentTime getTotalEmploymentTime() {
		return totalEmploymentTime;
	}

	public void setTotalEmploymentTime(EmploymentTime totalEmploymentTime) {
		this.totalEmploymentTime = totalEmploymentTime;
	}

	public WorkIndustry getWorkIndustry() {
		return workIndustry;
	}

	public void setWorkIndustry(WorkIndustry workIndustry) {
		this.workIndustry = workIndustry;
	}
}
