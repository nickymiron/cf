package com.credifair.web.model;

/**
 * Created by indrek.ruubel on 20/02/2016.
 */
public enum PaymentType {
    PRINCIPAL, INTEREST;
}
