package com.credifair.web.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by indrek.ruubel on 10/04/2016.
 */
public enum WorkIndustry {
	FINANCE, INFORMATION_TECHNOLOGY, PRODUCTION_MANUFACTURING, CONSTRUCTION, HEALTH_CARE;

	public static Collection<? extends String> getAll() {
		List<String> out = new ArrayList<>();
		for (WorkIndustry industry : values()) {
			out.add(industry.name());
		}
		return out;
	}
}
