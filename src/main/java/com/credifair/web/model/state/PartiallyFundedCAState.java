package com.credifair.web.model.state;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 10/04/2016.
 */
@Entity
@DiscriminatorValue(value = "PARTIALLY_FUNDED")
public class PartiallyFundedCAState extends CAState {

	public PartiallyFundedCAState() {
	}

	public PartiallyFundedCAState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		super(borrowerUserProfile, LocalDateTime.now(), creditApplication);
	}

	@Override
	public String getState() {
		return "PARTIALLY_FUNDED";
	}

}
