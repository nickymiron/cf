package com.credifair.web.model.state;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Entity
@DiscriminatorValue(value = "CANCELLED")
public class CancelledCAState extends CAState {

	public CancelledCAState() {
	}

	public CancelledCAState(BorrowerUserProfile borrowerUserProfile, LocalDateTime dateTime, CreditApplication creditApplication) {
		super(borrowerUserProfile, dateTime, creditApplication);
	}

	@Override
	public String getState() {
		return "CANCELLED";
	}
}
