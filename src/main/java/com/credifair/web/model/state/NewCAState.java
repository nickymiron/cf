package com.credifair.web.model.state;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Entity
@DiscriminatorValue(value = "NEW")
public class NewCAState extends CAState {

	public NewCAState() {
	}

	public NewCAState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		super(borrowerUserProfile, LocalDateTime.now(), creditApplication);
	}

	@Override
	public String getState() {
		return "NEW";
	}
}
