package com.credifair.web.model.state.action;

import com.credifair.web.model.state.CAState;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
@Entity
@Table(name = "castate_action", schema = "credifair")
public class CAStateAction {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	private Long id;

	@Column(name = "action")
	@Enumerated(EnumType.STRING)
	private CAStateActionEnum action;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_castate")
	private CAState caState;

	@Column(name = "date_time", columnDefinition="DATETIME(3)", nullable = false)
	public LocalDateTime dateTime;

	public CAStateAction() {
	}

	public CAStateAction(CAStateActionEnum action, CAState caState) {
		this.action = action;
		this.caState = caState;
		this.dateTime = LocalDateTime.now();
	}

	public Long getId() {
		return id;
	}

	public CAStateActionEnum getAction() {
		return action;
	}

	public CAState getCaState() {
		return caState;
	}

	public LocalDateTime getDateTime() {
		return dateTime;
	}
}
