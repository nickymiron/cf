package com.credifair.web.model.state;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Entity
@DiscriminatorValue(value = "PREPARING_OFFER")
public class PreparingOfferCAState extends CAState {

	public PreparingOfferCAState() {
	}

	public PreparingOfferCAState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		super(borrowerUserProfile, LocalDateTime.now(), creditApplication);
	}

	@Override
	public String getState() {
		return "PREPARING_OFFER";
	}
}
