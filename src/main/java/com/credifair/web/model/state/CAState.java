package com.credifair.web.model.state;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.state.action.CAStateAction;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Entity
@Table(name = "ca_state", schema = "credifair")
@Inheritance(strategy = InheritanceType.SINGLE_TABLE)
@DiscriminatorColumn(name = "STATE")
public abstract class CAState {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id")
	public Long id;

	@Column(name = "date_time", columnDefinition="DATETIME(3)", nullable = false)
	public LocalDateTime dateTime;

	@Column(name = "state", insertable = false, updatable = false)
	private String state;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_credit_application")
	public CreditApplication creditApplication;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "caState")
	public List<CAStateAction> actions;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_borrower_user_profile")
	public BorrowerUserProfile borrowerUserProfile;

	public CAState() {
	}

	public CAState(BorrowerUserProfile borrowerUserProfile, LocalDateTime dateTime, CreditApplication creditApplication) {
		this.borrowerUserProfile = borrowerUserProfile;
		this.dateTime = dateTime;
		this.creditApplication = creditApplication;
	}

	public String getState() {
		return state;
	}

	public List<CAStateAction> getActions() {
		return actions;
	}
}
