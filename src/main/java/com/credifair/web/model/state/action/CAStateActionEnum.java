package com.credifair.web.model.state.action;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
public enum CAStateActionEnum {
	CLICKED_GET_STARTED, PROVIDED_PERSONAL_INFO, PROVIDED_ADDRESS, UPLOADED_ID;
}
