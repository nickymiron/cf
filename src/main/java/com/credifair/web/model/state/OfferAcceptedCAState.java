package com.credifair.web.model.state;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;

import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Entity
@DiscriminatorValue(value = "OFFER_ACCEPTED")
public class OfferAcceptedCAState extends CAState {

	public OfferAcceptedCAState() {
	}

	public OfferAcceptedCAState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		super(borrowerUserProfile, LocalDateTime.now(), creditApplication);
	}

	@Override
	public String getState() {
		return "OFFER_ACCEPTED";
	}
}
