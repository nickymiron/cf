package com.credifair.web.model;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public enum BorrowerFlowStep {

	GET_STARTED, STEP1_PERSONAL_INFO, STEP2_ADDRESS, STEP3_ID_UPLOAD, STEP4_BANK_DETAILS,
	WAITING_FOR_OFFER,

//	APPLICATION_FORM, WAITING_FOR_OFFER,
	WAITING_OFFER_ACCEPTED, ID_DOCUMENTS_FORM, ADDRESS_DOCUMENTS_FORM,
	INCOME_DOCUMENTS_FORM, WAITING_PROCESSING, AGREE_TO_TERMS, DASHBOARD;
}
