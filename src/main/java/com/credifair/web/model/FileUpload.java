package com.credifair.web.model;

import com.credifair.web.model.verification.Verification;

import javax.persistence.*;
import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 02/04/2016.
 */
@Entity
@Table(name = "file_upload", schema = "credifair")
public class FileUpload {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;

	@Column(name = "filename")
	private String filename;

	@Column(name = "original_filename")
	private String originalFilename;

	@Column(name = "content_type")
	private String contentType;

	@Column(name = "uploaded", columnDefinition="DATETIME(3)")
	private LocalDateTime uploaded;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "fk_verification")
	private Verification verification;

	public FileUpload() {
	}

	public FileUpload(String filename, String originalFilename, String contentType, Verification verification) {
		this.filename = filename;
		this.originalFilename = originalFilename;
		this.contentType = contentType;
		this.uploaded = LocalDateTime.now();
		this.verification = verification;
	}

	public Long getId() {
		return id;
	}

	public String getFilename() {
		return filename;
	}

	public LocalDateTime getUploaded() {
		return uploaded;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public String getContentType() {
		return contentType;
	}

	public Verification getVerification() {
		return verification;
	}
}
