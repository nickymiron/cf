package com.credifair.web.job;

import com.credifair.web.service.LoanBalanceService;
import com.credifair.web.service.InterestService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@Component
@Transactional
public class DailyBorrowerBalanceCalculatorTask {

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    InterestService interestService;

    /**
     * Runs at every midnight
     * Cron schema:
     * second, minute, hour, day of month, month, day(s) of week
     */
    @Scheduled(cron = "0 55 23 * * ?") //, zone = "UTC")
    public void calculateInterests() {

        System.out.println("Calculating daily interests for all these suckers");

        try {
            interestService.calculateAllInterests();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
