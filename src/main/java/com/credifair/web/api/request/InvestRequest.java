package com.credifair.web.api.request;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 06/04/2016.
 */
public class InvestRequest {

	@JsonProperty("id")
	private Long creditApplicationId;

	@JsonProperty("amount")
	private BigDecimal amount;

	public InvestRequest() {
	}

	public InvestRequest(Long creditApplicationId, BigDecimal amount) {
		this.creditApplicationId = creditApplicationId;
		this.amount = amount;
	}

	public Long getCreditApplicationId() {
		return creditApplicationId;
	}

	public BigDecimal getAmount() {
		return amount;
	}
}
