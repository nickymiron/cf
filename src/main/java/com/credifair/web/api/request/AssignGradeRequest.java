package com.credifair.web.api.request;

import com.credifair.web.model.credit.CreditGradeConstructor;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 23/03/2016.
 */
public class AssignGradeRequest {

	private Long userId;
	private CreditGradeConstructor grade;
	private BigDecimal apr;
	private Integer gracePeriod;
	private BigDecimal creditLimit;

	public AssignGradeRequest() {
	}

	public AssignGradeRequest(Long userId, CreditGradeConstructor grade, BigDecimal apr, Integer gracePeriod, BigDecimal creditLimit) {
		this.userId = userId;
		this.grade = grade;
		this.gracePeriod = gracePeriod;
		this.apr = apr;
		this.creditLimit = creditLimit;
	}

	public Long getUserId() {
		return userId;
	}

	public CreditGradeConstructor getGrade() {
		return grade;
	}

	public BigDecimal getApr() {
		return apr;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public Integer getGracePeriod() {
		return gracePeriod;
	}
}
