package com.credifair.web.api.request;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
public class BankDetailsRequest {

	private String iban;
	private String sort1;
	private String sort2;
	private String sort3;
	private String sort;
	private String accountNumber;

	public BankDetailsRequest() {
	}

	public BankDetailsRequest(String iban, String sort1, String sort2, String sort3, String sort, String accountNumber) {
		this.iban = iban;
		this.sort1 = sort1;
		this.sort2 = sort2;
		this.sort3 = sort3;
		this.sort = sort;
		this.accountNumber = accountNumber;
	}

	public String getIban() {
		return iban;
	}

	public String getSort1() {
		return sort1;
	}

	public String getSort2() {
		return sort2;
	}

	public String getSort3() {
		return sort3;
	}

	public String getSort() {
		return sort;
	}

	public String getAccountNumber() {
		return accountNumber;
	}
}
