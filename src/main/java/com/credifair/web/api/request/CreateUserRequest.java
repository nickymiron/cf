package com.credifair.web.api.request;

import com.credifair.web.model.Country;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.NotNull;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
public class CreateUserRequest {

    @NotNull
    @NotBlank
    @Email
    private String email;

    @NotNull
    @NotBlank
    private String password;

    @NotNull
    private Country country;

    public CreateUserRequest() {
    }

    public CreateUserRequest(String email, String password, Country country) {
        this.email = email;
        this.password = password;
        this.country = country;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

    public Country getCountry() {
        return country;
    }
}
