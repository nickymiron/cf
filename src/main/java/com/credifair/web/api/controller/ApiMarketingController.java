package com.credifair.web.api.controller;

import com.credifair.web.model.EmailSubscriber;
import com.credifair.web.service.EmailSubscriberService;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * Created by indrek.ruubel on 16/01/2016.
 */
@Api(
        value="marketing",
        description = "Marketing related subjects, like email subscribing",
        produces = "application/json",
        consumes = "application/json")
@RestController
@RequestMapping("/api/v1/marketing")
@PreAuthorize("hasRole('ROLE_CLIENT')")
public class ApiMarketingController {

    @Autowired
    EmailSubscriberService emailSubscriberService;

    @RequestMapping(value = "/subscribe/{email}", method = {RequestMethod.POST})
    @ApiOperation(
            value = "Subscribe email",
            notes = "Subscribes an email to the service news feed",
            response = ResponseEntity.class)
    public ResponseEntity<Object> create(@PathVariable(value = "email") String email) {
        EmailSubscriber emailSubscriber = emailSubscriberService.saveSubscriber(email, null);
        if (emailSubscriber == null) {
            return new ResponseEntity<>("Already subscribed with this email",
                    HttpStatus.BAD_REQUEST);
        } else {
            return new ResponseEntity<>(new HashMap<String, String>(){{put("content", "Subscribed email " + email);}},
                    HttpStatus.CREATED);
        }
    }

}
