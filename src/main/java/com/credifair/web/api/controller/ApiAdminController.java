package com.credifair.web.api.controller;

import com.credifair.web.api.request.AssignGradeRequest;
import com.credifair.web.api.response.*;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.service.*;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 09/03/2016.
 */
@Api(
        value="admin",
        description = "Administrative operations",
        produces = "application/json",
        consumes = "application/json")
@RestController
@RequestMapping(
        value = "/api/v1/admin")
@PreAuthorize("hasRole('ROLE_ADMIN')")
public class ApiAdminController {

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    UserService userService;

    @Autowired
    BorrowerUserProfileService borrowerUserProfileService;

    @Autowired
    VerificationService verificationService;

    @ApiOperation(
            value = "Get borrowers balances overview",
            notes = "Get borrowers balances overview",
            response = ResponseEntity.class)
    @RequestMapping(value = "/borrowers", method = {RequestMethod.GET})
    public ResponseEntity borrowers() {
        List<LoanBalanceResponse> out = new ArrayList<>();
        List<LoanBalance> balances = loanBalanceService.findAllByEndBalanceGreaterThan(BigDecimal.ZERO);
        for (LoanBalance balance : balances) {
            BorrowerUserProfile borrowerUserProfile = balance.getBorrowerUserProfile();
            User user = borrowerUserProfile.getUser();
            CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
            out.add(new LoanBalanceResponse(user.getId(), balance.getCurrency(), balance.getEndBalance(),
                    creditApplication.getGracePeriodDays(), balance.getCreated().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"))));
        }
        return new ResponseEntity<>(out, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get list of borrowers waiting for application offer",
            notes = "Get list of borrowers waiting for application offer",
            response = ResponseEntity.class)
    @RequestMapping(value = "/assignGradeList", method = {RequestMethod.GET})
    public ResponseEntity assignGradeList() {

        List<BorrowerUserProfile> borrowers = creditApplicationService.getAllWaitingOffer();
        List<InitialOfferBorrowerResponse> out = new ArrayList<>();

        for (BorrowerUserProfile borrower : borrowers) {
            User user = borrower.getUser();
            out.add(new InitialOfferBorrowerResponse(user.getId(), user.getEmail()));
        }

        return new ResponseEntity<>(out, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get user's grade",
            notes = "Get user's grade",
            response = ResponseEntity.class)
    @RequestMapping(value = "/userGrade/{userId}", method = {RequestMethod.GET})
    public ResponseEntity userGrade(@PathVariable(value = "userId") Long userId) {

        User user = userService.getUserById(userId);
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        Map<String, Object> resp = new HashMap<>();
        CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
        if (creditApplication != null) {
            CreditGrade creditGrade = creditApplication.getCreditGrade();

            if (creditGrade != null) {
                resp.put("grade", creditGrade.getCreditGradeConstructor());
                resp.put("apr", creditGrade.getApr());
                resp.put("gracePeriodDays", creditApplication.getGracePeriodDays());
                resp.put("assigned", true);
            } else {
                resp.put("assigned", false);
            }
        } else {
            resp.put("assigned", false);
        }
        return new ResponseEntity<>(resp, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get list of grades",
            notes = "Get list of grades",
            response = ResponseEntity.class)
    @RequestMapping(value = "/grades", method = {RequestMethod.GET})
    public ResponseEntity grades() {

        CreditGradeConstructor[] grades = CreditGradeConstructor.values();
        List<CreditGradeResponse> out = new ArrayList<>();

        for (CreditGradeConstructor grade : grades) {
            if (grade.name().contains("TEST")) {
                continue;
            }
            out.add(new CreditGradeResponse(grade.name(), grade.getAprMin(), grade.getAprMax(), grade.getAprs(), grade.getGracePeriod()));
        }

        return new ResponseEntity<>(out, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get all applications",
            notes = "Get all applications",
            response = ResponseEntity.class)
    @RequestMapping(value = "/applicationsList", method = {RequestMethod.GET})
    public ResponseEntity applicationsList() {

        List<CreditApplicationResponse> out = new ArrayList<>();
        Iterable<CreditApplication> allApplications = creditApplicationService.getAllApplications();
        for (CreditApplication creditApplication : allApplications) {
            BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
            User user = borrowerUserProfile.getUser();
            CreditGrade creditGrade = creditApplication.getCreditGrade();
            out.add(new CreditApplicationResponse(creditApplication.getId(), user.getId(), user.getEmail(),
                    creditGrade == null ? null : creditGrade.getCreditGradeConstructor(),
                    creditGrade == null ? null : creditGrade.getApr(), creditApplication.getCreditLimit().getCreditLimit(), creditApplication.getState().getState()));
        }

        return new ResponseEntity<>(out, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Assign grade to user",
            notes = "Assign grade to user",
            response = ResponseEntity.class)
    @RequestMapping(value = "/assignGrade", method = {RequestMethod.POST})
    public ResponseEntity assignGrade(@RequestBody AssignGradeRequest request) {

        User user = userService.getUserById(request.getUserId());
        creditApplicationService.assignGradeToCAFromRequest(user, request);
        verificationService.checkIfGradeAssignedAndIdVerifiedAndSetToWaitingForInvestors(user);

        return new ResponseEntity<>(new HashMap<String, Object>(){{put("message", "OK");}}, HttpStatus.OK);
    }

}
