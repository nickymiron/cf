package com.credifair.web.api.controller;

import com.credifair.web.api.request.InvestRequest;
import com.credifair.web.api.response.market.OnMarketCreditApplicationResponse;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplicationInvestor;
import com.credifair.web.service.*;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
@Api(
		value="investor",
		description = "Investor operations",
		produces = "application/json",
		consumes = "application/json")
@RestController
@RequestMapping(
		value = "/api/v1/investor")
@PreAuthorize("hasRole('ROLE_USER')")
public class ApiInvestorController {

	@Autowired
	CreditApplicationService creditApplicationService;

	@Autowired
	LoanService loanService;

	@Autowired
	UserService userService;

	@Autowired
	InvestorStepService investorStepService;

	@Autowired
	InvestorStatsService investorStatsService;

	@Autowired
	InvestmentService investmentService;

	@Autowired
	UserDataSaverService userDataSaverService;

	private static final String GO_TO_STEP_KEY = "goToStep";


	@ApiOperation(
			value = "Get dashboard stats",
			notes = "Get dashboard stats",
			response = ResponseEntity.class)
	@RequestMapping(value = {"/info", "/info/{userId}"}, method = {RequestMethod.GET})
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	public ResponseEntity stats(@PathVariable Optional<Long> userId) {
		User user;
		if (userId.isPresent()) {
			user = userService.getUserById(userId.get());
		} else {
			user = userService.getLoggedInUser();
		}
		Map<String, Object> data = investorStatsService.getGeneralStats(user);

		return new ResponseEntity(data, HttpStatus.OK);
	}


	@ApiOperation(
			value = "Get list of applications waiting for investors",
			notes = "Get list of applications waiting for investors",
			response = ResponseEntity.class)
	@RequestMapping(value = {"/applicationList", "/applicationList/{userId}"}, method = {RequestMethod.GET})
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	public ResponseEntity info(@PathVariable Optional<Long> userId) {
		User user = userService.getLoggedInUser();
		List<OnMarketCreditApplicationResponse> out = creditApplicationService.getOnMarketApplications(user.getCountry().getCurrency());
		return new ResponseEntity(out, HttpStatus.OK);
	}

	@ApiOperation(
			value = "Invest into creditApplication",
			notes = "Invest into creditApplication",
			response = ResponseEntity.class)
	@RequestMapping(value = "/invest", method = {RequestMethod.POST})
	@PreAuthorize("hasAnyRole('ROLE_USER')")
	public ResponseEntity invest(@RequestBody InvestRequest request) {
		CreditApplicationInvestor creditApplicationInvestor = null;
		User user = userService.getLoggedInUser();
		try {
			investmentService.reserv(request.getCreditApplicationId(), request.getAmount(), user);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity(new HashMap<String, Object>() {{
				put("message", e.getMessage());
			}}, HttpStatus.OK);
		}

		return new ResponseEntity(new HashMap<String, Object>() {{
			put("message", "OK");
		}}, HttpStatus.OK);
	}


	@ApiOperation(
			value = "Get step to go to",
			notes = "Get step to go to",
			response = ResponseEntity.class)
	@RequestMapping(value = {"/getStep", "/getStep/{userId}"}, method = {RequestMethod.GET})
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	public ResponseEntity getStep(@PathVariable Optional<Long> userId) {
		InvestorFlowStep step = null;
		if (userId.isPresent()) {
			User user = userService.getUserById(userId.get());
			step = investorStepService.getStep(user);
		} else {
			step = investorStepService.getStep();
		}
		final InvestorFlowStep finalStep = step;
		return new ResponseEntity(new HashMap<String, Object>(){{ put(GO_TO_STEP_KEY, finalStep); }}, HttpStatus.OK);
	}

	@ApiOperation(
			value = "Get needed fields to generate form",
			notes = "Get needed fields to generate form",
			response = ResponseEntity.class)
	@RequestMapping(value = {"/formCriteria", "/formCriteria/{userId}"}, method = {RequestMethod.GET})
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	public ResponseEntity getNeededStepFields(@PathVariable Optional<Long> userId) {
		if (userId.isPresent()) {
			User user = userService.getUserById(userId.get());
			InvestorFlowStep step = investorStepService.getStep(user);
			Map<String, Object> stepData = investorStepService.getStepData(user, step);
			return new ResponseEntity(new HashMap<String, Object>(){{
				put("data", stepData);
			}}, HttpStatus.OK);
		} else {
			InvestorFlowStep step = investorStepService.getStep();
			Map<String, Object> stepData = investorStepService.getStepData(step);
			return new ResponseEntity(new HashMap<String, Object>(){{
				put("data", stepData);
			}}, HttpStatus.OK);
		}
	}

//	@ApiOperation(
//			value = "Update personal details of borrower user profile",
//			notes = "Update personal details of borrower user profile",
//			response = ResponseEntity.class)
//	@RequestMapping(value = "/details", method = {RequestMethod.PUT})
//	@PreAuthorize("hasRole('ROLE_USER')")
//	public ResponseEntity updateProfile(@RequestBody(required = false) Map<UserValueFieldType, Object> request) {
//
//		User user = userService.getLoggedInUser();
//		userDataSaverService.saveInvestorData(user, request);
//
//		InvestorFlowStep step = investorStepService.getStep();
//
//		return new ResponseEntity(new HashMap<String, Object>(){{put(GO_TO_STEP_KEY, step);}}, HttpStatus.OK);
//	}

	@ApiOperation(
			value = "Get investments",
			notes = "Get investments",
			response = ResponseEntity.class)
	@RequestMapping(value = {"/investments", "/investments/{userId}"}, method = {RequestMethod.GET})
	@PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
	public ResponseEntity investments(@PathVariable Optional<Long> userId) {
		User user;
		if (userId.isPresent()) {
			user = userService.getUserById(userId.get());
		} else {
			user = userService.getLoggedInUser();
		}
		List<Map<String, Object>> data = investorStatsService.getInvestments(user.getInvestorUserProfile());
		return new ResponseEntity(data, HttpStatus.OK);
	}

}
