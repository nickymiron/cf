package com.credifair.web.api.controller;

import com.credifair.web.api.request.BankDetailsRequest;
import com.credifair.web.api.request.CreateUserRequest;
import com.credifair.web.dao.FileUploadDao;
import com.credifair.web.dao.VerificationDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.state.action.CAStateActionEnum;
import com.credifair.web.model.transaction.Transaction;
import com.credifair.web.model.verification.Verification;
import com.credifair.web.service.*;
import com.wordnik.swagger.annotations.Api;
import com.wordnik.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.FileCopyUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.*;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@Api(
    value="customer",
    description = "Create an account, or list the available accounts for this user",
    produces = "application/json",
    consumes = "application/json")
@RestController
@RequestMapping(
        value = "/api/v1/customer")
@PreAuthorize("hasRole('ROLE_CLIENT')")
public class ApiCustomerController {

    @Autowired
    UserService userService;

    @Autowired
    LoanService loanService;

    @Autowired
    InvestorUserProfileService investorUserProfileService;

    @Autowired
    InterestService interestService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    VerificationService verificationService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    BorrowerStepService borrowerStepService;

    @Autowired
    LiabilityService liabilityService;

    @Autowired
    RecipientService recipientService;

    @Autowired
    CAStateService CAStateService;

    @Autowired
    UserDataSaverService userDataSaverService;

    @Autowired
    BorrowerUserProfileService borrowerUserProfileService;

    @Value(value = "${upload.folder}")
    String uploadFolder;

    @Autowired
    FileUploadDao fileUploadDao;

    @Autowired
    BalanceService balanceService;

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    VerificationDao verificationDao;

    private static final String GO_TO_STEP_KEY = "goToStep";

    @RequestMapping(value = "/", method = {RequestMethod.POST})
    @ApiOperation(
            value = "Create an account",
            notes = "Create an account with CrediFair",
            response = ResponseEntity.class)
    public ResponseEntity create(@RequestBody @Valid CreateUserRequest createUserRequest) {
        User user = userService.saveCustomer(createUserRequest.getEmail(), createUserRequest.getPassword(), createUserRequest.getCountry());
        if (user == null) {
            return new ResponseEntity<>(new HashMap<String, String>(){{ put("error", "There's a user already like that");}}, HttpStatus.CREATED);
        } else {
            return new ResponseEntity<>(new HashMap<String, String>(){{ put("greeting", "Created user " + user.getEmail());}}, HttpStatus.CREATED);
        }
    }

    @ApiOperation(
            value = "Get info about borrower account",
            notes = "Get info about borrower account",
            response = ResponseEntity.class)
    @RequestMapping(value = {"/info", "/info/{userId}"}, method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ResponseEntity info(@PathVariable Optional<Long> userId) {
        if (userId.isPresent()) {
            User user = userService.getUserById(userId.get());
            return new ResponseEntity(userService.getCustomerInfo(user), HttpStatus.OK);
        } else {
            return new ResponseEntity(userService.getCustomerInfo(), HttpStatus.OK);
        }
    }

    @ApiOperation(
            value = "Start application process",
            notes = "Start application process",
            response = ResponseEntity.class)
    @RequestMapping(value = "/startApplication", method = {RequestMethod.POST})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity startApplication() {
        User user = userService.getLoggedInUser();
        borrowerStepService.startApplicationProcess(user);
        BorrowerFlowStep step = borrowerStepService.getStep(user);
        return new ResponseEntity(new HashMap<String, Object>(){{ put(GO_TO_STEP_KEY, step); }}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get step to go to",
            notes = "Get step to go to",
            response = ResponseEntity.class)
    @RequestMapping(value = {"/getStep", "/getStep/{userId}"}, method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ResponseEntity getStep(@PathVariable Optional<Long> userId) {
        User user;
        if (userId.isPresent()) {
            user = userService.getUserById(userId.get());
        } else {
            user = userService.getLoggedInUser();
        }
        final BorrowerFlowStep finalStep = borrowerStepService.getStep(user);
        return new ResponseEntity(new HashMap<String, Object>(){{ put(GO_TO_STEP_KEY, finalStep); }}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get needed information about the step",
            notes = "Get needed information about the step",
            response = ResponseEntity.class)
    @RequestMapping(value = {"/stepData", "/stepData/{userId}"}, method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ResponseEntity getStepData(@PathVariable Optional<Long> userId) {
        User user;
        if (userId.isPresent()) {
            user = userService.getUserById(userId.get());
        } else {
            user = userService.getLoggedInUser();
        }
        BorrowerFlowStep step = borrowerStepService.getStep(user);
        Map<String, Object> stepData = borrowerStepService.getStepData(step);
        return new ResponseEntity(stepData, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Get transactions list",
            notes = "Get transactions list",
            response = ResponseEntity.class)
    @RequestMapping(value = {"/transactions", "/transactions/{userId}"}, method = {RequestMethod.GET})
    @PreAuthorize("hasAnyRole('ROLE_USER', 'ROLE_ADMIN')")
    public ResponseEntity transactions(@PathVariable Optional<Long> userId) {
        User user = null;
        if (userId.isPresent()) {
            user = userService.getUserById(userId.get());
        } else {
            user = userService.getLoggedInUser();
        }

        Iterable<Transaction> transactions = transactionService.findAllByUserAndTypeInOrderByCreatedDesc(user,
                new ArrayList<String>() {{
            add("BORROWER_BORROW");
            add("BORROWER_INTEREST_PAYBACK");
            add("BORROWER_PRINCIPAL_PAYBACK");
        }});

        List<HashMap<String, Object>> out = new ArrayList<>();

        for (Transaction transaction : transactions) {
            HashMap<String, Object> item = new HashMap<>();
            String minusPlus = "+";
            if (transaction.getType().equals("BORROWER_INTEREST_PAYBACK")) {
                minusPlus = "-";
            } else if (transaction.getType().equals("BORROWER_PRINCIPAL_PAYBACK")) {
                minusPlus = "-";
            }
            String type = transaction.getType();
            type = type.replace("BORROWER_", "");
            type = type.replace("_", " ");
            type = type.substring(0, 1).toUpperCase() + (type.substring(1, type.length())).toLowerCase();

            item.put("amountText", String.format("%s %s%s", minusPlus,
                    transaction.getCurrency().getSymbol(), transaction.getAmount().setScale(2, RoundingMode.HALF_UP)));
            item.put("type", type);

            DateTimeFormatter formatter = DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM);
            item.put("dateTimeFormatted", formatter.format(transaction.getCreated()));

            out.add(item);
        }
        return new ResponseEntity(new HashMap<String, Object>(){{ put("transactions", out); }}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Update personal details of borrower user profile",
            notes = "Update personal details of borrower user profile",
            response = ResponseEntity.class)
    @RequestMapping(value = "/details", method = {RequestMethod.PUT})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity updateProfile(@RequestBody(required = false) Map<UserValueFieldType, Object> request) {
        User user = userService.getLoggedInUser();
        try {
            userDataSaverService.saveBorrowerData(user, request);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
        }

        BorrowerFlowStep step = borrowerStepService.getStep(user);

        return new ResponseEntity(new HashMap<String, Object>(){{put(GO_TO_STEP_KEY, step);}}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Request loan",
            notes = "Request loan",
            response = ResponseEntity.class)
    @RequestMapping(value = "/requestLoan/{amount}", method = {RequestMethod.POST})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity requestLoan(@PathVariable(value = "amount") BigDecimal amount) {

        User user = userService.getLoggedInUser();

        boolean canRequestLoan = verificationService.canRequestLoan(user);
        if (canRequestLoan) {
            Loan loanRequest = null;
            try {
                loanRequest = loanService.requestAndFundLoan(amount, user.getBorrowerUserProfile());
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
            }
            final Loan finalLoanRequest = loanRequest;
            return new ResponseEntity(new HashMap<String, String>(){{
                put("message", String.format("Money (%s%s) is on it's way to your bank account!", finalLoanRequest.getAmount(), finalLoanRequest.getCurrency().getSymbol()));
                put("loanId", finalLoanRequest.getId().toString());
            }}, HttpStatus.OK);
        }

        return new ResponseEntity(new HashMap<String, String>(){{
            put("message", "Not allowed to request loans");
        }}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Calculate interests",
            notes = "Calculate interests",
            response = ResponseEntity.class)
    @RequestMapping(value = "/calculateInterest", method = {RequestMethod.POST})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity calculateInterest() {
        try {
            interestService.calculateAllInterests();
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new HashMap<String, String>(){{put("message", e.getMessage());}}, HttpStatus.INTERNAL_SERVER_ERROR);
        }

        return new ResponseEntity(new HashMap<String, String>(){{
            put("message", "Calculated interest");}}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Complete id docs upload step",
            notes = "Complete id docs upload step",
            response = ResponseEntity.class)
    @RequestMapping(value = "/idDocs", method = {RequestMethod.POST})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity idDocs() {

        User user = userService.getLoggedInUser();
        CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
        creditApplicationService.createCAStateAction(creditApplication.getState(), CAStateActionEnum.UPLOADED_ID);

        BorrowerFlowStep step = borrowerStepService.getStep(user);

        return new ResponseEntity(new HashMap<String, Object>(){{
            put(GO_TO_STEP_KEY, step);}}, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Send bank details",
            notes = "Send bank details",
            response = ResponseEntity.class)
    @RequestMapping(value = "/postBankDetails", method = {RequestMethod.POST})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity bankDetails(@RequestBody BankDetailsRequest createRequest) {

        User user = userService.getLoggedInUser();
        if (!recipientService.hasRecipientAccount(user)) {
            try {
                recipientService.saveRecipientFromRequest(createRequest, user);
            } catch (Exception e) {
                e.printStackTrace();
                return new ResponseEntity(e.getMessage(), HttpStatus.BAD_REQUEST);
            }
        }
        borrowerStepService.setToPreparingOffer(user);
        BorrowerFlowStep step = borrowerStepService.getStep(user);

        return new ResponseEntity(new HashMap<String, Object>(){{
            put(GO_TO_STEP_KEY, step);}}, HttpStatus.OK);
    }

//    @ApiOperation(
//            value = "Complete id docs upload step",
//            notes = "Complete id docs upload step",
//            response = ResponseEntity.class)
//    @RequestMapping(value = "/proofAddressDocs", method = {RequestMethod.POST})
//    @PreAuthorize("hasRole('ROLE_USER')")
//    public ResponseEntity proofAddressDocs() {
//
//        User user = userService.getLoggedInUser();
//
//        CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
//        CAState state = creditApplication.getState();
//        CAStateService.setAddressDocsSent(state);
//
//        BorrowerFlowStep step = borrowerStepService.getStep(user);
//
//        return new ResponseEntity(new HashMap<String, Object>(){{
//            put(GO_TO_STEP_KEY, step);}}, HttpStatus.OK);
//    }

//    @ApiOperation(
//            value = "Complete income docs upload step",
//            notes = "Complete income docs upload step",
//            response = ResponseEntity.class)
//    @RequestMapping(value = "/incomeDocs", method = {RequestMethod.POST})
//    @PreAuthorize("hasRole('ROLE_USER')")
//    public ResponseEntity incomeDocs() {
//
//        User user = userService.getLoggedInUser();
//
//        CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
//        CAState state = creditApplication.getState();
//        CAStateService.setIncomeDocsSent(state);
//        borrowerStepService.setToProcessing(user);
//
//        BorrowerFlowStep step = borrowerStepService.getStep(user);
//
//        return new ResponseEntity(new HashMap<String, Object>(){{
//            put(GO_TO_STEP_KEY, step);}}, HttpStatus.OK);
//    }


    @RequestMapping(method = RequestMethod.POST, value = "/upload")
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity handleFileUpload(@RequestParam("file") MultipartFile file) {

        User user = userService.getLoggedInUser();
        BorrowerFlowStep step = borrowerStepService.getStep(user);

        if (step.equals(BorrowerFlowStep.STEP3_ID_UPLOAD)) {

            String originalFilename = file.getOriginalFilename();
            String contentType = file.getContentType();

            Long userId = user.getId();
            String randomHash = UUID.randomUUID().toString();

            String filename = "user_id_" + userId + "_" + randomHash;

            if (!file.isEmpty()) {
                try {
                    BufferedOutputStream stream = new BufferedOutputStream(
                            new FileOutputStream(new File(uploadFolder + filename)));
                    FileCopyUtils.copy(file.getInputStream(), stream);
                    stream.close();
                }
                catch (Exception e) {
                    return new ResponseEntity(new HashMap<String, String>(){{put("error", "Failed uploading file");}}, HttpStatus.INTERNAL_SERVER_ERROR);
                }
            }
            else {
                return new ResponseEntity(new HashMap<String, String>(){{put("error", "File was empty, what are you trying to pull here?");}}, HttpStatus.BAD_REQUEST);
            }

            Verification verification = null;

            if (step.equals(BorrowerFlowStep.STEP3_ID_UPLOAD)) {
                verification = verificationService.findOrCreateByUserAndType(user, "ID");
            }

            FileUpload fileEntity = new FileUpload(filename, originalFilename, contentType, verification);
            fileUploadDao.save(fileEntity);

            return new ResponseEntity(new HashMap<String, String>(){{put("message", "OK");}}, HttpStatus.OK);
        } else {
            return new ResponseEntity(new HashMap<String, String>(){{put("message", "Not the right step");}}, HttpStatus.UNAUTHORIZED);
        }
    }

    @ApiOperation(
            value = "Payoff",
            notes = "Payoff",
            response = ResponseEntity.class)
    @RequestMapping(value = "/payOff/{amount}", method = {RequestMethod.POST})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity payOff(@PathVariable(value = "amount") BigDecimal amount) {
        User user = userService.getLoggedInUser();
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        try {
            loanBalanceService.payoff(amount, borrowerUserProfile);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity(new HashMap<String, Object>(){{ put("message", e.getMessage()); }}, HttpStatus.OK);
        }
        return new ResponseEntity(new HashMap<String, Object>(){{ put("message", "OK"); }}, HttpStatus.OK);
    }


    @ApiOperation(
            value = "Payoff details",
            notes = "Payoff details",
            response = ResponseEntity.class)
    @RequestMapping(value = "/payOffDetails", method = {RequestMethod.GET})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity payOffDetails() {
        User user = userService.getLoggedInUser();

        HashMap<String, Object> payoffDetails = loanBalanceService.getPayoffDetails(user);

        return new ResponseEntity(payoffDetails, HttpStatus.OK);
    }

    @ApiOperation(
            value = "Payoff schedule",
            notes = "Payoff schedule",
            response = ResponseEntity.class)
    @RequestMapping(value = "/payOffSchedule", method = {RequestMethod.GET})
    @PreAuthorize("hasRole('ROLE_USER')")
    public ResponseEntity payOffSchedule() {
        User user = userService.getLoggedInUser();

        List<HashMap<String, Object>> payoffDetails = loanBalanceService.getPayoffSchedule(user);

        return new ResponseEntity(new HashMap<String, Object>(){{ put("payments", payoffDetails); }}, HttpStatus.OK);
    }


}
