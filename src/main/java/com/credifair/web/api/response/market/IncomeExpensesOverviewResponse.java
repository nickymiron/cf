package com.credifair.web.api.response.market;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by indrek.ruubel on 09/04/2016.
 */
public class IncomeExpensesOverviewResponse {

	private BigDecimal netIncome;
	private BigDecimal otherIncome;
	private BigDecimal monthlyNetIncome;
	private BigDecimal loanLiabilitiesWithoutMortgage;
	private BigDecimal mortgage;
	private BigDecimal livingExpenses;
	private BigDecimal newCardMaximumMonthlyPayment;
	private BigDecimal otherLiabilities;
	private boolean dataVerified;
	private BigDecimal discretionaryIncome;
	private BigDecimal debtToIncomeRatio;

	private List<LiabilityResponse> liabilities;

	public IncomeExpensesOverviewResponse() {
	}

	public IncomeExpensesOverviewResponse(BigDecimal netIncome, BigDecimal otherIncome, BigDecimal monthlyNetIncome,
										  BigDecimal loanLiabilitiesWithoutMortgage, BigDecimal mortgage, BigDecimal livingExpenses, BigDecimal newCardMaximumMonthlyPayment,
										  BigDecimal otherLiabilities, boolean dataVerified, BigDecimal discretionaryIncome,
										  BigDecimal debtToIncomeRatio, List<LiabilityResponse> liabilities) {
		this.netIncome = netIncome;
		this.otherIncome = otherIncome;
		this.monthlyNetIncome = monthlyNetIncome;
		this.loanLiabilitiesWithoutMortgage = loanLiabilitiesWithoutMortgage;
		this.mortgage = mortgage;
		this.livingExpenses = livingExpenses;
		this.newCardMaximumMonthlyPayment = newCardMaximumMonthlyPayment;
		this.otherLiabilities = otherLiabilities;
		this.dataVerified = dataVerified;
		this.discretionaryIncome = discretionaryIncome;
		this.debtToIncomeRatio = debtToIncomeRatio;
		this.liabilities = liabilities;
	}

	public BigDecimal getNetIncome() {
		return netIncome;
	}

	public BigDecimal getOtherIncome() {
		return otherIncome;
	}

	public BigDecimal getMonthlyNetIncome() {
		return monthlyNetIncome;
	}

	public BigDecimal getLoanLiabilitiesWithoutMortgage() {
		return loanLiabilitiesWithoutMortgage;
	}

	public BigDecimal getMortgage() {
		return mortgage;
	}

	public BigDecimal getNewCardMaximumMonthlyPayment() {
		return newCardMaximumMonthlyPayment;
	}

	public BigDecimal getOtherLiabilities() {
		return otherLiabilities;
	}

	public boolean isDataVerified() {
		return dataVerified;
	}

	public BigDecimal getDiscretionaryIncome() {
		return discretionaryIncome;
	}

	public BigDecimal getDebtToIncomeRatio() {
		return debtToIncomeRatio;
	}

	public List<LiabilityResponse> getLiabilities() {
		return liabilities;
	}

	public BigDecimal getLivingExpenses() {
		return livingExpenses;
	}
}
