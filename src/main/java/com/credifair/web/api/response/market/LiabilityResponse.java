package com.credifair.web.api.response.market;

import com.credifair.web.model.LiabilitySecurity;
import com.credifair.web.model.LiabilityType;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 09/04/2016.
 */
public class LiabilityResponse {

	private LiabilityType type;
	private String creditor;
	private BigDecimal moPayment;
	private BigDecimal outstanding;
	private LiabilitySecurity security;

	public LiabilityResponse() {
	}

	public LiabilityResponse(LiabilityType type, String creditor, BigDecimal moPayment, BigDecimal outstanding, LiabilitySecurity security) {
		this.type = type;
		this.creditor = creditor;
		this.moPayment = moPayment;
		this.outstanding = outstanding;
		this.security = security;
	}

	public LiabilityType getType() {
		return type;
	}

	public String getCreditor() {
		return creditor;
	}

	public BigDecimal getMoPayment() {
		return moPayment;
	}

	public BigDecimal getOutstanding() {
		return outstanding;
	}

	public LiabilitySecurity getSecurity() {
		return security;
	}
}
