package com.credifair.web.api.response;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
public class FormFieldTypeWrapper {

	private String fieldName;
	private String type;
	private Object options;

	public FormFieldTypeWrapper(String fieldName, String type, Object options) {
		this.fieldName = fieldName;
		this.type = type;
		this.options = options;
	}

	public String getFieldName() {
		return fieldName;
	}

	public String getType() {
		return type;
	}

	public Object getOptions() {
		return options;
	}
}
