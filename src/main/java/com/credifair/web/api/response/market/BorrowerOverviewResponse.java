package com.credifair.web.api.response.market;

import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditGradeConstructor;

/**
 * Created by indrek.ruubel on 09/04/2016.
 */
public class BorrowerOverviewResponse {

	private CreditGradeConstructor grade;
	private Long age;
	private String country;
	private String city;
	private String jobTitle;
	private WorkIndustry workIndustry;
	private EmploymentTime currentEmploymentTime;
	private EmploymentTime totalEmploymentTime;
	private Education education;
	private MaritalStatus maritalStatus;
	private HomeOwnership homeOwnership;

	public BorrowerOverviewResponse() {
	}

	public BorrowerOverviewResponse(CreditGradeConstructor grade, Long age, String country, String city, String jobTitle, WorkIndustry workIndustry, EmploymentTime currentEmploymentTime, EmploymentTime totalEmploymentTime, Education education, MaritalStatus maritalStatus, HomeOwnership homeOwnership) {
		this.grade = grade;
		this.age = age;
		this.country = country;
		this.city = city;
		this.jobTitle = jobTitle;
		this.workIndustry = workIndustry;
		this.currentEmploymentTime = currentEmploymentTime;
		this.totalEmploymentTime = totalEmploymentTime;
		this.education = education;
		this.maritalStatus = maritalStatus;
		this.homeOwnership = homeOwnership;
	}

	public CreditGradeConstructor getGrade() {
		return grade;
	}

	public Long getAge() {
		return age;
	}

	public String getJobTitle() {
		return jobTitle;
	}

	public Education getEducation() {
		return education;
	}

	public MaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public HomeOwnership getHomeOwnership() {
		return homeOwnership;
	}

	public String getCountry() {
		return country;
	}

	public String getCity() {
		return city;
	}

	public WorkIndustry getWorkIndustry() {
		return workIndustry;
	}

	public EmploymentTime getCurrentEmploymentTime() {
		return currentEmploymentTime;
	}

	public EmploymentTime getTotalEmploymentTime() {
		return totalEmploymentTime;
	}
}
