package com.credifair.web.api.response.market;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 26/04/2016.
 */
public class BorrowerHistoryResponse {

	private BigDecimal overdue;
	private BigDecimal repaidPrincipal;
	private BigDecimal paidInterest;
	private BigDecimal amountBorrowed;
	private Integer repaimentsTotal;

	public BorrowerHistoryResponse() {
	}

	public BorrowerHistoryResponse(BigDecimal overdue, BigDecimal repaidPrincipal, BigDecimal paidInterest, BigDecimal amountBorrowed, Integer repaimentsTotal) {
		this.overdue = overdue;
		this.repaidPrincipal = repaidPrincipal;
		this.paidInterest = paidInterest;
		this.amountBorrowed = amountBorrowed;
		this.repaimentsTotal = repaimentsTotal;
	}

	public BigDecimal getOverdue() {
		return overdue;
	}

	public BigDecimal getRepaidPrincipal() {
		return repaidPrincipal;
	}

	public BigDecimal getPaidInterest() {
		return paidInterest;
	}

	public BigDecimal getAmountBorrowed() {
		return amountBorrowed;
	}

	public Integer getRepaimentsTotal() {
		return repaimentsTotal;
	}
}
