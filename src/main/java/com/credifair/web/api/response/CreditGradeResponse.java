package com.credifair.web.api.response;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by indrek.ruubel on 23/03/2016.
 */
public class CreditGradeResponse {

	private String name;
	private BigDecimal aprMin;
	private BigDecimal aprMax;
	private List<BigDecimal> aprs;
	private int gracePeriod;

	public CreditGradeResponse() {
	}

	public CreditGradeResponse(String name, BigDecimal aprMin, BigDecimal aprMax, List<BigDecimal> inaprs, int gracePeriod) {
		this.name = name;
		this.aprMin = aprMin;
		this.aprMax = aprMax;
		this.aprs = inaprs;
		this.gracePeriod = gracePeriod;
	}

	public String getName() {
		return name;
	}

	public BigDecimal getAprMin() {
		return aprMin;
	}

	public BigDecimal getAprMax() {
		return aprMax;
	}

	public int getGracePeriod() {
		return gracePeriod;
	}

	public List<BigDecimal> getAprs() {
		return aprs;
	}
}
