package com.credifair.web.api.response;

import com.credifair.web.model.Currency;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 06/03/2016.
 */
public class LoanBalanceResponse {

    private Long borrowerId;
    private Currency currency;
    private BigDecimal balance;
    private Integer interestFreeDays;
    private String lastCalculated;

    public LoanBalanceResponse() {
    }

    public LoanBalanceResponse(Long borrowerId, Currency currency, BigDecimal balance, Integer interestFreeDays, String lastCalculated) {
        this.borrowerId = borrowerId;
        this.currency = currency;
        this.balance = balance;
        this.interestFreeDays = interestFreeDays;
        this.lastCalculated = lastCalculated;
    }

    public Long getBorrowerId() {
        return borrowerId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }

    public Integer getInterestFreeDays() {
        return interestFreeDays;
    }

    public String getLastCalculated() {
        return lastCalculated;
    }
}
