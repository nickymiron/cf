package com.credifair.web.api.response;

import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 03/04/2016.
 */
public class VerificationFileResponse {

	private Long id;
	private String filename;
	private String originalFilename;
	private String contentType;
	private LocalDateTime created;

	public VerificationFileResponse() {
	}

	public VerificationFileResponse(Long id, String filename, String originalFilename, String contentType, LocalDateTime created) {
		this.id = id;
		this.filename = filename;
		this.originalFilename = originalFilename;
		this.contentType = contentType;
		this.created = created;
	}

	public Long getId() {
		return id;
	}

	public String getFilename() {
		return filename;
	}

	public LocalDateTime getCreated() {
		return created;
	}

	public String getOriginalFilename() {
		return originalFilename;
	}

	public String getContentType() {
		return contentType;
	}
}
