package com.credifair.web.api.response;

/**
 * Created by indrek.ruubel on 23/03/2016.
 */
public class InitialOfferBorrowerResponse {

	private Long userId;
	private String userEmail;

	public InitialOfferBorrowerResponse() {
	}

	public InitialOfferBorrowerResponse(Long userId, String userEmail) {
		this.userId = userId;
		this.userEmail = userEmail;
	}

	public Long getUserId() {
		return userId;
	}

	public String getUserEmail() {
		return userEmail;
	}
}
