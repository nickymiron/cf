package com.credifair.web.api.response;

import com.credifair.web.model.Currency;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 29/02/2016.
 */
public class BalanceResponse {

    private String dateTime;
    private Long investorId;
    private Currency currency;
    private BigDecimal balance;

    public BalanceResponse() {
    }

    public BalanceResponse(String dateTime, Long investorId, Currency currency, BigDecimal balance) {
        this.dateTime = dateTime;
        this.investorId = investorId;
        this.currency = currency;
        this.balance = balance;
    }

    public String getDateTime() {
        return dateTime;
    }

    public Long getInvestorId() {
        return investorId;
    }

    public Currency getCurrency() {
        return currency;
    }

    public BigDecimal getBalance() {
        return balance;
    }
}
