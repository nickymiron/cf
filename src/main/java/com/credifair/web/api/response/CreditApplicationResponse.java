package com.credifair.web.api.response;

import com.credifair.web.model.credit.CreditGradeConstructor;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 23/03/2016.
 */
public class CreditApplicationResponse {

	private Long id;
	private Long userId;
	private String userEmail;
	private CreditGradeConstructor grade;
	private BigDecimal apr;
	private String status;
	private BigDecimal creditLimit;

	public CreditApplicationResponse() {
	}

	public CreditApplicationResponse(Long id, Long userId, String userEmail, CreditGradeConstructor grade, BigDecimal apr, BigDecimal creditLimit, String status) {
		this.id = id;
		this.userId = userId;
		this.userEmail = userEmail;
		this.grade = grade;
		this.apr = apr;
		this.creditLimit = creditLimit;
		this.status = status;
	}

	public Long getUserId() {
		return userId;
	}

	public String getUserEmail() {
		return userEmail;
	}

	public CreditGradeConstructor getGrade() {
		return grade;
	}

	public BigDecimal getApr() {
		return apr;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public Long getId() {
		return id;
	}

	public String getStatus() {
		return status;
	}
}
