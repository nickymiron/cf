package com.credifair.web.api.response.market;

import com.credifair.web.model.credit.CreditGradeConstructor;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
public class OnMarketCreditApplicationResponse {

	private Long id;
	private CreditGradeConstructor credifairRating;
	private BigDecimal interest;
	private BigDecimal expectedReturn;
	private BigDecimal creditLimit;
	private BigDecimal funded;
	private BigDecimal percentageFunded;
	private BigDecimal userInvested;
	private BorrowerOverviewResponse borrowerOverview;
	private IncomeExpensesOverviewResponse incomeExpensesOverview;
	private BorrowerHistoryResponse history;

	public OnMarketCreditApplicationResponse() {
	}

	public OnMarketCreditApplicationResponse(Long id, CreditGradeConstructor credifairRating, BigDecimal interest, BigDecimal expectedReturn, BigDecimal creditLimit, BigDecimal funded, BigDecimal percentageFunded, BigDecimal userInvested, BorrowerOverviewResponse borrowerOverviewResponse, IncomeExpensesOverviewResponse incomeExpensesOverview, BorrowerHistoryResponse history) {
		this.id = id;
		this.credifairRating = credifairRating;
		this.interest = interest;
		this.expectedReturn = expectedReturn;
		this.creditLimit = creditLimit;
		this.funded = funded;
		this.percentageFunded = percentageFunded;
		this.userInvested = userInvested;
		this.borrowerOverview = borrowerOverviewResponse;
		this.incomeExpensesOverview = incomeExpensesOverview;
		this.history = history;
	}

	public Long getId() {
		return id;
	}

	public CreditGradeConstructor getCredifairRating() {
		return credifairRating;
	}

	public BigDecimal getInterest() {
		return interest;
	}

	public BigDecimal getExpectedReturn() {
		return expectedReturn;
	}

	public BigDecimal getCreditLimit() {
		return creditLimit;
	}

	public BigDecimal getFunded() {
		return funded;
	}

	public BorrowerOverviewResponse getBorrowerOverview() {
		return borrowerOverview;
	}

	public IncomeExpensesOverviewResponse getIncomeExpensesOverview() {
		return incomeExpensesOverview;
	}

	public BigDecimal getPercentageFunded() {
		return percentageFunded;
	}

	public BigDecimal getUserInvested() {
		return userInvested;
	}

	public BorrowerHistoryResponse getHistory() {
		return history;
	}
}
