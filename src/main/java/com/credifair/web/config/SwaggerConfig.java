package com.credifair.web.config;

import com.mangofactory.swagger.configuration.SpringSwaggerConfig;
import com.mangofactory.swagger.models.dto.ApiInfo;
import com.mangofactory.swagger.plugin.EnableSwagger;
import com.mangofactory.swagger.plugin.SwaggerSpringMvcPlugin;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

/**
 * Created by indrek.ruubel on 13/01/2016.
 */

@Configuration
@EnableSwagger
@Profile("!test")
public class SwaggerConfig {

    @Bean
    public SwaggerSpringMvcPlugin getSwaggerSpringPlugin(SpringSwaggerConfig springSwaggerConfig) {
        return new SwaggerSpringMvcPlugin(springSwaggerConfig)
                .apiInfo(apiInfo())
                .apiVersion("v1")
                .swaggerGroup("com.credifair.web.api")
                .includePatterns("/api.*");
    }

    private ApiInfo apiInfo() {
        ApiInfo apiInfo = new ApiInfo(
                "CrediFair API",
                "App that saves you tons of money.",
                null,
                null,
                null,
                null
        );
        return apiInfo;
    }
}
