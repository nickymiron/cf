package com.credifair.web.config;

import com.credifair.web.service.CustomUserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */

@Configuration
@EnableWebSecurity
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomUserDetailsService customUserDetailsService;

    @Autowired
    CustomSuccessHandler customSuccessHandler;

    @Autowired
    private CustomLogoutSuccessHandler customLogoutSuccessHandler;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception {
        auth
                .userDetailsService(customUserDetailsService)
                .passwordEncoder(new BCryptPasswordEncoder());
    }

    @Override
    public void configure(WebSecurity web) throws Exception {
        web
                .ignoring()
                .antMatchers("/js/**", "/css/**", "/lib/**", "/img/**", "/assets/**", "/favicon.ico", "/files/**");
                // Don't add /api/** , it messes up getting logged in user information on current user from oauth api
    }

    @Override
    @Bean
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
            .authorizeRequests()
            .antMatchers("/customer**", "/customer/**").hasAnyRole("BORROWER", "ADMIN")
            .antMatchers("/investor**", "/investor/**").hasAnyRole("INVESTOR", "ADMIN")
            .antMatchers("/admin**", "/admin/**").hasRole("ADMIN")
            .and()
            .formLogin()
            .loginPage("/login")
            .successHandler(customSuccessHandler)
            .and().logout().logoutUrl("/logout").logoutSuccessHandler(customLogoutSuccessHandler)
            .and()
            .csrf().disable();
    }

}