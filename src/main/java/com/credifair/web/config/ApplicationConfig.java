package com.credifair.web.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.math.MathContext;
import java.math.RoundingMode;
import java.util.TimeZone;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@Configuration
public class ApplicationConfig {

    public ApplicationConfig() {
//        configureUTC();
    }

    public void configureUTC() {
//        Joda, but should work here too http://joda-time.sourceforge.net/timezones.html
//        TimeZone.setDefault(TimeZone.getTimeZone("Etc/UTC"));
    }

    @Bean
    public MathContext mathContext() {
        return new MathContext(120, RoundingMode.HALF_UP);
    }

}
