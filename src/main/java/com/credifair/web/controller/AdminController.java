package com.credifair.web.controller;

import com.credifair.web.model.Currency;
import com.credifair.web.model.Role;
import com.credifair.web.model.User;
import com.credifair.web.model.payout.PayoutInstruction;
import com.credifair.web.model.transaction.bank.*;
import com.credifair.web.service.BankTransactionService;
import com.credifair.web.service.PayoutService;
import com.credifair.web.service.UserService;
import com.credifair.web.service.VerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 05/03/2016.
 */
@Controller
@RequestMapping("/admin")
public class AdminController extends WebController {

    @Autowired
    UserService userService;

    @Autowired
    BankTransactionService bankTransactionService;

    @Autowired
    PayoutService payoutService;

    @Autowired
    VerificationService verificationService;

    @RequestMapping(value = {"", "/"})
    public String index(Model model, HttpServletRequest request) throws Exception {
        setupConfig(model, request);
        setupDashboardData(model);
        return "admin/index";
    }

    @RequestMapping(value = {"/banktransaction/list", "/banktransaction/list/"})
    public String banktransactionlist(Model model, HttpServletRequest request) throws Exception {
        setupConfig(model, request);
        setupDashboardData(model);
        List<BankTransaction> bankTransactions = bankTransactionService.findAllByState(BankTransactionState.ACTIVE);
        model.addAttribute("transactions", bankTransactions);
        return "admin/banktransactionlist";
    }

    @RequestMapping(value = {"/banktransaction/{transactionId}", "/banktransaction/{transactionId}"})
    public String banktransactionLink(@PathVariable(value = "transactionId") Long transactionId, Model model, HttpServletRequest request) throws Exception {
        setupConfig(model, request);
        setupDashboardData(model);
        BankTransaction transaction = bankTransactionService.findOne(transactionId);
        Iterable<BankTransactionLink> links = bankTransactionService.findAllLinksOfTransaction(transaction);
        BigDecimal linkedAmount = BigDecimal.ZERO;
        for (BankTransactionLink link : links) {
            linkedAmount = linkedAmount.add(link.getAmount());
        }
        model.addAttribute("linkedAmount", linkedAmount);
        model.addAttribute("transaction", transaction);
        model.addAttribute("remainingUnlinked", transaction.getAmount().subtract(linkedAmount));
        return "admin/banktransaction";
    }

    @RequestMapping(value = {"/banktransaction/create", "/banktransaction/create/"})
    public String createtransaction(Model model, HttpServletRequest request) throws Exception {
        setupConfig(model, request);
        setupDashboardData(model);
        model.addAttribute("bankEnums", BankEnum.getAll());
        model.addAttribute("currencyEnums", Currency.getAll());
        return "admin/createtransaction";
    }

    @RequestMapping(value = {"/payout/list", "/payout/list/"})
    public String payoutInstructions(Model model, HttpServletRequest request) throws Exception {
        setupConfig(model, request);
        setupDashboardData(model);
        Iterable<PayoutInstruction> instructions = payoutService.getAllPendingInstructions();
        model.addAttribute("instructions", instructions);
        return "admin/payouts";
    }

    @RequestMapping(value = {"/payout/{payoutId}", "/payout/{payoutId}/"})
    public String payout(@PathVariable(value = "payoutId") Long payoutId, Model model, HttpServletRequest request) throws Exception {
        setupConfig(model, request);
        setupDashboardData(model);
        PayoutInstruction instruction = payoutService.findOne(payoutId);
        model.addAttribute("instruction", instruction);
        return "admin/payout";
    }

    @RequestMapping(value = {"/verify/{verificationId}"})
    public String verify(@PathVariable(value = "verificationId") Long verificationId, Model model, HttpServletRequest request) throws Exception {
        String referrer = request.getHeader("referer");
        verificationService.verifyVerificationById(verificationId);
        return "redirect:" + referrer;
    }

    @RequestMapping(value = {"/unverify/{verificationId}"})
    public String unverify(@PathVariable(value = "verificationId") Long verificationId, Model model, HttpServletRequest request) throws Exception {
        String referrer = request.getHeader("referer");
        verificationService.unverifyVerificationById(verificationId);
        return "redirect:" + referrer;
    }

    @RequestMapping(value = {"/linkTransaction"})
    public String linkTransaction(
            @RequestParam(value = "transactionId") Long transactionId,
            @RequestParam(value = "amount") BigDecimal amount,
            @RequestParam(value = "userId") Long userId,  Model model, HttpServletRequest request) throws  Exception {
        User user = userService.getUserById(userId);
        if (user == null) {
            model.addAttribute("error", "No user found with id: " +userId);
            return banktransactionLink(transactionId, model, request);
        }
        BankTransaction bankTransaction = bankTransactionService.findOne(transactionId);
        if (bankTransaction == null) {
            model.addAttribute("error", "No transaction found with id: " + transactionId);
            return banktransactionLink(transactionId, model, request);
        }
        if (!bankTransaction.getState().equals(BankTransactionState.ACTIVE)) {
            model.addAttribute("error", "Can't link, transaction has state: " + bankTransaction.getState());
            return banktransactionLink(transactionId, model, request);
        }

        try {
            bankTransactionService.linkTransactionToUser(amount, bankTransaction, user);
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
            return banktransactionLink(transactionId, model, request);
        }

        model.addAttribute("message", "Link successful!");
        return banktransactionLink(transactionId, model, request);
    }

    @RequestMapping(value = {"/submitTransaction"})
    public String submitTransaction(
            @RequestParam(name = "amount") BigDecimal amount,
            @RequestParam(name = "transactionId") String transactionId,
            @RequestParam(name = "bank") BankEnum bank,
            @RequestParam(name = "currency") Currency currency,
            @RequestParam(name = "reference") String reference,
            Model model, HttpServletRequest request) throws  Exception {
        try {
            bankTransactionService.createReceiveBankTransaction(amount, bank, currency, reference,
                    LocalDateTime.now(),
                    transactionId);
            model.addAttribute("message", "Saved!");
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
        }
        return createtransaction(model, request);
    }

    @RequestMapping(value = {"/approvePayoutInstruction"})
    public String approveInstruction(
            @RequestParam(name = "instructionId") Long instructionId,
            Model model, HttpServletRequest request) throws  Exception {
        User admin = userService.getLoggedInUser();
        try {
            payoutService.approveInstruction(instructionId, admin);
            model.addAttribute("message", "Transferred!");
        } catch (Exception e) {
            model.addAttribute("error", e.getMessage());
        }
        return payoutInstructions(model, request);
    }

    private void setupDashboardData(Model model) {
        Iterable<User> allUsers = userService.getAllUsers();
        List<Map<String, Object>> usersOut = new ArrayList<>();
        for (User user : allUsers) {
            Map<String, Object> userOut = new HashMap<>();
            List<String> rolesOut = new ArrayList<>();
            for (Role role : user.getRoles()) {
                rolesOut.add(role.getRole());
            }
            userOut.put("id", user.getId());
            userOut.put("cref", user.getId() + 10000);
            userOut.put("email", user.getEmail());
            userOut.put("roles", rolesOut);

            usersOut.add(userOut);
        }
        model.addAttribute("users", usersOut);
    }
}
