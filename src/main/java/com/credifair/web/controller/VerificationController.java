package com.credifair.web.controller;

import com.credifair.web.dao.FileUploadDao;
import com.credifair.web.model.FileUpload;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.FileSystemResource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.servlet.http.HttpServletResponse;

/**
 * Created by indrek.ruubel on 03/04/2016.
 */
@Controller
public class VerificationController {

	@Value("${upload.folder}")
	private String uploadFolder;

	@Autowired
	FileUploadDao fileUploadDao;

	@RequestMapping(value = "/files/{file_name}", method = RequestMethod.GET)
	@ResponseBody
	public FileSystemResource getFile(@PathVariable("file_name") String fileName, HttpServletResponse response) {
		FileUpload file = fileUploadDao.findByFilename(fileName);
		response.setContentType(file.getContentType());
		return new FileSystemResource(uploadFolder + fileName);
	}

}
