package com.credifair.web.controller;

import com.credifair.web.model.User;
import com.credifair.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@Controller
public class LandingController extends WebController {

    @Autowired
    UserService userService;

    public static final String ACCOUNT_PATH = "/account";

    @RequestMapping(value = "/")
    public String index() {
        return "index";
    }

    @RequestMapping(value = "/oauth")
    public String oauth() {
        return "oauth_client";
    }

    @RequestMapping(value = {ACCOUNT_PATH, ACCOUNT_PATH + "/"})
    public String index(Model model, HttpServletRequest request) throws Exception {
        User user = userService.getLoggedInUser();
        if (user == null) {
            return "redirect:/";
        } else {
            if (user.hasRole("ROLE_ADMIN")) {
                setupAdminConfig(model, user, request);
            } else {
                setupConfig(model, request);
            }
            return "/borrower/index";
        }
    }

    @RequestMapping(value = {ACCOUNT_PATH + "/{userId}", ACCOUNT_PATH + "{userId}/"})
    @PreAuthorize(value = "hasRole('ROLE_ADMIN')")
    public String dashboard(@PathVariable(value = "userId") Long userId, Model model, HttpServletRequest request) throws Exception {
        User user = userService.getUserById(userId);
        if (user == null) {
            return "redirect:/admin";
        }
        setupAdminConfig(model, user, request);
        return "/borrower/index";
    }

}
