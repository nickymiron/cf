package com.credifair.web.controller;

import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.state.CAState;
import com.credifair.web.model.verification.Verification;
import com.credifair.web.service.OauthService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;

import javax.servlet.http.HttpServletRequest;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 09/03/2016.
 */
public class WebController {

    @Autowired
    OauthService oauthService;

    protected void setupConfig(Model model, HttpServletRequest request) throws Exception {
        Map<String, Object> config = createConfig(request);
        model.addAttribute("config", config);
    }

    protected void setupAdminConfig(Model model, User user, HttpServletRequest request) throws Exception {
        Map<String, Object> config = createConfig(request);
        injectAdminConfig(config, user, model);
        setupUserGeneralInfo(model, user);
    }

    protected Map<String, Object> createConfig(HttpServletRequest request) throws Exception {
        String oauthToken = oauthService.getOauthToken(request);
        Map<String, Object> config = new HashMap<>();
        config.put("token", oauthToken);
        return config;
    }

    protected void injectAdminConfig(Map<String, Object> config, User user, Model model) {
        config.put("apiArgs", String.format("/%s", user.getId()));
        config.put("userId", String.valueOf(user.getId()));
        model.addAttribute("config", config);
    }

    protected void setupUserGeneralInfo(Model model, User user) {
        UserProfile userProfile = user.getUserProfile();
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();

        Address address = userProfile.getAddress();
        PersonalFinancialData personalFinancialData = borrowerUserProfile.getPersonalFinancialData();
        Map<String, Object> details = new HashMap<>();

        details.put("firstName", null);
        details.put("lastName", null);
        details.put("cref", null);
        details.put("dob", null);
        details.put("address", null);
        details.put("city", null);
        details.put("country", null);
        details.put("postalCode", null);
        details.put("caState", null);

        // General
        details.put("firstName", userProfile.getFirstName());
        details.put("lastName", userProfile.getLastName());
        details.put("email", user.getEmail());
        details.put("cref", user.getId() + 10000);
        details.put("country", user.getCountry().getCountry());
        LocalDate dateOfBirth = userProfile.getDateOfBirth();
        if (dateOfBirth != null) {
            details.put("dob", dateOfBirth.format(DateTimeFormatter.ISO_DATE));
        }
        if (address != null) {
            details.put("address", address.getAddress());
            details.put("city", address.getCity());
            details.put("postalCode", address.getPostalCode());
        }

        if (creditApplication != null) {
            CAState state = creditApplication.getState();
            if (state != null) {
                details.put("caState", state.getState());
            }
        }

        details.put("netIncome", null);
        details.put("otherIncome", null);
        details.put("homeOwnership", null);
        details.put("willProvideDocs", null);

        // Finance
        List<Map<String, Object>> liabilitiesArr = new ArrayList<>();
        if (personalFinancialData != null) {
            details.put("netIncome", personalFinancialData.getMonthlyNetIncome());
            details.put("otherIncome", personalFinancialData.getOtherRegularIncome());
            details.put("homeOwnership", personalFinancialData.getHomeOwnership());
            details.put("willProvideDocs", personalFinancialData.getWillProvideDocumentsOfIncomeAndExpenses());

            // Liabilities
            List<Liability> liabilities = personalFinancialData.getLiabilities();
            if (liabilities != null) {

                for (Liability liability : liabilities) {
                    Map<String, Object> obj = new HashMap<>();
                    LiabilityType typeOfLiability = liability.getTypeOfLiability();
                    String creditor = liability.getCreditor();
                    BigDecimal moPayment = liability.getMoPayment();
                    BigDecimal outstanding = liability.getOutstanding();
                    LiabilitySecurity security = liability.getSecurity();

                    obj.put("type", typeOfLiability);
                    obj.put("creditor", creditor);
                    obj.put("moPayment", moPayment);
                    obj.put("outstanding", outstanding);
                    obj.put("securedBy", security);
                    liabilitiesArr.add(obj);
                }
            }
        }

        // Verification
        List<Verification> verifications = user.getVerifications();
        List<Map<String, Object>> verificationsOut = new ArrayList<>();
        for (Verification verification : verifications) {
            Map<String, Object> verificationOut = new HashMap<>();
            List<Map<String, Object>> filesOut = new ArrayList<>();

            verificationOut.put("id", verification.getId());
            verificationOut.put("type", verification.getType());
            verificationOut.put("status", verification.getVerificationStatus().toString());

            for (FileUpload fileUpload : verification.getFiles()) {
                Map<String, Object> fileOut = new HashMap<>();
                fileOut.put("filename", fileUpload.getFilename());
                filesOut.add(fileOut);
            }
            verificationOut.put("files", filesOut);
            verificationsOut.add(verificationOut);
        }

        details.put("verifications", verificationsOut);
        details.put("liabilities", liabilitiesArr);
        model.addAttribute("userDetails", details);
    }

}
