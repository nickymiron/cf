package com.credifair.web.controller;

import com.credifair.web.model.User;
import com.credifair.web.service.AuthenticationService;
import com.credifair.web.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by indrek.ruubel on 07/03/2016.
 */
@Controller
public class RegisterController extends WebController {

    @Autowired
    private UserService userService;

    @Autowired
    private AuthenticationService authenticationService;

    @RequestMapping(value = "/register", consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE)
    public String register(@RequestParam String username, @RequestParam String password, Model model, HttpServletRequest request) throws Exception {

        User user = userService.saveCustomer(username, password);
        if (user != null) {
            authenticationService.login(user, password);
            setupConfig(model, request);
        }
        return "redirect:" + LandingController.ACCOUNT_PATH;
    }

}
