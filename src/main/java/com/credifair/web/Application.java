package com.credifair.web;

import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.model.transaction.bank.BankEnum;
import com.credifair.web.model.transaction.bank.BankTransaction;
import com.credifair.web.service.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import javax.annotation.PostConstruct;
import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.UUID;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@SpringBootApplication
public class Application {

    public static void main(String[] args) throws Exception {
        SpringApplication.run(Application.class, args);
    }

    // Quick setup for testing

    @Autowired
    UserService userService;

    @Autowired
    BalanceService balanceService;

    @Autowired
    BankTransactionService bankTransactionService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    VerificationService verificationService;

    @Autowired
    UserProfileService userProfileService;

    @Autowired
    EmploymentService employmentService;

    @Autowired
    InvestmentService investmentService;

    @Autowired
    AddressService addressService;

    @Autowired
    CreditLimitService creditLimitService;

    @Autowired
    PersonalFinancialDataService personalFinancialDataService;

    private static final String ADMIN_EMAIL = "admin@admin.admin";
    private static final String BORROWER_EMAIL = "test@test.test";
    private static final String A_EMAIL = "a@a.a";
    private static final String B_EMAIL = "b@b.b";
    private static final String C_EMAIL = "c@c.c";

    @PostConstruct
    public void setup() {
        setupDatabaseStuff();
    }

    @Transactional
    private void setupDatabaseStuff() {
        setupAdmin();
//        setupBorrower();
        setupInvestors();
    }

    private void injectMoneyToUserIfNoMoney(String email) {
        User user = userService.findByEmail(email);
        Balance aBalance = balanceService.getLatestBalance(user.getUserProfile());
        if (aBalance == null || aBalance.getClosingBalance().compareTo(BigDecimal.ZERO) == 0) {
            // No money, put money
            BankTransaction bankTransaction = null;
            try {
                bankTransaction = bankTransactionService.createReceiveBankTransaction(new BigDecimal(1000), BankEnum.SEB, Currency.EUR, "", LocalDateTime.now(), UUID.randomUUID().toString());
                bankTransactionService.linkTransactionToUser(new BigDecimal(1000), bankTransaction, user);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void setupAdmin() {
        User admin = userService.findByEmail(ADMIN_EMAIL);
        if (admin == null) {
            admin = userService.saveCustomer(ADMIN_EMAIL, "admin", Country.EE);
            userService.addRoleToUser(admin, "ROLE_ADMIN");
        }
    }

    private void setupBorrower() {
        User user = userService.findByEmail(BORROWER_EMAIL);
        if (user == null) {
            user = userService.saveCustomer(BORROWER_EMAIL, "test", Country.EE);

            injectMoneyToUserIfNoMoney(BORROWER_EMAIL);

            userProfileService.setBirthDate(LocalDate.now(), user.getUserProfile());
            PersonalFinancialData personalFinancialData = new PersonalFinancialData(user.getBorrowerUserProfile(),
                    new BigDecimal(100), new BigDecimal(100), HomeOwnership.LIVING_WITH_PARENTS, true, new ArrayList<>());
            personalFinancialDataService.save(personalFinancialData);
            Employment employment = new Employment(user.getBorrowerUserProfile(), EmploymentStatus.ENTREPRENEUR, WorkIndustry.CONSTRUCTION,
                    "fuck", EmploymentTime.LESS_THAN_SIX_MONTHS, EmploymentTime.LESS_THAN_SIX_MONTHS, "shit ltd.");
            employmentService.save(employment);
            Address address = new Address(user.getUserProfile(), "sdfs", "sdfs", "45345");
            addressService.save(address);

            CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
            creditApplication = creditApplicationService.assignGradeToCAWithMinimalAPR(creditApplication, CreditGradeConstructor.D);
            creditApplication.setGracePeriodDays(0);
            creditApplicationService.save(creditApplication);
            verificationService.putOnMarket(user);

        }
    }

    private void setupInvestors() {
//        User borrower = userService.findByEmail(BORROWER_EMAIL);
//        CreditApplication creditApplication = creditApplicationService.getCreditApplication(borrower);
//        creditLimitService.assignNewCreditLimit(creditApplication, new BigDecimal(2000), null);
        User user = userService.findByEmail(A_EMAIL);
        if (user == null) {
            user = userService.saveCustomer(A_EMAIL, "a", Country.EE);
            injectMoneyToUserIfNoMoney(A_EMAIL);
//            try {
//                investmentService.reserv(creditApplication, new BigDecimal(70), user);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }
        User user2 = userService.findByEmail(B_EMAIL);
        if (user2 == null) {
            user2 = userService.saveCustomer(B_EMAIL, "b", Country.EE);
            injectMoneyToUserIfNoMoney(B_EMAIL);
//            try {
//                investmentService.reserv(creditApplication, new BigDecimal(30), user2);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
        }
//        User user3 = userService.findByEmail(C_EMAIL);
//        if (user3 == null) {
//            user3 = userService.saveCustomer(C_EMAIL, "c", Country.EE);
//            injectMoneyToUserIfNoMoney(C_EMAIL);
//            try {
//                investmentService.reserv(creditApplication, new BigDecimal(30), user3);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        }
    }

}
