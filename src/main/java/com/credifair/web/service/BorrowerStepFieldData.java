package com.credifair.web.service;

import com.credifair.web.model.BorrowerFlowStep;
import com.credifair.web.model.UserValueFieldType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
public class BorrowerStepFieldData {

	public final static Map<BorrowerFlowStep, List<UserValueFieldType>> stepFieldMap = new HashMap<BorrowerFlowStep, List<UserValueFieldType>>(){{
		put(BorrowerFlowStep.STEP1_PERSONAL_INFO, new ArrayList<UserValueFieldType>(){{
			add(UserValueFieldType.FIRST_NAME);
			add(UserValueFieldType.LAST_NAME);
			add(UserValueFieldType.DATE_OF_BIRTH);
			add(UserValueFieldType.PHONE_NUMBER);
			add(UserValueFieldType.EDUCATION);
			add(UserValueFieldType.MARITAL_STATUS);
		}});

		put(BorrowerFlowStep.STEP2_ADDRESS, new ArrayList<UserValueFieldType>(){{
			add(UserValueFieldType.ADDRESS);
			add(UserValueFieldType.CITY);
			add(UserValueFieldType.POSTAL_CODE);
		}});

	}};

}
