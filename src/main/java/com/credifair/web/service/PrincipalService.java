package com.credifair.web.service;

import com.credifair.web.model.Currency;
import com.credifair.web.model.Investment;
import com.credifair.web.model.InvestmentSnapshop;
import com.credifair.web.model.Loan;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created by indrek.ruubel on 18/04/2016.
 */
@Service
public class PrincipalService {

	@Autowired
	BalanceService balanceService;

	@Autowired
	InvestmentService investmentService;

	@Autowired
	InterestService interestService;

	@Autowired
	MathContext mathContext;

	public BigDecimal payPrincipal(BigDecimal amount, Currency currency, Iterable<Investment> investments, String traceHash) {
		BigDecimal totalPrincipal = BigDecimal.ZERO;
		for (Investment investment : investments) {
			InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
			BigDecimal closingPrincipal = latestSnapshop.getClosingPrincipal();
			totalPrincipal = totalPrincipal.add(closingPrincipal);
		}

		if (totalPrincipal.compareTo(BigDecimal.ZERO) == 0) {
			return amount;
		}

		if (amount.compareTo(totalPrincipal) > 0) {
			// More money than total principal
			BigDecimal remaining = amount.subtract(totalPrincipal);
			// Iterate all investments, create transactions to investors of principal payments, update their balances
			for (Investment investment : investments) {
				InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
				balanceService.addPrincipalRepaymentToInvestor(latestSnapshop.getClosingPrincipal(), currency,
						investment.getInvestorUserProfile(), traceHash);
			}
			zeroPrincipalsAndClose(investments);
			return remaining;
		} else if (amount.compareTo(totalPrincipal) == 0) {
			// Equal
			// Iterate all investments, create transactions to investors of principal payments, update their balances
			for (Investment investment : investments) {
				InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
				balanceService.addPrincipalRepaymentToInvestor(latestSnapshop.getClosingPrincipal(), currency,
						investment.getInvestorUserProfile(), traceHash);
			}
			zeroPrincipalsAndClose(investments);
			return BigDecimal.ZERO;
		} else {
			// Less available
			// Iterate all investments, create transactions to investors of principal payments, update their balances
			try {
				dividePrincipals(amount, investments, traceHash);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return BigDecimal.ZERO;
		}
	}

	private void zeroPrincipalsAndClose(Iterable<Investment> investments) {
		Loan loan = investments.iterator().next().getLoan();
		investmentService.closeLoan(loan);
		for (Investment investment : investments) {
			investmentService.createPrincipalDecrementSnapshot(BigDecimal.ZERO, investment);
			investmentService.closeInvestment(investment);
		}
	}

	private void dividePrincipals(BigDecimal available, Iterable<Investment> investments, String traceHash) throws Exception {
		BigDecimal totalSubtracted = BigDecimal.ZERO;
		for (Investment investment : investments) {
			InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
			BigDecimal ratio = investment.getRatio();
			BigDecimal openingPrincipal = latestSnapshop.getClosingPrincipal();
			BigDecimal principalAmount = available.multiply(ratio, mathContext);
			BigDecimal closingPrincipal = openingPrincipal.subtract(principalAmount);

			balanceService.addPrincipalRepaymentToInvestor(principalAmount, investment.getCurrency(),
					investment.getInvestorUserProfile(), traceHash);

			investmentService.createPrincipalDecrementSnapshot(closingPrincipal, investment);
			totalSubtracted = totalSubtracted.add(principalAmount);
		}
		if (totalSubtracted.compareTo(available) != 0) {
			throw new Exception("Subtracted amount doesn't match the available");
		}
	}


}
