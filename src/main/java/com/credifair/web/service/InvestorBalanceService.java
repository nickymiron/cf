package com.credifair.web.service;

import com.credifair.web.dao.InvestorBalanceDao;
import com.credifair.web.model.Currency;
import com.credifair.web.model.InvestorBalance;
import com.credifair.web.model.InvestorUserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by indrek.ruubel on 20/04/2016.
 */
@Service
public class InvestorBalanceService {

	@Autowired
	InvestorBalanceDao investorBalanceDao;

	public InvestorBalance getLatestInvestorBalance(InvestorUserProfile investorUserProfile) {
		return investorBalanceDao.findTopByInvestorUserProfileOrderByIdDesc(investorUserProfile);
	}

	public InvestorBalance addInterestRepayment(BigDecimal interestRepayed, Currency currency, InvestorUserProfile investorUserProfile, String traceHash) {
		System.out.println("TODO: Do something with traceHash");
		InvestorBalance investorBalance = new InvestorBalance(BigDecimal.ZERO, interestRepayed,
				BigDecimal.ZERO, investorUserProfile, currency, LocalDateTime.now());
		investorBalanceDao.save(investorBalance);
		return investorBalance;
	}

	public InvestorBalance addPrincipalRepayment(BigDecimal principalRepayed, Currency currency, InvestorUserProfile investorUserProfile, String traceHash) {
		System.out.println("TODO: Do something with traceHash");
		InvestorBalance investorBalance = new InvestorBalance(principalRepayed, BigDecimal.ZERO,
				BigDecimal.ZERO, investorUserProfile, currency, LocalDateTime.now());
		investorBalanceDao.save(investorBalance);
		return investorBalance;
	}

	public InvestorBalance addCapitalDeployed(BigDecimal amount, Currency currency, InvestorUserProfile investorUserProfile, String traceHash) {
		System.out.println("TODO: Do something with traceHash");
		InvestorBalance investorBalance = new InvestorBalance(BigDecimal.ZERO, BigDecimal.ZERO,
				amount, investorUserProfile, currency, LocalDateTime.now());
		investorBalanceDao.save(investorBalance);
		return investorBalance;
	}

	public List<InvestorBalance> getAllInvestorBalancesDesc(InvestorUserProfile investor) {
		return investorBalanceDao.findAllByInvestorUserProfileOrderByIdDesc(investor);
	}
}
