package com.credifair.web.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.AutowireCapableBeanFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 20/02/2016.
 */
@Service
public class AutowireService implements ApplicationContextAware {

    private ApplicationContext ctx;

    public void autowireBean(Object bean){
        if(bean == null)
            return;
        AutowireCapableBeanFactory autowireCapableBeanFactory = ctx.getAutowireCapableBeanFactory();
        autowireCapableBeanFactory.autowireBean(bean);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.ctx = applicationContext;
    }
}
