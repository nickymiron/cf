package com.credifair.web.service;

import com.credifair.web.dao.PersonalFinancialDataDao;
import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.PersonalFinancialData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 23/04/2016.
 */
@Service
public class PersonalFinancialDataService {

	@Autowired
	PersonalFinancialDataDao personalFinancialDataDao;

	public PersonalFinancialData findByBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile) {
		return personalFinancialDataDao.findByBorrowerUserProfile(borrowerUserProfile);
	}

	public void save(PersonalFinancialData personalFinancialData) {
		personalFinancialDataDao.save(personalFinancialData);
	}
}
