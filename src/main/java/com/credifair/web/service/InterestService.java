package com.credifair.web.service;

import com.credifair.web.dao.LoanDao;
import com.credifair.web.dao.InvestmentDao;
import com.credifair.web.dao.InvestmentSnapshotDao;
import com.credifair.web.model.*;
import com.credifair.web.model.Currency;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.transaction.Transaction;
import com.google.common.collect.Lists;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.*;

/**
 * Created by indrek.ruubel on 29/01/2016.
 */
@Service
public class InterestService {

    private static final Logger logger = LoggerFactory.getLogger(InterestService.class);

    @Autowired
    LoanService loanService;

    @Autowired
    InvestmentSnapshotDao investmentSnapshotDao;

    @Autowired
    TransactionService transactionService;

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    InvestmentDao investmentDao;

    @Autowired
    LoanDao loanDao;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    BalanceService balanceService;

    @Autowired
    InvestmentService investmentService;

    @Autowired
    InvestorBalanceService investorBalanceService;

    @Value(value = "${cf.percentage.fee}")
    BigDecimal cfFeePercentage;

    @Autowired
    MathContext mathContext;

    private final Integer daysAYear = 365;

    public BigDecimal calculateDailyInterest(BigDecimal APR) throws Exception {
        if (APR.compareTo(BigDecimal.ONE) > 0) {
            throw new Exception("You sure about the interest? Represent it in a decimal form!");
        }
        return APR.divide(new BigDecimal(daysAYear), 15, RoundingMode.HALF_UP);
    }

    /**
     * @param amountDays - LinkedHashMap in args to keep order of insertion in a map.
     *                   Amount and number of days to calculate interest on it
     * @param interest - daily APR
     * @return
     */
    public BigDecimal compoundOnMultipleAmounts(List<AbstractMap.SimpleEntry> amountDays, BigDecimal interest) {
        BigDecimal totalAmountAndInterest = BigDecimal.ZERO;

        for (AbstractMap.SimpleEntry amountDay : amountDays) {
            BigDecimal amount = (BigDecimal) amountDay.getKey();
            Integer days = (Integer) amountDay.getValue();
            totalAmountAndInterest = totalAmountAndInterest.add(amount);
            BigDecimal interestEarned = compoundInterestOnAmountForNDays(interest, totalAmountAndInterest, days);
            totalAmountAndInterest = totalAmountAndInterest.add(interestEarned);
        }
        return totalAmountAndInterest;
    }

    public BigDecimal compoundInterestOnAmountForNDays(BigDecimal interest, final BigDecimal amount, Integer days) {
//        The Compound Interest Equation:
//        ((P*(1+i)^n) - P)
//        Where P is amount
//        Where i is interest
//        Where n is period
        BigDecimal first = (BigDecimal.ONE.add(interest)).pow(days);
        BigDecimal second = amount.multiply(first);
        BigDecimal third = second.subtract(amount);
        return third;
    }

    public List<Investment> calculateRatios(Iterable<Investment> loans) {
        return calculateRatios(Lists.newArrayList(loans));
    }

    public List<Investment> calculateRatios(List<Investment> investments) {
        BigDecimal total = BigDecimal.ZERO;

        // Calculate the total
        for (Investment investment : investments) {
            InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();

            BigDecimal closingFee = latestSnapshop.getClosingFee();
            BigDecimal closingInterest = latestSnapshop.getClosingInterest();
            BigDecimal closingPrincipal = latestSnapshop.getClosingPrincipal();
            BigDecimal totalAmount = closingFee.add(closingInterest);
            totalAmount = totalAmount.add(closingPrincipal);

            total = total.add(totalAmount);
        }

        // Calculate the ratios from total
        for (Investment investment : investments) {
            InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();

            BigDecimal closingFee = latestSnapshop.getClosingFee();
            BigDecimal closingInterest = latestSnapshop.getClosingInterest();
            BigDecimal closingPrincipal = latestSnapshop.getClosingPrincipal();
            BigDecimal totalAmount = closingFee.add(closingInterest);
            totalAmount = totalAmount.add(closingPrincipal);

            investment.setRatio(totalAmount.divide(total, 2, RoundingMode.HALF_UP));
        }
        return investments;
    }

    public List<Loan> calculateAllInterests() throws Exception {
        List<Loan> loans = findLoansWithBalances();

        // Sort them to a map
        Map<User, List<Loan>> map = new HashMap<>();

        for (Loan loan : loans) {
            BorrowerUserProfile borrowerUserProfile = loan.getBorrowerUserProfile();
            User borrower = borrowerUserProfile.getUser();
            if (map.containsKey(borrower)) {
                List<Loan> loanList = map.get(borrower);
                loanList.add(loan);
            } else {
                map.put(borrower, new ArrayList<Loan>(){{add(loan);}});
            }
        }

        // Pass every borrowers list of loans to calculation
        for (Map.Entry<User, List<Loan>> borrowerUserProfileListEntry : map.entrySet()) {
            calculateLoanInterest(borrowerUserProfileListEntry.getKey(), borrowerUserProfileListEntry.getValue());
        }

        return loans;
    }

    public void calculateLoanInterest(User user, List<Loan> loans) throws Exception {

        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        CreditApplication creditApplication = creditApplicationService.findByBorrowerUserProfile(borrowerUserProfile);
        if (creditApplication == null) {
            throw new Exception("CreditApplication cannot be null in case of active loan!");
        }

        int interestFreeDays = creditApplication.getGracePeriodDays();

        if (!loans.isEmpty() && !hasGoneOverGracePeriod(loans.get(0), interestFreeDays)) {
            System.out.println("Hasn't gone over grace period");
            return;
        }

        System.out.println("Starting to calc loan interests");

        for (Loan loan : loans) {

            BigDecimal totalInterestEarnedToday = BigDecimal.ZERO;
            Currency currency = loan.getCurrency();

            String traceHash = transactionService.generateTraceHash();

            if (!hasBeenCalculatedToday(loan)) {
                List<Investment> investments = investmentDao.findAllByLoan(loan);

                for (Investment investment : investments) {

                    InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();

                    int daysToSinceTargetReached = getDaysToCalculateInterestFor(investment);
                    int daysToCalculate = daysToSinceTargetReached - interestFreeDays;

                    BigDecimal openingFee = latestSnapshop.getClosingFee();
                    BigDecimal openingInterest = latestSnapshop.getClosingInterest();
                    BigDecimal openingPrincipal = latestSnapshop.getClosingPrincipal();

                    BigDecimal openingInterestAndPrincipalAndFeeSum = (openingPrincipal.add(openingInterest)).add(openingFee);

                    // Full APR calculation
                    BigDecimal interestAndFeeEarnedToday =
                            compoundInterestOnAmountForNDays(calculateDailyInterest(loan.getApr()),
                            openingInterestAndPrincipalAndFeeSum, daysToCalculate);

                    BigDecimal apr = loan.getApr();
                    BigDecimal investorApr = apr.subtract(cfFeePercentage);

                    BigDecimal investorInterestEarned = (investorApr.multiply(interestAndFeeEarnedToday)).divide(apr, mathContext);
                    BigDecimal investorFeeEarned = interestAndFeeEarnedToday.subtract(investorInterestEarned);

                    BigDecimal closingInterest = openingInterest.add(investorInterestEarned);
                    BigDecimal closingFee = openingFee.add(investorFeeEarned);

                    BigDecimal totalFeePayed = latestSnapshop.getTotalFeePayed();
                    BigDecimal totalInterestPayed = latestSnapshop.getTotalInterestPayed();
                    BigDecimal totalPrincipalPayed = latestSnapshop.getTotalPrincipalPayed();

                    System.out.println("TODO: Figure out or create fee incremented snapshot");
                    InvestmentSnapshop investmentSnapshop =
                            new InvestmentSnapshop(openingInterest, closingInterest, openingPrincipal,
                                    openingPrincipal, openingFee, closingFee, totalFeePayed, totalInterestPayed, totalPrincipalPayed, investment, InvestmentSnapshop.DifferenceType.INCREMENTED_INTEREST);
                    investmentSnapshotDao.save(investmentSnapshop);
                    investment.setLatestSnapshop(investmentSnapshop);
                    investmentDao.save(investment);

                    // Create transactions
                    Transaction transaction = transactionService.createFeeIncrementTransaction(closingFee.subtract(openingFee), currency, user, traceHash);
                    transactionService.createInvestmentSnapshotTransactionLink(transaction, investmentSnapshop.getId());

                    Transaction interestIncrement = transactionService.createInvestorInterestIncrementTransaction(closingInterest.subtract(openingInterest), currency, user, traceHash);
                    transactionService.createInvestmentSnapshotTransactionLink(interestIncrement, investmentSnapshop.getId());

                    // Aggregate
                    totalInterestEarnedToday = totalInterestEarnedToday.add(interestAndFeeEarnedToday);
                }

                // Update loan
                loan.setLastCalculatedInterest(LocalDateTime.now());
                loanDao.save(loan);

                // Create new transaction to borrower as interest
                if (totalInterestEarnedToday.compareTo(BigDecimal.ZERO) > 0) {
                    logger.info("Creating interest transaction to user");
                    LoanBalance loanBalance = loanBalanceService.addInterest(borrowerUserProfile, totalInterestEarnedToday, currency);
                    Transaction transaction = transactionService.createBorrowerInterestIncrementTransaction(totalInterestEarnedToday, currency, user, traceHash);
                    transactionService.createLoanBalanceTransactionLink(transaction, loanBalance.getId());
                }
            } else {
                System.out.println("Has been calculated today");
            }
        }
    }

    public static int calculateDaysBetweenRoundToCeling(LocalDateTime date1, LocalDateTime date2, long marginInMillis) {
        Duration duration = Duration.between(date1, date2);
        int days = (int) duration.toDays();
        // Accuracy in millis in DB
        Duration d = Duration.ofDays(days);
        Duration leftover = duration.minus(d);
        long leftoverMillis = leftover.toMillis();
        leftoverMillis = leftoverMillis - marginInMillis;
        if (leftoverMillis > 0l) {
            days += 1;
        }
        return days;
    }

    public boolean hasOverGracePeriod(BorrowerUserProfile borrowerUserProfile) {
        CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
        List<Loan> loans = findBorrowerLoansWithBalances(borrowerUserProfile);
        if (loans.isEmpty()) {
            return false;
        } else {
            return hasGoneOverGracePeriod(loans.get(0), creditApplication.getGracePeriodDays());
        }
    }

    public boolean hasGoneOverGracePeriod(Loan loan, int gracePeriodDays) {
        int days = calculateDaysBetweenRoundToCeling(loan.getFundedDateTime(), LocalDateTime.now(), 0);
        days = days - gracePeriodDays;
        return days > 0 ? true : false;
    }

    public boolean hasBeenCalculatedToday(Loan loan) {
        LocalDateTime startOfDay = LocalDate.now().atStartOfDay();
        LocalDateTime lastCalculatedInterest = loan.getLastCalculatedInterest();
        if (lastCalculatedInterest == null) {
            return false;
        }
        if (lastCalculatedInterest.isAfter(startOfDay) || lastCalculatedInterest.isEqual(startOfDay)) {
            return true;
        }
        return false;
    }

    public static int getDaysToCalculateInterestFor(Investment investment) {
        InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
        LocalDateTime lastInterestCalculation = latestSnapshop.getCreated();
        if (latestSnapshop.getType().equals(InvestmentSnapshop.DifferenceType.INCREMENTED_PRINCIPAL)) {
            // First snapshot does not calc interest, only adds principal, so it doesn't count
            lastInterestCalculation = null;
        }
        if (lastInterestCalculation == null) {
            lastInterestCalculation = investment.getInvestedDateTime();
            Duration duration = Duration.between(lastInterestCalculation.toLocalDate().atStartOfDay(), LocalDateTime.now());
            int days = (int) duration.toDays() + 1;
            return days;
        }
        Duration duration = Duration.between(lastInterestCalculation.toLocalDate().atStartOfDay(), LocalDateTime.now());
        int days = (int) duration.toDays();
        return days;
    }

    public List<Loan> findLoansWithBalances() {
        return loanDao.findAllByFundedDateTimeIsNotNullAndAmountGreaterThanOrderByFundedDateTimeAscBorrowerUserProfileAsc(BigDecimal.ZERO);
    }

    public List<Loan> findBorrowerLoansWithBalances(BorrowerUserProfile borrowerUserProfile) {
        return loanDao.findAllByAmountIsGreaterThanEqualAndBorrowerUserProfileAndClosedDateTimeIsNullOrderByFundedDateTimeAsc(BigDecimal.ZERO, borrowerUserProfile);
    }

    public BigDecimal payInterest(BigDecimal amount, Currency currency, Iterable<Investment> investments, String traceHash) {
        BigDecimal totalInterest = BigDecimal.ZERO;
        for (Investment investment : investments) {
            InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
            BigDecimal closingInterest = latestSnapshop.getClosingInterest();
            totalInterest = totalInterest.add(closingInterest);
        }

        if (totalInterest.compareTo(BigDecimal.ZERO) == 0) {
            return amount;
        }

        if (amount.compareTo(totalInterest) > 0) {
            // More money than total interest
            BigDecimal remaining = amount.subtract(totalInterest);
            // Iterate all investments, create transactions to investors of interest payments, update their balances
            for (Investment investment : investments) {
                InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
                balanceService.addInterestRepaymentToInvestor(latestSnapshop.getClosingInterest(), currency,
                        investment.getInvestorUserProfile(), traceHash);
            }
            zeroInterests(investments);
            return remaining;
        } else if (amount.compareTo(totalInterest) == 0) {
            // Equal
            // Iterate all investments, create transactions to investors of interest payments, update their balances
            for (Investment investment : investments) {
                InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
                balanceService.addInterestRepaymentToInvestor(latestSnapshop.getClosingInterest(), currency,
                        investment.getInvestorUserProfile(), traceHash);
            }
            zeroInterests(investments);
            return BigDecimal.ZERO;
        } else {
            // Less available
            // Iterate all investments, create transactions to investors of interest payments, update their balances
            try {
                divideInterests(amount, investments, traceHash);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return BigDecimal.ZERO;
        }
    }

    private void zeroInterests(Iterable<Investment> investments) {
        for (Investment investment : investments) {
            investmentService.createInterestDecrementSnapshot(BigDecimal.ZERO, investment);
        }
    }

    public void divideInterests(BigDecimal available, Iterable<Investment> investments, String traceHash) throws Exception {
        BigDecimal totalSubtracted = BigDecimal.ZERO;
        for (Investment investment : investments) {
            InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
            BigDecimal ratio = investment.getRatio();
            BigDecimal openingInterest = latestSnapshop.getClosingInterest();
            BigDecimal interestAmount = available.multiply(ratio, mathContext);
            BigDecimal closingInterest = openingInterest.subtract(interestAmount);

            balanceService.addInterestRepaymentToInvestor(interestAmount, investment.getCurrency(),
                    investment.getInvestorUserProfile(), traceHash);

            investmentService.createInterestDecrementSnapshot(closingInterest, investment);
            totalSubtracted = totalSubtracted.add(interestAmount);
        }
        if (totalSubtracted.compareTo(available) != 0) {
            throw new Exception("Subtracted amount doesn't match the available");
        }
    }
}
