package com.credifair.web.service;

import com.credifair.web.dao.InvestorUserProfileDao;
import com.credifair.web.dao.UserDao;
import com.credifair.web.model.InvestorUserProfile;
import com.credifair.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 07/02/2016.
 */
@Service
public class InvestorUserProfileService {

    @Autowired
    InvestorUserProfileDao investorUserProfileDao;

    @Autowired
    UserDao userDao;

    public InvestorUserProfile createInvestorUserProfileForUser(User user) {
        InvestorUserProfile investorUserProfile = user.getInvestorUserProfile();
        if (investorUserProfile != null) {
            return investorUserProfile;
        } else {
            InvestorUserProfile profile = new InvestorUserProfile(user);
            investorUserProfileDao.save(profile);
            user.setInvestorUserProfile(profile);
            userDao.save(user);
            return profile;
        }
    }

    public InvestorUserProfile findById(Long id) {
        return investorUserProfileDao.findOne(id);
    }
}
