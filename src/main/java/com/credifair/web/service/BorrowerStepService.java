package com.credifair.web.service;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.dao.BorrowerUserProfileDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditLimit;
import com.credifair.web.model.state.CAState;
import com.credifair.web.model.state.FundedCAState;
import com.credifair.web.model.state.PartiallyFundedCAState;
import com.credifair.web.model.state.action.CAStateAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.credifair.web.model.state.action.CAStateActionEnum;

/**
 * Created by indrek.ruubel on 19/03/2016.
 */
@Service
@Transactional
public class BorrowerStepService {

	@Autowired
	VerificationService verificationService;

	@Autowired
	BorrowerUserProfileDao borrowerUserProfileDao;

	@Autowired
	CreditApplicationService creditApplicationService;

	@Autowired
	RecipientService recipientService;

	@Autowired
	CAStateService caStateService;

	@Autowired
	PersonalFinancialDataService personalFinancialDataService;

	@Autowired
	LiabilityService liabilityService;

	@Autowired
	UserService userService;

	public CreditApplication startApplicationProcess(User user) {
		CreditApplication creditApplication = creditApplicationService.getStarted(user);
		return creditApplication;
	}

	public void setToPreparingOffer(User user) {
		caStateService.setToPreparingOffer(user.getBorrowerUserProfile());
	}

	public void setToOfferCreated(User user) {
		caStateService.setToOfferCreated(user.getBorrowerUserProfile());
	}

	public void setToWaitingForInvestors(User user) {
		caStateService.setToWaitingForInvestors(user.getBorrowerUserProfile());
	}

	public void setToProcessing(User user) {
		caStateService.setToProcessing(user.getBorrowerUserProfile());
	}

	public void putCreditApplicationOnMarket(User user) {
		caStateService.setToWaitingForInvestors(user.getBorrowerUserProfile());
	}

	public BorrowerFlowStep getStep(User user) {
		CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
		CAState state = creditApplication.getState();

		return getStateStep(state);
	}

	private BorrowerFlowStep getStateStep(CAState state) {
		String strState = state.getState();

		List<CAStateAction> listOfActions = caStateService.findActionsByState(state);
		List<CAStateActionEnum> actions = listOfActions.stream().map(CAStateAction::getAction).collect(Collectors.toList());

		if (strState.equals("NEW")) {
//			if (actions.size() == 0) {
//				return BorrowerFlowStep.GET_STARTED;
//			} else
			if (actions.size() == 0) {
				return BorrowerFlowStep.STEP1_PERSONAL_INFO;
			} else if(actions.contains(CAStateActionEnum.PROVIDED_PERSONAL_INFO) && actions.size() == 1) {
				return BorrowerFlowStep.STEP2_ADDRESS;
			} else if (actions.contains(CAStateActionEnum.PROVIDED_PERSONAL_INFO)
					&& actions.contains(CAStateActionEnum.PROVIDED_ADDRESS) && actions.size() == 2) {
				return BorrowerFlowStep.STEP3_ID_UPLOAD;
			} else if (actions.contains(CAStateActionEnum.PROVIDED_PERSONAL_INFO)
					&& actions.contains(CAStateActionEnum.PROVIDED_ADDRESS)
					&& actions.contains(CAStateActionEnum.UPLOADED_ID) && actions.size() == 3) {
				return BorrowerFlowStep.STEP4_BANK_DETAILS;
			}
		} else if (strState.equals("PREPARING_OFFER")) {
			return BorrowerFlowStep.WAITING_PROCESSING;
		} else if (strState.equals("OFFER_CREATED")) {
			return BorrowerFlowStep.WAITING_OFFER_ACCEPTED;
		} else if (strState.equals("WAITING_INVESTORS")) {
			return BorrowerFlowStep.WAITING_PROCESSING;
		} else if (strState.equals("PARTIALLY_FUNDED")) {
			return BorrowerFlowStep.DASHBOARD;
		} else if (strState.equals("FUNDED")) {
			return BorrowerFlowStep.DASHBOARD;
		}

		return BorrowerFlowStep.GET_STARTED;
	}

	public boolean canRequestLoan(User user) {
		CAState latest = caStateService.getLatestState(user);
		return (latest instanceof PartiallyFundedCAState) || (latest instanceof FundedCAState);
	}

	public Map<String, Object> getStepData(BorrowerFlowStep step) {
		User user = userService.getLoggedInUser();
		return getStepData(user, step);
	}

	public Map<String, Object> getStepData(User user, BorrowerFlowStep step) {
		Map<String, Object> data = new HashMap<>();
		List<FormFieldTypeWrapper> fields = new ArrayList<>();

		List<UserValueFieldType> userValueFieldTypes = BorrowerStepFieldData.stepFieldMap.get(step);
		if (userValueFieldTypes != null) {
			fields.addAll(userValueFieldTypes.stream().map(UserValueFieldType::wrapIt).collect(Collectors.toList()));
		}

		if (step.equals(BorrowerFlowStep.STEP1_PERSONAL_INFO)) {
			data.put("stepNumber", 0);
			data.put("formTitle", "GENERAL_INFORMATION14");
			data.put("formDescription", "GENERAL_DESC14");
		} else if (step.equals(BorrowerFlowStep.STEP2_ADDRESS)) {
			data.put("stepNumber", 0);
			data.put("formTitle", "GENERAL_INFORMATION24");
			data.put("formDescription", "GENERAL_DESC24");
			data.put("country", user.getCountry().getCountry());
		} else if (step.equals(BorrowerFlowStep.WAITING_OFFER_ACCEPTED)) {
			CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
			CreditLimit creditLimit = creditApplication.getCreditLimit();
			CreditGrade creditGrade = creditApplication.getCreditGrade();
			data.put("apr", creditGrade.getApr());
			data.put("creditLimit", creditLimit.getCreditLimit());
			data.put("gracePeriod", creditApplication.getGracePeriodDays());

			data.put("stepNumber", 1);
			data.put("formTitle", "CREDIT_OFFER");
			data.put("formDescription", "CREDIT_OFFER_DESC");
		}

//		if (step.equals(BorrowerFlowStep.APPLICATION_FORM)) {
//
//			UserProfile userProfile = user.getUserProfile();
//			BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
//			Employment employment = borrowerUserProfile.getEmployment();
//			PersonalFinancialData personalFinancialData = personalFinancialDataService.findByBorrowerUserProfile(borrowerUserProfile);
//			List<Liability> liabilities = null;
//			if (personalFinancialData != null) {
//				liabilities = liabilityService.findAllByPersonalFinancialData(personalFinancialData);
//			}
//
//			if (step1NotFinished(userProfile)) {
//				fields.add(UserValueFieldType.FIRST_NAME.wrapIt());
//				fields.add(UserValueFieldType.LAST_NAME.wrapIt());
//				fields.add(UserValueFieldType.DATE_OF_BIRTH.wrapIt());
//				fields.add(UserValueFieldType.PHONE_NUMBER.wrapIt());
//				fields.add(UserValueFieldType.EDUCATION.wrapIt());
//				fields.add(UserValueFieldType.MARITAL_STATUS.wrapIt());
//				fields.add(UserValueFieldType.COUNTRY.wrapIt());
//				fields.add(UserValueFieldType.ADDRESS.wrapIt());
//				fields.add(UserValueFieldType.CITY.wrapIt());
//				fields.add(UserValueFieldType.POSTAL_CODE.wrapIt());
//				data.put("stepNumber", 0);
//				data.put("formTitle", "GENERAL_INFORMATION14");
//				data.put("formDescription", "GENERAL_DESC14");
//			} else if (step2NotFinished(employment)) {
//				fields.add(UserValueFieldType.EMPLOYMENT_STATUS.wrapIt());
//				fields.add(UserValueFieldType.WORK_INDUSTRY.wrapIt());
//				fields.add(UserValueFieldType.CURRENT_JOB_TITLE.wrapIt());
//				fields.add(UserValueFieldType.CURRENT_EMPLOYMENT_TIME.wrapIt());
//				fields.add(UserValueFieldType.TOTAL_EMPLOYMENT_TIME.wrapIt());
//				fields.add(UserValueFieldType.CURRENT_EMPLOYER.wrapIt());
//				data.put("stepNumber", 0);
//				data.put("formTitle", "GENERAL_INFORMATION24");
//				data.put("formDescription", "GENERAL_DESC24");
//			} else if (step3NotFinished(personalFinancialData)) {
//				fields.add(UserValueFieldType.HOME_OWNERSHIP.wrapIt());
//				fields.add(UserValueFieldType.MONTHLY_NET_INCOME.wrapIt());
//				fields.add(UserValueFieldType.OTHER_REGULAR_INCOME.wrapIt());
//				fields.add(UserValueFieldType.LIVING_EXPENSES.wrapIt());
//				fields.add(UserValueFieldType.WILL_YOU_PROVIDE_VERIFICATION_OF_YOUR_INCOME_AND_EXPENSES.wrapIt());
//				data.put("stepNumber", 0);
//				data.put("formTitle", "GENERAL_INFORMATION34");
//				data.put("formDescription", "GENERAL_DESC34");
//			} else if (step4NotFinished(liabilities)) {
//				fields.add(UserValueFieldType.LIABILITIES.wrapIt());
//				data.put("stepNumber", 0);
//				data.put("formTitle", "GENERAL_INFORMATION44");
//				data.put("formDescription", "GENERAL_DESC44");
//			}
//
//		}else if (step.equals(BorrowerFlowStep.WAITING_FOR_OFFER)) {
//			data.put("stepNumber", 1);
//			data.put("formTitle", "CREDIT_OFFER");
//			data.put("formDescription", "CREDIT_OFFER_DESC");
//		} else if (step.equals(BorrowerFlowStep.WAITING_OFFER_ACCEPTED)) {
//			CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
//			CreditGrade creditGrade = creditApplication.getCreditGrade();
//			data.put("apr", creditGrade.getApr());
//			data.put("creditLimit", creditApplication.getCreditLimit());
//			data.put("gracePeriod", creditApplication.getGracePeriodDays());
//
//			data.put("stepNumber", 1);
//			data.put("formTitle", "CREDIT_OFFER");
//			data.put("formDescription", "CREDIT_OFFER_DESC");
//		} else if(step.equals(BorrowerFlowStep.WAITING_PROCESSING)) {
//			data.put("stepNumber", 3);
//			data.put("formTitle", "PROCESSING");
//			data.put("formDescription", "PROCESSING_DESC");
//		}
		data.put("fields", fields);
		return data;
	}

	private boolean step1NotFinished(UserProfile userProfile) {

		if (userProfile.getFirstName() == null || userProfile.getLastName() == null
				|| userProfile.getDateOfBirth() == null || userProfile.getPhoneNumber() == null
				|| userProfile.getEducation() == null || userProfile.getMaritalStatus() == null
				|| userProfile.getAddress() == null) {
			return true;
		}
		return false;
	}

	private boolean step2NotFinished(Employment employment) {
		if (employment == null) {
			return true;
		}
		if (employment.getEmploymentStatus() == null
				|| employment.getWorkIndustry() == null
				|| employment.getJobTitle() == null || employment.getCurrentEmploymentTime() == null
				|| employment.getTotalEmploymentTime() == null
				|| employment.getCurrentEmployer() == null) {
			return true;
		}

		return false;
	}

	private boolean step3NotFinished(PersonalFinancialData personalFinancialData) {
		if (personalFinancialData == null) {
			return true;
		}
		if (personalFinancialData.getHomeOwnership() == null
				|| personalFinancialData.getMonthlyNetIncome() == null
				|| personalFinancialData.getOtherRegularIncome() == null
				|| personalFinancialData.getLivingExpenses() == null
				|| personalFinancialData.getWillProvideDocumentsOfIncomeAndExpenses() == null) {
			return true;
		}
		return false;
	}

	private boolean step4NotFinished(List<Liability> liabilities) {
		if (liabilities.isEmpty()) {
			return true;
		}
		return false;
	}
}
