package com.credifair.web.service;

import com.credifair.web.dao.InvestmentDao;
import com.credifair.web.dao.LoanDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.MathContext;
import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 29/01/2016.
 */
@Service
public class LoanService {

    @Autowired
    LoanDao loanDao;

    @Autowired
    InvestmentDao investmentDao;

    @Autowired
    LoanBalanceService loanBalanceService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    BalanceService balanceService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Autowired
    InvestmentService investmentService;

    @Value(value = "${minimal.investment.amount}")
    BigDecimal minimalAmount;

    @Autowired
    MathContext mathContext;

    @Autowired
    PayoutService payoutService;

    @Autowired
    AutowireService autowireService;

    public synchronized Loan createLoanRequest(BigDecimal amount, BorrowerUserProfile borrowerUserProfile) throws Exception {
        CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
        if (creditApplication == null || creditApplication.getCreditGrade() == null) {
            throw new Exception("No credit application or credit grade found");
        }
        Currency currency = creditApplication.getCurrency();
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        Loan loan = new Loan(amount, creditGrade.getCreditGradeConstructor(), currency, borrowerUserProfile, creditGrade.getApr());
        loanDao.save(loan);
        return loan;
    }

    public synchronized Loan createLoanRequest(BigDecimal amount, User user) throws Exception {
        return createLoanRequest(amount, user.getBorrowerUserProfile());
    }

    @Transactional(rollbackOn = Exception.class)
    public synchronized Loan requestAndFundLoan(BigDecimal requestedAmount, BorrowerUserProfile borrowerUserProfile) throws Exception {
        if (requestedAmount.compareTo(minimalAmount) < 0) {
            // Less or equal than minimal
            Country country = borrowerUserProfile.getUser().getCountry();
            throw new Exception(String.format("Sorry, but the minimal amount you can borrow at once is %s%s", minimalAmount, country.getCurrency().getSymbol()));
        }
        Map<InvestorUserProfile, BigDecimal> allAvailable = creditApplicationService.getAvailableAmountFromInvestors(borrowerUserProfile);
        BigDecimal totalAvailable = allAvailable.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);

        if (totalAvailable.compareTo(requestedAmount) < 0) {
            throw new Exception("Not enough investor money to fund the request");
        }

        // All guys listed have at least minimal amount here
        // Minimal is 10
        // First guy has 35
        // Second guy has 70
        // Split it!
        // Strategy: Iterate each investor in 1 whole increments,
        // until you reach none of the investors have a whole left anymore, than split the leftover by ratio

        BigDecimal wholes = requestedAmount.divide(minimalAmount);
        BigDecimal leftOver = requestedAmount;

        int wholeCount = wholes.intValue();

        while (wholeCount > 0) {
            // While we have a whole to share
            for (Map.Entry<InvestorUserProfile, BigDecimal> investorUserProfileBigDecimalEntry : allAvailable.entrySet()) {
                if (wholeCount == 0) {
                    continue;
                }
                InvestorUserProfile investor = investorUserProfileBigDecimalEntry.getKey();
                BigDecimal investorLeftAmount = investorUserProfileBigDecimalEntry.getValue();
                if (investorLeftAmount.compareTo(minimalAmount) >= 0) {
                    // Leftover is larger than minimal unit, give a share
                    investorLeftAmount = investorLeftAmount.subtract(minimalAmount);

                    BigDecimal invested = (investor.getInvestmentHolder() == null ? BigDecimal.ZERO : investor.getInvestmentHolder());
                    invested = invested.add(minimalAmount);
                    investor.setInvestmentHolder(invested);
                    leftOver = leftOver.subtract(minimalAmount);

                    allAvailable.put(investor, investorLeftAmount);
                    // Subtract 1 whole
                    wholeCount--;
                }
            }

        }


        if (leftOver.compareTo(BigDecimal.ZERO) > 0) {
            // There's still leftover under whole
            // Find first guy that has enough leftOver and sums more than minimal unit
            Map.Entry<InvestorUserProfile, BigDecimal> entry = null;

            for (Map.Entry<InvestorUserProfile, BigDecimal> investorUserProfileBigDecimalEntry : allAvailable.entrySet()) {
                BigDecimal amount = investorUserProfileBigDecimalEntry.getValue();
                if (amount.compareTo(leftOver) >= 0 && amount.compareTo(minimalAmount) >= 0) {
                    entry = investorUserProfileBigDecimalEntry;
                    break;
                }
            }

            if (entry != null) {
                InvestorUserProfile investor = entry.getKey();
                BigDecimal amountLeft = entry.getValue();

                amountLeft = amountLeft.subtract(leftOver);
                BigDecimal invested = (investor.getInvestmentHolder() == null ? BigDecimal.ZERO : investor.getInvestmentHolder());
                invested = invested.add(leftOver);
                investor.setInvestmentHolder(invested);
                allAvailable.put(investor, amountLeft);
            }
        }

        // We have the splits, do investments now
        Loan loanRequest = createLoanRequest(requestedAmount, borrowerUserProfile);
        for (InvestorUserProfile investorUserProfile : allAvailable.keySet()) {
            if (investorUserProfile.getInvestmentHolder().compareTo(BigDecimal.ZERO) > 0) {
                investmentService.invest(investorUserProfile.getInvestmentHolder(), loanRequest, investorUserProfile);
            }
        }

        // Setup payout instruction
        payoutService.createTargetInstruction(loanRequest, borrowerUserProfile.getUser());

        return loanRequest;
    }

    public Loan findById(Long id) {
        return loanDao.findOne(id);
    }

    public boolean isLoanFunded(Loan loan) {
        BigDecimal neededAmount = loan.getAmount();
        BigDecimal collected = BigDecimal.ZERO;
        List<Investment> investments = investmentDao.findAllByLoan(loan);
        for (Investment investment : investments) {
            collected = collected.add(investment.getLatestSnapshop().getClosingPrincipal());
        }

        if (neededAmount.compareTo(collected) == 0) {
            return true;
        } else if (neededAmount.compareTo(collected) < 0) {
            System.out.println("More than needed for funding collected");
            return false;
        } else {
            return false;
        }
    }

    public Iterable<Investment> findActiveInvestmentsWithPrincipal(InvestorUserProfile investorUserProfile, BorrowerUserProfile borrowerUserProfile) {
        // These are ongoing investments with deployed funds, we find out how much of creditApplicationInvestor amount is deployed
        Iterable<Investment> loanInvestments =
                investmentDao.findAllByInvestorUserProfileAndLoanBorrowerUserProfileWhereClosedDateTimeIsNotNullAndPrincipalGraterThan0(investorUserProfile.getId(), borrowerUserProfile.getId());
        return loanInvestments;
    }

    public void setFundedDateTimeToLoan(Loan loan) {
        loan.setFundedDateTime(LocalDateTime.now());
        loanDao.save(loan);
    }

    public void save(Loan loan) {
        loanDao.save(loan);
    }

    public Loan findFirstActiveLoanWithPrincipal(BorrowerUserProfile borrowerUserProfile) {
        return loanDao.findTopByFundedDateTimeIsNotNullAndBorrowerUserProfileAndClosedDateTimeIsNullOrderByFundedDateTimeAsc(borrowerUserProfile);
    }

//    public Loan findLastActiveWithPrincipal(BorrowerUserProfile borrowerUserProfile) {
//        return loanDao.findTopByFundedDateTimeIsNotNullAndBorrowerUserProfileAndClosedDateTimeIsNullOrderByFundedDateTimeDesc(borrowerUserProfile);
//    }
}
