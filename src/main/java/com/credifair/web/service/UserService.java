package com.credifair.web.service;

import com.credifair.web.dao.BorrowerUserProfileDao;
import com.credifair.web.dao.InvestorUserProfileDao;
import com.credifair.web.dao.UserDao;
import com.credifair.web.dao.UserProfileDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditLimit;
import com.credifair.web.model.recipient.Recipient;
import com.credifair.web.model.recipient.RecipientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCrypt;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by indrek.ruubel on 13/01/2016.
 */
@Transactional
@Service
public class UserService {

    @Autowired
    private UserDao userDao;

    @Autowired
    private RoleService roleService;

    @Autowired
    private LoanBalanceService loanBalanceService;

    @Autowired
    BalanceService balanceService;

    @Autowired
    BorrowerUserProfileDao borrowerUserProfileDao;

    @Autowired
    InvestorUserProfileDao investorUserProfileDao;

    @Autowired
    UserProfileDao userProfileDao;

    @Autowired
    VerificationService verificationService;

    @Autowired
    BorrowerStepService borrowerStepService;

    @Autowired
    InvestorStepService investorStepService;

    @Autowired
    LiabilityService liabilityService;

    @Autowired
    RecipientService recipientService;

    @Autowired
    CreditApplicationService creditApplicationService;

    @Value("${cf.default.country}")
    private Country defaultCountry;

    public UserService() {
    }

    public UserService(UserDao userDao, RoleService roleService, BorrowerUserProfileDao borrowerUserProfileDao, InvestorUserProfileDao investorUserProfileDao, UserProfileDao userProfileDao) {
        this.userDao = userDao;
        this.roleService = roleService;
        this.borrowerUserProfileDao = borrowerUserProfileDao;
        this.investorUserProfileDao = investorUserProfileDao;
        this.userProfileDao = userProfileDao;
    }

    private static String ROLE_USER = "ROLE_USER";

    public User saveCustomer(String email, String password) {
        return saveCustomer(email, password, defaultCountry);
    }

    public User saveCustomer(String email, String password, Country country) {
        User user = userDao.findByEmail(email);
        if (user == null) {
            BorrowerUserProfile borrowerUserProfile = createBorrowerProfile();
            InvestorUserProfile investorUserProfile = createInvestorProfile();
            UserProfile userProfile = createUserProfile();
            user = new User(email, BCrypt.hashpw(password, BCrypt.gensalt()), new ArrayList<>(), new Date(), new Date(), borrowerUserProfile, investorUserProfile, userProfile, country);
            Role roleBorrower = roleService.findByRole(ROLE_USER);
            if (roleBorrower == null) {
                roleBorrower = roleService.saveRole(ROLE_USER);
            }
            user.getRoles().add(roleBorrower);
            userDao.save(user);
            creditApplicationService.createCreditApplication(user, country.getCurrency());

            return user;
        }
        return null;
    }

    public void addRoleToUser(User user, String roleStr) {
        Role role = roleService.findByRole(roleStr);
        if (role == null) {
            role = roleService.saveRole(roleStr);
        }
        user.getRoles().add(role);
        userDao.save(user);
    }

    private BorrowerUserProfile createBorrowerProfile() {
        BorrowerUserProfile borrowerUserProfile = new BorrowerUserProfile();
        borrowerUserProfileDao.save(borrowerUserProfile);
        return borrowerUserProfile;
    }

    private InvestorUserProfile createInvestorProfile() {
        InvestorUserProfile investorUserProfile = new InvestorUserProfile();
        investorUserProfileDao.save(investorUserProfile);
        return investorUserProfile;
    }

    private UserProfile createUserProfile() {
        UserProfile userProfile = new UserProfile();
        userProfileDao.save(userProfile);
        return userProfile;
    }

    public Map<String, Object> getCustomerInfo() {
        User user = getLoggedInUser();
        return getCustomerInfo(user);
    }

    public Map<String, Object> getCustomerInfo(User user) {
        Map<String, Object> out = new HashMap<>();
        if (user == null) {
            return out;
        }

        UserProfile userProfile = user.getUserProfile();
        LoanBalance latestLoanBalance = getLatestLoanBalance(user);
        CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
        CreditLimit creditLimit = creditApplication.getCreditLimit();
        CreditGrade creditGrade = creditApplication.getCreditGrade();
        if (creditGrade != null) {
            String aprText = (creditGrade.getApr().multiply(new BigDecimal(100))).toBigInteger().toString();
            out.put("apr", creditGrade.getApr());
            out.put("aprText", aprText + "%");
        }

        Currency currency = user.getCountry().getCurrency();
        BigDecimal loanBalance = latestLoanBalance == null ? BigDecimal.ZERO : latestLoanBalance.getClosingPrincipal();
        String loanBalanceText =
                String.format("%s %s", currency.getSymbol(), loanBalance.setScale(2, RoundingMode.HALF_UP).toString());
        Recipient recipientAccount = recipientService.getRecipientAccount(user);
        HashMap<String, String> recipient = transformRecipient(recipientAccount);

        BigDecimal liveCreditLimit = creditApplicationService.getLiveCreditLimit(user.getBorrowerUserProfile());
        String liveCreditLimitText =
                String.format("%s %s", currency.getSymbol(), liveCreditLimit.setScale(2, RoundingMode.HALF_DOWN).toString());

        out.put("firstName", userProfile.getFirstName() == null ? "" : userProfile.getFirstName());
        out.put("lastName", userProfile.getLastName() == null ? "" : userProfile.getLastName());
        out.put("loanBalance", loanBalance);
        out.put("loanBalanceText", loanBalanceText);
        out.put("gracePeriodDays", creditApplication.getGracePeriodDays());
        out.put("creditLimitMax", creditLimit.getCreditLimit());
        out.put("creditLimitLive", liveCreditLimit);
        out.put("creditLimitLiveText", liveCreditLimitText);
        out.put("currency", currency);
        out.put("currencySymbol", currency.getSymbol());
        out.put("recipient", recipient);

        return out;
    }

    private HashMap<String, String> transformRecipient(Recipient recipient) {
        if (recipient == null) {
            return null;
        }
        HashMap<String, String> map = new HashMap<>();
        if (recipient.getType().equals(RecipientType.IBAN)) {
            map.put("type", recipient.getType().toString());
            map.put("iban", recipient.getIban());
        } else if (recipient.getType().equals(RecipientType.SORT)) {
            map.put("type", recipient.getType().toString());
            map.put("sort", recipient.getSortCode());
            map.put("accountNumber", recipient.getAccountNumber());
        }
        return map;
    }

    public User getLoggedInUser() {
        Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (principal instanceof org.springframework.security.core.userdetails.User) {
            org.springframework.security.core.userdetails.User user = (org.springframework.security.core.userdetails.User) principal;
            return userDao.findByEmail(user.getUsername());
        } else {
            return null;
        }
    }

    public LoanBalance getLatestLoanBalance(User user) {
        return loanBalanceService.getLatestLoanBalanceForBorrower(user.getBorrowerUserProfile());
    }

    public Balance getLatestBalance(User user) {
        return balanceService.getLatestBalance(user.getUserProfile());
    }

    public User getUserById(Long userId) {
        return userDao.findOne(userId);
    }

    public Iterable<User> getAllUsers() {
        return userDao.findAll();
    }

    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }

    public User findByUserProfile(UserProfile userProfile) {
        return userDao.findByUserProfile(userProfile);
    }
}
