package com.credifair.web.service;

import com.credifair.web.dao.InvestmentDao;
import com.credifair.web.dao.InvestmentSnapshotDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditApplicationInvestor;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditGradeConstructor;
import com.credifair.web.model.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Service
public class InvestmentService {

	@Autowired
	InvestmentDao investmentDao;

	@Autowired
	InvestmentSnapshotDao investmentSnapshotDao;

	@Autowired
	BalanceService balanceService;

	@Autowired
	LoanBalanceService loanBalanceService;

	@Autowired
	CreditApplicationService creditApplicationService;

	@Autowired
	LoanService loanService;

	@Value(value = "${minimal.investment.amount}")
	private BigDecimal minimalInvestmentAmount;

	@Autowired
	UserService userService;

	@Autowired
	InvestorBalanceService investorBalanceService;

	@Autowired
	CAStateService CAStateService;

	@Autowired
	TransactionService transactionService;

	public synchronized Investment invest(BigDecimal amount, Loan loan, InvestorUserProfile investorInvestorUserProfile) throws Exception {
		if (amount.compareTo(minimalInvestmentAmount) < 0) {
			// Less or equal than minimal
			throw new Exception(String.format("Sorry, but the minimal amount you can borrow at once is %s%s", minimalInvestmentAmount, loan.getCurrency().getSymbol()));
		}
		User investorUser = investorInvestorUserProfile.getUser();
		UserProfile investorUserProfile = investorUser.getUserProfile();
		if (!balanceService.hasEnoughMoneyOnBalance(investorUserProfile, amount)) {
			throw new Exception("Not enough available funds");
		}
		// Tracehash
		String traceHash = transactionService.generateTraceHash();

		// Get the borrower's grade
		BorrowerUserProfile borrowerUserProfile = loan.getBorrowerUserProfile();
		CreditApplication creditApplication = creditApplicationService.getCreditApplication(borrowerUserProfile.getUser());
		CreditGrade creditGrade = creditApplication.getCreditGrade();
		if (creditGrade == null) {
			throw new Exception("No credit grade assgined");
		}

		CreditGradeConstructor creditGradeConstructor = creditGrade.getCreditGradeConstructor();

		// Create new investment
		Investment investment = new Investment(loan, creditGradeConstructor, loan.getCurrency(), investorInvestorUserProfile);
		investmentDao.save(investment);

		// Create new snapshot of investment balance
		InvestmentSnapshop investmentSnapshop =
				new InvestmentSnapshop(BigDecimal.ZERO, BigDecimal.ZERO,
						BigDecimal.ZERO, amount, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, investment,
						InvestmentSnapshop.DifferenceType.INCREMENTED_PRINCIPAL);
		investmentSnapshotDao.save(investmentSnapshop);
		investment.setLatestSnapshop(investmentSnapshop);
		investmentDao.save(investment);

		// Subtract from balance
		Balance latestBalance = balanceService.subtractInvestment(amount, loan.getCurrency(), investorUserProfile);
		Transaction investmentTransaction = transactionService.createInvestmentTransaction(amount, loan.getCurrency(), investorUser, traceHash);
		transactionService.createBalanceTransactionLink(investmentTransaction, latestBalance.getId());
		transactionService.createInvestmentSnapshotTransactionLink(investmentTransaction, investmentSnapshop.getId());

		// Add capital deployed to investorBalance
		investorBalanceService.addCapitalDeployed(amount, loan.getCurrency(), investorInvestorUserProfile, traceHash);

		if (loanService.isLoanFunded(loan)) {
			// Transfer money to borrower, update loanBalance
			loanBalanceService.addPrincipal(loan, traceHash);
			loanService.setFundedDateTimeToLoan(loan);
		}

		return investment;
	}

	public synchronized CreditApplicationInvestor reserv(Long creditApplicationId, BigDecimal amount, User investorUser) throws Exception {
		CreditApplication creditApplication = creditApplicationService.getApplicationById(creditApplicationId);
		return reserv(creditApplication, amount, investorUser);
	}

	public synchronized CreditApplicationInvestor reserv(CreditApplication creditApplication, BigDecimal amount, User investorUser) throws Exception {
		if (amount.compareTo(minimalInvestmentAmount) < 0) {
			throw new Exception("Minimal amount is " + minimalInvestmentAmount);
		}

		if (creditApplication != null) {
			boolean hasMoney = balanceService.hasEnoughMoneyOnBalance(investorUser.getUserProfile(), amount);
			if (!hasMoney) {
				throw new Exception("Not enough money on balance");
			}
			InvestorUserProfile investorUserProfile = investorUser.getInvestorUserProfile();
			CreditApplicationInvestor creditApplicationInvestor = creditApplicationService.findByInvestorUserProfileAndCreditApplication(investorUserProfile, creditApplication);
			BigDecimal fundingNeeded = creditApplicationService.calculateFundingLeft(creditApplication);
			if (creditApplicationInvestor == null) {
				// No prior investment to this CA

				if (amount.compareTo(fundingNeeded) > 0) {
					// Too much funding provided
					throw new Exception("Too much funding provided");
				} else if (amount.compareTo(fundingNeeded) == 0) {
					// Exact amount
					creditApplicationInvestor = new CreditApplicationInvestor(investorUserProfile,
							creditApplication, amount, LocalDateTime.now());
					creditApplicationService.save(creditApplicationInvestor);
					CAStateService.setToFundedCAState(creditApplication.getBorrowerUserProfile());
					return creditApplicationInvestor;
				} else {
					// Less than needed
					creditApplicationInvestor = new CreditApplicationInvestor(investorUserProfile,
							creditApplication, amount, LocalDateTime.now());
					creditApplicationService.save(creditApplicationInvestor);
					CAStateService.setToPartiallyFundedCAState(creditApplication.getBorrowerUserProfile());
					return creditApplicationInvestor;
				}
			} else {
				throw new Exception("Already funded");
			}
		} else {
			throw new Exception("No credit application found");
		}
	}

	public Iterable<Investment> findAllActiveInvestmentsOnBorrowerOrderByAscending(BorrowerUserProfile borrowerUserProfile) {
		return investmentDao.findAllActiveInvestmentsOnBorrowerOrderByAscending(borrowerUserProfile.getId());
	}

	public InvestmentSnapshop createFeeDecrementSnapshot(BigDecimal closingFee, Investment investment, String traceHash) {
		InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
		// Interest is not changed, so copy paste
		BigDecimal openingInterest = latestSnapshop.getOpeningInterest();
		BigDecimal closingInterest = latestSnapshop.getClosingInterest();
		// Fee is changed, so last closing will become new opening
		BigDecimal openingFee = latestSnapshop.getClosingFee();
		// Principals copy paste
		BigDecimal openingPrincipal = latestSnapshop.getOpeningPrincipal();
		BigDecimal closingPrincipal = latestSnapshop.getClosingPrincipal();

		BigDecimal totalInterestPayed = latestSnapshop.getTotalInterestPayed();
		BigDecimal totalPrincipalPayed = latestSnapshop.getTotalPrincipalPayed();

		BigDecimal feePayed = openingFee.subtract(closingFee);
		BigDecimal totalFeePayed = latestSnapshop.getTotalFeePayed();
		totalFeePayed = totalFeePayed.add(feePayed);

		InvestmentSnapshop snapshop = new InvestmentSnapshop(
				openingInterest, closingInterest,
				openingPrincipal, closingPrincipal,
				openingFee, closingFee, totalFeePayed, totalInterestPayed, totalPrincipalPayed, investment, InvestmentSnapshop.DifferenceType.DECREMENTED_FEE);
		investmentSnapshotDao.save(snapshop);
		investment.setLatestSnapshop(snapshop);
		investmentDao.save(investment);

		// Create fee transaction and link it to this snapshot
		Transaction transaction = transactionService.createCFFeeTransaction(feePayed, investment.getCurrency(), investment.getLoan().getBorrowerUserProfile().getUser(), traceHash);
		transactionService.createInvestmentSnapshotTransactionLink(transaction, snapshop.getId());
		return snapshop;
	}

	public InvestmentSnapshop createInterestDecrementSnapshot(BigDecimal closingInterest, Investment investment) {
		InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
		// Interest is changed, take last closing to be the new opening
		BigDecimal openingInterest = latestSnapshop.getClosingInterest();
		// Fee is copy paste
		BigDecimal openingFee = latestSnapshop.getOpeningFee();
		BigDecimal closingFee = latestSnapshop.getClosingFee();
		// Principal is copy paste
		BigDecimal openingPrincipal = latestSnapshop.getOpeningPrincipal();
		BigDecimal closingPrincipal = latestSnapshop.getClosingPrincipal();

		BigDecimal totalFeePayed = latestSnapshop.getTotalFeePayed();
		BigDecimal totalPrincipalPayed = latestSnapshop.getTotalPrincipalPayed();

		BigDecimal interestPayed = openingInterest.subtract(closingInterest);
		BigDecimal totalInterestPayed = latestSnapshop.getTotalInterestPayed();
		totalInterestPayed = totalInterestPayed.add(interestPayed);

		InvestmentSnapshop snapshop = new InvestmentSnapshop(
				openingInterest, closingInterest,
				openingPrincipal, closingPrincipal,
				openingFee, closingFee, totalFeePayed, totalInterestPayed, totalPrincipalPayed, investment, InvestmentSnapshop.DifferenceType.DECREMENTED_INTEREST);
		investmentSnapshotDao.save(snapshop);
		investment.setLatestSnapshop(snapshop);
		investmentDao.save(investment);
		return snapshop;
	}

	public InvestmentSnapshop createPrincipalDecrementSnapshot(BigDecimal closingPrincipal, Investment investment) {
		InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
		// Interest is not changed, copy paste
		BigDecimal openingInterest = latestSnapshop.getOpeningInterest();
		BigDecimal closingInterest = latestSnapshop.getClosingInterest();
		// Fee is not changed, copy paste
		BigDecimal openingFee = latestSnapshop.getOpeningFee();
		BigDecimal closingFee = latestSnapshop.getClosingFee();
		// Principals last closing will become new opening
		BigDecimal openingPrincipal = latestSnapshop.getClosingPrincipal();

		BigDecimal totalFeePayed = latestSnapshop.getTotalFeePayed();
		BigDecimal totalInterestPayed = latestSnapshop.getTotalInterestPayed();

		BigDecimal principalPayed = openingPrincipal.subtract(closingPrincipal);
		BigDecimal totalPrincipalPayed = latestSnapshop.getTotalPrincipalPayed();
		totalPrincipalPayed = totalPrincipalPayed.add(principalPayed);

		InvestmentSnapshop snapshop = new InvestmentSnapshop(
				openingInterest, closingInterest,
				openingPrincipal, closingPrincipal,
				openingFee, closingFee, totalFeePayed, totalInterestPayed, totalPrincipalPayed, investment, InvestmentSnapshop.DifferenceType.DECREMENTED_PRINCIPAL);
		investmentSnapshotDao.save(snapshop);
		investment.setLatestSnapshop(snapshop);
		investmentDao.save(investment);
		return snapshop;
	}

	public void closeInvestment(Investment investment) {
		investment.setClosedDateTime(LocalDateTime.now());
		investmentDao.save(investment);
	}

	public void closeLoan(Loan loan) {
		loan.setClosedDateTime(LocalDateTime.now());
		loanService.save(loan);
	}

	public Iterable<Investment> findAllByInvestorUserProfile(InvestorUserProfile investor) {
		return investmentDao.findAllByInvestorUserProfile(investor);
	}

	public List<Investment> findAllActiveInvestmentsForInvestors(InvestorUserProfile ... investorUserProfiles) {
		List<Long> ids = new ArrayList<>();
		for (InvestorUserProfile investorUserProfile : investorUserProfiles) {
			ids.add(investorUserProfile.getId());
		}
		return investmentDao.findAllActiveInvestmentsForInvestors(ids);
	}

	public Iterable<Investment> findAllActiveInvestmentsForInvestorAndBorrower(InvestorUserProfile investorUserProfile, BorrowerUserProfile borrowerUserProfile) {
		return investmentDao.findAllByInvestorUserProfileAndLoanBorrowerUserProfileWhereClosedDateTimeIsNotNullAndPrincipalGraterThan0(investorUserProfile.getId(), borrowerUserProfile.getId());
	}

	public List<Investment> findAllByLoan(Loan loan) {
		return investmentDao.findAllByLoan(loan);
	}
}
