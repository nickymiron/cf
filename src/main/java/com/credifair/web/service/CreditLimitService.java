package com.credifair.web.service;

import com.credifair.web.dao.CreditApplicationDao;
import com.credifair.web.dao.CreditLimitDao;
import com.credifair.web.model.User;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditLimit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
@Service
public class CreditLimitService {

	@Autowired
	CreditLimitDao creditLimitDao;

	@Autowired
	CreditApplicationDao creditApplicationDao;

	BigDecimal defaultInitialLimit = BigDecimal.ZERO;

	public CreditLimit createInitialCreditLimit(CreditApplication creditApplication, User createdBy) {
		CreditLimit creditLimit = new CreditLimit(defaultInitialLimit, creditApplication, createdBy);
		creditLimitDao.save(creditLimit);
		return creditLimit;
	}

	public CreditLimit assignNewCreditLimit(CreditApplication creditApplication, BigDecimal limit, User createdBy) {
		CreditLimit creditLimit = new CreditLimit(limit, creditApplication, createdBy);
		creditLimitDao.save(creditLimit);
		creditApplication.setCreditLimit(creditLimit);
		creditApplicationDao.save(creditApplication);
		return creditLimit;
	}

	public CreditLimit getCreditLimit(CreditApplication creditApplication) {
		return creditLimitDao.findTopByCreditApplicationOrderByIdDesc(creditApplication);
	}
}
