package com.credifair.web.service;

import com.credifair.web.dao.InvestmentSnapshotDao;
import com.credifair.web.model.*;
import com.credifair.web.model.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 23/04/2016.
 */
@Service
public class InvestorStatsService {

	@Autowired
	BalanceService balanceService;

	@Autowired
	InvestmentService investmentService;

	@Autowired
	InvestorStateService investorStateService;

	@Autowired
	TransactionService transactionService;

	@Autowired
	InvestmentSnapshotDao investmentSnapshotDao;

	@Value(value = "${cf.percentage.fee}")
	BigDecimal cfFeePercentage;

	@Autowired
	InvestorBalanceService investorBalanceService;

	public Map<String, Object> getGeneralStats(User user) {
		InvestorUserProfile investorUserProfile = user.getInvestorUserProfile();
		UserProfile userProfile = user.getUserProfile();
		Map<String, Object> data = new HashMap<>();

		List<Investment> activeInvestments = investmentService.findAllActiveInvestmentsForInvestors(investorUserProfile);
		BigDecimal outstandingPrincipal = BigDecimal.ZERO;
		for (Investment activeInvestment : activeInvestments) {
			InvestmentSnapshop latestSnapshop = activeInvestment.getLatestSnapshop();
			outstandingPrincipal = outstandingPrincipal.add(latestSnapshop.getClosingPrincipal());
		}

		List<Transaction> deposits = transactionService.getAllUserTransactionsByType(user, "DEPOSIT");
		BigDecimal totalDeposits = BigDecimal.ZERO;
		for (Transaction deposit : deposits) {
			totalDeposits = totalDeposits.add(deposit.getAmount());
		}

		List<Transaction> withdraws = transactionService.getAllUserTransactionsByType(user, "WITHDRAW");
		BigDecimal totalWithdraws = BigDecimal.ZERO;
		for (Transaction withdraw : withdraws) {
			totalWithdraws = totalWithdraws.add(withdraw.getAmount());
		}

		String message = investorStateService.additionalInformationNeeded(investorUserProfile);
		data.put("additionalInformationNeededMessage", message);

		Balance latestBalance = balanceService.getLatestBalance(userProfile);
		if (latestBalance != null) {
			data.put("firstName", userProfile.getFirstName());
			data.put("availableFunds", latestBalance.getClosingBalance());
			data.put("currency", latestBalance.getCurrency().getSymbol()); // TODO : Figure out more generic place
			// Account value
			// Account value is comprised of the following calculation:
			// Available funds + Reserved funds + Outstanding principal - Overdue principal
			BigDecimal accountValue = latestBalance.getClosingBalance().add(outstandingPrincipal);
			data.put("accountValue", accountValue);
			// Net profit is comprised of the following calculation: Account value - Deposits + Withdrawals
			BigDecimal netProfit = (accountValue.subtract(totalDeposits)).add(totalWithdraws);
			data.put("netProfit", netProfit);
			data.put("deposits", totalDeposits);
			data.put("withdraws", totalWithdraws);
			data.put("outstandingPrincipal", outstandingPrincipal);
		}
		return data;
	}

	public List<Map<String, Object>> getInvestments(InvestorUserProfile investorUserProfile) {

		List<Map<String, Object>> data = new ArrayList<>();

		Iterable<Investment> investments = investmentService.findAllByInvestorUserProfile(investorUserProfile);

		for (Investment investment : investments) {
			Loan loan = investment.getLoan();
			Map<String, Object> obj = new HashMap<>();
			InvestmentSnapshop initial = investmentSnapshotDao.findTopByInvestmentOrderByIdAsc(investment);
			InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
			obj.put("investment", initial.getClosingPrincipal());
			obj.put("cfRating", investment.getGrade());
			obj.put("currency", investment.getCurrency());

			String dateInvested = investment.getInvestedDateTime().format(DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm"));
			obj.put("dateInvested", dateInvested);
			obj.put("status", loan.getClosedDateTime() == null ? "ACTIVE" : "CLOSED");
			obj.put("apr", loan.getApr());
			obj.put("investorApr", loan.getApr().subtract(cfFeePercentage));
			obj.put("interestOwed", latestSnapshop.getClosingInterest());
			obj.put("outstandingPrincipal", latestSnapshop.getClosingPrincipal());
			obj.put("totalRepaidPrincipal", latestSnapshop.getTotalPrincipalPayed());
			obj.put("totalRepaidInterest", latestSnapshop.getTotalInterestPayed());
			data.add(obj);
		}

		return data;
	}
}
