package com.credifair.web.service;

import com.credifair.web.dao.UserProfileDao;
import com.credifair.web.model.UserProfile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;

/**
 * Created by indrek.ruubel on 03/05/2016.
 */
@Service
public class UserProfileService {

	@Autowired
	UserProfileDao userProfileDao;


	public void setBirthDate(LocalDate date, UserProfile userProfile) {
		userProfile.setDateOfBirth(date);
		userProfileDao.save(userProfile);
	}

}
