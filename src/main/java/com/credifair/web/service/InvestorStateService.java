package com.credifair.web.service;

import com.credifair.web.dao.InvestorStateDao;
import com.credifair.web.dao.InvestorUserProfileDao;
import com.credifair.web.model.InvestorFlowStep;
import com.credifair.web.model.InvestorUserProfile;
import com.credifair.web.model.UserProfile;
import com.credifair.web.model.investor.state.InvestorState;
import com.credifair.web.model.investor.state.NewInvestorState;
import com.credifair.web.model.investor.state.VerifiedInvestorState;
import com.credifair.web.model.investor.state.WaitingFirstDepositInvestorState;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 13/04/2016.
 */
@Service
public class InvestorStateService {

	@Autowired
	InvestorStateDao investorStateDao;

	@Autowired
	InvestorStepService investorStepService;

	@Autowired
	InvestorUserProfileDao investorUserProfileDao;
	private InvestorUserProfile toVerifiedInvestorState;

	public InvestorState createVerifiedState(InvestorUserProfile investorUserProfile) {
		VerifiedInvestorState newState = new VerifiedInvestorState(investorUserProfile);
		investorStateDao.save(newState);
		return newState;
	}

	public InvestorState createNewState(InvestorUserProfile investorUserProfile) {
		NewInvestorState newState = new NewInvestorState(investorUserProfile);
		investorStateDao.save(newState);
		return newState;
	}

	public InvestorState createWaitingFirstDepositState(InvestorUserProfile investorUserProfile) {
		WaitingFirstDepositInvestorState newState = new WaitingFirstDepositInvestorState(investorUserProfile);
		investorStateDao.save(newState);
		return newState;
	}

	public InvestorState setToNewInvestorState(InvestorUserProfile investorUserProfile) {
		InvestorState newState = createNewState(investorUserProfile);
		investorUserProfile.setState(newState);
		investorUserProfileDao.save(investorUserProfile);
		return newState;
	}

	public InvestorState setToWaitingFirstDepositInvestorState(InvestorUserProfile investorUserProfile) {
		InvestorState waitingFirstDepositState = createWaitingFirstDepositState(investorUserProfile);
		investorUserProfile.setState(waitingFirstDepositState);
		investorUserProfileDao.save(investorUserProfile);
		return waitingFirstDepositState;
	}

	public InvestorState setToVerifiedInvestorState(InvestorUserProfile investorUserProfile) {
		InvestorState verifiedState = createVerifiedState(investorUserProfile);
		investorUserProfile.setState(verifiedState);
		investorUserProfileDao.save(investorUserProfile);
		return verifiedState;
	}

	public void assignStateToInvestor(InvestorState state, InvestorUserProfile investorUserProfile) {
		investorUserProfile.setState(state);
		investorUserProfileDao.save(investorUserProfile);
	}

	public String additionalInformationNeeded(InvestorUserProfile investorUserProfile) {
		InvestorFlowStep step = investorStepService.getStep(investorUserProfile.getUser());
		if (step.equals(InvestorFlowStep.INVESTOR_DASHBOARD)) {
			return null;
		} else if (step.equals(InvestorFlowStep.INVESTOR_NEW)) {
			return "We need some additional information about you.";
		} else {
			// Deposit needed
			return "We need a first deposit from you.";
		}
	}
}
