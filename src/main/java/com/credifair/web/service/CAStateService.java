package com.credifair.web.service;

import com.credifair.web.dao.CAStateActionDao;
import com.credifair.web.dao.StateDao;
import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.User;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.state.*;
import com.credifair.web.model.state.action.CAStateAction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Service
public class CAStateService {

	@Autowired
	private StateDao stateDao;

	@Autowired
	private CAStateActionDao caStateActionDao;

	@Autowired
	private CreditApplicationService creditApplicationService;

	public CAState getLatestState(User user) {
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		return stateDao.findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(borrowerUserProfile);
	}

	public CAState createNewState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		NewCAState newState = new NewCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createPreparingOfferState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		PreparingOfferCAState newState = new PreparingOfferCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createOfferCreatedState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		OfferCreatedCAState newState = new OfferCreatedCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createOfferAcceptedState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		OfferAcceptedCAState newState = new OfferAcceptedCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createProcessingState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		ProcessingCAState newState = new ProcessingCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createWaitingInvestorsState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		WaitingInvestorsCAState newState = new WaitingInvestorsCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createFundedState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		FundedCAState newState = new FundedCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState createPartiallyFundedState(BorrowerUserProfile borrowerUserProfile, CreditApplication creditApplication) {
		PartiallyFundedCAState newState = new PartiallyFundedCAState(borrowerUserProfile, creditApplication);
		stateDao.save(newState);
		return newState;
	}

	public CAState setToPreparingOffer(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
		CAState processingState = createPreparingOfferState(borrowerUserProfile, creditApplication);
		creditApplicationService.assignStateToCA(processingState, creditApplication);
		return processingState;
	}

	public CAState setToOfferCreated(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
		CAState processingState = createOfferCreatedState(borrowerUserProfile, creditApplication);
		creditApplicationService.assignStateToCA(processingState, creditApplication);
		return processingState;
	}

	public CAState setToProcessing(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
		CAState processingState = createProcessingState(borrowerUserProfile, creditApplication);
		creditApplicationService.assignStateToCA(processingState, creditApplication);
		return processingState;
	}

	public CAState setToWaitingForInvestors(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = creditApplicationService.findByBorrowerUserProfile(borrowerUserProfile);
		CAState waitingInvestorsState = createWaitingInvestorsState(borrowerUserProfile, creditApplication);
		creditApplicationService.assignStateToCA(waitingInvestorsState, creditApplication);
		return waitingInvestorsState;
	}

	public CAState setToFundedCAState(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
		CAState fundedState = createFundedState(borrowerUserProfile, creditApplication);
		creditApplicationService.assignStateToCA(fundedState, creditApplication);
		return fundedState;
	}

	public CAState setToPartiallyFundedCAState(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = creditApplicationService.findByBorrowerUserProfile(borrowerUserProfile);
		CAState partiallyFundedState = createPartiallyFundedState(borrowerUserProfile, creditApplication);
		creditApplicationService.assignStateToCA(partiallyFundedState, creditApplication);
		return partiallyFundedState;
	}

	public CAState findByCreditApplicationOrderByIdDesc(CreditApplication creditApplication) {
		return stateDao.findTopByCreditApplicationOrderByIdDesc(creditApplication);
	}

	public List<CAStateAction> findActionsByState(CAState state) {
		return caStateActionDao.findAllByCaState(state);
	}
}
