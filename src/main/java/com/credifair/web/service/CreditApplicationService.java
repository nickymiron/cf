package com.credifair.web.service;

import com.credifair.web.api.request.AssignGradeRequest;
import com.credifair.web.api.response.market.*;
import com.credifair.web.dao.BorrowerUserProfileDao;
import com.credifair.web.dao.CAStateActionDao;
import com.credifair.web.dao.CreditApplicationDao;
import com.credifair.web.dao.CreditApplicationInvestorDao;
import com.credifair.web.model.*;
import com.credifair.web.model.Currency;
import com.credifair.web.model.credit.*;
import com.credifair.web.model.state.CAState;
import com.credifair.web.model.state.action.CAStateAction;
import com.credifair.web.model.state.action.CAStateActionEnum;
import com.credifair.web.model.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.*;

/**
 * Created by indrek.ruubel on 16/03/2016.
 */
@Service
public class CreditApplicationService {

	@Autowired
	CreditApplicationDao creditApplicationDao;

	@Autowired
	CAStateService CAStateService;

	@Autowired
	CreditGradeService creditGradeService;

	@Autowired
	BorrowerUserProfileDao borrowerUserProfileDao;

	@Autowired
	UserService userService;

	@Autowired
	CreditApplicationInvestorDao creditApplicationInvestorDao;

	@Autowired
	BalanceService balanceService;

	@Value(value = "${minimal.investment.amount}")
	private BigDecimal minimalInvestmentAmount;

	@Autowired
	VerificationService verificationService;

	@Autowired
	InvestmentService investmentService;

	@Autowired
	TransactionService transactionService;

	@Autowired
	CAStateActionDao caStateActionDao;

	@Autowired
	CreditLimitService creditLimitService;

	@Autowired
	LoanService loanService;
	private BorrowerHistoryResponse borrowerHistory;

	public CreditApplication getCreditApplication(User user) {
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		return creditApplicationDao.findByBorrowerUserProfile(borrowerUserProfile);
	}

	public List<BorrowerUserProfile> getAllWaitingOffer() {
		List<CreditApplication> waiting = creditApplicationDao.findAllWithState("PREPARING_OFFER");
		List<BorrowerUserProfile> out = new ArrayList<>();
		for (CreditApplication creditApplication : waiting) {
			BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
			out.add(borrowerUserProfile);
		}
		return out;
	}

	public List<BorrowerUserProfile> getAllProcessing() {
		List<CreditApplication> waiting = creditApplicationDao.findAllWithState("PROCESSING");
		List<BorrowerUserProfile> out = new ArrayList<>();
		for (CreditApplication creditApplication : waiting) {
			BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
			out.add(borrowerUserProfile);
		}
		return out;
	}

	public List<BorrowerUserProfile> getAllWaitingInvestors() {
		List<CreditApplication> waiting = creditApplicationDao.findAllWithState("WAITING_INVESTORS");
		List<BorrowerUserProfile> out = new ArrayList<>();
		for (CreditApplication creditApplication : waiting) {
			BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
			out.add(borrowerUserProfile);
		}
		return out;
	}

	public CreditApplication getStarted(User user) {
		CreditApplication creditApplication = getCreditApplication(user);
		CAState state = creditApplication.getState();
		createCAStateAction(state, CAStateActionEnum.CLICKED_GET_STARTED);
		return creditApplication;
	}

	public CAStateAction createCAStateAction(CAState caState, CAStateActionEnum action) {
		CAStateAction actionObj = new CAStateAction(action, caState);
		caStateActionDao.save(actionObj);
		return actionObj;
	}

	public CreditApplication createCreditApplication(User user, Currency currency) {
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = getCreditApplication(user);
		if (creditApplication == null) {
			creditApplication = new CreditApplication(borrowerUserProfile);
			creditApplication.setCurrency(currency);
			creditApplicationDao.save(creditApplication);
			CreditLimit creditLimit = creditLimitService.createInitialCreditLimit(creditApplication, null);
			creditApplication.setCreditLimit(creditLimit);
			CAState newState = CAStateService.createNewState(borrowerUserProfile, creditApplication);
			creditApplication.setState(newState);
			creditApplicationDao.save(creditApplication);
		}
		return creditApplication;
	}

	public CreditApplication assignGradeToCAFromRequest(User user, AssignGradeRequest gradeRequest) {
		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
		CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
		return assignGradeToCAFromRequest(creditApplication, gradeRequest);
	}

	public CreditApplication assignGradeToCAFromRequest(CreditApplication creditApplication, AssignGradeRequest gradeRequest) {
		User admin = userService.getLoggedInUser();
		if (creditApplication != null) {
			CreditGrade creditGrade = creditGradeService.createGrade(creditApplication, gradeRequest.getGrade(), gradeRequest.getApr());
			CAState offerCreatedState = CAStateService.createOfferCreatedState(creditApplication.getBorrowerUserProfile(), creditApplication);
			creditApplication.setState(offerCreatedState);
			creditApplication.setCreditGrade(creditGrade);
			creditApplication.setGracePeriodDays(gradeRequest.getGracePeriod());
			CreditLimit creditLimit = creditLimitService.assignNewCreditLimit(creditApplication, gradeRequest.getCreditLimit(), admin);
			creditApplication.setCreditLimit(creditLimit);
			creditApplicationDao.save(creditApplication);
			return creditApplication;
		}
		return null;
	}

	public CreditApplication assignGradeToCAWithMinimalAPR(CreditApplication creditApplication, CreditGradeConstructor constructor) {
		CreditGrade creditGrade = creditGradeService.createGrade(creditApplication, constructor, constructor.getAprMin());
		CAState offerCreatedState = CAStateService.createOfferCreatedState(creditApplication.getBorrowerUserProfile(), creditApplication);
		creditApplication.setState(offerCreatedState);
		creditApplication.setCreditGrade(creditGrade);
		creditApplication.setGracePeriodDays(constructor.getGracePeriod());
		creditApplication.setCurrency(Currency.EUR);
		creditApplicationDao.save(creditApplication);
		return creditApplication;
	}

	public void assignStateToCA(CAState state, CreditApplication creditApplication) {
		creditApplication.setState(state);
		creditApplicationDao.save(creditApplication);
	}

	public Iterable<CreditApplication> getAllApplications() {
		return creditApplicationDao.findAll();
	}

	public CreditApplication getApplicationById(Long applicationId) {
		return creditApplicationDao.findOne(applicationId);
	}

	public List<CreditApplication> getAllApplicationsWaitingForInvestorsForCurrency(Currency currency) {
		return creditApplicationDao.findAllWithStatesForCurrency(new ArrayList<String>(){{
			add("WAITING_INVESTORS");
			add("PARTIALLY_FUNDED");
		}}, currency.name());
	}

	public BigDecimal findActiveDeployedAmountOnCA(CreditApplication creditApplication, InvestorUserProfile investorUserProfile) {
		BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
		// Find loaninvestments of creditApplication and investor, where there is principal left
		Iterable<Investment> loanInvestments = loanService.findActiveInvestmentsWithPrincipal(investorUserProfile, borrowerUserProfile);
		BigDecimal deployed = BigDecimal.ZERO;
		for (Investment investment : loanInvestments) {
			deployed = deployed.add(investment.getLatestSnapshop().getClosingPrincipal());
		}
		return deployed;
	}

	public BigDecimal findActiveDeployedAmountOnCA(CreditApplicationInvestor creditApplicationInvestor) {
		CreditApplication creditApplication = creditApplicationInvestor.getCreditApplication();
		InvestorUserProfile investorUserProfile = creditApplicationInvestor.getInvestorUserProfile();
		return findActiveDeployedAmountOnCA(creditApplication, investorUserProfile);
	}

	public CreditApplicationInvestor findUserInvestment(CreditApplication creditApplication, InvestorUserProfile investorUserProfile) {
		CreditApplicationInvestor invested = creditApplicationInvestorDao.findByInvestorUserProfileAndCreditApplication(investorUserProfile, creditApplication);
		return invested;
	}

	public List<OnMarketCreditApplicationResponse> getOnMarketApplications(Currency currency) {
		// TODO: Iterative queries, gets slow in the end, refactor at some point
		List<CreditApplication> onMarket = getAllApplicationsWaitingForInvestorsForCurrency(currency);
		List<OnMarketCreditApplicationResponse> out = new ArrayList<>();
		User user = userService.getLoggedInUser();
		InvestorUserProfile investorUserProfile = user.getInvestorUserProfile();
		for (CreditApplication creditApplication : onMarket) {
			CreditGrade creditGrade = creditApplication.getCreditGrade();
			BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
			User borrowerUser = borrowerUserProfile.getUser();
			if (borrowerUser == user) {
				continue;
			}
			UserProfile userProfile = borrowerUser.getUserProfile();
			Address address = userProfile.getAddress();
			PersonalFinancialData personalFinancialData = borrowerUserProfile.getPersonalFinancialData();
			Employment employment = borrowerUserProfile.getEmployment();
			BorrowerOverviewResponse borrowerOverviewResponse = getBorrowerOverview(user.getCountry(), creditGrade, employment, personalFinancialData, address, userProfile);
			IncomeExpensesOverviewResponse incomeExpenses = personalFinancialData == null ? null : getIncomeExpenses(creditApplication, personalFinancialData);
			BorrowerHistoryResponse history = getBorrowerHistory(borrowerUserProfile);
			BigDecimal funded = calculateFunded(creditApplication);
			CreditLimit creditLimit = creditApplication.getCreditLimit();
			BigDecimal creditLimitNumber = creditLimit.getCreditLimit();
			BigDecimal percentageFunded = funded.multiply(new BigDecimal(100)).divide(creditLimitNumber);
			CreditApplicationInvestor userInvestment = findUserInvestment(creditApplication, investorUserProfile);
			out.add(new OnMarketCreditApplicationResponse(creditApplication.getId(), creditGrade.getCreditGradeConstructor(),
					creditGrade.getApr(), creditGrade.getApr().subtract(new BigDecimal(0.02)),
					creditApplication.getCreditLimit().getCreditLimit(), funded, percentageFunded,
					userInvestment == null ? BigDecimal.ZERO : userInvestment.getMaxAmount(), borrowerOverviewResponse, incomeExpenses, history));
		}
		return out;
	}

	public BigDecimal calculateFundingLeft(CreditApplication creditApplication) {
		BigDecimal funded = calculateFunded(creditApplication);
		CreditLimit creditLimit = creditLimitService.getCreditLimit(creditApplication);
		return creditLimit.getCreditLimit().subtract(funded);
	}

	private BigDecimal calculateFunded(CreditApplication creditApplication) {
		BigDecimal funded = BigDecimal.ZERO;
		List<CreditApplicationInvestor> creditApplicationInvestors = creditApplicationInvestorDao.findAllByCreditApplication(creditApplication);
		if (creditApplicationInvestors != null) {
			for (CreditApplicationInvestor creditApplicationInvestor : creditApplicationInvestors) {
				BigDecimal maxAmount = creditApplicationInvestor.getMaxAmount();
				funded = funded.add(maxAmount);
			}
		}
		return funded;
	}

	private BorrowerOverviewResponse getBorrowerOverview(Country country, CreditGrade creditGrade, Employment employment,
														 PersonalFinancialData personalFinancialData, Address address,
														 UserProfile userProfile) {
		BorrowerOverviewResponse borrowerOverviewResponse = new BorrowerOverviewResponse(creditGrade.getCreditGradeConstructor(),
				userProfile.calculateAge(),
				country.getCountry(),
				address.getCity(),
				employment == null ? null : employment.getJobTitle(),
				employment == null ? null : employment.getWorkIndustry(),
				employment == null ? null : employment.getCurrentEmploymentTime(),
				employment == null ? null : employment.getTotalEmploymentTime(),
				userProfile.getEducation(),
				userProfile.getMaritalStatus(),
				personalFinancialData == null ? null : personalFinancialData.getHomeOwnership());
		return borrowerOverviewResponse;
	}

	private IncomeExpensesOverviewResponse getIncomeExpenses(CreditApplication creditApplication, PersonalFinancialData personalFinancialData) {
		boolean incomeVerified = verificationService.isIncomeVerified(creditApplication);
		List<Liability> liabilities = personalFinancialData.getLiabilities();
		List<LiabilityResponse> liabilityResponses = new ArrayList<>();
		BigDecimal totalNetIncome = personalFinancialData.getMonthlyNetIncome().add(personalFinancialData.getOtherRegularIncome());
		BigDecimal totalLiabilities = BigDecimal.ZERO;
		BigDecimal mortgage = BigDecimal.ZERO;
		for (Liability liability : liabilities) {
			liabilityResponses.add(new LiabilityResponse(liability.getTypeOfLiability(), liability.getCreditor(), liability.getMoPayment(),
					liability.getOutstanding(), liability.getSecurity()));
			if (liability.getTypeOfLiability().equals(LiabilityType.MORTGAGE)) {
				mortgage = mortgage.add(liability.getMoPayment());
			} else {
				totalLiabilities = totalLiabilities.add(liability.getMoPayment());
			}
		}

		BigDecimal totalDebt = totalLiabilities.add(mortgage);
		BigDecimal debtToIncomeRation = totalDebt.divide(totalNetIncome, 2, RoundingMode.HALF_UP);
		BigDecimal discretionaryIncome = totalNetIncome.subtract(totalDebt);
		BigDecimal livingExpenses = personalFinancialData.getLivingExpenses();

		IncomeExpensesOverviewResponse incomeExpenses = new IncomeExpensesOverviewResponse(personalFinancialData.getMonthlyNetIncome(),
				personalFinancialData.getOtherRegularIncome(), totalNetIncome,
				totalLiabilities, mortgage, livingExpenses, null, null, incomeVerified, discretionaryIncome, debtToIncomeRation, liabilityResponses);
		return incomeExpenses;
	}


	public CreditApplication findByBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile) {
		return creditApplicationDao.findByBorrowerUserProfile(borrowerUserProfile);
	}

	/**
	 * Calculate how much is currently available credit from investors
	 * @param borrowerUserProfile
	 * @return
	 */
	public BigDecimal getLiveCreditLimit(BorrowerUserProfile borrowerUserProfile) {
		Map<InvestorUserProfile, BigDecimal> available = getAvailableAmountFromInvestors(borrowerUserProfile);
		if (available == null) {
			return BigDecimal.ZERO;
		}
		return available.values().stream().reduce(BigDecimal.ZERO, BigDecimal::add);
	}

	public Map<InvestorUserProfile, BigDecimal> getAvailableAmountFromInvestors(BorrowerUserProfile borrowerUserProfile) {
		CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
		if (creditApplication == null) {
			return null;
		}
		List<CreditApplicationInvestor> investors = creditApplication.getCreditApplicationInvestors();
		Map<InvestorUserProfile, BigDecimal> out = new LinkedHashMap<>();
		for (CreditApplicationInvestor investor : investors) {
			BigDecimal maxAmount = investor.getMaxAmount();
			InvestorUserProfile investorUserProfile = investor.getInvestorUserProfile();
			// See how much there is active deployment from this investor on this borrower
			Iterable<Investment> active = investmentService.findAllActiveInvestmentsForInvestorAndBorrower(investorUserProfile, borrowerUserProfile);
			BigDecimal deployed = BigDecimal.ZERO;
			for (Investment investment : active) {
				deployed = deployed.add(investment.getLatestSnapshop().getClosingPrincipal());
			}
			BigDecimal undeployed = maxAmount.subtract(deployed);
			if (undeployed.compareTo(BigDecimal.ZERO) <= 0) {
				continue;
			} else {
				Balance onBalance = balanceService.getLatestBalance(investorUserProfile.getUser().getUserProfile());
				BigDecimal closingBalance = onBalance.getClosingBalance();
				if (closingBalance.compareTo(minimalInvestmentAmount) >= 0) {
					// Greater or equal to the minimal amount
					if (closingBalance.compareTo(undeployed) >= 0) {
						out.put(investorUserProfile, undeployed);
					} else {
						// Less available than undeployed
						out.put(investorUserProfile, closingBalance);
					}
				}
			}
		}
		return out;
	}

	public CreditApplicationInvestor findByInvestorUserProfileAndCreditApplication(InvestorUserProfile investorUserProfile, CreditApplication creditApplication) {
		return creditApplicationInvestorDao.findByInvestorUserProfileAndCreditApplication(investorUserProfile, creditApplication);
	}

	public void save(CreditApplicationInvestor creditApplicationInvestor) {
		creditApplicationInvestorDao.save(creditApplicationInvestor);
	}

	public void save(CreditApplication creditApplication) {
		creditApplicationDao.save(creditApplication);
	}

	public BorrowerHistoryResponse getBorrowerHistory(BorrowerUserProfile borrowerUserProfile) {
		User user = borrowerUserProfile.getUser();
		List<Transaction> principalRepaiments = transactionService.getAllUserTransactionsByType(user, "BORROWER_PRINCIPAL_PAYBACK");
		List<Transaction> interestPaiments = transactionService.getAllUserTransactionsByType(user, "BORROWER_INTEREST_PAYBACK");
		List<Transaction> borrows = transactionService.getAllUserTransactionsByType(user, "BORROWER_BORROW");
		List<Transaction> repayments = transactionService.getAllUserTransactionsByType(user, "FEE_PAYBACK");

		BigDecimal overdue = BigDecimal.ZERO;
		BigDecimal repaidPrincipal = BigDecimal.ZERO;
		BigDecimal paidInterest = BigDecimal.ZERO;
		BigDecimal amountBorrowed = BigDecimal.ZERO;
		Integer repaimentsTotal = repayments.size();

		for (Transaction principalRepaiment : principalRepaiments) {
			repaidPrincipal = repaidPrincipal.add(principalRepaiment.getAmount());
		}

		for (Transaction interestPaiment : interestPaiments) {
			paidInterest = paidInterest.add(interestPaiment.getAmount());
		}

		for (Transaction borrow : borrows) {
			amountBorrowed = amountBorrowed.add(borrow.getAmount());
		}

		BorrowerHistoryResponse historyResponse = new BorrowerHistoryResponse(overdue, repaidPrincipal, paidInterest, amountBorrowed, repaimentsTotal);
		return historyResponse;
	}
}
