package com.credifair.web.service;

import com.credifair.web.model.User;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 07/03/2016.
 */
@Service
public class AuthenticationService {

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    public Authentication login(User user, String plainPassword) {
        UserDetails userDetails = customUserDetailsService.loadUserByUsername(user.getEmail());
        return login(userDetails, plainPassword);
    }

    public Authentication login(UserDetails userDetails, String plainPassword) {
        Authentication auth =
                new UsernamePasswordAuthenticationToken(userDetails, plainPassword, userDetails.getAuthorities());
        SecurityContextHolder.getContext().setAuthentication(auth);
        return auth;
    }

}
