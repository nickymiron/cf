package com.credifair.web.service;

import com.credifair.web.dao.EmailSubscriberDao;
import com.credifair.web.model.EmailSubscriber;
import com.credifair.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;

/**
 * Created by indrek.ruubel on 16/01/2016.
 */
@Service
public class EmailSubscriberService {

    @Autowired
    private EmailSubscriberDao emailSubscriberDao;

    public EmailSubscriberService() {
    }

    public EmailSubscriberService(EmailSubscriberDao emailSubscriberDao) {
        this.emailSubscriberDao = emailSubscriberDao;
    }

    public EmailSubscriber saveSubscriber(String email, User user) {
        EmailSubscriber emailSubscriber = emailSubscriberDao.findByEmail(email);
        if (emailSubscriber == null) {
            emailSubscriber = new EmailSubscriber(email, user, new Date(), new Date());
            emailSubscriberDao.save(emailSubscriber);
            return emailSubscriber;
        }
        return null;
    }

}
