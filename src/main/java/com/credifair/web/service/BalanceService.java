package com.credifair.web.service;

import com.credifair.web.dao.BalanceDao;
import com.credifair.web.dao.CreditApplicationInvestorDao;
import com.credifair.web.dao.InvestorBalanceDao;
import com.credifair.web.model.*;
import com.credifair.web.model.transaction.DepositTransaction;
import com.credifair.web.model.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

/**
 * Created by indrek.ruubel on 13/03/2016.
 */
@Service
public class BalanceService {

	@Autowired
	BalanceDao balanceDao;

	@Autowired
	TransactionService transactionService;

	@Autowired
	UserService userService;

	@Autowired
	InvestorBalanceDao investorBalanceDao;

	@Autowired
	CreditApplicationInvestorDao creditApplicationInvestorDao;

	public Balance getLatestBalance(UserProfile userProfile) {
		Balance balance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(userProfile);
		return balance;
	}

	public synchronized Transaction addDeposit(BigDecimal amount, Currency currency, UserProfile userProfile) {
		Balance latestBalance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(userProfile);
		BigDecimal openingBalance = latestBalance == null ? BigDecimal.ZERO : latestBalance.getClosingBalance();
		BigDecimal closingBalance = openingBalance.add(amount);

		Balance newBalance = new Balance(openingBalance, closingBalance, userProfile, currency);
		balanceDao.save(newBalance);

		User user = userService.findByUserProfile(userProfile);

		String traceHash = transactionService.generateTraceHash();
		Transaction transaction = transactionService.createDepositTransaction(amount, currency, user, traceHash);
		transactionService.createBalanceTransactionLink(transaction, newBalance.getId());
		return transaction;
	}

	public Balance addInterestRepaymentToInvestor(BigDecimal amount, Currency currency, InvestorUserProfile investorUserProfile, String traceHash) {
		Balance latestBalance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(investorUserProfile.getUser().getUserProfile());
		BigDecimal openingBalance = latestBalance == null ? BigDecimal.ZERO : latestBalance.getClosingBalance();
		BigDecimal closingBalance = openingBalance.add(amount);

		Balance newBalance = new Balance(openingBalance, closingBalance, investorUserProfile.getUser().getUserProfile(), currency);
		balanceDao.save(newBalance);

		InvestorBalance investorBalance = new InvestorBalance(BigDecimal.ZERO, amount, BigDecimal.ZERO, investorUserProfile, currency);
		investorBalanceDao.save(investorBalance);

		Transaction transaction = transactionService.createInvestorInterestRepaymentTransaction(amount, currency, investorUserProfile.getUser(), traceHash);
		transactionService.createBalanceTransactionLink(transaction, newBalance.getId());
		transactionService.createInvestorBalanceTransactionLink(transaction, investorBalance.getId());
		return newBalance;
	}

	public Balance addPrincipalRepaymentToInvestor(BigDecimal amount, Currency currency, InvestorUserProfile investorUserProfile, String traceHash) {
		Balance latestBalance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(investorUserProfile.getUser().getUserProfile());
		BigDecimal openingBalance = latestBalance == null ? BigDecimal.ZERO : latestBalance.getClosingBalance();
		BigDecimal closingBalance = openingBalance.add(amount);

		Balance newBalance = new Balance(openingBalance, closingBalance, investorUserProfile.getUser().getUserProfile(), currency);
		balanceDao.save(newBalance);

		InvestorBalance investorBalance = new InvestorBalance(amount, BigDecimal.ZERO, BigDecimal.ZERO, investorUserProfile, currency);
		investorBalanceDao.save(investorBalance);

		Transaction transaction = transactionService.createInvestorPrincipalRepaymentTransaction(amount, currency, investorUserProfile.getUser(), traceHash);
		transactionService.createBalanceTransactionLink(transaction, newBalance.getId());
		transactionService.createInvestorBalanceTransactionLink(transaction, investorBalance.getId());
		return newBalance;
	}

	public Balance subtractInvestment(BigDecimal amount, Currency currency, UserProfile userProfile) throws Exception {
		Balance latestBalance = balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(userProfile);
		BigDecimal openingBalance = latestBalance == null ? BigDecimal.ZERO : latestBalance.getClosingBalance();
		BigDecimal closingBalance = openingBalance.subtract(amount);

		if (closingBalance.compareTo(BigDecimal.ZERO) < 0) {
			throw new Exception("Balance can't be negative!");
		}

		Balance newBalance = new Balance(openingBalance, closingBalance, userProfile, currency);
		balanceDao.save(newBalance);
		return newBalance;
	}

	public boolean hasEnoughMoneyOnBalance(UserProfile userProfile, BigDecimal amount) {
		boolean hasFunds = false;
		Balance latestBalance = getLatestBalance(userProfile);
		if (latestBalance == null) {
			return hasFunds;
		}
		BigDecimal endBalance = latestBalance.getClosingBalance();
		if (endBalance.compareTo(amount) >= 0) {
			hasFunds = true;
		}
		return hasFunds;
	}

	public List<Balance> findAllByUserProfile(UserProfile userProfile) {
		return balanceDao.findAllByUserProfile(userProfile);
	}

	public Balance subtractRepayment(BigDecimal amount, Currency currency, UserProfile userProfile) throws Exception {
		Balance latestBalance = getLatestBalance(userProfile);
		amount = amount.setScale(latestBalance.getClosingBalance().scale(), RoundingMode.HALF_UP);
		if (latestBalance.getClosingBalance().compareTo(amount) < 0) {
			throw new Exception("Not enough funds");
		}

		BigDecimal openingBalance = latestBalance.getClosingBalance();
		BigDecimal closingBalance = openingBalance.subtract(amount);

		if (closingBalance.compareTo(BigDecimal.ZERO) < 0) {
			throw new Exception("Balance can't be negative!");
		}

		Balance newBalance = new Balance(openingBalance, closingBalance, userProfile, currency);
		balanceDao.save(newBalance);
		return newBalance;
	}

	public Balance getLatestBalanceForUserWithEmail(String email) {
		User user = userService.findByEmail(email);
		if (user != null) {
			UserProfile userProfile = user.getUserProfile();
			return balanceDao.findTopByUserProfileOrderByCreatedDescIdDesc(userProfile);
		}
		return null;
	}
}
