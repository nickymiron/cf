package com.credifair.web.service;

import com.credifair.web.dao.PayoutInstructionDao;
import com.credifair.web.model.Loan;
import com.credifair.web.model.User;
import com.credifair.web.model.payout.PayoutInstruction;
import com.credifair.web.model.payout.PayoutState;
import com.credifair.web.model.payout.PayoutType;
import com.credifair.web.model.recipient.Recipient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by indrek.ruubel on 29/05/2016.
 */
@Service
public class PayoutService {

	private List<String> adminEmails;

	@Autowired
	RecipientService recipientService;

	@Autowired
	PayoutInstructionDao payoutInstructionDao;

	@PostConstruct
	public void setup() {
		adminEmails = new ArrayList<>();
		adminEmails.add("indrekruubel@gmail.com");
	}

	public PayoutInstruction createTargetInstruction(Loan loan, User user) {
		Recipient recipientAccount = recipientService.getRecipientAccount(user);
		PayoutInstruction instruction = new PayoutInstruction(PayoutType.TARGET, PayoutState.PENDING,
				loan.getAmount(), loan.getCurrency(), user.getCountry(), recipientAccount, loan);
		payoutInstructionDao.save(instruction);
		return instruction;
	}

	public void notifyAdmins() {
		// Send email
	}

	public Iterable<PayoutInstruction> getAllPendingInstructions() {
		return payoutInstructionDao.findAllByPayoutState(PayoutState.PENDING);
	}

	public PayoutInstruction findOne(Long payoutId) {
		return payoutInstructionDao.findOne(payoutId);
	}

	public void approveInstruction(Long instructionId, User admin) throws Exception {
		PayoutInstruction instruction = payoutInstructionDao.findOne(instructionId);
		if (instruction == null) {
			throw new Exception("No instruction found");
		}
		instruction.setPayoutState(PayoutState.TRANSFERRED);
		instruction.setTransferred(LocalDateTime.now());
		instruction.setTransferredBy(admin);
		payoutInstructionDao.save(instruction);
	}
}
