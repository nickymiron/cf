package com.credifair.web.service;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.model.*;
import com.credifair.web.model.investor.state.InvestorState;
import com.credifair.web.model.transaction.Transaction;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 11/04/2016.
 */
@Service
public class InvestorStepService {

	@Autowired
	UserService userService;

	@Autowired
	TransactionService transactionService;

	@Autowired
	InvestorStateService investorStateService;

	@Autowired
	InvestorUserProfileService investorUserProfileService;

	public InvestorFlowStep getStep() {
		User user = userService.getLoggedInUser();
		return getStep(user);
	}

	public InvestorFlowStep getStep(User user) {
		UserProfile userProfile = user.getUserProfile();
		InvestorUserProfile investorUserProfile = investorUserProfileService.findById(user.getInvestorUserProfile().getId());

		InvestorState state = investorUserProfile.getState();
		if (state == null) {
			if (step1NotFinished(userProfile)) {
				investorStateService.setToNewInvestorState(investorUserProfile);
				return InvestorFlowStep.INVESTOR_NEW;
			} else {
				List<Transaction> deposits = transactionService.getAllUserTransactionsByType(user, "DEPOSIT");
				if (deposits.size() > 0) {
					investorStateService.setToVerifiedInvestorState(investorUserProfile);
					return InvestorFlowStep.INVESTOR_DASHBOARD;
				} else {
					investorStateService.setToWaitingFirstDepositInvestorState(investorUserProfile);
					return InvestorFlowStep.DEPOSIT;
				}
			}
		}

		String strState = state.getState();

		if (strState.equals("NEW")) {
			if (step1NotFinished(userProfile)) {
				return InvestorFlowStep.INVESTOR_NEW;
			} else {
				investorStateService.setToWaitingFirstDepositInvestorState(investorUserProfile);
				return InvestorFlowStep.DEPOSIT;
			}
		} else if (strState.equals("WAITING_FIRST_DEPOSIT")) {
			return InvestorFlowStep.DEPOSIT;
		} else if (strState.equals("VERIFIED")) {
			return InvestorFlowStep.INVESTOR_DASHBOARD;
		}

		return InvestorFlowStep.INVESTOR_DASHBOARD;
	}

	private boolean step1NotFinished(UserProfile userProfile) {

		if (userProfile.getFirstName() == null || userProfile.getLastName() == null
				|| userProfile.getDateOfBirth() == null || userProfile.getPhoneNumber() == null
				|| userProfile.getEducation() == null || userProfile.getMaritalStatus() == null
				|| userProfile.getAddress() == null) {
			return true;
		}
		return false;
	}

	public Map<String, Object> getStepData(InvestorFlowStep step) {
		User user = userService.getLoggedInUser();
		return getStepData(user, step);
	}

	public Map<String, Object> getStepData(User user, InvestorFlowStep step) {
		Map<String, Object> data = new HashMap<>();
		List<FormFieldTypeWrapper> fields = new ArrayList<>();
		if (step.equals(InvestorFlowStep.INVESTOR_NEW)) {

			fields.add(UserValueFieldType.FIRST_NAME.wrapIt());
			fields.add(UserValueFieldType.LAST_NAME.wrapIt());
			fields.add(UserValueFieldType.DATE_OF_BIRTH.wrapIt());
			fields.add(UserValueFieldType.PHONE_NUMBER.wrapIt());
			fields.add(UserValueFieldType.EDUCATION.wrapIt());
			fields.add(UserValueFieldType.MARITAL_STATUS.wrapIt());
			fields.add(UserValueFieldType.COUNTRY.wrapIt());
			fields.add(UserValueFieldType.ADDRESS.wrapIt());
			fields.add(UserValueFieldType.CITY.wrapIt());
			fields.add(UserValueFieldType.POSTAL_CODE.wrapIt());
			data.put("stepNumber", 0);
			data.put("formTitle", "INVESTOR_INFORMATION");
			data.put("formDescription", "INVESTOR_DESC");

		}
		data.put("fields", fields);
		return data;
	}
}
