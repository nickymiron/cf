package com.credifair.web.service;

import com.credifair.web.dao.BankTransactionDao;
import com.credifair.web.dao.BankTransactionLinkDao;
import com.credifair.web.model.Currency;
import com.credifair.web.model.User;
import com.credifair.web.model.transaction.DepositTransaction;
import com.credifair.web.model.transaction.Transaction;
import com.credifair.web.model.transaction.bank.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
@Service
public class BankTransactionService {

	@Autowired
	BankTransactionDao bankTransactionDao;

	@Autowired
	BankTransactionLinkDao bankTransactionLinkDao;

	@Autowired
	TransactionService transactionService;

	@Autowired
	BalanceService balanceService;

	public BankTransaction createReceiveBankTransaction(BigDecimal amount, BankEnum bank, Currency currency, String reference, LocalDateTime bankCreated, String transactionId) throws Exception {
		BankTransaction transaction = bankTransactionDao.findByBankTransactionIdAndBankName(transactionId, bank.toString());
		if (transaction == null) {
			if (bank.equals(BankEnum.SEB)) {
				transaction = new SebBankTransaction(currency, amount, transactionId, reference, BankTransactionState.ACTIVE, BankTransactionType.RECEIVE, bankCreated);
			} else if (bank.equals(BankEnum.BARCLAYS)) {
				transaction = new BarclaysBankTransaction(currency, amount, transactionId, reference, BankTransactionState.ACTIVE, BankTransactionType.RECEIVE, bankCreated);
			}

			if (transaction == null) {
				throw new Exception("No such bankTransaction type found");
			} else {
				bankTransactionDao.save(transaction);
				return transaction;
			}
		} else {
			throw new Exception("Transaction Id already in database");
		}
	}

	@Transactional(rollbackOn = Exception.class)
	public BankTransactionLink linkTransactionToUser(BigDecimal amount, BankTransaction bankTransaction, User user) throws Exception {
		Currency currency = bankTransaction.getCurrency();

		BigDecimal totalLinked = BigDecimal.ZERO;
		Iterable<BankTransactionLink> links = bankTransactionLinkDao.findAllByBankTransaction(bankTransaction);
		for (BankTransactionLink link : links) {
			totalLinked = totalLinked.add(link.getAmount());
		}
		totalLinked = totalLinked.add(amount);
		if (totalLinked.compareTo(bankTransaction.getAmount()) == 0) {
			// All amount linked
			bankTransaction.setState(BankTransactionState.LINKED);
		} else if (totalLinked.compareTo(bankTransaction.getAmount()) > 0) {
			// The amount provided exceeds the amount
			throw new Exception("Too much money specified");
		}

		bankTransaction.setLastUpdated(LocalDateTime.now());
		bankTransactionDao.save(bankTransaction);

		Transaction transaction = balanceService.addDeposit(amount, currency, user.getUserProfile());

		BankTransactionLink link = new BankTransactionLink(user, amount, bankTransaction, (DepositTransaction) transaction);
		bankTransactionLinkDao.save(link);
		return link;
	}

	public List<BankTransaction> findAllByState(BankTransactionState state) {
		return bankTransactionDao.findAllByState(state);
	}

	public BankTransaction findOne(Long transactionId) {
		return bankTransactionDao.findOne(transactionId);
	}

	public Iterable<BankTransactionLink> findAllLinksOfTransaction(BankTransaction transaction) {
		return bankTransactionLinkDao.findAllByBankTransaction(transaction);
	}
}
