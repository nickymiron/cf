package com.credifair.web.service;

import com.credifair.web.dao.TransactionDao;
import com.credifair.web.dao.TransactionLinkDao;
import com.credifair.web.model.Currency;
import com.credifair.web.model.User;
import com.credifair.web.model.transaction.*;
import com.credifair.web.model.transaction.link.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.UUID;

/**
 * Created by indrek.ruubel on 03/02/2016.
 */
@Service
public class TransactionService {

    @Autowired
    TransactionDao transactionDao;

    @Autowired
    TransactionLinkDao transactionLinkDao;

//    public Transaction addLoanTransactionToBorrower(Loan loan, User user, Currency currency) throws Exception {
//        if (loan.getFundedDateTime() == null || !sumInvestorsMatchLoan(loan)) {
//            throw new Exception("Loan not funded fully");
//        }
//
//        Transaction transaction = new Transaction(loan.getAmount(), user, LocalDateTime.now(), TransactionType.BORROWER_LOAN, loan, currency);
//        borrowerTransactionDao.save(transaction);
//        loanBalanceService.calculateNewLoanBalance(transaction, user);
//        return transaction;
//    }
//
//    public Transaction addInterestAndCalculateLoanBalance(BigDecimal interest, User user, Currency currency) {
//        Transaction transaction = new Transaction(interest, user, LocalDateTime.now(), TransactionType.BORROWER_INTEREST, null, currency);
//        borrowerTransactionDao.save(transaction);
//        loanBalanceService.calculateNewLoanBalance(transaction, user);
//        return transaction;
//    }
//
//    public Transaction addPayoffInterestTransactionAndCalculateLoanBalance(BigDecimal payoff, User user, Currency currency) {
//        Transaction transaction = new Transaction(payoff, user, LocalDateTime.now(), TransactionType.BORROWER_PAYOFF_INTEREST, null, currency);
//        borrowerTransactionDao.save(transaction);
//        loanBalanceService.calculateNewLoanBalance(transaction, user);
//        return transaction;
//    }
//
//    public Transaction addPayoffPrincipalTransactionAndCalculateLoanBalance(BigDecimal payoff, User user, Currency currency) {
//        Transaction transaction = new Transaction(payoff, user, LocalDateTime.now(), TransactionType.BORROWER_PAYOFF_PRINCIPAL, null, currency);
//        borrowerTransactionDao.save(transaction);
//        loanBalanceService.calculateNewLoanBalance(transaction, user);
//        return transaction;
//    }
//
//    public Transaction addInvestedAmountToTransactionsAndCalculateBalance(Investment investment, User investorUser, Currency currency) {
//        Transaction transaction = new Transaction(TransactionType.INVESTOR_INVESTMENT, investment.getPrincipal(), investorUser, LocalDateTime.now(),
//                null, investment, null, currency);
//        transactionDao.save(transaction);
//        balanceService.calculateNewBalance(transaction, investorUser, currency);
//        return transaction;
//    }
//
//    public Transaction addPayInAmountToTransactionsAndCalculateBalance(BigDecimal amount, User user, Currency currency) {
//        Transaction transaction = new Transaction(TransactionType.PAYIN,
//                amount, user, LocalDateTime.now(), null, null, null, currency);
//        transactionDao.save(transaction);
//        balanceService.calculateNewBalance(transaction, user, currency);
//        return transaction;
//    }
//
//    public Transaction addInterestPaymentAmountToTransactionsAndCalculateBalance(BigDecimal amount, User user, Currency currency) {
//        Transaction transaction = new Transaction(TransactionType.INVESTOR_INTEREST_PAYMENT, amount, user, LocalDateTime.now(),
//                null, null, null, currency);
//        transactionDao.save(transaction);
//        balanceService.calculateNewBalance(transaction, user, currency);
//        return transaction;
//    }
//
//    public Transaction addPrincipalPaymentAmountToTransactionsAndCalculateBalance(BigDecimal amount, User user, Currency currency) {
//        Transaction transaction = new Transaction(TransactionType.INVESTOR_PRINCIPAL_PAYMENT, amount, user, LocalDateTime.now(),
//                null, null, null, currency);
//        transactionDao.save(transaction);
//        balanceService.calculateNewBalance(transaction, user, currency);
//        return transaction;
//    }
//
//    public List<Transaction> getAllUserTransactionsByType(User user, TransactionType type) {
//        return transactionDao.findAllByTypeAndUser(type, user);
//    }
//
//    private boolean sumInvestorsMatchLoan(Loan loan) {
//        BigDecimal targetAmount = loan.getAmount();
//        BigDecimal collected = BigDecimal.ZERO;
//        List<Investment> investments = investmentDao.findAllByLoan(loan);
//        for (Investment investment : investments) {
//            collected = collected.add(investment.getPrincipal());
//        }
//
//        if (targetAmount.compareTo(collected) == 0) {
//            return true;
//        } else if (collected.compareTo(targetAmount) > 0) {
//            // More collected
//            System.out.println("Collected more than needed, investigate");
//            return true;
//        }
//        return false;
//    }

    public String generateTraceHash() {
        String traceHash = UUID.randomUUID().toString();
        List<Transaction> tx = transactionDao.findAllByTraceHash(traceHash);
        while(!tx.isEmpty()) {
            traceHash = UUID.randomUUID().toString();
            tx = transactionDao.findAllByTraceHash(traceHash);
        }
        return traceHash;
    }

    public Transaction createDepositTransaction(BigDecimal amount , Currency currency, User user, String traceHash) {
        DepositTransaction transaction = new DepositTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createInvestorInterestRepaymentTransaction(BigDecimal amount , Currency currency, User user, String traceHash) {
        InvestorInterestPaybackTransaction transaction = new InvestorInterestPaybackTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createInvestorPrincipalRepaymentTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        InvestorPrincipalPaybackTransaction transaction = new InvestorPrincipalPaybackTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createCFFeeTransaction(BigDecimal amount , Currency currency, User user, String traceHash) {
        FeePaybackTransaction transaction = new FeePaybackTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createInvestmentTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        InvestorInvestTransaction transaction = new InvestorInvestTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public TransactionLink createBalanceTransactionLink(Transaction transaction, Long referenceId) {
        BalanceTransactionLink transactionLink = new BalanceTransactionLink(transaction, referenceId);
        transactionLinkDao.save(transactionLink);
        return transactionLink;
    }

    public TransactionLink createLoanBalanceTransactionLink(Transaction transaction, Long referenceId) {
        LoanBalanceTransactionLink transactionLink = new LoanBalanceTransactionLink(transaction, referenceId);
        transactionLinkDao.save(transactionLink);
        return transactionLink;
    }

    public TransactionLink createInvestorBalanceTransactionLink(Transaction transaction, Long referenceId) {
        InvestorBalanceTransactionLink transactionLink = new InvestorBalanceTransactionLink(transaction, referenceId);
        transactionLinkDao.save(transactionLink);
        return transactionLink;
    }

    public TransactionLink createInvestmentSnapshotTransactionLink(Transaction transaction, Long referenceId) {
        InvestmentSnapshotTransactionLink transactionLink = new InvestmentSnapshotTransactionLink(transaction, referenceId);
        transactionLinkDao.save(transactionLink);
        return transactionLink;
    }

    public Transaction createBorrowerPrincipalRepayedTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        BorrowerPrincipalPaybackTransaction transaction = new BorrowerPrincipalPaybackTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createBorrowerInterestRepayedTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        BorrowerInterestPaybackTransaction transaction = new BorrowerInterestPaybackTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createBorrowerBorrowTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        BorrowerBorrowTransaction transaction = new BorrowerBorrowTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createFeeIncrementTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        FeeIncrementTransaction transaction = new FeeIncrementTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createInvestorInterestIncrementTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        InvestorInterestIncrementTransaction transaction = new InvestorInterestIncrementTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public Transaction createBorrowerInterestIncrementTransaction(BigDecimal amount, Currency currency, User user, String traceHash) {
        BorrowerInterestIncrementTransaction transaction = new BorrowerInterestIncrementTransaction(amount, currency, user, traceHash, TransactionState.COMPLETED);
        transactionDao.save(transaction);
        return transaction;
    }

    public List<Transaction> findAllByUserOrderByCreatedDesc(User user) {
        return transactionDao.findAllByUserOrderByCreatedDesc(user);
    }

    public List<Transaction> getAllUserTransactionsByType(User user, String transactionType) {
        return transactionDao.findAllByUserAndType(user, transactionType);
    }

    public Iterable<Transaction> findAll() {
        return transactionDao.findAll();
    }

    public Iterable<TransactionLink> findAllTransactionLinks() {
        return transactionLinkDao.findAll();
    }

    public Iterable<Transaction> getAllTransactionsByType(String type) {
        return transactionDao.findAllByType(type);
    }

    public Iterable<Transaction> findAllWhereUserIn(User...users) {
        return transactionDao.findAllByUserIn(users);
    }

    public Iterable<TransactionLink> findAllLinksWhereTransactionIn(Iterable<Transaction> transactions) {
        return transactionLinkDao.findAllByTransactionIn(transactions);
    }

    public Iterable<Transaction> findAllByUserAndTypeInOrderByCreatedDesc(User user, List<String> transactionTypes) {
        return transactionDao.findAllByUserAndTypeInOrderByCreatedDesc(user, transactionTypes);
    }
}
