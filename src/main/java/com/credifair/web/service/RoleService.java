package com.credifair.web.service;

import com.credifair.web.dao.RoleDao;
import com.credifair.web.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 13/01/2016.
 */
@Transactional
@Service
public class RoleService {

    @Autowired
    RoleDao roleDao;

    public RoleService() {
    }

    public RoleService(RoleDao roleDao) {
        this.roleDao = roleDao;
    }

    public Role findByRole(String role) {
        return roleDao.findByRole(role);
    }

    public Role saveRole(String role) {
        Role roleObj = new Role(role);
        roleDao.save(roleObj);
        return roleObj;
    }

}
