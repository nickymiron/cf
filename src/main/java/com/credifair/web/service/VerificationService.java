package com.credifair.web.service;

import com.credifair.web.dao.VerificationDao;
import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.User;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.verification.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * Created by indrek.ruubel on 14/03/2016.
 */
@Service
public class VerificationService {

	@Autowired
	CreditApplicationService creditApplicationService;

	@Autowired
	LiabilityService liabilityService;

	@Autowired
	UserService userService;

	@Autowired
	BorrowerStepService borrowerStepService;

	@Autowired
	VerificationDao verificationDao;

	public boolean canRequestLoan(User user) {
		return borrowerStepService.canRequestLoan(user);
	}

	public void verifyVerificationById(Long verificationId) {
		User verifier = userService.getLoggedInUser();
		Verification verification = verificationDao.findOne(verificationId);
		verification.setVerificationStatus(VerificationStatus.VERIFIED);
		verification.setVerified(LocalDateTime.now());
		verification.setVerifiedBy(verifier);
		verificationDao.save(verification);

		User user = verification.getUser();
		checkIfGradeAssignedAndIdVerifiedAndSetToWaitingForInvestors(user);
	}

	public void unverifyVerificationById(Long verificationId) {
		User verifier = userService.getLoggedInUser();
		Verification verification = verificationDao.findOne(verificationId);
		verification.setVerificationStatus(VerificationStatus.NOT_VERIFIED);
		verification.setVerified(LocalDateTime.now());
		verification.setVerifiedBy(verifier);
		verificationDao.save(verification);

//		User user = verification.getUser();
//		checkIfVerificationSuccessAndChangeState(user);
	}

	public Verification findOrCreateByUserAndType(User user, String type) {
		Verification verification = findVerificationType(user, type);
		if (verification != null) {
			return verification;
		}

		if (type.equals("ID")) {
			verification = new IdVerification(user);
			verificationDao.save(verification);
			return verification;
		} else if (type.equals("ADDRESS")) {
			verification = new AddressVerification(user);
			verificationDao.save(verification);
			return verification;
		} else if (type.equals("INCOME")) {
			verification = new IncomeVerification(user);
			verificationDao.save(verification);
			return verification;
		}
		return verification;
	}

	public Verification findVerificationType(User user, String type) {
		return verificationDao.findByUserAndType(user.getId(), type);
	}

	public void checkIfGradeAssignedAndIdVerifiedAndSetToWaitingForInvestors(User user) {
		Verification id = findOrCreateByUserAndType(user, "ID");

		CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
		CreditGrade creditGrade = creditApplication.getCreditGrade();
		if (id.isAccepted() && creditGrade != null) {
			borrowerStepService.setToWaitingForInvestors(user);
		}
	}

	public boolean isIncomeVerified(CreditApplication creditApplication) {
		BorrowerUserProfile borrowerUserProfile = creditApplication.getBorrowerUserProfile();
		User user = borrowerUserProfile.getUser();
		Verification income = findVerificationType(user, "INCOME");
		if (income != null) {
			VerificationStatus verificationStatus = income.getVerificationStatus();
			return verificationStatus.equals(VerificationStatus.VERIFIED) ? true : false;
		} else {
			return false;
		}
	}

	public void putOnMarket(User user) {
		borrowerStepService.putCreditApplicationOnMarket(user);
	}
}
