package com.credifair.web.service;

import com.credifair.web.dao.EmploymentDao;
import com.credifair.web.model.Employment;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 03/05/2016.
 */
@Service
public class EmploymentService {

	@Autowired
	EmploymentDao employmentDao;

	public void save(Employment employment) {
		employmentDao.save(employment);
	}

}
