package com.credifair.web.service;

import com.credifair.web.dao.AddressDao;
import com.credifair.web.model.Address;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 03/05/2016.
 */
@Service
public class AddressService {

	@Autowired
	AddressDao addressDao;

	public void save(Address address){
		addressDao.save(address);
	}

}
