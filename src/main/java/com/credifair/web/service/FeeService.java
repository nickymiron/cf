package com.credifair.web.service;

import com.credifair.web.model.Currency;
import com.credifair.web.model.Investment;
import com.credifair.web.model.InvestmentSnapshop;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.math.MathContext;

/**
 * Created by indrek.ruubel on 18/04/2016.
 */
@Service
public class FeeService {

	@Autowired
	BalanceService balanceService;

	@Autowired
	InvestmentService investmentService;

	@Autowired
	InterestService interestService;

	@Autowired
	MathContext mathContext;

	public BigDecimal payFee(BigDecimal amount, Currency currency, Iterable<Investment> investments, String traceHash) {
		BigDecimal totalFee = BigDecimal.ZERO;
		for (Investment investment : investments) {
			InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
			BigDecimal closingFee = latestSnapshop.getClosingFee();
			totalFee = totalFee.add(closingFee);
		}

		if (totalFee.compareTo(BigDecimal.ZERO) == 0) {
			return amount;
		}

		if (amount.compareTo(totalFee) > 0) {
			// More money than total fee
			BigDecimal remaining = amount.subtract(totalFee);
			zeroFees(investments, traceHash);
			return remaining;
		} else if (amount.compareTo(totalFee) == 0) {
			// Equal
			zeroFees(investments, traceHash);
			return BigDecimal.ZERO;
		} else {
			// Less available
			try {
				divideFees(amount, investments, traceHash);
			} catch (Exception e) {
				e.printStackTrace();
			}
			return BigDecimal.ZERO;
		}
	}

	private void zeroFees(Iterable<Investment> investments, String traceHash) {
		for (Investment investment : investments) {
			investmentService.createFeeDecrementSnapshot(BigDecimal.ZERO, investment, traceHash);
		}
	}

	public void divideFees(BigDecimal available, Iterable<Investment> investments, String traceHash) throws Exception {
		BigDecimal totalSubtracted = BigDecimal.ZERO;
		for (Investment investment : investments) {
			InvestmentSnapshop latestSnapshop = investment.getLatestSnapshop();
			BigDecimal ratio = investment.getRatio();
			BigDecimal openingFee = latestSnapshop.getClosingFee();
			BigDecimal feeAmount = available.multiply(ratio, mathContext);
			BigDecimal closingFee = openingFee.subtract(feeAmount);
			investmentService.createFeeDecrementSnapshot(closingFee, investment, traceHash);
			totalSubtracted = totalSubtracted.add(feeAmount);
		}
		if (totalSubtracted.compareTo(available) != 0) {
			throw new Exception("Subtracted amount doesn't match the available");
		}
	}

}
