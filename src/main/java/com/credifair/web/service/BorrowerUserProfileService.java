package com.credifair.web.service;

import com.credifair.web.dao.BorrowerUserProfileDao;
import com.credifair.web.dao.UserDao;
import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by indrek.ruubel on 07/02/2016.
 */
@Service
public class BorrowerUserProfileService {

    @Autowired
    BorrowerUserProfileDao borrowerUserProfileDao;

    @Autowired
    UserDao userDao;

    public BorrowerUserProfile createBorrowerUserProfileForUser(User user) {
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        if (borrowerUserProfile == null) {
            borrowerUserProfile = new BorrowerUserProfile(user);
            borrowerUserProfileDao.save(borrowerUserProfile);
            user.setBorrowerUserProfile(borrowerUserProfile);
            userDao.save(user);
        }
        return borrowerUserProfile;
    }
}
