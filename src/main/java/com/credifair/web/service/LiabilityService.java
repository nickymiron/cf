package com.credifair.web.service;

import com.credifair.web.dao.LiabilityDao;
import com.credifair.web.model.Liability;
import com.credifair.web.model.PersonalFinancialData;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by indrek.ruubel on 20/03/2016.
 */
@Service
public class LiabilityService {

	@Autowired
	LiabilityDao liabilityDao;

	public List<Liability> findAllByPersonalFinancialData(PersonalFinancialData personalFinancialData) {
		return liabilityDao.findAllByPersonalFinancialData(personalFinancialData);
	}
}
