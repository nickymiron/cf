package com.credifair.web.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.Serializable;
import java.util.*;

/**
 * Created by indrek.ruubel on 09/03/2016.
 */
@Service
public class OauthService {

    private String OAUTH_APP_ID = "api-client";

    @Autowired
    AuthenticationService authenticationService;

    @Autowired
    DefaultTokenServices defaultTokenServices;

    @Autowired
    CustomUserDetailsService customUserDetailsService;

    public String getOauthToken(HttpServletRequest request) throws Exception {

        HttpSession session = request.getSession();
        String oauthAccessToken = (String) session.getAttribute("oauth_token");
        if (oauthAccessToken == null) {
            Object principal = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            if (principal instanceof User) {
                User userPrincipal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
                OAuth2AccessToken token = getToken(userPrincipal);
                oauthAccessToken = token.getValue();
                session.setAttribute("oauth_token", oauthAccessToken);
            }
        }
        return oauthAccessToken;
    }

    public OAuth2AccessToken getToken(User userPrincipal) {
        Collection<GrantedAuthority> authorities = userPrincipal.getAuthorities();

        Map<String, String> requestParameters = new HashMap<>();
        String clientId = OAUTH_APP_ID;
        boolean approved = true;
        Set<String> scope = new HashSet<>();
        scope.add("read write");
        Set<String> resourceIds = new HashSet<>();
        Set<String> responseTypes = new HashSet<>();
        responseTypes.add("code");
        Map<String, Serializable> extensionProperties = new HashMap<>();

        OAuth2Request oAuth2Request = new OAuth2Request(requestParameters, clientId,
                authorities, approved, scope,
                resourceIds, null, responseTypes, extensionProperties);

        UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(userPrincipal, null, authorities);
        OAuth2Authentication auth = new OAuth2Authentication(oAuth2Request, authenticationToken);

        OAuth2AccessToken token = defaultTokenServices.createAccessToken(auth);
        return token;
    }

}
