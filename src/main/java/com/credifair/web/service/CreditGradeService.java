package com.credifair.web.service;

import com.credifair.web.dao.CreditGradeDao;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditGrade;
import com.credifair.web.model.credit.CreditGradeConstructor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by indrek.ruubel on 02/04/2016.
 */
@Service
public class CreditGradeService {

	@Autowired
	CreditGradeDao creditGradeDao;

	public CreditGrade createGrade(CreditApplication creditApplication, CreditGradeConstructor creditGradeConstructor, BigDecimal apr) {
		CreditGrade creditGrade = new CreditGrade(creditApplication, creditGradeConstructor, apr);
		creditGradeDao.save(creditGrade);
		return creditGrade;
	}

}
