package com.credifair.web.service;

import com.credifair.web.api.response.FormFieldTypeWrapper;
import com.credifair.web.dao.*;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.state.CAState;
import com.credifair.web.model.state.action.CAStateActionEnum;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 01/04/2016.
 */
@Service
@Transactional
public class UserDataSaverService {

	@Autowired
	BorrowerStepService borrowerStepService;

	@Autowired
	InvestorStepService investorStepService;

	@Autowired
	UserProfileDao userProfileDao;

	@Autowired
	AddressDao addressDao;

	@Autowired
	EmploymentDao employmentDao;

	@Autowired
	LiabilityDao liabilityDao;

	@Autowired
	PersonalFinancialDataDao personalFinancialDataDao;

	@Autowired
	CAStateService caStateService;

	@Autowired
	CreditApplicationService creditApplicationService;

	@Autowired
	CAStateService CAStateService;

	@Autowired
	InvestorStateService investorStateService;

	@Transactional(rollbackOn = Exception.class)
	public void saveBorrowerData(User user, Map<UserValueFieldType, Object> data) throws Exception {
		BorrowerFlowStep step = borrowerStepService.getStep(user);
		CreditApplication creditApplication = creditApplicationService.getCreditApplication(user);
		Map<String, Object> stepData = borrowerStepService.getStepData(step);
		saveData(user, stepData, data);
		advanceToNextStep(step, creditApplication);
	}

	private void advanceToNextStep(BorrowerFlowStep step, CreditApplication creditApplication) {
		CAState caState = creditApplication.getState();
		if (step.equals(BorrowerFlowStep.STEP1_PERSONAL_INFO)) {
			creditApplicationService.createCAStateAction(caState, CAStateActionEnum.PROVIDED_PERSONAL_INFO);
		} else if(step.equals(BorrowerFlowStep.STEP2_ADDRESS)) {
			creditApplicationService.createCAStateAction(caState, CAStateActionEnum.PROVIDED_ADDRESS);
		}
	}

	private String normalizeFieldType(String fieldName) {
		fieldName = fieldName.replace("_", " ");
		fieldName = fieldName.substring(0, 1).toUpperCase() + (fieldName.substring(1, fieldName.length()).toLowerCase());
		return  fieldName;
	}

	private void saveData(User user, Map<String, Object> formCriteria, Map<UserValueFieldType, Object> givenFields) throws Exception {
		List<FormFieldTypeWrapper> neededFields = (List<FormFieldTypeWrapper>) formCriteria.get("fields");

		UserProfile userUserProfile = user.getUserProfile();
		Address address = userUserProfile.getAddress();

		for (FormFieldTypeWrapper neededField : neededFields) {
			String fieldName = neededField.getFieldName();
			UserValueFieldType neededFieldType = UserValueFieldType.valueOf(fieldName);
			Object givenValue = givenFields.get(neededFieldType);

			if (givenValue == null || givenValue instanceof String && ((String) givenValue).trim().length() == 0) {
				throw new Exception("Field '" + normalizeFieldType(fieldName) + "' is required");
			}

			UserProfile userProfile = user.getUserProfile();

			// Step 1
			if (neededFieldType.equals(UserValueFieldType.FIRST_NAME)) {
				userProfile.setFirstName((String)givenValue);
				userProfileDao.save(userProfile);
			}
			if (neededFieldType.equals(UserValueFieldType.LAST_NAME)) {
				userProfile.setLastName((String)givenValue);
				userProfileDao.save(userProfile);
			}
			if (neededFieldType.equals(UserValueFieldType.DATE_OF_BIRTH)) {
				DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
				LocalDate date = LocalDate.parse((String) givenValue, formatter);
				userProfile.setDateOfBirth(date);
				userProfileDao.save(userProfile);
			}
			if (neededFieldType.equals(UserValueFieldType.PHONE_NUMBER)) {
				userProfile.setPhoneNumber(String.valueOf(givenValue));
				userProfileDao.save(userProfile);
			}
			if (neededFieldType.equals(UserValueFieldType.EDUCATION)) {
				userProfile.setEducation(Education.valueOf((String)givenValue));
				userProfileDao.save(userProfile);
			}
			if (neededFieldType.equals(UserValueFieldType.MARITAL_STATUS)) {
				userProfile.setMaritalStatus(MaritalStatus.valueOf((String) givenValue));
				userProfileDao.save(userProfile);
			}

			// Step 2
			if (address == null) {
				address = new Address();
				address.setUserProfile(userProfile);
				addressDao.save(address);
			}
			if (neededFieldType.equals(UserValueFieldType.ADDRESS)) {
				address.setAddress((String) givenValue);
				addressDao.save(address);
			}

			if (neededFieldType.equals(UserValueFieldType.CITY)) {
				address.setCity((String) givenValue);
				addressDao.save(address);
			}
			if (neededFieldType.equals(UserValueFieldType.POSTAL_CODE)) {
				address.setPostalCode(String.valueOf(givenValue));
				addressDao.save(address);
			}

		}
	}

//	public void saveInvestorData(User user, Map<UserValueFieldType, Object> data) {
//		InvestorUserProfile investorUserProfile = user.getInvestorUserProfile();
//		InvestorFlowStep step = investorStepService.getStep(user);
//		Map<String, Object> neededFields = investorStepService.getStepData(step);
//		saveData(user, neededFields, data);
//	}

//	private boolean saveData(User user, Map<String, Object> neededFields, Map<UserValueFieldType, Object> givenFields) {
//		boolean formComplete = false;
//		List<FormFieldTypeWrapper> fieldsMap = (List<FormFieldTypeWrapper>) neededFields.get("fields");
//
//		UserProfile userProfile = user.getUserProfile();
//		Address address = userProfile.getAddress();
//		BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
//		Employment employment = borrowerUserProfile.getEmployment();
//		PersonalFinancialData personalFinancialData = borrowerUserProfile.getPersonalFinancialData();
//
//		for (FormFieldTypeWrapper formFieldTypeWrapper : fieldsMap) {
//			String fieldName = formFieldTypeWrapper.getFieldName();
//			UserValueFieldType neededField = UserValueFieldType.valueOf(fieldName);
//			Object givenValue = givenFields.get(neededField);
//
//			if (givenValue == null) {
//				continue;
//			}
//
//			if (neededField.equals(UserValueFieldType.FIRST_NAME)) {
//				userProfile.setFirstName((String)givenValue);
//				userProfileDao.save(userProfile);
//			}
//			if (neededField.equals(UserValueFieldType.LAST_NAME)) {
//				userProfile.setLastName((String)givenValue);
//				userProfileDao.save(userProfile);
//			}
//			if (neededField.equals(UserValueFieldType.DATE_OF_BIRTH)) {
//				DateTimeFormatter formatter = DateTimeFormatter.ISO_DATE_TIME;
//				LocalDate date = LocalDate.parse((String) givenValue, formatter);
//				userProfile.setDateOfBirth(date);
//				userProfileDao.save(userProfile);
//			}
//			if (neededField.equals(UserValueFieldType.PHONE_NUMBER)) {
//				userProfile.setPhoneNumber(String.valueOf((Integer)givenValue));
//				userProfileDao.save(userProfile);
//			}
//			if (neededField.equals(UserValueFieldType.EDUCATION)) {
//				userProfile.setEducation(Education.valueOf((String)givenValue));
//				userProfileDao.save(userProfile);
//			}
//			if (neededField.equals(UserValueFieldType.MARITAL_STATUS)) {
//				userProfile.setMaritalStatus(MaritalStatus.valueOf((String) givenValue));
//				userProfileDao.save(userProfile);
//			}
//
//			if (address == null) {
//				address = new Address();
//				address.setUserProfile(userProfile);
//				addressDao.save(address);
//			}
//
//			if (neededField.equals(UserValueFieldType.ADDRESS)) {
//				address.setAddress((String) givenValue);
//				addressDao.save(address);
//			}
//
//			if (neededField.equals(UserValueFieldType.COUNTRY)) {
//				Country country = Country.valueOf((String) givenValue);
//				address.setCountry(country);
//				addressDao.save(address);
//			}
//			if (neededField.equals(UserValueFieldType.CITY)) {
//				address.setCity((String) givenValue);
//				addressDao.save(address);
//			}
//			if (neededField.equals(UserValueFieldType.POSTAL_CODE)) {
//				address.setPostalCode(String.valueOf((Integer)givenValue));
//				addressDao.save(address);
//			}
//
//			if (employment == null) {
//				employment = new Employment();
//				employment.setBorrowerUserProfile(borrowerUserProfile);
//				employmentDao.save(employment);
//			}
//
//			if (neededField.equals(UserValueFieldType.EMPLOYMENT_STATUS)) {
//				employment.setEmploymentStatus(EmploymentStatus.valueOf((String) givenValue));
//				employmentDao.save(employment);
//			}
//
//			if (neededField.equals(UserValueFieldType.WORK_INDUSTRY)) {
//				employment.setWorkIndustry(WorkIndustry.valueOf((String)givenValue));
//				employmentDao.save(employment);
//			}
//			if (neededField.equals(UserValueFieldType.CURRENT_JOB_TITLE)) {
//				employment.setJobTitle((String) givenValue);
//				employmentDao.save(employment);
//			}
//			if (neededField.equals(UserValueFieldType.CURRENT_EMPLOYMENT_TIME)) {
//				employment.setCurrentEmploymentTime(EmploymentTime.valueOf((String)givenValue));
//				employmentDao.save(employment);
//			}
//			if (neededField.equals(UserValueFieldType.TOTAL_EMPLOYMENT_TIME)) {
//				employment.setTotalEmploymentTime(EmploymentTime.valueOf((String)givenValue));
//				employmentDao.save(employment);
//			}
//			if (neededField.equals(UserValueFieldType.CURRENT_EMPLOYER)) {
//				employment.setCurrentEmployer((String) givenValue);
//				employmentDao.save(employment);
//			}
//			if (personalFinancialData == null) {
//				personalFinancialData = new PersonalFinancialData();
//				personalFinancialData.setBorrowerUserProfile(borrowerUserProfile);
//				personalFinancialDataDao.save(personalFinancialData);
//			}
//			if (neededField.equals(UserValueFieldType.HOME_OWNERSHIP)) {
//				personalFinancialData.setHomeOwnership(HomeOwnership.valueOf((String) givenValue));
//				personalFinancialDataDao.save(personalFinancialData);
//			}
//			if (neededField.equals(UserValueFieldType.MONTHLY_NET_INCOME)) {
//				BigDecimal netIncome = new BigDecimal((Integer) givenValue);
//				personalFinancialData.setMonthlyNetIncome(netIncome);
//				personalFinancialDataDao.save(personalFinancialData);
//			}
//			if (neededField.equals(UserValueFieldType.OTHER_REGULAR_INCOME)) {
//				BigDecimal otherIncome = new BigDecimal((Integer) givenValue);
//				personalFinancialData.setOtherRegularIncome(otherIncome);
//				personalFinancialDataDao.save(personalFinancialData);
//			}
//			if (neededField.equals(UserValueFieldType.LIVING_EXPENSES)) {
//				BigDecimal livingExpenses = new BigDecimal((Integer) givenValue);
//				personalFinancialData.setLivingExpenses(livingExpenses);
//				personalFinancialDataDao.save(personalFinancialData);
//			}
//			if (neededField.equals(UserValueFieldType.WILL_YOU_PROVIDE_VERIFICATION_OF_YOUR_INCOME_AND_EXPENSES)) {
//				String answer = (String) givenValue;
//				personalFinancialData.setWillProvideDocumentsOfIncomeAndExpenses((answer == "YES" ? true : false));
//				personalFinancialDataDao.save(personalFinancialData);
//			}
//			if(neededField.equals(UserValueFieldType.LIABILITIES)) {
//				List<LinkedHashMap> liabilities = (List<LinkedHashMap>) givenValue;
//				for (LinkedHashMap liabilityReq : liabilities) {
//					Liability liability = new Liability(personalFinancialData,
//							LiabilityType.valueOf((String)liabilityReq.get("type")),
//							(String)liabilityReq.get("creditor"),
//							new BigDecimal((Integer)liabilityReq.get("mo_payment")),
//							new BigDecimal((Integer)liabilityReq.get("outstanding")),
//							LiabilitySecurity.valueOf((String)liabilityReq.get("secured_by")));
//					liabilityDao.save(liability);
//				}
//				Map<String, Object> data = borrowerStepService.getStepData(BorrowerFlowStep.APPLICATION_FORM);
//				List<Object> fields = (List<Object>) data.get("fields");
//				if (fields.isEmpty()) {
//					CreditApplication creditApplication = creditApplicationService.getStarted(user);
//					CAState state = CAStateService.createPreparingOfferState(user.getBorrowerUserProfile(), creditApplication);
//					creditApplicationService.assignStateToCA(state, creditApplication);
//					formComplete = true;
//				}
//			}
//		}
//		return formComplete;
//	}
}
