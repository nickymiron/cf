package com.credifair.web.service;

import com.credifair.web.dao.LoanBalanceDao;
import com.credifair.web.model.*;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.transaction.Transaction;
import com.google.common.collect.Iterables;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.FormatStyle;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by indrek.ruubel on 07/02/2016.
 */
@Service
public class LoanBalanceService {

    @Autowired
    LoanBalanceDao loanBalanceDao;

    @Autowired
    LoanService loanService;

    @Autowired
    BalanceService balanceService;

    @Autowired
    InvestmentService investmentService;

    @Autowired
    InterestService interestService;

    @Autowired
    TransactionService transactionService;

    @Autowired
    PrincipalService principalService;

    @Autowired
    FeeService feeService;

    public LoanBalance getLatestLoanBalanceForBorrower(BorrowerUserProfile borrowerUserProfile) {
        LoanBalance balance = loanBalanceDao.findTopByBorrowerUserProfileOrderByCreatedDesc(borrowerUserProfile);
        return balance;
    }

    public List<LoanBalance> findAllByEndBalanceGreaterThan(BigDecimal amount) {
        List<LoanBalance> balances = loanBalanceDao.findAllByClosingPrincipalGreaterThanOrderByCreatedAscBorrowerUserProfileAsc(amount);

        Map<BorrowerUserProfile, LoanBalance> map = new HashMap<>();
        for (LoanBalance balance : balances) {
            BorrowerUserProfile borrowerUserProfile = balance.getBorrowerUserProfile();
            map.put(borrowerUserProfile, balance);
        }
        return new ArrayList<>(map.values());
    }

    public LoanBalance addPrincipal(Loan loan, String traceHash) throws Exception {
        if(!loanService.isLoanFunded(loan)) {
            throw new Exception("Loan not fully funded!");
        }

        BorrowerUserProfile borrowerUserProfile = loan.getBorrowerUserProfile();
        LoanBalance latestLoanBalance = getLatestLoanBalanceForBorrower(borrowerUserProfile);

        BigDecimal openingInterest = latestLoanBalance == null ? BigDecimal.ZERO : latestLoanBalance.getClosingInterest();
        BigDecimal openingPrincipal = latestLoanBalance == null ? BigDecimal.ZERO : latestLoanBalance.getClosingPrincipal();
        BigDecimal closingPrincipal = openingPrincipal.add(loan.getAmount());

        LoanBalance loanBalance = new LoanBalance(loan.getCurrency(), openingInterest,
                openingInterest, openingPrincipal, closingPrincipal, borrowerUserProfile,
                LoanBalance.DifferenceType.INCREMENTED_PRINCIPAL);
        loanBalanceDao.save(loanBalance);
        Transaction transaction = transactionService.createBorrowerBorrowTransaction(loan.getAmount(), loan.getCurrency(), borrowerUserProfile.getUser(), traceHash);
        transactionService.createBalanceTransactionLink(transaction, loanBalance.getId());
        return loanBalance;
    }

    public LoanBalance addInterest(BorrowerUserProfile borrowerUserProfile, BigDecimal interestEarned, Currency currency) {
        LoanBalance latestLoanBalance = getLatestLoanBalanceForBorrower(borrowerUserProfile);

        BigDecimal openingInterest = latestLoanBalance == null ? BigDecimal.ZERO : latestLoanBalance.getClosingInterest();
        BigDecimal closingInterest = openingInterest.add(interestEarned);
        BigDecimal openingPrincipal = latestLoanBalance == null ? BigDecimal.ZERO : latestLoanBalance.getClosingPrincipal();

        LoanBalance loanBalance = new LoanBalance(currency, openingInterest,
                closingInterest, openingPrincipal, openingPrincipal, borrowerUserProfile,
                LoanBalance.DifferenceType.INCREMENTED_INTEREST);

        loanBalanceDao.save(loanBalance);
        return loanBalance;
    }

    public LoanBalance subtractInterestPayment(BorrowerUserProfile borrowerUserProfile, BigDecimal amount, Currency currency, boolean zeroPayoff) {
        LoanBalance latest = getLatestLoanBalanceForBorrower(borrowerUserProfile);
        BigDecimal openingPrincipal = latest.getOpeningPrincipal();
        BigDecimal closingPrincipal = latest.getClosingPrincipal();
        BigDecimal openingInterest = latest.getClosingInterest();
        amount = amount.setScale(openingInterest.scale(), RoundingMode.HALF_UP);
        BigDecimal closingInterest = openingInterest.subtract(amount);
        if ((openingInterest.compareTo(amount) == 0) || zeroPayoff) {
            // Try to avoid having -0.000001
            System.out.println("Try to avoid having -0.000001 interest on loanBalance");
            closingInterest = BigDecimal.ZERO;
        }

        LoanBalance loanBalance = new LoanBalance(currency, openingInterest,
                closingInterest, openingPrincipal, closingPrincipal, borrowerUserProfile,
                LoanBalance.DifferenceType.DECREMENTED_INTEREST);

        loanBalanceDao.save(loanBalance);
        return loanBalance;
    }

    public LoanBalance subtractPrincipalPayment(BorrowerUserProfile borrowerUserProfile, BigDecimal amount, Currency currency, boolean zeroPayoff) {
        LoanBalance latest = getLatestLoanBalanceForBorrower(borrowerUserProfile);
        BigDecimal openingPrincipal = latest.getClosingPrincipal();
        BigDecimal closingPrincipal = openingPrincipal.subtract(amount);
        if ((openingPrincipal.compareTo(amount) == 0) || zeroPayoff) {
            // Try to avoid having -0.000001
            System.out.println("Try to avoid having -0.000001 principal on loanBalance");
            closingPrincipal = BigDecimal.ZERO;
        }
        BigDecimal openingInterest = latest.getOpeningInterest();
        BigDecimal closingInterest = latest.getClosingInterest();

        LoanBalance loanBalance = new LoanBalance(currency, openingInterest,
                closingInterest, openingPrincipal, closingPrincipal, borrowerUserProfile,
                LoanBalance.DifferenceType.DECREMENTED_PRINCIPAL);

        loanBalanceDao.save(loanBalance);
        return loanBalance;
    }

    @Transactional(rollbackOn = Exception.class)
    public synchronized void payoff(BigDecimal available, BorrowerUserProfile borrowerUserProfile) throws Exception {
        UserProfile userProfile = borrowerUserProfile.getUser().getUserProfile();
        boolean hasMoney = balanceService.hasEnoughMoneyOnBalance(userProfile, available);
        if (!hasMoney) {
            throw new Exception("Not enough balance");
        }

        // Generate traceHash to track the transactions
        String traceHash = transactionService.generateTraceHash();

        Iterable<Investment> investments =
            investmentService.findAllActiveInvestmentsOnBorrowerOrderByAscending(borrowerUserProfile);

        if (Iterables.isEmpty(investments)) {
            throw new Exception("No active investments");
        }

        // Calculate investments ratios
        interestService.calculateRatios(investments);

        // TODO: Get it from creditApplication?
        Currency currency = investments.iterator().next().getCurrency();

        // GLOBAL VARIABLES
        BigDecimal totalFeeRepayed = BigDecimal.ZERO;
        BigDecimal totalInterestRepayed = BigDecimal.ZERO;
        BigDecimal totalPrincipalRepayed = BigDecimal.ZERO;

        BigDecimal totalRepayed = BigDecimal.ZERO;

        // BUCKETS
        // 1. FEE BUCKET

        BigDecimal leftOver = feeService.payFee(available, currency, investments, traceHash);
        totalFeeRepayed = available.subtract(leftOver);
        totalRepayed = totalRepayed.add(totalFeeRepayed);
        available = leftOver;

        if (available.compareTo(BigDecimal.ZERO) <= 0) {
            // No money left for anything else
            // Subtract from borrower balance
            balanceService.subtractRepayment(totalRepayed, currency,
                    borrowerUserProfile.getUser().getUserProfile());
            return;
        }

        leftOver = interestService.payInterest(available, currency, investments, traceHash);
        totalInterestRepayed = available.subtract(leftOver);
//        // Subtract from borrower loanbalance
//        // 0.032876712328800
        totalRepayed = totalRepayed.add(totalInterestRepayed);
        subtractInterestPayment(borrowerUserProfile, totalFeeRepayed.add(totalInterestRepayed), currency,
                (leftOver.compareTo(BigDecimal.ZERO) > 0));
        available = leftOver;

        if (available.compareTo(BigDecimal.ZERO) <= 0) {
            // No money left for anything else
            // Subtract from balance
            balanceService.subtractRepayment(totalRepayed, currency,
                    borrowerUserProfile.getUser().getUserProfile());
            return;
        }

        leftOver = principalService.payPrincipal(available, currency, investments, traceHash);
        totalPrincipalRepayed = available.subtract(leftOver);
        subtractPrincipalPayment(borrowerUserProfile, totalPrincipalRepayed, currency,
                (leftOver.compareTo(BigDecimal.ZERO) > 0));
        totalRepayed = totalRepayed.add(totalPrincipalRepayed);

        // Subtract from balance
        Balance borrowerBalance = balanceService.subtractRepayment(totalRepayed, currency,
                borrowerUserProfile.getUser().getUserProfile());

        // Create transactions subtracting interest and principal from borrower
        Transaction principalRepayedTransaction = transactionService.createBorrowerPrincipalRepayedTransaction(totalPrincipalRepayed, currency, borrowerUserProfile.getUser(), traceHash);
        transactionService.createLoanBalanceTransactionLink(principalRepayedTransaction, borrowerBalance.getId());

        Transaction interestRepayedTransaction = transactionService.createBorrowerInterestRepayedTransaction(totalInterestRepayed, currency, borrowerUserProfile.getUser(), traceHash);
        transactionService.createLoanBalanceTransactionLink(interestRepayedTransaction, borrowerBalance.getId());


        available = leftOver;

        if (available.compareTo(BigDecimal.ZERO) > 0) {
            System.out.println("Tried paying off more than needed, do nothing");
        }
    }

    public List<HashMap<String, Object>> getPayoffSchedule(User user) {
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();
        BigDecimal minPayment = BigDecimal.TEN;
        LoanBalance loanBalance = getLatestLoanBalanceForBorrower(borrowerUserProfile);
        BigDecimal principal = loanBalance.getClosingPrincipal();
        BigDecimal interest = loanBalance.getClosingInterest();
        List<HashMap<String, Object>> out = new ArrayList<>();

        // Figure out what is the next date to pay
        CreditApplication creditApplication = borrowerUserProfile.getCreditApplication();
        Integer gracePeriodDays = creditApplication.getGracePeriodDays();
        Loan loan = loanService.findFirstActiveLoanWithPrincipal(borrowerUserProfile);

        LocalDateTime gracePeriodReachedDate = loan.getFundedDateTime().plusDays(gracePeriodDays);

        LocalDateTime nextPayment;
        LocalDateTime now = LocalDateTime.now();
        int dayOfMonth = gracePeriodReachedDate.getDayOfMonth();
        if (now.getDayOfMonth() > dayOfMonth) {
            LocalDateTime nextMonth = now.plusMonths(1);
            nextPayment = LocalDateTime.of(nextMonth.getYear(), nextMonth.getMonth(), dayOfMonth, 0, 0);
        } else if (now.getDayOfMonth() == dayOfMonth) {
            nextPayment = now.plusMonths(1);
        } else {
            // Less
            nextPayment = LocalDateTime.of(now.getYear(), now.getMonth(), dayOfMonth, 0, 0);
        }

        // Figure out how many months to pay
        BigDecimal totalPrincipal = loanBalance.getClosingPrincipal();
        BigDecimal monthsDecimal = totalPrincipal.divide(minPayment);
        double monthsDouble = monthsDecimal.doubleValue();
        int months = monthsDecimal.toBigInteger().intValue();
//        if (monthsDouble % minPayment.intValue() > 0) {
//        }

        if ((monthsDouble == Math.floor(monthsDouble)) && !Double.isInfinite(monthsDouble)) {
            // No remainder
        } else {
            months++;
        }


        Currency currency = creditApplication.getCurrency();
        BigDecimal totalInterest = loanBalance.getClosingInterest();
        BigDecimal monthlyInterest = totalInterest.divide(new BigDecimal(months), 2, RoundingMode.HALF_UP);


        while (months > 0) {
            BigDecimal principalMonth = minPayment;
            if (principal.compareTo(minPayment) < 0) {
                principalMonth = principal;
                principal = principal.subtract(principal);
            } else {
                principal = principal.subtract(principalMonth);
            }
            principalMonth = principalMonth.setScale(2, RoundingMode.HALF_UP);
            BigDecimal total = principalMonth.add(monthlyInterest);

            HashMap<String, Object> entry = new HashMap<>();
            entry.put("dateTime", DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(nextPayment));
            entry.put("principal", String.format("%s %s", principalMonth, currency.getSymbol()));
            entry.put("interest", String.format("%s %s", monthlyInterest.setScale(2, RoundingMode.HALF_UP), currency.getSymbol()));
            entry.put("total", String.format("%s %s", total, currency.getSymbol()));
            entry.put("totalTextNumber", String.format("%s %s", currency.getSymbol(), total));
            entry.put("totalText", total);
            entry.put("dateTimeText", String.format("%s (in %s days)",
                    DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM).format(nextPayment), Duration.between(LocalDateTime.now(), nextPayment).toDays()));
            out.add(entry);

            nextPayment = nextPayment.plusMonths(1);
            months--;
        }

        return out;
    }

    public HashMap<String, Object> getPayoffDetails(User user) {

        HashMap<String, Object> out = new HashMap<>();
        BorrowerUserProfile borrowerUserProfile = user.getBorrowerUserProfile();

        if (!interestService.hasOverGracePeriod(borrowerUserProfile)) {
            out.put("duePaymentAvailable", false);
            return out;
        } else {
            Currency currency = user.getCountry().getCurrency();
            List<HashMap<String, Object>> payoffSchedule = getPayoffSchedule(user);
            HashMap<String, Object> firstPayment = payoffSchedule.get(0);
            out.put("duePaymentAvailable", true);
            out.put("amountText", firstPayment.get("totalText"));
            out.put("currency", currency);
            out.put("currencySymbol", currency.getSymbol());
            out.put("duePaymentAmountText", firstPayment.get("totalTextNumber"));
            out.put("duePaymentTimeLeftText", firstPayment.get("dateTimeText"));
            out.put("recipientName", "Nikolay Mironenko");
            out.put("recipientType", "SORT");
            out.put("recipientUkSort", "30-23-45");
            out.put("recipientAccountNumber", "234242223");
            return out;
        }

//        return new HashMap<String, Object>(){{
//            put("duePaymentAvailable", true);
//            put("amountText", "100.00");
//            put("currency", "USD");
//            put("currencySymbol", "$");
//            put("duePaymentAmountText", "$ 100.00");
//            put("duePaymentTimeLeftText", "25th May (in 3 days)");
//            put("recipientName", "Nikolay Mironenko");
//            put("recipientType", "SORT");
//            put("recipientUkSort", "30-23-45");
//            put("recipientAccountNumber", "234242223");
//        }};

    }

}
