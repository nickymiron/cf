package com.credifair.web.service;

import com.credifair.web.api.request.BankDetailsRequest;
import com.credifair.web.dao.RecipientDao;
import com.credifair.web.model.User;
import com.credifair.web.model.recipient.IbanRecipient;
import com.credifair.web.model.recipient.Recipient;
import com.credifair.web.model.recipient.SortRecipient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
@Service
public class RecipientService {

	@Autowired
	RecipientDao recipientDao;

	public boolean hasRecipientAccount(User user) {
		Recipient recipient = getRecipientAccount(user);
		return recipient == null ? false : true;
	}

	public Recipient getRecipientAccount(User user) {
		List<Recipient> recipients = recipientDao.findAllByUser(user);
		if (recipients == null || recipients.isEmpty()) {
			return null;
		}
		return recipients.get(0);
	}

	public void saveIbanRecipient(String iban, User user) {
		Recipient recipient = new IbanRecipient(user, iban);
		recipientDao.save(recipient);
	}

	public void saveSortRecipient(String sort, String accountNumber, User user) {
		Recipient recipient = new SortRecipient(user, sort, accountNumber);
		recipientDao.save(recipient);
	}

	public void saveRecipientFromRequest(BankDetailsRequest request, User user) throws Exception {
		if (request.getIban() != null && !request.getIban().isEmpty()) {
			// Save iban
			saveIbanRecipient(request.getIban(), user);
		} else if (request.getSort() != null
				&& request.getAccountNumber() != null
				&& !request.getSort().isEmpty()
				&& !request.getAccountNumber().isEmpty()) {
			// Save sort
			saveSortRecipient(request.getSort(), request.getAccountNumber(), user);
		} else {
			throw new Exception("No recipient saved");
		}
	}
}
