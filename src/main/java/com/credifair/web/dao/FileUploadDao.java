package com.credifair.web.dao;

import com.credifair.web.model.FileUpload;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 02/04/2016.
 */
@Transactional
public interface FileUploadDao extends CrudRepository<FileUpload, Long> {
	FileUpload findByFilename(String filename);
}
