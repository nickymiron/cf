package com.credifair.web.dao;

import com.credifair.web.model.UserProfile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 31/03/2016.
 */
@Transactional
public interface UserProfileDao extends CrudRepository<UserProfile, Long> {
}
