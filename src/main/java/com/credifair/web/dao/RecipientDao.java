package com.credifair.web.dao;

import com.credifair.web.model.User;
import com.credifair.web.model.recipient.Recipient;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 22/03/2016.
 */
@Transactional
public interface RecipientDao extends CrudRepository<Recipient, Long> {
	List<Recipient> findAllByUser(User user);
}
