package com.credifair.web.dao;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.LoanBalance;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@Transactional
public interface LoanBalanceDao extends CrudRepository<LoanBalance, Long> {
    LoanBalance findTopByBorrowerUserProfileOrderByCreatedDesc(BorrowerUserProfile borrowerUserProfile);
    List<LoanBalance> findAllByClosingPrincipalGreaterThanOrderByCreatedAscBorrowerUserProfileAsc(BigDecimal amount);
}
