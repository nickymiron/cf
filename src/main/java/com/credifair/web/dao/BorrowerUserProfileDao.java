package com.credifair.web.dao;

import com.credifair.web.model.BorrowerUserProfile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@Transactional
public interface BorrowerUserProfileDao extends CrudRepository<BorrowerUserProfile, Long> {
}
