package com.credifair.web.dao;

import com.credifair.web.model.transaction.bank.BankTransaction;
import com.credifair.web.model.transaction.bank.BankTransactionState;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
@Transactional
public interface BankTransactionDao extends CrudRepository<BankTransaction, Long> {
	BankTransaction findByBankTransactionId(String transactionId);
	BankTransaction findByBankTransactionIdAndBankName(String transactionId, String bankName);
	List<BankTransaction> findAllByState(BankTransactionState state);
}
