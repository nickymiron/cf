package com.credifair.web.dao;

import com.credifair.web.model.verification.Verification;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 03/04/2016.
 */
@Transactional
public interface VerificationDao extends CrudRepository<Verification, Long> {

	@Query(value = "SELECT * FROM verification ve WHERE ve.fk_user = ?1 AND type = ?2", nativeQuery = true)
	Verification findByUserAndType(Long userId, String type);
}
