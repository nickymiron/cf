package com.credifair.web.dao;

import com.credifair.web.model.transaction.bank.BankTransaction;
import com.credifair.web.model.transaction.bank.BankTransactionLink;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 28/04/2016.
 */
@Transactional
public interface BankTransactionLinkDao extends CrudRepository<BankTransactionLink, Long> {
	Iterable<BankTransactionLink> findAllByBankTransaction(BankTransaction transaction);
}
