package com.credifair.web.dao;

import com.credifair.web.model.Investment;
import com.credifair.web.model.InvestorUserProfile;
import com.credifair.web.model.Loan;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@Transactional
public interface InvestmentDao extends CrudRepository<Investment, Long> {
    List<Investment> findAllByLoan(Loan loan);

    // Fetch all that have principal greater 0
    // They all belong to a borrower
    // closed datetime is null
    // They belong to a investor
    @Query(value = "select * from investment i " +
            "left join loan l on i.fk_loan=l.id " +
            "left join investment_snapshot i_s on i.fk_investment_snapshot=i_s.id " +
            "where i.fk_investor_user_profile=?1 " +
            "and l.fk_borrower_user_profile=?2 " +
            "and i.closed_datetime is null " +
            "and i_s.closing_principal > 0", nativeQuery = true)
    Iterable<Investment> findAllByInvestorUserProfileAndLoanBorrowerUserProfileWhereClosedDateTimeIsNotNullAndPrincipalGraterThan0(Long investorId, Long borrowerId);

    // closed datetime is null
    // All belong to one borrower
    // closing principal is larger than 0
    // order by invested_datetime ascending
    @Query(value = "select * from investment i " +
            "left join investment_snapshot i_s on i.fk_investment_snapshot=i_s.id " +
            "left join loan l on i.fk_loan=l.id " +
            "where i.closed_datetime is null " +
            "and l.fk_borrower_user_profile =?1 " +
            "and i_s.closing_principal > 0 " +
            "order by i.invested_datetime asc", nativeQuery = true)
    Iterable<Investment> findAllActiveInvestmentsOnBorrowerOrderByAscending(Long borrowerId);
    Iterable<Investment> findAllByInvestorUserProfile(InvestorUserProfile investor);

    @Query(value = "select * from investment i " +
            "left join investment_snapshot i_s on i.fk_investment_snapshot=i_s.id " +
            "left join loan l on i.fk_loan=l.id " +
            "where i.closed_datetime is null " +
            "and i.fk_investor_user_profile in ?1 " +
            "and i_s.closing_principal > 0 " +
            "order by i.invested_datetime asc", nativeQuery = true)
    List<Investment> findAllActiveInvestmentsForInvestors(List<Long> investorUserProfileIds);
}