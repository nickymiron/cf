package com.credifair.web.dao;

import com.credifair.web.model.EmailSubscriber;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 16/01/2016.
 */
@Transactional
public interface EmailSubscriberDao extends CrudRepository<EmailSubscriber, Long> {
    EmailSubscriber findByEmail(String email);
}
