package com.credifair.web.dao;

import com.credifair.web.model.InvestorUserProfile;
import com.credifair.web.model.investor.state.InvestorState;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 13/04/2016.
 */
@Transactional
public interface InvestorStateDao extends CrudRepository<InvestorState, Long> {
	InvestorState findTopByInvestorUserProfileOrderByIdDesc(InvestorUserProfile investorUserProfile);
}
