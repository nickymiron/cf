package com.credifair.web.dao;

import com.credifair.web.model.credit.CreditGrade;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 02/04/2016.
 */
@Transactional
public interface CreditGradeDao extends CrudRepository<CreditGrade, Long> {
}
