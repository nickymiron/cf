package com.credifair.web.dao;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.Loan;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.math.BigDecimal;
import java.util.List;

/**
 * Created by indrek.ruubel on 29/01/2016.
 */
@Transactional
public interface LoanDao extends CrudRepository<Loan, Long> {
    List<Loan> findAllByFundedDateTimeIsNotNullAndAmountGreaterThanOrderByFundedDateTimeAscBorrowerUserProfileAsc(BigDecimal amount);
    List<Loan> findAllByAmountIsGreaterThanEqualAndBorrowerUserProfileAndClosedDateTimeIsNullOrderByFundedDateTimeAsc(BigDecimal amount, BorrowerUserProfile borrowerUserProfile);
    Loan findTopByFundedDateTimeIsNotNullAndBorrowerUserProfileAndClosedDateTimeIsNullOrderByFundedDateTimeDesc(BorrowerUserProfile borrowerUserProfile);
    Loan findTopByFundedDateTimeIsNotNullAndBorrowerUserProfileAndClosedDateTimeIsNullOrderByFundedDateTimeAsc(BorrowerUserProfile borrowerUserProfile);
}
