package com.credifair.web.dao;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.Currency;
import com.credifair.web.model.credit.CreditApplication;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * Created by indrek.ruubel on 16/03/2016.
 */
@Transactional
public interface CreditApplicationDao extends CrudRepository<CreditApplication, Long> {
	CreditApplication findByBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile);

	@Query(value = "SELECT * FROM credit_application left join ca_state ON credit_application.fk_state=ca_state.id WHERE ca_state.state = ?1", nativeQuery = true)
	List<CreditApplication> findAllWithState(String state);

	@Query(value = "SELECT * FROM credit_application left join ca_state ON credit_application.fk_state=ca_state.id WHERE ca_state.state IN ?1 AND currency = ?2", nativeQuery = true)
	List<CreditApplication> findAllWithStatesForCurrency(List<String> states, String currency);
}
