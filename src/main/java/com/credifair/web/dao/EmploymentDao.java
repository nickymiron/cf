package com.credifair.web.dao;

import com.credifair.web.model.Employment;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 01/04/2016.
 */
@Transactional
public interface EmploymentDao extends CrudRepository<Employment, Long> {
}
