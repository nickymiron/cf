package com.credifair.web.dao;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.state.CAState;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 28/03/2016.
 */
@Transactional
public interface StateDao extends CrudRepository<CAState, Long> {
	CAState findTopByBorrowerUserProfileOrderByDateTimeDescIdDesc(BorrowerUserProfile borrowerUserProfile);
	CAState findTopByCreditApplicationOrderByIdDesc(CreditApplication creditApplication);
}
