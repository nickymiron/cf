package com.credifair.web.dao;

import com.credifair.web.model.transaction.Transaction;
import com.credifair.web.model.transaction.link.TransactionLink;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 17/04/2016.
 */
@Transactional
public interface TransactionLinkDao extends CrudRepository<TransactionLink, Long> {
	Iterable<TransactionLink> findAllByTransactionIn(Iterable<Transaction> transactions);
}
