package com.credifair.web.dao;

import com.credifair.web.model.InvestorUserProfile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 03/02/2016.
 */
@Transactional
public interface InvestorUserProfileDao extends CrudRepository<InvestorUserProfile, Long> {
}
