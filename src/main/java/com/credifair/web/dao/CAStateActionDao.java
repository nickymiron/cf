package com.credifair.web.dao;

import com.credifair.web.model.state.CAState;
import com.credifair.web.model.state.action.CAStateAction;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
@Transactional
public interface CAStateActionDao extends CrudRepository<CAStateAction, Long> {
	List<CAStateAction> findAllByCaState(CAState state);
}
