package com.credifair.web.dao;

import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditLimit;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 11/05/2016.
 */
@Transactional
public interface CreditLimitDao extends CrudRepository<CreditLimit, Long> {
	CreditLimit findTopByCreditApplicationOrderByIdDesc(CreditApplication creditApplication);
}
