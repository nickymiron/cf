package com.credifair.web.dao;

import com.credifair.web.model.payout.PayoutInstruction;
import com.credifair.web.model.payout.PayoutState;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by indrek.ruubel on 29/05/2016.
 */
@Transactional
public interface PayoutInstructionDao extends CrudRepository<PayoutInstruction, Long> {
	Iterable<PayoutInstruction> findAllByPayoutState(PayoutState state);
}
