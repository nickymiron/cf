package com.credifair.web.dao;

import com.credifair.web.model.User;
import com.credifair.web.model.UserProfile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 11/01/2016.
 */
@Transactional
public interface UserDao extends CrudRepository<User, Long> {
    User findByEmail(String email);
    User findByUserProfile(UserProfile userProfile);
}
