package com.credifair.web.dao;

import com.credifair.web.model.Investment;
import com.credifair.web.model.InvestmentSnapshop;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 07/02/2016.
 */
@Transactional
public interface InvestmentSnapshotDao extends CrudRepository<InvestmentSnapshop, Long> {
	InvestmentSnapshop findTopByInvestmentOrderByIdAsc(Investment investment);
}
