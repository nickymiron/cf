package com.credifair.web.dao;

import com.credifair.web.model.BorrowerUserProfile;
import com.credifair.web.model.PersonalFinancialData;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 01/04/2016.
 */
@Transactional
public interface PersonalFinancialDataDao extends CrudRepository<PersonalFinancialData, Long> {
	PersonalFinancialData findByBorrowerUserProfile(BorrowerUserProfile borrowerUserProfile);
}
