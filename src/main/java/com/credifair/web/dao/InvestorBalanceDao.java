package com.credifair.web.dao;

import com.credifair.web.model.InvestorBalance;
import com.credifair.web.model.InvestorUserProfile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 20/04/2016.
 */
@Transactional
public interface InvestorBalanceDao extends CrudRepository<InvestorBalance, Long> {
	InvestorBalance findTopByInvestorUserProfileOrderByIdDesc(InvestorUserProfile investorUserProfile);
	List<InvestorBalance> findAllByInvestorUserProfileOrderByIdDesc(InvestorUserProfile investorUserProfile);
}
