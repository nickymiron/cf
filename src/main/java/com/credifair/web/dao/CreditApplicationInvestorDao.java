package com.credifair.web.dao;

import com.credifair.web.model.InvestorUserProfile;
import com.credifair.web.model.credit.CreditApplication;
import com.credifair.web.model.credit.CreditApplicationInvestor;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 06/04/2016.
 */
@Transactional
public interface CreditApplicationInvestorDao extends CrudRepository<CreditApplicationInvestor, Long> {
	List<CreditApplicationInvestor> findByInvestorUserProfile(InvestorUserProfile investorUserProfile);
	CreditApplicationInvestor findByInvestorUserProfileAndCreditApplication(InvestorUserProfile investorUserProfile, CreditApplication creditApplication);
	List<CreditApplicationInvestor> findAllByCreditApplication(CreditApplication creditApplication);
}
