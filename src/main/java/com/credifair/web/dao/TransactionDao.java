package com.credifair.web.dao;

import com.credifair.web.model.User;
import com.credifair.web.model.transaction.Transaction;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 02/02/2016.
 */
@Transactional
public interface TransactionDao extends CrudRepository<Transaction, Long> {
    List<Transaction> findAllByUserOrderByCreatedDesc(User user);
    List<Transaction> findAllByTraceHash(String traceHash);
    List<Transaction> findAllByUserAndType(User user, String transactionType);
    Iterable<Transaction> findAllByType(String type);
    Iterable<Transaction> findAllByUserIn(User[] users);
    Iterable<Transaction> findAllByUserAndTypeInOrderByCreatedDesc(User user, List<String> transactionTypes);

}
