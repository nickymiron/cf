package com.credifair.web.dao;

import com.credifair.web.model.Balance;
import com.credifair.web.model.UserProfile;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 13/03/2016.
 */
@Transactional
public interface BalanceDao extends CrudRepository<Balance, Long> {
	Balance findTopByUserProfileOrderByCreatedDescIdDesc(UserProfile userProfile);
	List<Balance> findAllByUserProfile(UserProfile userProfile);
}
