package com.credifair.web.dao;

import com.credifair.web.model.Liability;
import com.credifair.web.model.PersonalFinancialData;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;
import java.util.List;

/**
 * Created by indrek.ruubel on 20/03/2016.
 */
@Transactional
public interface LiabilityDao extends CrudRepository<Liability, Long> {
	List<Liability> findAllByPersonalFinancialData(PersonalFinancialData personalFinancialData);
//	List<Liability> findByUser(User user);
}
