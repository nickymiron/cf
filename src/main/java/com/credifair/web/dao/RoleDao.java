package com.credifair.web.dao;

import com.credifair.web.model.Role;
import org.springframework.data.repository.CrudRepository;

import javax.transaction.Transactional;

/**
 * Created by indrek.ruubel on 14/01/2016.
 */
@Transactional
public interface RoleDao extends CrudRepository<Role, Long> {
    Role findByRole(String role);
}
