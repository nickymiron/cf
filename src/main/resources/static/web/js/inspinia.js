// INSPINIA Landing Page Custom scripts
$(document).ready(function () {

    // Highlight the top nav as scrolling
    $('body').scrollspy({
        target: '.navbar-fixed-top',
        offset: 80
    })

    // Page scrolling feature
    $('a.page-scroll').bind('click', function(event) {
        var link = $(this);
        $('html, body').stop().animate({
            scrollTop: $(link.attr('href')).offset().top - 70
        }, 500);
        event.preventDefault();
    });

});

// Activate WOW.js plugin for animation on scrol
new WOW().init();

// Angular app
var app = angular.module('app', [
    'ui.bootstrap'
]);

app.controller('navigationController', function($uibModal) {
    var vm = this;
    vm.loginModal = function() {
        return $uibModal.open({
            animation: true,
            templateUrl: '/views/web/loginModal.html',
            controller: 'loginModalInstanceController as vm',
            size: 'sm'
        });
    };

    vm.registerModal = function() {
        return $uibModal.open({
            animation: true,
            templateUrl: '/views/web/registerModal.html',
            controller: 'registerModalInstanceController as vm',
            size: 'sm'
        });
    };
});

app.controller('loginModalInstanceController', function($uibModalInstance) {

    var vm = this;

    vm.close = $uibModalInstance.dismiss;
});

app.controller('registerModalInstanceController', function($uibModalInstance) {

    var vm = this;

    vm.close = $uibModalInstance.dismiss;
});
