(function(angular) {
	'use strict';

	angular.module('inspinia').factory('stepMapper', [function() {
		var service = {};

		var map = {
			'GET_STARTED' : 'customer.get_started',
			'STEP1_PERSONAL_INFO' : 'application.form.steps',
			'STEP2_ADDRESS' : 'application.form.steps',
			'STEP3_ID_UPLOAD' : 'application.iddocuments',
			'WAITING_FOR_OFFER' : 'application.form.waitingoffer',
			'WAITING_OFFER_ACCEPTED' : 'application.form.offer',
			'ADDRESS_DOCUMENTS_FORM' : 'application.addressdocs',
			'INCOME_DOCUMENTS_FORM' : 'application.incomedocuments',
			'WAITING_PROCESSING' : 'application.form.processing',

			'INVESTOR_DASHBOARD': 'investor.dashboard',
			'INVESTOR_NEW': 'application.investorform.steps',
			'DEPOSIT': 'index.add_funds',
			'DASHBOARD': 'customer.dashboard'
		};

		service.map = function(enumValue) {
			return map[enumValue];
		};

		return service;
	}]);

})(angular);