(function(angular) {
	'use strict';

	angular.module('inspinia').provider('TemplateResolver', [function () {
		var resolver = {
			getUrl: function(page, templateName) {
				var url = '/js/' + page + '/' + templateName + '.html';
				return url;
			}
		};

		return {
			$get: function () {
				return resolver;
			}
		};
	}]);

})(angular);