(function(angular) {
	'use strict';

	angular.module('inspinia').factory('restClient', ['$http', '$state', 'stepMapper', function($http, $state, stepMapper) {
		var service = {};

		service.request = function(method, url, data, followRedirect, reload) {
			var req = {
				method: method,
				url: '/api/v1' + url + (_config.apiArgs ? _config.apiArgs : ''),
				headers: {
					'Authorization': 'Bearer ' + _config.token
				},
				data: data
			};
			return $http(req).then(function(response) {
				if (followRedirect && response.data.goToStep) {
					$state.go(stepMapper.map(response.data.goToStep), {}, { reload: reload });
					return;
				}
				return response.data;
			});
		};

		return service;
	}]);

})(angular);