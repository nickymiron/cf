/**
 * INSPINIA - Responsive Admin Theme
 *
 */
function config($translateProvider) {

    $translateProvider
        .translations('en', {

            // Define all menu elements
            DASHBOARD: 'Dashboard',
            BORROWERS: 'Borrowers',
            TRANSACTIONS: 'Transactions',
            GRAPHS: 'Graphs',
            MAILBOX: 'Mailbox',
            WIDGETS: 'Widgets',
            METRICS: 'Metrics',
            FORMS: 'Forms',
            APPVIEWS: 'App views',
            OTHERPAGES: 'Other pages',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Miscellaneous',
            GRIDOPTIONS: 'Grid options',
            TABLES: 'Tables',
            COMMERCE: 'E-commerce',
            GALLERY: 'Gallery',
            MENULEVELS: 'Menu levels',
            ANIMATIONS: 'Animations',
            LANDING: 'Landing page',
            LAYOUTS: 'Layouts',
            INVESTMENTS: 'Investments',
            ADD_FUNDS: 'Add funds',
            WITHDRAW: 'Withdraw',
            SETTINGS: 'Settings',
            BORROW: 'Borrow',
            ACCOUNT: 'Account',
            MY_CARD: 'My card',
            APPLY_FOR_CARD: 'Apply for card',

            LOAN_BALANCES: 'Loan balances',
            REVIEW_DOCS: 'Review docs',

            // Define some custom text
            WELCOME: 'Welcome',
            MESSAGEINFO: 'You have 42 messages and 6 notifications.',
            SEARCH: 'Search for something...',
            NEXT: 'Next',
            MY_LOANS: 'My loans',

            // Form
            GENERAL_INFORMATION14: 'General Information 1/4',
            GENERAL_INFORMATION24: 'General Information 2/4',
            GENERAL_INFORMATION34: 'General Information 3/4',
            LIABILITIES44: 'Liabilities information 4/4',
            FIRST_NAME: 'First name',
            LAST_NAME: 'Last name',
            DATE_OF_BIRTH: 'Birthday',
            MOBILE_NUMBER: 'Mobile number',
            COUNTRY: 'Country',
            ADDRESS: 'Address',
            CITY: 'City',
            POSTAL_CODE: 'Postal code',

            WAITING_FOR_OFFER: 'Waiting for the offer',
            WAITING_FOR_FINAL_OFFER: 'Waiting for documents reviewing',

            HOME_OWNERSHIP: 'Home ownership',
            EDUCATION: 'Education',
            MARITAL_STATUS: 'Marital status',
            NUMBER_OF_DEPENDANTS: 'Number of dependants',
            EMPLOYMENT_STATUS: 'Employment status',
            OCCUPATION_AREA: 'Occupation area',
            CURRENT_JOB_TITLE: 'Current job title',
            EMPLOYMENT_TIME_AT_CURRENT_EMPLOYER: 'Employement time at current employer',
            TOTAL_FULLTIME_EXPERIENCE: 'Total full-time experience',
            CURRENT_EMPLOYER: 'Current employer',
            PAYMENT_DAY: 'Payment day',

            INCOME: 'Income',
            MONTHLY_NET_INCOME: 'Official salary (net of taxes)',
            PENSION: 'Pension (net of taxes)',
            CHILD_BENEFIT: 'Child benefit (net of taxes)',
            SOCIAL_BENEFITS: 'Social benefits (net of taxes)',
            PARENTAL_BENEFITS: 'Parental benefits (net of taxes)',
            ALIMONY: 'Alimony (net of taxes)',
            OTHER_REGULAR_INCOME: 'Other regular income (net of taxes)',
            WILL_YOU_PROVIDE_VERIFICATION_OF_YOUR_INCOME_AND_EXPENSES: 'Will you provide verification of your income and expenses',

            ADD_LIABILITY: 'Add liability',
            GET_CARD_OFFER: 'Get card offer',

            VERIFICATION: 'Verification',

            CREDIT_OFFER: 'Credit offer',
            CREDIT_OFFER_DESC: 'Here\'s your personal credit offer from us.',

            YOUR_OFFER: 'Your offer',

            ACCEPT: 'Accept',

            UNITED_KINGDOM: 'United Kingdom',

            BANK_DETAILS: 'Bank details',
            IBAN: 'IBAN',
            UK_SORT_CODE: 'UK Sort code',
            ACCOUNT_NUMBER: 'Account number',

            APPROVE_FOR_MARKET: 'Approve for market',
            ACTIVATE: 'Activate',

            MARKET: 'Market',
            INVEST: 'Invest',

            SIGN_DOCS: 'Terms of use agreement',
            AGREE_TO_TERMS_OF_USE: 'Agree to terms of use',

            ASSIGN_GRADE: 'Assign grade',

            ALL_APPLICATIONS: 'All applications'


        })
        .translations('es', {

            // Define all menu elements
            DASHBOARD: 'Salpicadero',
            BORROWERS: 'Some guys',
            TRANSACTIONS: 'Transactions spanish',
            GRAPHS: 'Gráficos',
            MAILBOX: 'El correo',
            WIDGETS: 'Widgets',
            METRICS: 'Métrica',
            FORMS: 'Formas',
            APPVIEWS: 'Vistas app',
            OTHERPAGES: 'Otras páginas',
            UIELEMENTS: 'UI elements',
            MISCELLANEOUS: 'Misceláneo',
            GRIDOPTIONS: 'Cuadrícula',
            TABLES: 'Tablas',
            COMMERCE: 'E-comercio',
            GALLERY: 'Galería',
            MENULEVELS: 'Niveles de menú',
            ANIMATIONS: 'Animaciones',
            LANDING: 'Página de destino',
            LAYOUTS: 'Esquemas',

            // Define some custom text
            WELCOME: 'Bienvenido',
            MESSAGEINFO: 'Usted tiene 42 mensajes y 6 notificaciones.',
            SEARCH: 'Busca algo ...',
            DEMO: 'Internacionalización (a veces abreviado como \"I18N, que significa\" I - dieciocho letras N \") es el proceso de planificación e implementación de productos y servicios de manera que se pueden adaptar fácilmente a las lenguas y culturas locales específicas, un proceso llamado localización El proceso de internacionalización. a veces se llama la traducción o la habilitación de localización.'
        });

    $translateProvider.preferredLanguage('en');

}

angular
    .module('inspinia')
    .config(config)
