(function(angular) {
	'use strict';

	angular.module('inspinia').controller('getStartedController', ['restClient', '$state', function(restClient, $state) {
		var vm = this;

		vm.getStarted = function() {
			restClient.request('POST', '/customer/startApplication', null, true);
		}
	}
	]);

})(angular);

