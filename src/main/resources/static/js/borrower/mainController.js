(function(angular) {
	'use strict';

	angular.module('inspinia').controller('mainController', ['restClient', '$state', function(restClient, $state) {
		var vm = this;

		function getUserInfo() {
			restClient.request('GET', '/customer/info').then(function(response) {
				vm.info = response;
			});
		}

		getUserInfo();
	}
	]);

})(angular);

