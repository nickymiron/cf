/**
 * INSPINIA - Responsive Admin Theme
 *
 * Inspinia theme use AngularUI Router to manage routing and views
 * Each view are defined as state.
 * Initial there are written state for all view in theme.
 *
 */
function config($stateProvider, $urlRouterProvider, $ocLazyLoadProvider, IdleProvider, KeepaliveProvider) {

    // Configure Idle settings
    IdleProvider.idle(5); // in seconds
    IdleProvider.timeout(120); // in seconds

    $urlRouterProvider.otherwise("/investor");

    $ocLazyLoadProvider.config({
        // Set to true if you want to see what and when is dynamically loaded
        debug: false
    });

    $stateProvider

        .state('index', {
            abstract: true,
            templateUrl: "/js/borrower/page/page.html"
        })

        // Account dashboard
        //.state('index.dashboard', {
        //    url: "/",
        //    templateUrl: "/js/account/dashboard/accountDashboard.html",
        //    controller: "accountDashboardController as vm"
        //})

        // Account add funds
        .state('index.add_funds', {
            url: "/addfunds",
            templateUrl: "/js/account/addfunds/addfunds.html",
            controller: "addFundsController as vm"
        })

        // Account withdraw
        .state('index.withdraw', {
            url: "/withdraw",
            templateUrl: "/js/account/withdraw/withdraw.html"
        })

        // Account settings
        .state('index.settings', {
            url: "/settings",
            templateUrl: "/js/account/settings/settings.html"
        })

        // Investor

        .state('investor', {
            abstract: true,
            templateUrl: "/js/borrower/page/page.html"
        })

        // Investor dashboard
        .state('investor.dashboard', {
            url: "/investor",
            templateUrl: "/js/investor/dashboard/investorDashboard.html",
            controller: "investorDashboardController as vm"
        })

        // Investor market
        .state('investor.market', {
            url: "/market",
            templateUrl: "/js/investor/market/market.html",
            controller: "marketController as vm"
        })

        // Investor investments
        .state('investor.investments', {
            url: "/investments",
            templateUrl: "/js/investor/investments/investments.html",
            controller: "investmentsController as vm"
        })

        // Customer
        .state('customer', {
            abstract: true,
            templateUrl: "/js/borrower/page/page.html"
        })

        // Customer dashboard
        .state('customer.dashboard', {
            url: "/customer",
            templateUrl: "/js/borrower/dashboard/dashboard.html",
            controller: "dashboardController as vm"
        })

        // Customer dashboard
        .state('customer.get_started', {
            url: "/getstarted",
            templateUrl: "/js/borrower/getstarted/getstarted.html",
            controller: "getStartedController as vm"
        })

        // Customer loans
        .state('customer.my_loans', {
            url: "/loans",
            templateUrl: "/js/borrower/myloans/myloans.html"
        })

        // Customer card
        .state('customer.my_card', {
            url: "/card",
            templateUrl: "/js/borrower/card/card.html"
        })

        // Customer card
        .state('customer.apply_for_card', {
            url: "/applycard",
            templateUrl: "/js/borrower/applycard/applycard.html"
        })

        // Form flow
        .state('application', {
            abstract: true,
            templateUrl: "/js/borrower/page/page.html"
        })

        // INVESTOR STEPS

        .state('application.investorform', {
            url: '/data',
            templateUrl: "/js/investor/dashboard/application/investorForm.html",
            controller: 'investorFormController as vm',
            resolve: {
                formCriteria: ['restClient', function(restClient) {
                    return restClient.request('GET', '/investor/formCriteria').then(function(response) {
                        return response;
                    });
                }],
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['/css/plugins/datapicker/angular-datapicker.css', '/js/vendor/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['/js/vendor/plugins/ui-select/select.min.js', '/css/plugins/ui-select/select.min.css']
                        },
                    ])
                }
            }
        })

        .state('application.investorform.steps', {
            url: '/aboutyou',
            templateUrl: "/js/borrower/dashboard/application/formStep.html"
        })

        // BORROWER STEPS START

        .state('application.form', {
            url: '/application',
            templateUrl: "/js/borrower/dashboard/application/form.html",
            controller: 'formController as vm',
            resolve: {
                formCriteria: ['restClient', function(restClient) {
                    return restClient.request('GET', '/customer/stepData').then(function(response) {
                        return response;
                    });
                }],
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            name: 'datePicker',
                            files: ['/css/plugins/datapicker/angular-datapicker.css', '/js/vendor/plugins/datapicker/angular-datepicker.js']
                        },
                        {
                            name: 'ui.select',
                            files: ['/js/vendor/plugins/ui-select/select.min.js', '/css/plugins/ui-select/select.min.css']
                        },
                    ])
                }
            }
        })

        .state('application.form.steps', {
            url: '/personal',
            templateUrl: "/js/borrower/dashboard/application/formStep.html"
        })

        .state('application.form.waitingoffer', {
            url: '/waitingoffer',
            templateUrl: "/js/borrower/dashboard/application/waitingOfferStep.html"
        })

        .state('application.form.offer', {
            url: '/offer',
            templateUrl: "/js/borrower/dashboard/application/offerStep.html"
        })

        .state('application.iddocuments', {
            url: '/application/iddocs',
            templateUrl: "/js/borrower/dashboard/application/idDocumentsUploadStep.html",
            controllerAs: 'vm',
            controller: ['restClient', function(restClient) {
                var vm = this;

                vm.continue = function() {
                    restClient.request('POST', '/customer/idDocs', {}, true);
                };
            }],
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['/css/plugins/dropzone/basic.css','/css/plugins/dropzone/dropzone.css','/js/vendor/plugins/dropzone/dropzone.js']
                        }
                    ]);
                }
            }
        })

        .state('application.addressdocs', {
            url: '/application/addressdocs',
            templateUrl: "/js/borrower/dashboard/application/addressProofDocumentsUploadStep.html",
            controllerAs: 'vm',
            controller: ['restClient', function(restClient) {
                var vm = this;

                vm.continue = function() {
                    restClient.request('POST', '/customer/proofAddressDocs', {}, true);
                };
            }],
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['/css/plugins/dropzone/basic.css','/css/plugins/dropzone/dropzone.css','/js/vendor/plugins/dropzone/dropzone.js']
                        }
                    ]);
                }
            }
        })

        .state('application.incomedocuments', {
            url: '/application/incomedocs',
            templateUrl: "/js/borrower/dashboard/application/incomeDocumentUploadStep.html",
            controllerAs: 'vm',
            controller: ['restClient', function(restClient) {
                var vm = this;

                vm.continue = function() {
                    restClient.request('POST', '/customer/incomeDocs', {}, true);
                };
            }],
            resolve: {
                loadPlugin: function ($ocLazyLoad) {
                    return $ocLazyLoad.load([
                        {
                            files: ['/css/plugins/dropzone/basic.css','/css/plugins/dropzone/dropzone.css','/js/vendor/plugins/dropzone/dropzone.js']
                        }
                    ]);
                }
            }
        })


        .state('application.form.processing', {
            url: '/processing',
            templateUrl: "/js/borrower/dashboard/application/processingStep.html"
        })
		//
        //.state('application.form.signdocs', {
        //    url: '/signdocs',
        //    templateUrl: "/js/borrower/dashboard/application/signDocsStep.html"
        //})
		//
        //.state('application.form.bankdetails', {
        //    url: '/bankdetails',
        //    templateUrl: "/js/borrower/dashboard/application/bankDetailsStep.html"
        //})

        // END STEPS

        //.state('index.transactions', {
        //    url: "/transactions",
        //    templateUrl: "/js/borrower/transactions/transactions.html",
        //    data: { pageTitle: 'Transactions' }
        //});

}
angular
    .module('inspinia')
    .config(config)
    .run(function($rootScope, $state) {
        $rootScope.$state = $state;
    });
