(function() {

	'use strict';

	angular.module('inspinia')
		.directive('transactionsTable', ['TemplateResolver',
			function (TemplateResolver) {
				return {
					restrict: 'E',
					templateUrl: TemplateResolver.getUrl('borrower', 'transactions/transactionsTable/transactionsTable'),
					controller : 'transactionsTableController as vm',
					controllerAs: 'vm',
					bindToController: true
				};
			}]
		);

})(angular);
