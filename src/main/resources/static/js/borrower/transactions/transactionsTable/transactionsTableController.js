(function(angular) {
	'use strict';

	angular.module('inspinia').controller('transactionsTableController',
		['restClient', function(restClient) {

			var vm = this;

			restClient.request('GET', '/customer/transactions', {}).then(function(response) {
				vm.transactions = response;
			});
		}
		]);

})(angular);