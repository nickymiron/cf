(function() {

	'use strict';

	angular.module('inspinia')
		.directive('dynamicField', ['TemplateResolver',
			function (TemplateResolver) {
				return {
					restrict: 'E',
					scope: {
						field: '=',
						data: '=',
						form: '=',
						formController: '='
					},
					replace: true,
					templateUrl: TemplateResolver.getUrl('borrower', 'dashboard/application/dynamicField'),
					controller: function(){},
					controllerAs: 'vm',
					bindToController: true
				};
			}]
		);

})(angular);