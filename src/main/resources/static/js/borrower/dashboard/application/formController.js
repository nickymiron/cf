(function(angular) {
	'use strict';

	angular.module('inspinia').controller('formController',
		['restClient', 'formCriteria', function(restClient, formCriteria) {

			var vm = this;

			vm.formCriteria = formCriteria;

			vm.addLiability = function() {
				vm.data = vm.data || {LIABILITIES : []};
				vm.data.LIABILITIES.push({});
			};

			vm.continue = function() {
				restClient.request('PUT', '/customer/details', vm.data, true, true);
			};

		}
		]);

})(angular);