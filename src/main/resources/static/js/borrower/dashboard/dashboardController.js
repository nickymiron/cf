(function(angular) {
	'use strict';

	angular.module('inspinia').controller('dashboardController', ['restClient', '$state', function(restClient, $state) {
		var vm = this;
		restClient.request('GET', '/customer/getStep', null, true);

		var getUserInfo = function() {
			restClient.request('GET', '/customer/info').then(function(response) {
				vm.info = response;
			});
		}

		vm.requestLoan = function() {
			restClient.request('POST', '/customer/requestLoan/' + vm.requestAmount).then(function(data) {
				vm.requestAmount = '';
				getUserInfo();
			});
		};

		vm.calculateInterest = function() {
			restClient.request('POST', '/customer/calculateInterest').then(function() {
				getUserInfo();
			});
		}

		getUserInfo();
	}
	]);

})(angular);

