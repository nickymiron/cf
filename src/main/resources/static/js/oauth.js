var app = angular.module("app", []);

app.controller('OauthController', ['$scope', '$http', '$timeout', function($scope, $http, $timeout) {
    'use strict';

    var vm = this;

    vm.user = {
        username: 'api-client',
        password: '12345'
    };

    vm.newUser = {
        country: 'GB'
    };

    vm.getClientToken = function() {
        var req = {
            method: 'POST',
            url: '/api/v1/oauth/token?grant_type=client_credentials',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Accept': 'application/json',
                'Authorization': 'Basic ' + btoa(vm.user.username + ':' + vm.user.password)
            }
        };

        $http(req).then(function(resp){
            vm.clientToken = resp.data.access_token;
        }, function(resp){
            console.log(resp.data);
        });
    };

    vm.registerUser = function() {
        var req = {
            method: 'POST',
            url: '/api/v1/customer/',
            headers: {
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + vm.clientToken
            },
            data: vm.newUser
        };

        $http(req).then(function(resp){
            vm.newUser.registered = resp.data;
        }, function(resp){
            console.log(resp.data);
        });
    };

    vm.getAccessToken = function() {

        vm.accessTokenRequest = {
            password : vm.newUser.password,
            username : vm.newUser.email,
            grant_type : 'password',
            scope : 'read write'
        }

        var req = {
            method: 'POST',
            url: '/api/v1/oauth/token',
            headers: {
                'Content-Type': 'application/x-www-form-urlencoded',
                'Authorization': 'Basic ' + btoa(vm.user.username + ':' + vm.user.password),
                'Accept': 'application/json',
            },
            transformRequest: function(obj) {
                var str = [];
                for(var p in obj)
                    str.push(encodeURIComponent(p) + "=" + encodeURIComponent(obj[p]));
                return str.join("&");
            },
            data: vm.accessTokenRequest
        };

        $http(req).then(function(resp){
            vm.accessToken = resp.data.access_token;
        }, function(resp){
            console.log(resp.data);
        });
    };

    //vm.accessToken = "36414218-fa10-4190-b9e2-71a55e5c2bd2";

    vm.fireRequest = function() {
        var req = {
            method: 'GET',
            url: '/api/v1/customer/info',
            headers: {
                'Authorization': 'Bearer ' + vm.accessToken
            }
        };

        $http(req).then(function(resp){
            vm.response = resp.data;
        }, function(resp){
            vm.response = resp.data;
        });
    };

}]);