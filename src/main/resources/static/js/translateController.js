(function(angular) {
	'use strict';

	angular.module('inspinia').controller('translateController', ['$translate', '$scope', function($translate, $scope) {

		$scope.changeLanguage = function (langKey) {
			$translate.use(langKey);
			$scope.language = langKey;
		};

	}
	]);

})(angular);

