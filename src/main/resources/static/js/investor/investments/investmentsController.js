(function(angular) {
	'use strict';

	angular.module('inspinia').controller('investmentsController', ['restClient', function(restClient) {
		var vm = this;

		restClient.request('GET', '/investor/investments', null).then(function(investments) {
			vm.investmentsList = investments;
		});
	}
	]);

})(angular);

