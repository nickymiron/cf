(function(angular) {
	'use strict';

	angular.module('inspinia').controller('marketController', ['restClient', function(restClient) {
		var vm = this;

		var getApplications = function() {
			restClient.request('GET', '/investor/applicationList').then(function (data) {
				vm.applicationList = data;
			});
		};

		vm.invest = function(application) {
			var query = {
				id: application.id,
				amount: application.fund
			};

			restClient.request('POST', '/investor/invest', query).then(function() {
				getApplications();
			});
		};

		getApplications();
	}
	]);

})(angular);

