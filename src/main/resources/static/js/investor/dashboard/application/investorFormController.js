(function(angular) {
	'use strict';

	angular.module('inspinia').controller('investorFormController',
		['restClient', 'formCriteria', function(restClient, formCriteria) {

			var vm = this;

			vm.formCriteria = formCriteria;

			vm.continue = function() {
				restClient.request('PUT', '/investor/details', vm.data, true, true);
			};

		}
		]);

})(angular);