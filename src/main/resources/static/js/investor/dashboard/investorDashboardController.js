(function(angular) {
	'use strict';

	angular.module('inspinia').controller('investorDashboardController', ['restClient', '$state', function(restClient, $state) {
		var vm = this;

		restClient.request('GET', '/investor/info', null).then(function(info) {
			vm.info = info;
		});

		vm.goToApplicationStep = function() {
			restClient.request('GET', '/investor/getStep', null, true);
		}
	}
	]);

})(angular);

