(function(angular) {
	'use strict';

	angular.module('inspinia').controller('addFundsController', ['restClient', function(restClient) {
		var vm = this;

		vm.payOff = function() {
			restClient.request('POST', '/customer/payOff/' + vm.amountPayoff).then(function(data) {
				vm.amountPayoff = '';
			});
		};

	}
	]);

})(angular);

