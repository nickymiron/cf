(function(angular) {
	'use strict';

	angular.module('inspinia').factory('adminRestClient', ['$http', function($http) {
		var service = {};

		service.request = function(method, url, data) {
			var req = {
				method: method,
				url: '/api/v1' + url,
				headers: {
					'Authorization': 'Bearer ' + _config.token
				},
				data: data
			};
			return $http(req).then(function(response) {
				return response.data;
			});
		};
		return service;
	}]);

})(angular);