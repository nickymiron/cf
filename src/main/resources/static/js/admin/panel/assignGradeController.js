(function(angular) {
	'use strict';

	angular.module('inspinia').controller('assignGradeController', ['adminRestClient', function(adminRestClient) {
		var vm = this;

		var getUserGrade = function() {
			adminRestClient.request('GET', '/admin/userGrade/' + _config.userId).then(function (data) {
				vm.grade = data;
			});
		};

		adminRestClient.request('GET', '/admin/grades').then(function(data) {
			vm.grades = data;
		});

		vm.assignGrade = function(grade) {
			var query = {
				userId: _config.userId,
				grade: grade.name,
				gracePeriod: grade.gracePeriod,
				apr: grade.selectedApr,
				creditLimit: grade.creditLimit
			};
			adminRestClient.request('POST', '/admin/assignGrade', query).then(function() {
				getUserGrade();
			});
		};

		getUserGrade();
	}
	]);

})(angular);

