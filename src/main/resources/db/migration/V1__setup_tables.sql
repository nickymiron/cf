-- MySQL dump 10.13  Distrib 5.6.28, for osx10.8 (x86_64)
--
-- Host: localhost    Database: credifair
-- ------------------------------------------------------
-- Server version	5.6.28

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `bor_balance`
--

DROP TABLE IF EXISTS `bor_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bor_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin_balance` decimal(19,2) DEFAULT NULL,
  `begin_datetime` tinyblob,
  `end_balance` decimal(19,2) DEFAULT NULL,
  `end_datetime` tinyblob,
  `interest_earned` decimal(19,2) DEFAULT NULL,
  `transactions_amount` decimal(19,2) DEFAULT NULL,
  `fk_borrower_user_profile` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_3vt2lr27ajjc19u78pbylqqr1` (`fk_borrower_user_profile`),
  CONSTRAINT `FK_3vt2lr27ajjc19u78pbylqqr1` FOREIGN KEY (`fk_borrower_user_profile`) REFERENCES `borrower_user_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bor_balance`
--

LOCK TABLES `bor_balance` WRITE;
/*!40000 ALTER TABLE `bor_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `bor_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bor_transaction`
--

DROP TABLE IF EXISTS `bor_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bor_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bor_transaction`
--

LOCK TABLES `bor_transaction` WRITE;
/*!40000 ALTER TABLE `bor_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `bor_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bor_transaction_bor_balance`
--

DROP TABLE IF EXISTS `bor_transaction_bor_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bor_transaction_bor_balance` (
  `fk_bor_balance` bigint(20) NOT NULL,
  `fk_bor_transaction` bigint(20) NOT NULL,
  KEY `FK_noyyb79msclbxw6nfy2k2byvb` (`fk_bor_transaction`),
  KEY `FK_nj0y48yx2wl2ui4q1i7cqeqsd` (`fk_bor_balance`),
  CONSTRAINT `FK_nj0y48yx2wl2ui4q1i7cqeqsd` FOREIGN KEY (`fk_bor_balance`) REFERENCES `bor_balance` (`id`),
  CONSTRAINT `FK_noyyb79msclbxw6nfy2k2byvb` FOREIGN KEY (`fk_bor_transaction`) REFERENCES `bor_transaction` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bor_transaction_bor_balance`
--

LOCK TABLES `bor_transaction_bor_balance` WRITE;
/*!40000 ALTER TABLE `bor_transaction_bor_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `bor_transaction_bor_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `borrower_user_profile`
--

DROP TABLE IF EXISTS `borrower_user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `borrower_user_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_9brp4lh9t777uhq28ls9nyfy3` (`user`),
  CONSTRAINT `FK_9brp4lh9t777uhq28ls9nyfy3` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `borrower_user_profile`
--

LOCK TABLES `borrower_user_profile` WRITE;
/*!40000 ALTER TABLE `borrower_user_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `borrower_user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `email_subscriber`
--

DROP TABLE IF EXISTS `email_subscriber`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `email_subscriber` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `fk_user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_8xm56p23cufqbcbn61tgylwmn` (`fk_user`),
  CONSTRAINT `FK_8xm56p23cufqbcbn61tgylwmn` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `email_subscriber`
--

LOCK TABLES `email_subscriber` WRITE;
/*!40000 ALTER TABLE `email_subscriber` DISABLE KEYS */;
/*!40000 ALTER TABLE `email_subscriber` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investor_balance`
--

DROP TABLE IF EXISTS `investor_balance`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investor_balance` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `begin_balance` decimal(19,2) DEFAULT NULL,
  `begin_datetime` tinyblob,
  `end_datetime` tinyblob,
  `transactions_amount` decimal(19,2) DEFAULT NULL,
  `fk_investor_profile` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_1j25rnqule6uy8104nv2b2w0v` (`fk_investor_profile`),
  CONSTRAINT `FK_1j25rnqule6uy8104nv2b2w0v` FOREIGN KEY (`fk_investor_profile`) REFERENCES `investor_user_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investor_balance`
--

LOCK TABLES `investor_balance` WRITE;
/*!40000 ALTER TABLE `investor_balance` DISABLE KEYS */;
/*!40000 ALTER TABLE `investor_balance` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investor_transaction`
--

DROP TABLE IF EXISTS `investor_transaction`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investor_transaction` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `type` varchar(255) DEFAULT NULL,
  `fk_investor_balance` bigint(20) DEFAULT NULL,
  `fk_loan_investment` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_mdni72ns9p4cbeln9evovex6v` (`fk_investor_balance`),
  KEY `FK_1dbep5djeliojlksr9qc19vs9` (`fk_loan_investment`),
  CONSTRAINT `FK_1dbep5djeliojlksr9qc19vs9` FOREIGN KEY (`fk_loan_investment`) REFERENCES `loan_investment` (`id`),
  CONSTRAINT `FK_mdni72ns9p4cbeln9evovex6v` FOREIGN KEY (`fk_investor_balance`) REFERENCES `investor_balance` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investor_transaction`
--

LOCK TABLES `investor_transaction` WRITE;
/*!40000 ALTER TABLE `investor_transaction` DISABLE KEYS */;
/*!40000 ALTER TABLE `investor_transaction` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `investor_user_profile`
--

DROP TABLE IF EXISTS `investor_user_profile`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `investor_user_profile` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) DEFAULT NULL,
  `last_name` varchar(255) DEFAULT NULL,
  `user` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_2br0ku0sq8qnxqkeh0hg5e7h2` (`user`),
  CONSTRAINT `FK_2br0ku0sq8qnxqkeh0hg5e7h2` FOREIGN KEY (`user`) REFERENCES `user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `investor_user_profile`
--

LOCK TABLES `investor_user_profile` WRITE;
/*!40000 ALTER TABLE `investor_user_profile` DISABLE KEYS */;
/*!40000 ALTER TABLE `investor_user_profile` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan`
--

DROP TABLE IF EXISTS `loan`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loan` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `closed_datetime` tinyblob,
  `interest` decimal(19,2) DEFAULT NULL,
  `requested_datetime` tinyblob,
  `target_reach_datetime` tinyblob,
  `fk_bor_transaction` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_saad2bjuhwtywy2acmgcutum0` (`fk_bor_transaction`),
  CONSTRAINT `FK_saad2bjuhwtywy2acmgcutum0` FOREIGN KEY (`fk_bor_transaction`) REFERENCES `bor_transaction` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan`
--

LOCK TABLES `loan` WRITE;
/*!40000 ALTER TABLE `loan` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `loan_investment`
--

DROP TABLE IF EXISTS `loan_investment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `loan_investment` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `amount` decimal(19,2) DEFAULT NULL,
  `invested_datetime` tinyblob,
  `start_datetime` tinyblob,
  `fk_loan` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_7vqmitlv246kuvqrh75809ste` (`fk_loan`),
  CONSTRAINT `FK_7vqmitlv246kuvqrh75809ste` FOREIGN KEY (`fk_loan`) REFERENCES `loan` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `loan_investment`
--

LOCK TABLES `loan_investment` WRITE;
/*!40000 ALTER TABLE `loan_investment` DISABLE KEYS */;
/*!40000 ALTER TABLE `loan_investment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `role`
--

DROP TABLE IF EXISTS `role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `role` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `role` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `role`
--

LOCK TABLES `role` WRITE;
/*!40000 ALTER TABLE `role` DISABLE KEYS */;
/*!40000 ALTER TABLE `role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `last_updated` datetime DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `borrower_user_profile_id` bigint(20) DEFAULT NULL,
  `investor_user_profile_id` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_pyv6c664lbebjymoqj8cgmyn9` (`borrower_user_profile_id`),
  KEY `FK_rjby8r688u2pujsjruwfssrui` (`investor_user_profile_id`),
  CONSTRAINT `FK_pyv6c664lbebjymoqj8cgmyn9` FOREIGN KEY (`borrower_user_profile_id`) REFERENCES `borrower_user_profile` (`id`),
  CONSTRAINT `FK_rjby8r688u2pujsjruwfssrui` FOREIGN KEY (`investor_user_profile_id`) REFERENCES `investor_user_profile` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_role`
--

DROP TABLE IF EXISTS `user_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_role` (
  `fk_user` bigint(20) NOT NULL,
  `fk_role` bigint(20) NOT NULL,
  KEY `FK_sr8882220cob61l4kc95wq5l4` (`fk_role`),
  KEY `FK_e6hgvev124sjwxb2ged573q91` (`fk_user`),
  CONSTRAINT `FK_e6hgvev124sjwxb2ged573q91` FOREIGN KEY (`fk_user`) REFERENCES `user` (`id`),
  CONSTRAINT `FK_sr8882220cob61l4kc95wq5l4` FOREIGN KEY (`fk_role`) REFERENCES `role` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_role`
--

LOCK TABLES `user_role` WRITE;
/*!40000 ALTER TABLE `user_role` DISABLE KEYS */;
/*!40000 ALTER TABLE `user_role` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-28 19:22:12
